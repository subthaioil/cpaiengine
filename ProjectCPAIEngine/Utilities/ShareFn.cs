﻿using com.pttict.engine.utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Web.UI.HtmlControls;
using ProjectCPAIEngine.Model;
using System.Globalization;
using ProjectCPAIEngine.DAL;
using System.Web.Configuration;
using System.Drawing;
using com.pttict.downstream.common.utilities;
using System.CodeDom.Compiler;
using System.CodeDom;

namespace ProjectCPAIEngine.Utilities
{
    public enum TypeCcontrol
    {
        Label = 1,
        TextBox = 2,
        Dropdownlst = 3
    }
    public enum GridMode
    {
        Read = 1,
        Edit = 2
    }
    public class ShareFn : BasicBean
    {
        UserPermissionDAL _permissionDAL = new UserPermissionDAL();
        Random rnd = new Random();
        public string postStrData(string destinationUrl, string requestString)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestString);
            request.ContentType = "text/plain; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();

                return responseStr;
            }
            return null;
        }
        
        public Stream GetStream(string url)
        {
            HttpWebRequest aRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse aResponse = (HttpWebResponse)aRequest.GetResponse();

            return aResponse.GetResponseStream();
        }

        public string RaplceParamXML(List<ReplaceParam>_param ,string XML)
        {
            if (Const.User != null)
            {
                foreach (var _item in _param)
                {
                    XML = XML.Replace(_item.ParamName, _item.ParamVal);
                }
                XML = XML.Replace("#user", Const.User.UserName);
                XML = XML.Replace("#appuser", ConstantPrm.ENGINECONF.EnginAppID);
                XML = XML.Replace("#password", ConstantPrm.ENGINECONF.EnginAppPassword);
                XML = XML.Replace("#req_txn_id", ConstantPrm.EnginGetEngineID());
                return XML;
            }
            else
            {
                return null;
            }
        }

        public string postXMLData(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }

        public string GenControlGrid(TypeCcontrol _typeCtrl, string _DataVal, string EditClassID, GridMode _mode)
        {
            return GenControlGrid(_typeCtrl, _DataVal, EditClassID, _mode, "");
        }

        public string GenControlGrid(TypeCcontrol _typeCtrl, string _DataVal, string EditClassID, GridMode _mode, string width)
        {
            string retCtrlHtml = "";
            if (_mode == GridMode.Edit)
            {
                switch (_typeCtrl)
                {
                    case TypeCcontrol.Label:
                        retCtrlHtml = string.Format("<label id=\"lb{0}\" {2}>{1}</label> ", ConstantPrm.GetDynamicCtrlID(), _DataVal, (string.IsNullOrEmpty(width) ? "" : (width == "0") ? "style=\"width:100%;\"" : string.Format("style=\"width:{0}px;\"", width)));
                        break;
                    case TypeCcontrol.TextBox:
                        retCtrlHtml = string.Format("<input type=\"text\" onchange=\"formatNumber(this);\" class=\"{0} form-control\" id=\"{1}\"  value=\"{2}\" {3} />", EditClassID, ConstantPrm.GetDynamicCtrlID(), _DataVal, (string.IsNullOrEmpty(width) ? "" : (width == "0") ? "style=\"width:100%;\"" : string.Format("style=\"width:{0}px;\"", width)));
                        break;
                    default:
                        retCtrlHtml = string.Format("<label id=\"lb{0}\" {2}>{1}</label> ", ConstantPrm.GetDynamicCtrlID(), _DataVal, (string.IsNullOrEmpty(width) ? "" : (width == "0") ? "style=\"width:100%;\"" : string.Format("style=\"width:{0}px;\"", width)));
                        break;
                }
            }
            else
            {
                retCtrlHtml = string.Format("<label id=\"lb{0}\" class=\"{2}\">{1}</label> ", ConstantPrm.GetDynamicCtrlID(), _DataVal, EditClassID);
            }
            return retCtrlHtml;
        }
        public string GenControlDropdown(List<string> Data, string _DataVal, string EditClassID, GridMode _mode)
        {
            string retCtrlHtml = "";
            if (_mode == GridMode.Edit)
            {
                string ddl = string.Format("<select class=\"{0} form-control\" style=\"width:150px;\"  id=\"{1}\">", EditClassID, ConstantPrm.GetDynamicCtrlID());
                foreach (var ObjItem in Data)
                {
                    if (ObjItem == _DataVal)
                    {
                        ddl += "<option value=\"" + ObjItem + "\" selected>" + ObjItem + "</option>";
                    }
                    else
                    {
                        ddl += "<option value=\"" + ObjItem + "\" >" + ObjItem + "</option>";
                    }
                }
                ddl += "</select>";
                retCtrlHtml = ddl;


            }
            else
            {
                retCtrlHtml = string.Format("<label id=\"lb{0}\">{1}</label> ", ConstantPrm.GetDynamicCtrlID(), _DataVal);
            }
            return retCtrlHtml;
        }

        public string GenControlDropdown(object Data, string fieldText, string fieldVal, string _DataVal, string EditClassID, GridMode _mode)
        {
            string retCtrlHtml = "";
            List<object> objlist = (Data as IEnumerable<object>).Cast<object>().ToList();
            if (_mode == GridMode.Edit)
            {

                string ddl = string.Format("<select class=\"{0} form-control\" style=\"width:150px;\"  id=\"{1}\">", EditClassID, ConstantPrm.GetDynamicCtrlID());
                foreach (var ObjItem in objlist)
                {
                    if (ObjItem.GetType().GetProperty(fieldVal).GetValue(ObjItem, null).ToString() == _DataVal)
                    {
                        ddl += "<option value=\"" + ObjItem.GetType().GetProperty(fieldVal).GetValue(ObjItem, null) + "\" selected>" + ObjItem.GetType().GetProperty(fieldText).GetValue(ObjItem, null) + "</option>";
                    }
                    else
                    {
                        ddl += "<option value=\"" + ObjItem.GetType().GetProperty(fieldVal).GetValue(ObjItem, null) + "\" >" + ObjItem.GetType().GetProperty(fieldText).GetValue(ObjItem, null) + "</option>";
                    }
                }
                ddl += "</select>";
                retCtrlHtml = ddl;


            }
            else
            {
                foreach (var ObjItem in objlist)
                {
                    if (ObjItem.GetType().GetProperty(fieldVal).GetValue(ObjItem, null).ToString() == _DataVal)
                    {
                        retCtrlHtml = string.Format("<label id=\"lb{0}\" class=\"{2}\">{1}</label> ", ConstantPrm.GetDynamicCtrlID(), ObjItem.GetType().GetProperty(fieldText).GetValue(ObjItem, null), EditClassID);
                    }
                }

            }
            return retCtrlHtml;
        }

        public void CopyObject(object source, object target, bool BaseSource)
        {
            if (BaseSource)
            {
                foreach (PropertyInfo propA in source.GetType().GetProperties())
                {
                    PropertyInfo propB = target.GetType().GetProperty(propA.Name);
                    propB.SetValue(target, source.GetType().GetProperty(propA.Name).GetValue(source, null), null);
                }
            }
            else
            {
                foreach (PropertyInfo propA in target.GetType().GetProperties())
                {
                    PropertyInfo propB = target.GetType().GetProperty(propA.Name);
                    propB.SetValue(target, source.GetType().GetProperty(propA.Name).GetValue(source, null), null);
                }
            }
        }
        public void CopyObject(object source, object target)
        {
            CopyObject(source, target, false);
        }

        public StringBuilder GetResponeParamVal(ResponseData _respone, string _key)
        {
            StringBuilder _retVal = new StringBuilder();
            if (_respone != null && _respone.resp_parameters != null && _respone.resp_parameters.Count > 0)
            {
                var _lstKey = _respone.resp_parameters.Where(x => x.k == _key).ToList();
                if (_lstKey.Count > 0)
                {
                    _retVal.Append(_lstKey[0].v);
                }
            }
            return _retVal;
        }

        public void LoadControlDropdown(DropDownList _ddl, object _objMaster, string fieldVal, string fieldText, bool AutoAddOther)
        {
            string Value = ""; string Text = "";
            IEnumerable listData = _objMaster as IEnumerable;
            if (listData != null)
            {
                foreach (var ObjItem in listData)
                {

                    foreach (PropertyInfo propA in ObjItem.GetType().GetProperties())
                    {
                        if (propA.Name == fieldVal) Value = ObjItem.GetType().GetProperty(propA.Name).GetValue(ObjItem, null).ToString();
                        if (propA.Name == fieldText) Text = ObjItem.GetType().GetProperty(propA.Name).GetValue(ObjItem, null).ToString();
                    }
                    ListItem list = new ListItem(Text, Value);
                    _ddl.Items.Add(list);
                }
            }
            if (AutoAddOther)
            {
                bool HaveOther = false;
                foreach (var ObjItem in listData)
                {
                    foreach (PropertyInfo propA in ObjItem.GetType().GetProperties())
                    {
                        if (propA.Name == fieldText)
                        {
                            var val = ObjItem.GetType().GetProperty(propA.Name).GetValue(ObjItem, null).ToString();
                            if (val != null && val.ToUpper().Trim() == "OTHERS") HaveOther = true;
                        }
                    }
                }
                if (!HaveOther)
                {
                    ListItem list = new ListItem("Others", "Others");
                    _ddl.Items.Add(list);
                }
            }
        }
        public void LoadControlDropdown(DropDownList _ddl, List<string> _lstobjMaster, bool AutoAddOther, bool AutoOerder = true)
        {
            List<string> _objMaster = new List<string>();
            if (AutoOerder) { _objMaster = _lstobjMaster.OrderBy(x => x).ToList(); }
            else { _objMaster = _lstobjMaster; }
            foreach (var ObjItem in _objMaster)
            {
                if (ObjItem != null)
                {
                    ListItem list = new ListItem(ObjItem, ObjItem);
                    _ddl.Items.Add(list);
                }
            }
            if (AutoAddOther)
            {
                bool HaveOther = false;
                foreach (var item in _objMaster)
                {
                    if (item != null && item.ToUpper().Trim() == "OTHERS") HaveOther = true;
                }
                if (!HaveOther)
                {
                    ListItem list = new ListItem("Others", "Others");
                    _ddl.Items.Add(list);
                }
            }

        }
        public string GetTextValByObject(object _lstobjMaster, string fieldVal, string fieldText, string Val, string ValOther = "")
        {
            string TextVal = "";
            IEnumerable listData = _lstobjMaster as IEnumerable;
            if (listData != null)
            {
                foreach (var ObjItem in listData)
                {
                    if (ObjItem.GetType().GetProperty(fieldVal).GetValue(ObjItem, null).ToString() == Val)
                    {
                        TextVal = ObjItem.GetType().GetProperty(fieldText).GetValue(ObjItem, null).ToString();
                        if (TextVal.ToUpper() == "OTHERS" && ValOther != "")
                        {
                            TextVal += " : " + ValOther;
                        }
                    }
                }
            }
            if (TextVal == "" && !string.IsNullOrEmpty(ValOther)) TextVal = "Others : " + ValOther;
            return TextVal;
        }
        public string CheckDropdownVal(DropDownList _ddl, string _txtVal)
        {
            if (_ddl.SelectedValue == "Others") return _txtVal;
            else { return _ddl.SelectedItem.Value; }
        }
        public void SetValDropDown(DropDownList _ddl, string _Val, TextBox _txtbox)
        {
            ListItem selectedListItem = _ddl.Items.FindByValue(_Val);

            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            }
            else
            {
                _ddl.SelectedValue = "Others";
                _txtbox.Text = _Val;
            }
        }
        public void SetValDropDown(DropDownList _ddl, string _Val, HtmlInputText _txtbox)
        {
            ListItem selectedListItem = _ddl.Items.FindByValue(_Val);

            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            }
            else
            {
                _ddl.SelectedValue = "Others";
                _txtbox.Value = _Val;
            }
        }
        public string ConvertDateFormat(string DateString, bool AddTime = false)
        {
            return ConvertDateFormat(DateString, false, AddTime);
        }
        public string ConvertDateFormat(string DateString, bool ConvertymdTodmg, bool AddTime = false)
        {
            if (ConvertymdTodmg)
            {
                if (DateString != "")
                {
                    DateTime dt = DateTime.ParseExact(DateString, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (dt == null)
                    {
                        string[] datesplit = DateString.Split('-');
                        if (DateString.Length >= 10 && datesplit.Length >= 3)
                        {
                            return string.Format("{0}/{1}/{2}", datesplit[2], datesplit[1], datesplit[0]);
                        }
                        else
                        {
                            return "";
                        }
                    }
                    else { return dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + ((AddTime) ? " 00:00:00" : ""); }
                }
                else { return ""; }
            }
            else
            {
                return DateString + ((AddTime) ? " 00:00:00" : "");
            }
        }
        public string ConvertDateFormatTo(string DateString, bool ConvertymdTodmg, bool AddTime = false)
        {
            if (ConvertymdTodmg)
            {
                if (DateString != "")
                {
                    DateTime dt = DateTime.ParseExact(DateString, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (dt == null)
                    {
                        string[] datesplit = DateString.Split('-');
                        if (DateString.Length >= 10 && datesplit.Length >= 3)
                        {
                            return string.Format("{0}/{1}/{2}", datesplit[2], datesplit[1], datesplit[0]);
                        }
                        else
                        {
                            return "";
                        }
                    }
                    else { return dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + ((AddTime) ? " 23:59:59" : ""); }
                }
                else { return ""; }
            }
            else
            {
                return DateString + ((AddTime) ? " 23:59:59" : "");
            }
        }
        public string ConvertDateFormatBack(string DateString)
        {
            return ConvertDateFormatBack(DateString, false);
        }
        public string ConvertDateFormatBack(string DateString, bool ConvertymdTodmg)
        {
            if (ConvertymdTodmg)
            {
                //form Database Format dd/mm/yyyy
                if (DateString == null) return "";
                string[] datesplit = DateString.Split('/');
                string[] datesplit2 = DateString.Split('-');
                if (DateString.Length >= 10 && datesplit.Length >= 3)
                {
                    return string.Format("{0}-{1}-{2}", datesplit[2], datesplit[1], datesplit[0]);
                }
                else if (DateString.Length >= 10 && datesplit2.Length >= 3)
                {
                    return string.Format("{0}-{1}-{2}", datesplit2[2], datesplit2[1], datesplit2[0]);
                }
                else
                {
                    return "";
                }
            }
            else { return DateString; }
        }
        public string ConvertDateFormatBackFormat(string DateString, string _format)
        {
            //form Database Format dd/mm/yyyy
            if (DateString == null) DateString = "";
            string[] datesplit = DateString.Split('/');
            string[] datesplit2 = DateString.Split('-');
            if (DateString.Length >= 10 && datesplit.Length >= 3)
            {
                DateTime _Dt;
                DateTime.TryParse(string.Format("{0}-{1}-{2}", datesplit[2], datesplit[1], datesplit[0]), out _Dt);
                if (_Dt != null)
                {
                    return _Dt.ToString(_format);
                }
                else
                {
                    return "";
                }
            }
            else if (DateString.Length >= 10 && datesplit2.Length >= 3)
            {
                DateTime _Dt2;
                DateTime.TryParse(string.Format("{0}-{1}-{2}", datesplit2[2], datesplit2[1], datesplit2[0]), out _Dt2);
                if (_Dt2 != null)
                {
                    return _Dt2.ToString(_format);
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return null;
            }
        }

        public string ConvertDateTimeFormatBack(string DateTimeString)
        {
            return ConvertDateTimeFormatBack(DateTimeString, false);
        }
        public string ConvertDateTimeFormatBack(string DateTimeString, bool ConvertymdTodmg)
        {
            if (ConvertymdTodmg)
            {
                //form Database Format dd/mm/yyyy
                if (DateTimeString == null) return "";
                string[] datesplit = DateTimeString.Split('/');
                string[] datesplit2 = DateTimeString.Split('-');
                string[] timesplit = DateTimeString.Split(':');
                if (DateTimeString.Length >= 10 && datesplit.Length >= 3 && timesplit.Length >= 2)
                {
                    return string.Format("{0}-{1}-{2} {3}:{4}", datesplit[2], datesplit[1], datesplit[0], timesplit[0].Substring(timesplit[0].Length - 2), timesplit[1].Substring(0, 2));
                }
                else if (DateTimeString.Length >= 10 && datesplit2.Length >= 3 && timesplit.Length >= 2)
                {
                    return string.Format("{0}-{1}-{2} {3}:{4}", datesplit2[2], datesplit2[1], datesplit2[0], timesplit[0].Substring(timesplit[0].Length - 2), timesplit[1].Substring(0, 2));
                }
                else
                {
                    return "";
                }
            }
            else { return DateTimeString; }
        }
        public string ConvertDateTimeFormatBackFormat(string DateTimeString, string _format)
        {
            //form Database Format dd/mm/ hh:mm
            if (DateTimeString == null) DateTimeString = "";
            string[] datesplit = DateTimeString.Split('/');
            string[] datesplit2 = DateTimeString.Split('-');
            string[] timesplit = DateTimeString.Split(':');
            if (DateTimeString.Length >= 10 && datesplit.Length >= 3 && timesplit.Length >= 2)
            {
                DateTime _Dt;
                DateTime.TryParse(string.Format("{0}-{1}-{2} {3}:{4}", datesplit[2].Substring(0, 4), datesplit[1], datesplit[0], timesplit[0].Substring(timesplit[0].Length - 2), timesplit[1].Substring(0, 2)), out _Dt);
                if (_Dt != null)
                {
                    return _Dt.ToString(_format);
                }
                else
                {
                    return "";
                }
            }
            else if (DateTimeString.Length >= 10 && datesplit2.Length >= 3 && timesplit.Length >= 2)
            {
                DateTime _Dt2;
                DateTime.TryParse(string.Format("{0}-{1}-{2} {3}:{4}", datesplit2[2].Substring(0, 4), datesplit2[1], datesplit2[0], timesplit[0].Substring(timesplit[0].Length - 2), timesplit[1].Substring(0, 2)), out _Dt2);
                if (_Dt2 != null)
                {
                    return _Dt2.ToString(_format);
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return null;
            }
        }

        public double ToUnixTimestamp(DateTime value)
        {
            return Convert.ToInt64((value - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);

        }
        public int GetDifferenceInDaysX(DateTime startDate, DateTime endDate, bool countMe = false)
        {
            TimeSpan ts = endDate - startDate;
            int totalDays = (int)Math.Ceiling(ts.TotalDays);
            if (ts.TotalDays < 1 && ts.TotalDays > 0)
                totalDays = 1;
            else
                totalDays = (int)(ts.TotalDays);

            if (countMe)
                totalDays += 1;

            return totalDays;
        }

        /// <summary>
        /// Get Day Text from between date
        /// </summary>
        /// <param name="startDate">dd/MM/yyyy</param>
        /// <param name="endDate">dd/MM/yyyy</param>
        /// <returns></returns>
        public string DateBetweenToDayText(string startDate, string endDate)
        {
            var rtn = "Day";

            try
            {
                var fday = "{0} Day";
                var fdays = "{0} Days";
                var sDate = Convert.ToDateTime(ConvertDateFormatBackFormat(startDate));
                var eDate = Convert.ToDateTime(ConvertDateFormatBackFormat(endDate));
                var day = GetDifferenceInDaysX(sDate, eDate, true);

                if (day > 1)
                {
                    rtn = string.Format(fdays, day.ToString());
                }
                else
                {
                    rtn = string.Format(fday, day.ToString());
                }
            }
            catch
            {
                rtn = "Day";
            }

            return rtn;
        }

        public static string ConvertDateTimeToDateStringFormat(DateTime pDate, string DateStringFormat = "yyyy-MM-dd")
        {
            return pDate.ToString(DateStringFormat, CultureInfo.InvariantCulture);
        }

        public static string ConvertDateTimeToDateStringFormat(DateTime? pDate, string DateStringFormat = "yyyy-MM-dd")
        {
            if (pDate == null)
                return "";
            else
                return Convert.ToDateTime(pDate).ToString(DateStringFormat, CultureInfo.InvariantCulture);
        }

        public DateTime? ConvertDateFormatBackFormat(string DateString)
        {
            //form Database Format dd/mm/yyyy
            //string[] datesplit = DateString.Split('/');
            if (DateString.Length >= 10)//&& datesplit.Length >= 3)
            {
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
                DateTime _Dt;
                //DateTime.TryParse(string.Format("{0}-{1}-{2}", datesplit[2], datesplit[1], datesplit[0]), out _Dt);
                DateTime.TryParseExact(DateString,
                       "dd/MM/yyyy",
                       CultureInfo.InvariantCulture,
                       DateTimeStyles.None,
                       out _Dt);

                if (_Dt != null)
                {
                    _Dt = _Dt.Date;
                    return _Dt;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static DateTime? ConvertStringDateFormatToDatetime(string DateString, string DateStringFormat = "dd/MM/yyyy")
        {
            try
            {
                if (String.IsNullOrEmpty(DateString))
                    return null;

                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
                DateTime _Dt;

                DateTime.TryParseExact(DateString,
                       DateStringFormat,
                       CultureInfo.InvariantCulture,
                       DateTimeStyles.None,
                       out _Dt);

                if (_Dt != null)
                {
                    _Dt = _Dt.Date;
                    return _Dt;
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }

        }

        public static string ToLiteral(string input)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                {
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                    return writer.ToString();
                }
            }
        }

        /// <summary>
        /// Convert String date to Date format
        /// </summary>
        /// <param name="DateString">Date String ex.(31/01/2016 05:00)</param>
        /// <param name="DateStringFormat">Date String ex. (dd/MM/yyyy HH:mm)<</param>
        /// <param name="_ConvertToFormat">Convert To Date Format ex. (dd MMMM)</param>
        /// <returns></returns>
        public static string ConvertStringDateToDateFormat(string DateString, string DateStringFormat, string _ConvertToFormat)
        {
            //form Database Format dd/mm/yyyy HH:mm
            string timeString = DateString;

            try
            {
                IFormatProvider culture = new CultureInfo("en-US", true);
                DateTime dateVal = DateTime.ParseExact(DateString, DateStringFormat, culture);

                return dateVal.ToString(_ConvertToFormat, culture);
            }
            catch
            {
                return "";
            }

        }

        /// <summary>
        /// Get Color Class by Color Code
        /// </summary>
        /// <param name="pColorCode">Color Code Ex.(#ffffff)</param>
        /// <returns>Color Class For GanttView Activity</returns>
        public static string getColorClass(string pColorCode)
        {
            string rtn = "";
            if (String.IsNullOrEmpty(pColorCode))
            {
                return "Silver";    // #8f8f8f
            }
            switch (pColorCode.ToLower())
            {
                case "#ab1e41":
                    rtn = "ganttDeepRed";
                    break;
                case "#77242e":
                    rtn = "ganttBrickRed";
                    break;
                case "#d11f2d":
                    rtn = "ganttBrightRed";
                    break;
                case "#ea417a":
                    rtn = "ganttBrightPink";
                    break;
                case "#dca4af":
                    rtn = "ganttDustyPink";
                    break;
                case "#fcb4cc":
                    rtn = "ganttLightPink";
                    break;
                case "#fcbca0":
                    rtn = "ganttPeach";
                    break;
                case "#f48572":
                    rtn = "ganttCoral";
                    break;
                case "#fc8c2a":
                    rtn = "ganttTangerine";
                    break;
                case "#f16d25":
                    rtn = "ganttVibrantOrange";
                    break;
                case "#c54134":
                    rtn = "ganttGinger";
                    break;
                case "#c36655":
                    rtn = "ganttTerraCotta";
                    break;
                case "#b88253":
                    rtn = "ganttTan";
                    break;
                case "#653c2a":
                    rtn = "ganttChocolate";
                    break;
                case "#594745":
                    rtn = "ganttEarthBrown";
                    break;
                case "#88754b":
                    rtn = "ganttGold";
                    break;
                case "#dd7b24":
                    rtn = "ganttAmber";
                    break;
                case "#f8a81d":
                    rtn = "ganttGoldenYellow";
                    break;
                case "#fdcf08":
                    rtn = "ganttBrightYellow";
                    break;
                case "#f6e068":
                    rtn = "ganttLightYellow";
                    break;
                case "#d6da4f":
                    rtn = "ganttChartreuse";
                    break;
                case "#add690":
                    rtn = "ganttLightGreen";
                    break;
                case "#8a8b47":
                    rtn = "ganttOliveGreen";
                    break;
                case "#78993e":
                    rtn = "ganttLime";
                    break;
                case "#124534":
                    rtn = "ganttDarkGreen";
                    break;
                case "#136736":
                    rtn = "ganttFoliageGreens";
                    break;
                case "#05994d":
                    rtn = "ganttBrightGreen";
                    break;
                case "#009b7a":
                    rtn = "ganttEmerald";
                    break;
                case "#a1d4cb":
                    rtn = "ganttAqua";
                    break;
                case "#4cc1af":
                    rtn = "ganttTurquoise";
                    break;
                case "#036781":
                    rtn = "ganttTeal";
                    break;
                case "#72b0c7":
                    rtn = "ganttSkyBlue";
                    break;
                case "#99bfe6":
                    rtn = "ganttLightBlue";
                    break;
                case "#8093cd":
                    rtn = "ganttPeriwinkle";
                    break;
                case "#0172b8":
                    rtn = "ganttBrightBlue";
                    break;
                case "#212d69":
                    rtn = "ganttDeepBlue";
                    break;
                case "#bb3e99":
                    rtn = "ganttLavender";
                    break;
                case "#a17691":
                    rtn = "ganttMauve";
                    break;
                case "#b984ba":
                    rtn = "ganttAmethyst";
                    break;
                case "#643293":
                    rtn = "ganttBluePurples";
                    break;
                case "#862c8e":
                    rtn = "ganttRedPurples";
                    break;
                case "#3b1a53":
                    rtn = "ganttDeepPurples";
                    break;
                case "#898d8c":
                    rtn = "ganttNeutralGray";
                    break;
                case "#57585a":
                    rtn = "ganttCharcoalGray";
                    break;
                case "#a8a089":
                    rtn = "ganttTaupe";
                    break;
                case "#fae1a8":
                    rtn = "ganttIvory";
                    break;
                case "#8f8f8f":
                    rtn = "ganttSilver";
                    break;
                case "#000000":
                    rtn = "ganttBlack";
                    break;
                case "#ffffff":
                    rtn = "ganttWhite";
                    break;
                default: // #8f8f8f
                    rtn = "ganttSilver";
                    break;
            }

            return rtn;
        }

        public string GetDefaultButtonCSS(string ButtonName)
        {
            string CssButton = "";
            switch (ButtonName.ToUpper())
            {
                case "EDIT":
                    CssButton = "btn btn-default";
                    //CssButton = "btn btn-info";
                    break;
                case "REQUEST":
                    CssButton = "btn btn-default";
                    //CssButton = "btn btn-primary";
                    break;
                case "CANCEL":
                    CssButton = "btn btn-default";
                    //CssButton = "btn btn-danger";
                    break;
                case "REJECT":
                    CssButton = "btn btn-default";
                    //CssButton = "btn btn-warning";
                    break;
                case "APPROVE":
                    CssButton = "btn btn-success";
                    break;
                case "BTNPRINT":
                    CssButton = "btn btn-default";
                    //CssButton = "btn btn-info";
                    break;
                case "SAVE DRAFT":
                    CssButton = "btn btn-default";
                    //CssButton = "btn btn-info";
                    break;
                case "VERIFIED":
                    CssButton = "btn btn-success";
                    break;
                case "VERIFY":
                    CssButton = "btn btn-success";
                    break;
                case "ENDORSED":
                    CssButton = "btn btn-success";
                    break;
                default:
                    CssButton = "btn btn-default";
                    break;

            }
            return CssButton;
        }

        public string GetMessageBTN(string ButtonName)
        {
            string CssButton = "";
            switch (ButtonName.ToUpper())
            {
                case "EDIT":
                    CssButton = "Comment :";
                    //CssButton = "btn btn-info";
                    break;
                case "REQUEST":
                    CssButton = "Comment :";
                    break;
                case "CANCEL":
                    CssButton = "Reason for cancel : The transaction cannot be reused.";
                    break;
                case "REJECT":
                    CssButton = "Reason for reject : The transaction can be resubmit and approve again";
                    break;
                case "APPROVE":
                    CssButton = "Reason for approve";
                    break;
                case "CERTIFIED":
                    CssButton = "Reason for certified";
                    break;
                case "BTNPRINT":
                    CssButton = "Comment :";
                    break;
                case "SAVE DRAFT":
                    CssButton = "Comment :";
                    break;
                default:
                    CssButton = "Comment :";
                    break;

            }
            return CssButton;
        }

        public string intTostrMoney(string Money, bool CanString = false, string ifNoString = "0")
        {
            if (string.IsNullOrEmpty(Money)) return ifNoString;
            else if (Money.ToUpper() == "N/A") return "N/A";
            else
            {
                double _out = 0;
                double.TryParse(Money.Replace(",", ""), out _out);
                if (_out == 0 && CanString)
                {
                    return Money;
                }
                else
                {
                    return (string.IsNullOrEmpty(Money)) ? "0" : _out.ToString("#,##0.####");
                }

            }

        }

        public string intTostrMoney2(string Money, bool CanString = false)
        {
            if (string.IsNullOrEmpty(Money)) return "-";
            else if (Money.ToUpper() == "N/A") return "N/A";
            else
            {
                double _out = 0;
                double.TryParse(Money.Replace(",", ""), out _out);
                if (_out == 0 && CanString)
                {
                    return Money;
                }
                else
                {
                    return _out.ToString("#,##0.0000");
                }

            }

        }

        public double strTodouble(string val)
        {
            double _dob = 0;
            double.TryParse(val, out _dob);
            return _dob;
        }
        public void MessageBoxShow(Page page, string _MSG)
        {
            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "", "OpenBoxPopup('" + HttpUtility.HtmlEncode(_MSG) + "');", true);
        }
        public void MessageBoxShowLink(Page page, string _MSG, string Link)
        {
            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "", "OpenBoxPopupLink('" + HttpUtility.HtmlEncode(_MSG) + "','" + Link + "');", true);
        }
        public void MessageBoxConfirm(Page page, string _MSG, string CallBackFunction)
        {
            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "", "OpenBoxConfirm('" + HttpUtility.HtmlEncode(_MSG) + "','" + CallBackFunction + "');", true);
        }
        public void MessageBoxPrompt(Page page, string _MSG, string CallBackFunction)
        {
            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "", "OpenBoxPrompt('" + HttpUtility.HtmlEncode(_MSG) + "','" + CallBackFunction + "');", true);
        }
        public string GetPathLogo()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "logo-thaioil.png");
        }
        public string GetPathSignature(List<ApproveItem> _ApproveList, string _Action, bool HeadSection = false)
        {
            double a = 0; double b = 0;
            return GetPathSignature(_ApproveList, _Action, ref a, ref b, HeadSection);
        }
        public string GetPathSignature(List<ApproveItem> _ApproveList, string _Action, ref double Height, ref double Width, bool HeadSection = false)
        {
            List<string> extentions = new List<string>(); extentions.Add(".jpg"); extentions.Add(".png"); extentions.Add(".gif");
            string pathSignature = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", "NoSignature.png"); ;
            string tmpPath = "";
            if (_ApproveList != null)
            {
                var Objlst = _ApproveList.Where(x => x.approve_action.ToUpper() == _Action.ToUpper()).ToList();
                if (_Action == "APPROVE_2" && (Objlst != null && Objlst.Count == 0))
                {
                    Objlst = _ApproveList.Where(x => x.approve_action.ToUpper() == "APPROVE_3".ToUpper()).ToList();
                }
                if (Objlst.Count > 0)
                {
                    tmpPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", Objlst[0].approve_action_by);
                    if (HeadSection)
                    {
                        string SectionHeadName = _permissionDAL.GetSectionHead(Objlst[0].approve_action_by);
                        if (SectionHeadName != null)
                        {
                            tmpPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", SectionHeadName);
                        }
                    }
                }
            }
            foreach (string _itemExt in extentions)
            {
                if (File.Exists(tmpPath + _itemExt))
                {
                    Bitmap img = new Bitmap(tmpPath + _itemExt);
                    if (img.Width > 76) Width = 2;
                    if (img.Height > 37) Height = 1;

                    pathSignature = tmpPath + _itemExt;
                    break;
                }
            }
            return pathSignature;
        }

        public string GetExplanImage(string PathExplan, string URL = "")
        {
            if (!string.IsNullOrEmpty(URL))
            {
                string path = Path.Combine(URL, PathExplan);
                using (var client = new WebClient())
                {
                    client.DownloadFile(path, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PathExplan));
                }
            }
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PathExplan);
        }

        public static bool IsFileReady(string sFilename)
        {
            // If the file can be opened for exclusive access it means that the file
            // is no longer locked by another process.
            try
            {
                using (FileStream inputStream = File.Open(sFilename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    return inputStream.Length > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public string GetNameOffApprove(List<ApproveItem> _ApproveList, string _Action, bool HeadSection = false)
        {
            string NameAction = "";
            if (_ApproveList != null)
            {
                var Objlst = _ApproveList.Where(x => x.approve_action.ToUpper() == _Action.ToUpper()).ToList();
                if (_Action == "APPROVE_2" && (Objlst != null && Objlst.Count == 0))
                {
                    Objlst = _ApproveList.Where(x => x.approve_action.ToUpper() == "APPROVE_3".ToUpper()).ToList();
                }
                if (Objlst.Count > 0)
                {
                    NameAction = Objlst[0].approve_action_by;
                    if (HeadSection)
                    {
                        string SectionHeadName = _permissionDAL.GetSectionHead(Objlst[0].approve_action_by);
                        if (SectionHeadName != null)
                        {
                            NameAction = SectionHeadName;
                        }
                    }
                }
            }

            return _permissionDAL.GetUserInfomationName(NameAction);
        }

        public string GetPathofPersonalImg(string UserName)
        {
            List<string> extentions = new List<string>(); extentions.Add(".jpg"); extentions.Add(".png"); extentions.Add(".gif");
            string pathSignature = GetSiteRootUrl("content/images/Emp/NoImage.png");
            string tmpPath = GetSiteRootUrl("content/images/Emp/" + UserName);
            foreach (string _itemExt in extentions)
            {
                if (File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Emp", UserName) + _itemExt))
                {
                    pathSignature = tmpPath + _itemExt;
                    break;
                }
            }
            return pathSignature;
        }

        public void DisableAllControl(Control _CtrlAll)
        {
            foreach (Control _ctrl in _CtrlAll.Controls)
            {
                if (_ctrl.Controls.Count > 0)
                {
                    DisableAllControl(_ctrl);
                }
                else
                {
                    if (_ctrl.GetType() == typeof(Button))
                    {
                        ((Button)_ctrl).Visible = false;
                    }
                    if (_ctrl.GetType() == typeof(TextBox))
                    {
                        ((TextBox)_ctrl).Attributes.Add("readonly", "readonly");
                    }
                    if (_ctrl.GetType() == typeof(DropDownList))
                    {
                        ((DropDownList)_ctrl).Attributes.Add("disabled", "disabled");
                    }
                    if (_ctrl.GetType() == typeof(HtmlSelect))
                    {
                        ((HtmlSelect)_ctrl).Attributes.Add("disabled", "disabled");
                    }
                    if (_ctrl.GetType() == typeof(HtmlInputText))
                    {
                        ((HtmlInputText)_ctrl).Attributes.Add("readonly", "readonly");
                    }
                    if (_ctrl.GetType() == typeof(HtmlTextArea))
                    {
                        ((HtmlTextArea)_ctrl).Attributes.Add("readonly", "readonly");
                    }
                }
            }
        }

        public string GetORDINALNUMBERS(int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }

        public string GetSiteRootUrl(string url)
        {
            try
            {
                string protocol;

                if (HttpContext.Current.Request.IsSecureConnection)
                    protocol = "https";
                else
                    protocol = "http";

                log.Debug(" protocol : " + protocol);

                StringBuilder uri = new StringBuilder(protocol + "://");

                string hostname = HttpContext.Current.Request.Url.Host;

                log.Debug(" hostname : " + hostname);

                uri.Append(hostname);

                int port = HttpContext.Current.Request.Url.Port;

                log.Debug(" port : " + port);

                if (port != 80 && port != 443)
                {
                    uri.Append(":");
                    uri.Append(port.ToString());
                }

                string APPName = System.Configuration.ConfigurationManager.AppSettings["AppName"];
                log.Debug(" APPName : " + APPName);

                if (url != "")
                {
                    return Path.Combine(uri.ToString(), APPName, url);
                }
                else
                {
                    return "";
                }

            }
            catch (Exception e)
            {
                log.Error("# error :  " + e.StackTrace);
                log.Error("# error :  " + e.InnerException);
                log.Error("# error :  " + e.Message);
                return "";
            }

        }

        public string GetProductShortName(string ProductName, List<ProductJson> Products)
        {
            string ShourtName = "";
            foreach (var item in Products)
            {
                if (ProductName.IndexOf(item.full_name) >= 0)
                {
                    ShourtName = item.short_name;
                    break;
                }
            }
            if (string.IsNullOrEmpty(ShourtName) && ProductName != "") ShourtName = ProductName;
            return ShourtName;
        }

        public string getRandColor()
        {

            string hexOutput = String.Format("{0:X}", rnd.Next(0, 0xFFFFFF));
            while (hexOutput.Length < 6)
                hexOutput = "0" + hexOutput;
            return "#" + hexOutput;
        }

        /// <summary>
        /// Create by aekkasit : 17/112016 : Generate Code By DateTime
        /// </summary>
        /// <param name="pPrefix">Prefix</param>
        /// <param name="pDateFormat">String date format</param>
        /// <returns></returns>
        public static string GenerateCodeByDate(string pPrefix, string pDateFormat = "yyMMddHHmmssff")
        {
            string Code = pPrefix + ConvertDateTimeToDateStringFormat(DateTime.Now, pDateFormat);
            return Code;
        }

        /// <summary>
        /// Create by aekkasit : 21/112016 : Generate OTP ID
        /// </summary>
        /// <param name="GenType">Alphanumeric=1, Numeric=2</param>
        /// <param name="Digit">Digit Lenght</param>
        /// <returns></returns>
        public static string GenerateOTP(string GenType, int Digit)
        {
            string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "1234567890";

            string characters = numbers;
            if (GenType == "1")
            {
                characters += alphabets + small_alphabets + numbers;
            }
            int length = Digit;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }

        /// <summary>
        /// Create by aekkasit : 25/112016 : Check Date is valid format
        /// </summary>
        /// <param name="pDate">Sting Date : ex. (25/11/2016 09:00)</param>
        /// <param name="pDateFormat">Date Format : ex. (dd/MM/yyyy HH:mm)</param>
        /// <returns>true = is valid , false = is not valid</returns>
        public static bool IsValidDateFormat(string pDateString, string pDateFormat = "dd/MM/yyyy")
        {
            try
            {
                DateTime dateTime;
                return DateTime.TryParseExact(pDateString, pDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Create by aekkasit : 25/112016 : Check Decimail is valid format
        /// </summary>
        /// <param name="numFormat">To check the numbers</param>
        /// <returns>true = is valid , false = is not valid</returns>
        public static bool IsValidDecimalFormat(string numFormat)
        {
            try
            {
                decimal temp;
                var is_number = decimal.TryParse(numFormat, out temp);
                return is_number;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Create by aekkasit : 25/112016 : Convert string date to Date
        /// </summary>
        /// <param name="pDateString">Sting Date : ex. (25/11/2016 09:00)</param>
        /// <param name="pDateFormat">Date Format : ex. (dd/MM/yyyy HH:mm)</param>
        /// <returns>DateTime</returns>
        public static DateTime ConvertStrDateToDate(string pDateString, string pDateFormat = "dd/MM/yyyy")
        {

            try
            {
                DateTime dateTime;
                DateTime.TryParseExact(pDateString, pDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
                return dateTime;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int ExcelColumnNameToNumber(string columnName)
        {
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");

            columnName = columnName.ToUpperInvariant();

            int sum = 0;

            for (int i = 0; i < columnName.Length; i++)
            {
                sum *= 26;
                sum += (columnName[i] - 'A' + 1);
            }

            return sum;
        }

        public static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }


        public static string EncodeString(string key)
        {
            try
            {
                byte[] utf8Bytes = System.Text.Encoding.UTF8.GetBytes(key);
                string dataString = Convert.ToBase64String(utf8Bytes, 0, utf8Bytes.Length);
                return dataString;
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    return key;
                }
                else
                {
                    throw ex;
                }                
            }           
        }

        public static string DecodeString(string key)
        {
            try
            {
                byte[] stringToByte = Convert.FromBase64String(key);
                string dataString = System.Text.Encoding.UTF8.GetString(stringToByte, 0, stringToByte.Length);
                return dataString;
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    return key;
                }
                else
                {
                    throw ex;
                }
            }
            
        }

        public static string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

    }

    public static class ShareFNS
    {
        public static string Encrypt(this object textToEncrypt)
        {
            if (textToEncrypt == null || Convert.ToString(textToEncrypt) == string.Empty) return string.Empty;
            //throw new ArgumentNullException("EncodeWithKey", "The message cannot be null");

            var textToEncryptString = Convert.ToString(textToEncrypt);
            string key = ConstantPrm.URLEncryptKey;

            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.IV = new byte[8];

            //Creates the key based on the password and stores it in a byte array.
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(key, new byte[0]);
            des.Key = pdb.CryptDeriveKey("RC2", "MD5", 128, new byte[8]);

            MemoryStream ms = new MemoryStream(textToEncryptString.Length * 2);
            CryptoStream encStream = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
            byte[] plainBytes = Encoding.UTF8.GetBytes(textToEncryptString);
            encStream.Write(plainBytes, 0, plainBytes.Length);
            encStream.FlushFinalBlock();
            byte[] encryptedBytes = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(encryptedBytes, 0, (int)ms.Length);
            encStream.Close();

            return Convert.ToBase64String(encryptedBytes);
        }

        public static string Decrypt(this string encryptedText)
        {
            if (string.IsNullOrEmpty(encryptedText)) return string.Empty;
            //throw new ArgumentNullException("DecodeMD5WithKey", "The encrypted message cannot be null");

            string key = ConstantPrm.URLEncryptKey;

            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.IV = new byte[8];

            //Creates the key based on the password and stores it in a byte array.
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(key, new byte[0]);
            des.Key = pdb.CryptDeriveKey("RC2", "MD5", 128, new byte[8]);

            //This line protects the + signs that get replaced by spaces when the parameter is not urlencoded when sent.
            if (encryptedText.Count() == 31)
            {
                encryptedText += " ";
            }
            encryptedText = encryptedText.Replace(" ", "+");
            MemoryStream ms = new MemoryStream(encryptedText.Length * 2);
            CryptoStream decStream = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);

            byte[] plainBytes;
            try
            {
                byte[] encBytes = Convert.FromBase64String(Convert.ToString(encryptedText));
                decStream.Write(encBytes, 0, encBytes.Length);
                decStream.FlushFinalBlock();
                plainBytes = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(plainBytes, 0, (int)ms.Length);
                decStream.Close();
            }
            catch (CryptographicException e)
            {
                throw new ApplicationException("Cannot decrypt message.  Possibly, the password is wrong", e);
            }

            return Encoding.UTF8.GetString(plainBytes);
        }


    }
}