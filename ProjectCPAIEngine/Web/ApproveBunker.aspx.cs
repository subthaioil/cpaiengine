﻿using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Web
{
    public partial class ApprovePurchase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ForTest
            //http://localhost:50131/Web/ApproveBunker.aspx?token=89b7e01a8af24dc98102e6be1d3470a3
            
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["token"] != null)
                {
                    string TokenID = Request.QueryString["token"].ToString();
                    CheckApprove(TokenID);
                }
                else
                {
                    Response.Redirect("~/web/login.aspx");
                }
            }
        }

        public void CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {
                        if (item.UserType.Equals("CRUDE"))
                        {
                            urlPage = "BunkerPreDuePurchase.aspx";
                        }
                        else
                        {
                            urlPage = "BunkerPreDuePurchaseCMMT.aspx";
                        }

                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                            }
                            strTransID = item.TransactionID;
                        }

                       /* UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        Const.User = _user;*/

                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;

                        path = "~/web/" + urlPage + "?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt()+ "&PURNO="+ strPurNo.Encrypt();
                    }
                }
            }
            Response.Redirect(path);
        }

        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }
    }
}