﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/MainSite.Master"  ValidateRequest="false" CodeBehind="BunkerPreDuePurchase.aspx.cs" Inherits="ProjectCPAIEngine.Web.BunkerPreDuePurchase" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .PriceOfferTable {
            width: 100%;
        }

            .PriceOfferTable .HeaderTable td {
                /*background-color: silver;*/
                text-align: center;
                font-weight: bold;
                vertical-align: middle;
            }
            .uploadifyQueueItem{
                width:550px;
                  
            }
            .uploadifyQueueItemExpain{
                width:550px;
                  
            }
            .fileName{
                cursor: pointer;
            }
            .glyphicon-remove{
                  cursor: pointer;
            }
    </style>

    <script type="text/javascript">    
       
        window.onbeforeunload = function (evt) {
            var btnclick = false;
            if ($('#<%= EventButton.ClientID %>').val() == "0") {
                btnclick = true;
            }
            if (btnclick) {
                return "If you leave this page, your information will not be updated.";
            }
           
        }
        

        function SaveAllOfferTable(type) {
            var objlst = [];
            $('.TrData').each(function () {
                var row = $(this);
                var OfferSaveData = new Object();
                OfferSaveData.keyitem = row.find(".rdoOfferList").val();
                OfferSaveData.supplier = row.find(".supplier").val() == "" ? row.find(".supplier").text() : row.find(".supplier").val();
                OfferSaveData.contact_person = row.find(".contact_person").val() == "" ? row.find(".contact_person").text() : row.find(".contact_person").val();
                OfferSaveData.purchasing_term = row.find(".purchasing_term").val() == "" ? row.find(".purchasing_term").text() : row.find(".purchasing_term").val();
               // OfferSaveData.others_cost_barge = row.find(".others_cost_barge").val();
                OfferSaveData.others_cost_surveyor = row.find(".others_cost_surveyor").val() == "" ? row.find(".others_cost_surveyor").text() : row.find(".others_cost_surveyor").val();
                OfferSaveData.final_price = row.find(".final_price").val() == "" ? row.find(".final_price").text() : row.find(".final_price").val();
                OfferSaveData.total_price = row.find(".total_price").val() == "" ? row.find(".total_price").text() : row.find(".total_price").val();
                OfferSaveData.note = row.find(".note").val() == "" ? row.find(".note").text() : row.find(".note").val();
                //OfferSaveData.pricebasedate = row.find("#txtPriceBaseDate").val();
                var roundoff = $(".roundOffer").length;
                var _itemData = "";
                for (i = 1; i < roundoff + 1; i++) {
                    row.find(".round_value_fix_" + i).each(function () {
                        _itemData += "^fix_" + i + "|" + ($(this).val() == "" ? $(this).text() : $(this).val())
                    });
                    row.find(".round_value_floating_" + i).each(function () {
                        _itemData += "^floating_" + i + "|" + ($(this).val() == "" ? $(this).text() : $(this).val())
                    });
                }

                //row.find(".quantity_value").each(function () {
                //    _itemData += "^QUANTITY|" + $(this).val()

                //});
                OfferSaveData.item = _itemData;

                objlst.push(OfferSaveData);

            });
            return objlst;
        }

        function CallMethodUpdateProduct() {
            var Product = $('#<%= ddlProduct.ClientID %> option:selected').text();
            if (Product == "Others") Product = $('#<%= txtProductOthers.ClientID %> ').val();
            $.ajax({
                type: "POST",
                url: 'BunkerPreDuePurchase.aspx/CallUpdateProduct',
                data: JSON.stringify({ Product: Product }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    SetClieckRefresh();
                },
                error: function (e) {
                    alert("Something Wrong.");
                    SetClieckRefresh();
                }
            });
        }



        function CallOfferMethod(KeyID, EventClick) {
            //$('#<%= hdfDateNote.ClientID %>').val($("#txtPriceBaseDate").val());
            //Save before deleteing
            CallItemEvent("SAVEOFFER");
            $.ajax({
                type: "POST",
                url: 'BunkerPreDuePurchase.aspx/SetEventRecord',
                data: JSON.stringify({ KeyID: KeyID, EventClick: EventClick }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    SetClieckRefresh();
                },
                error: function (e) {
                    //alert("Something Wrong.");
                    SetClieckRefresh();
                }
            });
        } 
       
        function CallItemEvent(_EventClick) {
            //$('#<%= hdfDateNote.ClientID %>').val($("#txtPriceBaseDate").val());
            $.ajax({
                type: "POST",
                url: 'BunkerPreDuePurchase.aspx/CallSaveItemEvent',
                data: JSON.stringify({ EventClick: _EventClick, jsonData: SaveAllOfferTable()
                    ,Product: $("#<%=ddlProduct.ClientID %>").find("option:selected").text()
                    ,Producttxt: $('#<%= txtProductOthers.ClientID %> ').val()
                    ,Volumn: $('#<%= txtProductVolume.ClientID %> ').val()
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    SetClieckRefresh();
                },
                error: function (e) {
                    //alert("Something Wrong.");
                    SetClieckRefresh();
                }
            });
        }
        

        function CallBackSubmitPrompt(Message, Event) {
            $('#<%= hdfNoteAction.ClientID %>').val(Message);
            $('#<%= hdfEventClick.ClientID %>').val(Event);
            console.log(Event);
            if (Event == "Draft" || Event == "DraftPreview" || Event == "Submit") {
                $('#<%= btnEvent.ClientID %>').click();
                LoadingProcess();
            } else {
                $('#<%= btnEventCommand.ClientID %>').click();
                LoadingProcess();
            }

        }

        function OpenNewTabF(url, Fname) {
            window.open(url, '_blank');
            LoadFileUpload();
            SaveClick("");
        }
        function SaveClick(tmp) {
            OpenBoxBungerPrompt('Comment :', 'DraftReClick')
        }

        function OpenBoxBungerPrompt(MSG, Event) {
            if (Event.toUpperCase() == "SUBMIT") {
                if (!ValidateBeforeSave()) return false;
            }
            $('#<%= EventButton.ClientID %> ').val("1");
            if (Event != "DraftReClick") CallItemEvent('SAVEOFFER');
            else { Event = "Draft" }
            SaveFileUpload();
            if (Event != "DraftPreview") {
                if (Event.toUpperCase() == "REJECT") {
                    bootbox.prompt(MSG, function (result) {
                        if (result === "" || result === null) {//case cancle popup return null  >> result === null ||
                            if (result === "") {
                                OpenBoxPopup("Please Key Reason for Reject.");
                            }
                            $('#<%= EventButton.ClientID %>').val("0");
                        } else {
                            var CallBackFunction = "CallBackSubmitPrompt";
                            window[CallBackFunction](result, Event);
                        }
                    });
                } else if (Event.toUpperCase() == "CANCEL") {
                    bootbox.prompt(MSG, function (result) {
                        if (result === null || result === "") {
                            if (result === "") {
                                OpenBoxPopup("Please Key Reason for Cancel.");
                            }
                            $('#<%= EventButton.ClientID %>').val("0");
                        } else {
                            var CallBackFunction = "CallBackSubmitPrompt";
                            window[CallBackFunction](result, Event);                            
                        }                                     
                    });
                } else if (Event.toUpperCase() == "SUBMIT") {
                    bootbox.confirm({
                        message: "Are you sure to submit?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                            },
                            cancel: {
                                label: 'No',
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                var CallBackFunction = "CallBackSubmitPrompt";
                                window[CallBackFunction]("-", Event);
                            }
                        }
                    });
                } else if (Event.toUpperCase() == "APPROVE")
                {
                    bootbox.prompt(MSG, function (result) {
                        if (result === null)
                        {
                             $('#<%= EventButton.ClientID %>').val("0");
                        } else
                        {
                            if (result == "") {
                                result = "-";
                            }
                            var CallBackFunction = "CallBackSubmitPrompt";
                            window[CallBackFunction](result, Event);
                        }
                    });
                }
                else {
                    var CallBackFunction = "CallBackSubmitPrompt";
                    window[CallBackFunction]("-", Event);
                }
            } else {
                var CallBackFunction = "CallBackSubmitPrompt";
                window[CallBackFunction]("", Event);
            }
        }

        function ValidateOfferItem() {
            if ($('.TrData').length <= 0) {
                OpenBoxPopup("ไม่สามารถบันทึกโดยไม่มี Offer ได้");
                return false;
            }

            var isCheck = false;
            $('input:radio[name="rdoffer"]').each(function () {
                if ($(this).is(':checked')) {
                    isCheck = true;
                }
            });
            if (!isCheck) {
                OpenBoxPopup("กรุณาเลือก Offer");
                return false;
            }

            return true;
        }

        <%--function OpenBoxButtonPrompt(MSG) {
            if (Event != "DraftPreview" && Event != "Draft" && Event != "DraftReClick" && Event != "SAVE DRAFT") {
                if (!ValidateBeforeSave()) return false;
            }
            $('#<%= EventButton.ClientID %> ').val("1");
            if (Event != "DraftReClick") CallItemEvent('SAVEOFFER');
            else { Event = "Draft" }
            SaveFileUpload();
            bootbox.prompt(MSG, function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        //OpenBoxPopup("Please Key Note.");
                        $('#<%= hdfNoteAction.ClientID %>').val(result);
                        return true;
                    }
                    return false;
                } else {
                    $('#<%= hdfNoteAction.ClientID %>').val(result);
                    return true;
                }
            });
        }--%>

        function SetEventCtrl() {
            $('#<%= ddlProduct.ClientID %>').change(function () { CheckControl(); CallMethodUpdateProduct(); });
            $('#<%= ddlVassel.ClientID %>').change(function () { CheckControl(); });
            $('#<%= ddlTypeOfPurchase.ClientID %>').change(function () { CheckControl(); });
            $('#<%= ddlSupplyingLocation.ClientID %>').change(function () { CheckControl(); });
            $('#<%= ddlReason.ClientID %>').change(function () { CheckControl(); });
            $('#<%= ddlPaymentTerm.ClientID %>').change(function () {
                CheckControl();
                $('#rdoDayZero').prop("checked", false);
                $('#rdoDayOne').prop("checked", false);
                $('#<%= txtPaymentTermDays.ClientID %>').val('');
                if ($('#<%= ddlPaymentTerm.ClientID %> option:selected').text() != "Others" && $('#<%= ddlPaymentTerm.ClientID %> option:selected').text().indexOf("payment") <= 0) {
                    $('#rdoDayOne').prop("checked", true);
                }
                SetValDatePurChase();
            });
            $('.txtProductOthers').change(function () { CallMethodUpdateProduct(); });
            CheckControl();
        }

        function CheckControl() {
            if ($('#<%= ddlProduct.ClientID %> option:selected').text() == "Others") { $('#<%= txtProductOthers.ClientID %>').show(); } else { $('#<%= txtProductOthers.ClientID %>').hide(); }


            if ($('#<%= ddlVassel.ClientID %> option:selected').text() == "Others")
            {
                $('#<%= txtVassel.ClientID %>').show();
                $('#<%= hdfVesselID.ClientID %>').val($('#<%= ddlVassel.ClientID %> option:selected').val());
            }
            else
            {                 
                $('#<%= txtVassel.ClientID %>').hide();
                $('#<%= hdfVesselID.ClientID %>').val($('#<%= ddlVassel.ClientID %> option:selected').val());
            }


            if ($('#<%= ddlTypeOfPurchase.ClientID %> option:selected').text() == "Others") { $('#<%= txtTypeofpurchase.ClientID %>').show(); } else { $('#<%= txtTypeofpurchase.ClientID %>').hide(); }
            if ($('#<%= ddlSupplyingLocation.ClientID %> option:selected').text() == "Others") { $('#<%= txtSupplyingLocation.ClientID %>').show(); } else { $('#<%= txtSupplyingLocation.ClientID %>').hide(); }
            if ($('#<%= ddlReason.ClientID %> option:selected').text() == "Others" ||$('#<%= ddlReason.ClientID %> option:selected').text() == "Best price") { $('#<%= txtReason.ClientID %>').show(); } else { $('#<%= txtReason.ClientID %>').hide(); }
            if ($('#<%= ddlPaymentTerm.ClientID %> option:selected').text() == "Others") {
                $('.txtPaymentTermRadio').show();
                $('.txtPaymentTermRadioL').hide();
                $('.divPaymentTermRadio').hide();
            }
            else if ($('#<%= ddlPaymentTerm.ClientID %> option:selected').text().indexOf("payment") > 0) {
                $('.txtPaymentTermRadio').hide();
                $('.txtPaymentTermRadioL').hide();
                $('.divPaymentTermRadio').hide();
            }
            else {
                $('.txtPaymentTermRadio').show();
                $('.txtPaymentTermRadioL').show();
                $('.divPaymentTermRadio').show();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2"  ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hdfNoteAction" />
    <asp:HiddenField runat="server" ID="hdfDateNote" />
    <asp:HiddenField runat="server" ID="EventButton" Value="0" />
    <div class="main-content-area">
        <div class="wrapper-detail">
            <div>
                <h1><b>Bunker Purchase</b></h1>
            </div>
            <div class="form-horizontal">                 
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Task ID</label>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtTaskID" class="form-control" ReadOnly="true" Text="" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Purchase No.</label>
                            <div class="col-md-4">
                               <asp:TextBox ID="txtPurchaseNo" class="form-control" ReadOnly="true" Text="" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Purchased Date<span style="color:red">*</span></label>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtPurchasedDate" runat="server" class="form-control input-date-picker txtPurchasedDate"  ></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Delivery Date<span style="color:red">*</span></label>
                            <div class="col-md-4">
                                <!-- if change ID please also change js command -->
                                <input type="text" id="txtDeliveryDaterange" runat="server" class="form-control input-date-picker txtDeliveryDaterange"  value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">For Vessel<span style="color:red">*</span></label>
                            <div class="col-md-4">
                                <asp:DropDownList ID="ddlVassel" class="form-control ddlVassel" runat="server">
                                </asp:DropDownList>

                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtVassel" class="form-control txtVassel" placeholder="Please enter detail" runat="server"></asp:TextBox>
                                <%-- <input type="text" class="form-control" value="" placeholder="Please enter detail" >--%>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Voyage<span style="color:red">*</span></label>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtVoyage" runat="server" class="form-control txtVoyage" placeholder="Please enter detail"></asp:TextBox>
                                <%--<input type="text" class="form-control" value="">--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Voyage No.<span style="color:red">*</span></label>
                            <div class="col-md-4">
                              <asp:TextBox ID="txtTripNo" class="form-control txtTripNo" Text="TC" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Supply Location<span style="color:red">*</span></label>
                            <div class="col-md-4">
                                <asp:DropDownList ID="ddlSupplyingLocation" class="form-control ddlSupplyingLocation" runat="server" Style="text-align: left">
                                </asp:DropDownList>
                                <%-- <select class="form-control">
                                            <option>Other</option>
                                        </select>--%>
                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtSupplyingLocation" class="form-control txtSupplyingLocation" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                        <div class="row">
                            <label class="col-md-3 control-label">Product<span style="color:red">*</span></label>
                            <div class="col-md-4">
                                <asp:DropDownList ID="ddlProduct" class="form-control ddlProduct" runat="server">
                                </asp:DropDownList>
                            </div>
                             <div class="col-md-4">
                                <asp:TextBox ID="txtProductOthers" class="form-control txtProductOthers" runat="server"></asp:TextBox>
                            </div>
                       
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label">Volume ( MT )<span style="color:red">*</span></label>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtProductVolume" class="form-control txtProductVolume numberCtrl" placeholder="Please enter detail" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                             <label class="col-md-3 control-label">Type of Purchase<span style="color:red">*</span></label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlTypeOfPurchase" class="form-control ddlTypeOfPurchase" runat="server" Style="text-align: left">
                            </asp:DropDownList>
                            <%--<select class="form-control">
                                            <option>Other</option>
                                        </select>--%>
                        </div>
                        <div class="col-md-4">
                            <input type="text" id="txtTypeofpurchase" runat="server" class="form-control txtTypeofpurchase" placeholder="Please enter detail" />
                            <%--<input type="text" class="form-control" value="" placeholder="Please enter detail">--%>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label">Contract type<span style="color:red">*</span></label>
                            <div class="col-md-4">
                                <asp:DropDownList ID="ddlContractType" class="form-control ddlContractType" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Any other special terms and conditions</label>
                        <textarea class="form-control txtAnyOtherSpecial" id="txtAnyOtherSpecial" runat="server"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Explanation (as necessary)</label>
                        <div>
                            <asp:TextBox  CssClass="txtExplanation" runat="server" ID="txtExplanation"></asp:TextBox>
                        </div>
                        <!-- <div><img src="images/mock-textediter.jpg"></div> -->
                    </div>
                </div>
                 <div class="col-md-12">
                    <div class="form-group">
                        <button runat="server" id="btn_Explanupload_MultiFile" class="btn btn-success" type="button" onclick="addExplanAttachFile('CRUDE','F10000001')"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span>Select File</button><b id="txt_Explanupload_MultiFile">Only Image files</b>
                        <input type="file" accept="image/*" max-file-size=4194304 runat="server" id="Explanupload_MultiFile" name="Explanupload_MultiFile" multiple="multiple" style="display:none;" />
                                <div id="ExplanfilesUploaded"></div>
                        </div>
                     </div>
            </div>
            <h2 class="title">Fuel Oil / Gas Oil Price Offer<span style="color:red">*</span></h2>
            <div class="row">
                <div class="col-md-12">
                    <asp:UpdatePanel runat="server" ID="UPNGird">
                        <ContentTemplate>
                            <div style="overflow-x: scroll;">
                                <div id="htmlTag"><% =HTMLTable.ToString() %>  </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAddNewSupplier" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnAddNewOffer" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnrefresh" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <asp:Button runat="server" ID="btnAddNewSupplier" CssClass="btn btn-success" Text="Add New Supplier" OnClientClick="CallItemEvent('AddSup');return false;"  />
                </div>
                <div class="col-md-6" style="text-align: right;">
                    <asp:Button runat="server" ID="btnAddNewOffer" Text="Add New Offer" CssClass="btn btn-primary"  OnClientClick="CallItemEvent('AddOff');return false;" />
                    <asp:Button runat="server" ID="btnrefresh" Text="refresh" OnClick="btnrefresh_Click" Style="display: none;" />
                </div>
            </div>
            <h2 class="title">Payment Terms</h2>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="col-md-6">
                            <asp:DropDownList ID="ddlPaymentTerm" class="form-control ddlPaymentTerm" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-md-6 txtPaymentTermRadio">
                            <input type="text" class="form-control txtPaymentTermDays" runat="server" id="txtPaymentTermDays" value="" />
                        </div>
                        <%--<div class="col-md-3 txtPaymentTermRadioL">
                            days from B/L date
                        </div>--%>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group divPaymentTermRadio" >
                        <div class="col-md-6">
                            <div class="radio">
                                <input id="rdoDayOne" class="rdoDayTerm" name="nrdoDay" value="DayOne" type="radio" />
                                <label for="rdoDayOne">
                                      days from B/L Date (B/L Date = Day One)
                                </label>                               
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="radio">
                                 <input id="rdoDayZero" class="rdoDayTerm" name="nrdoDay" value="DayZero" type="radio" />
                                <label for="rdoDayZero">
                                    days after B/L Date (B/L Date = Day Zero)
                                </label>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hdfDayTerm" />
                    </div>
                </div>
            </div>
            <h2 class="title">Proposal</h2>
            <div class="form-horizontal">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Award to<span style="color:red">*</span></label>
                            <div class="col-md-8">
                                <%-- <input type="text" class="form-control" value="Scandinavian Bunkering"  disabled>--%>
                                  <input type="text" id="txtAwardTotmp" class="form-control txtAwardTotmp" disabled runat="server" />
                                <asp:TextBox ID="txtAwardTo" class="form-control txtAwardTo"  style="display:none"  runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Award price<span style="color:red">*</span></label>
                            <div class="col-md-8">
                                <input type="text" id="txtAwardPricetmp" class="form-control txtAwardPricetmp" disabled runat="server" />
                                <input type="text" id="txtAwardPrice" class="form-control txtAwardPrice" style="display:none"   runat="server" />
                                <%-- <input type="text" class="form-control" value="MOPS380 (12Aug / 15Aug / 16Aug) +$3.75" >--%>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Reason</label>
                            <div class="col-md-4">
                                <asp:DropDownList ID="ddlReason" class="form-control ddlReason" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control txtReason" maxlength="50"  value="" runat="server" id="txtReason" placeholder="Please enter detail">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-top: -40px;">
                        <div class="form-group">
                            <label class="control-label" style="padding-bottom: 15px;">NOTE</label>
                            <textarea class="form-control txtNote" runat="server" id="txtNote" style="min-height: 123px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="title">Reason for Reject</h2>
            <div class="form-horizontal">
                <div class="row">                    
                    <div class="col-md-6" style="margin-top: -40px;">
                        <div class="form-group">
                            <br />
                            <textarea class="form-control txtNote" runat="server" id="txtNoteReason" readonly="readonly" style="min-height: 60px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <h2 id="txtAttach_File" class="title">Attach File</h2>
            <div class="form-horizontal">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-8">
                                <button runat="server" id="btn_upload_MultiFile" class="btn btn-success" type="button" onclick="addAttachFile('CRUDE','F10000001')"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span>Select File</button><b id="txtupload_MultiFile">PDF, Word, Excel, Image, Outlook</b>
                                <input type="file" runat="server" id="upload_MultiFile" name="upload_MultiFile" multiple="multiple" accept=".msg,.pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" style="display:none;"/>
                                <div id="filesUploaded"></div>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper-button" runat="server" id="buttonDiv">
                <input type="button" id="btnSaveDrafttmp" runat="server" class="btn btn-default" onclick="OpenBoxBungerPrompt('Comment :', 'Draft'); return false;" value="SAVE DRAFT" />
                <input type="button" id="btnSavePreviewtmp" runat="server" class="btn btn-default" onclick="OpenBoxBungerPrompt('Comment :', 'DraftPreview'); return false;" value="SAVE &amp; PREVIEW" />
                <input type="button" id="btnSubmittmp" runat="server" class="btn btn-default" onclick="OpenBoxBungerPrompt('Comment :', 'Submit'); return false;" value="SUBMIT" />
                <asp:Button ID="btnEvent" class="btn btn-info" runat="server" Text="EventClick" Style="display: none;" OnClick="btnEvent_Click" />
                <asp:Button ID="btnEventCommand" class="btn btn-info" runat="server" Text="btnEventCommand" Style="display: none;" OnClick="_btn_Click" />
                <asp:HiddenField runat="server" ID="hdfEventClick" />
                 <asp:HiddenField runat="server" ID="hdfOfferFinalPrice" />
                 <asp:HiddenField runat="server" ID="hdfFileUpload" />
                <asp:HiddenField runat="server" ID="hdfExplanFileUpload" />
                <asp:HiddenField runat="server" ID="hdfVesselID" />
            </div>
        </div>
    </div>

    <script type="text/javascript">
         $('.rdoDayTerm').change(function () {
             SetValDatePurChase();
         });

         function SetValDatePurChase() {
             if ($('#rdoDayZero').is(':checked')) {
                $('#<%= hdfDayTerm.ClientID %>').val($('#rdoDayZero').val());
            }
            else if ($('#rdoDayOne').is(':checked')) {
                $('#<%= hdfDayTerm.ClientID %>').val($('#rdoDayOne').val());
               
            }
            else {
                $('#<%= hdfDayTerm.ClientID %>').val("");
            }      
         }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            LoadAffterPostBack();
        });

        function formatNumber(e) {
            if ($(e).closest('.TrData').children('td:eq(0)').find('.rdoOfferList').is(':checked')) {
                if ($(e).attr("class").indexOf("round_value_fix") > -1 || $(e).attr("class").indexOf("round_value_floating") > -1) {
                    calculateFinalPrice(e);
                } else if ($(e).attr("class").indexOf("others_cost_surveyor") > -1 || $(e).attr("class").indexOf("final_price") > -1) {
                    //calculateTotalPrice(e);
                }
            }

            if (!isNaN(parseFloat($(e).val()))) {
                $(e).val(parseFloat(parseFloat($(e).val()).toFixed(4)));
            }
        }

        function calculateFinalPrice(e) {
            var _pos = $(e).closest('.TrData').children('td').length - 5;
            var fix_pos = ($(e).closest('.TrData').children('td').length - 9) / 2;
            var fix_price = "";
            if ($('#<%= ddlTypeOfPurchase.ClientID %> option:selected').val() == "Fixed Price") {
                fix_price = $(e).closest('.TrData').children().find('.round_value_fix_' + fix_pos).last().val();
            } else if ($('#<%= ddlTypeOfPurchase.ClientID %> option:selected').val() == "Floating Price") {
                fix_price = $(e).closest('.TrData').children().find('.round_value_floating_' + fix_pos).last().val();
            }
            if (!isNaN(parseFloat(fix_price))) {
                fix_price = parseFloat(parseFloat(fix_price).toFixed(4));
            }

            $(e).closest('.TrData').children("td:eq(" + _pos + ")").find('.final_price').val(fix_price); // set final price
            $('#<%= txtAwardPrice.ClientID %>').val(fix_price);
            if ("<% =EditPageMode.ToString().ToUpper() %>" == "TRUE") {
                $('#<%= txtAwardPricetmp.ClientID %>').val(fix_price);
            }
            //calculateTotalPrice(e);

            return fix_price;
        }

        function calculateTotalPrice(e) {
            var _pos = $(e).closest('.TrData').children('td').length - 5;
            var final_price = parseFloat($(e).closest('.TrData').children("td:eq(" + _pos + ")").find('.final_price').val());
            if (final_price != undefined && !isNaN(parseFloat(final_price))) {
                var surveyor_pos = $(e).closest('.TrData').children('td').length - 4;
                var surveyor_cost = $(e).closest('.TrData').children("td:eq(" + surveyor_pos + ")").find('.others_cost_surveyor').val();
                if (surveyor_cost != undefined && !isNaN(parseFloat(surveyor_cost))) {
                    surveyor_cost = parseFloat(parseFloat(surveyor_cost).toFixed(4));
                    var total_pos = $(e).closest('.TrData').children('td').length - 3;
                    $(e).closest('.TrData').children("td:eq(" + total_pos + ")").find('.total_price').val(final_price + surveyor_cost); // set total price

                    return final_price + surveyor_cost;
                }
            }
        }

        $(document).ready(function () {
            $("#tblNegotiation input[type='text']").change(function () {
                if (!isNaN(parseFloat($(this).val()))) {
                    $(this).val(parseFloat(parseFloat($(this).val()).toFixed(4)));
                }
            });

            $('#<%= txtAwardPricetmp.ClientID %>').keydown(function () { return false; });
             $('#<%= txtAwardTotmp.ClientID %>').keydown(function(){return false;});
            LoadAffterPostBack();
            LoadFileUpload();
            if ("<% =EditPageMode.ToString().ToUpper() %>" != "TRUE") {
                $('#<%= btn_upload_MultiFile.ClientID %>').hide();
                $('#<%= btn_upload_MultiFile.ClientID %>').attr('style', 'visibility: hidden');
                $('#<%= btn_Explanupload_MultiFile.ClientID %>').hide();
                $('#<%= btn_Explanupload_MultiFile.ClientID %>').attr('style', 'visibility: hidden');                
                $('#txt_Explanupload_MultiFile').hide(); 
                $('#txtupload_MultiFile').hide();
                $('#txtAttach_File').hide();
            }


        });

        function MakeUploadTag(fileName) {
            var i = 0, length = fileName.length;
            var TmpFilename = fileName;
            for (i; i < length; i++) {
                TmpFilename = TmpFilename.replace("/", "|").replace("\\", "|")
            }
            var spltVal = TmpFilename.split("|");
            //alert(spltVal[spltVal.length - 1]);
            var htmlTag='<div id=\"'+fileName+'\" class=\"uploadifyQueue\" style width=\"450px\">';
            htmlTag += '<div class=\"uploadifyQueueItem FileAttach\" value=\"' + fileName + '\">';
            if ("<% =EditPageMode.ToString().ToUpper() %>" == "TRUE") {
                htmlTag += '<div class="cancel">';
                htmlTag += '<span class="glyphicon glyphicon-remove removeAttach"></span>';
                htmlTag += '</div>';
            }
            htmlTag += '<a target=\"_blank\" href=\"../' + fileName + '\">' + spltVal[spltVal.length - 1] + '</a>';
            htmlTag+='</div>';
            htmlTag += '</div>';           
            return htmlTag;
        }
        function MakeUploadTagExplan(fileName, MSG) {
            var i = 0, length = fileName.length;
            var TmpFilename = fileName;
            for (i; i < length; i++) {
                TmpFilename = TmpFilename.replace("/", "|").replace("\\", "|")
            }
            var spltVal = TmpFilename.split("|");
            var htmlTag = '<div id=\"' + fileName + '\" class=\"uploadifyQueue\" style width=\"450px\">';
            htmlTag += '<table><tr><td>';
            htmlTag += '<div class=\"uploadifyQueueItem ExplanAttach\" value=\"' + fileName + '\">';
            if ("<% =EditPageMode.ToString().ToUpper() %>" == "TRUE") {
                htmlTag += '<div class="cancel">';
                htmlTag += '<span class="glyphicon glyphicon-remove removeExplanAttach"></span>';
                htmlTag += '</div>';
            }
            htmlTag += '<a target=\"_blank\" href=\"../' + fileName + '\">' + spltVal[spltVal.length - 1] + '</a>';
            htmlTag+='</div>';
            htmlTag += '</div>';
            htmlTag += '</td><td width="10px;"></td><td vertical-align: middle;>';
            htmlTag += '<input class="form-control txtUpload" style="width:400px; vertical-align: middle;" value=\"' + MSG + '\" id="txtUpload" ></input>';
            htmlTag += '</td><tr><table>';
            return htmlTag;
        }

        function CheckLengthCtrl() {
            CheckLength('<%= txtTripNo.ClientID %>', '50');
            CheckLength('<%= txtVoyage.ClientID %>', '100');
            CheckLength('<%= txtProductVolume.ClientID %>', '20');
            $('.ValTableControl').each(function () {
                CheckLength(this.id, '50');
            });
            CheckLength('<%= txtPaymentTermDays.ClientID %>', '100');
            CheckLength('<%= txtReason.ClientID %>', '50');
            CheckLength('<%= txtSupplyingLocation.ClientID %>', '100');
            CheckLength('<%= txtProductOthers.ClientID %>', '900');
            CheckLength('<%= txtTypeofpurchase.ClientID %>', '100');
            CheckDateFormat('<%= txtPurchasedDate.ClientID %>');
             CheckDateFormat('<%= txtDeliveryDaterange.ClientID %>');
        }

        function LoadAffterPostBack() {
            

            var DayTerm = $('#<%= hdfDayTerm.ClientID %>').val();
            if (DayTerm != "") {
                if (DayTerm == "DayZero") {
                    $('#rdoDayZero').prop("checked", true);
                }
                else if (DayTerm == "DayOne") {
                    $('#rdoDayOne').prop("checked", true);
                }
            }
            $('#' + $('#<%= hdfOfferFinalPrice.ClientID %>').val()).prop("checked", true);
            $('.rdoOfferList').click(function () {
                if ("<% =EditPageMode.ToString().ToUpper() %>" != "TRUE") {    
                    $('#' + $('#<%= hdfOfferFinalPrice.ClientID %>').val()).prop("checked", true);
                    return false;
                   
                }
            });
            $('.rdoDayTerm').click(function () {
                if ("<% =EditPageMode.ToString().ToUpper() %>" != "TRUE") {
                    return false;
                }
            });
            $('.rdoOfferList').change(function () {
                if ("<% =EditPageMode.ToString().ToUpper() %>" == "TRUE") {
                    calculateFinalPrice(this);
                    //calculateTotalPrice(this);
                    CheckSelectOferList()
                }
            });
            //LoadDateNote();
            $('.final_price').change(function () {
                if ($(this).closest('tr').children('td:eq(0)').find('.rdoOfferList').is(':checked')) {
                    CheckSelectOferList();
                }
            });
            $('.supplier').change(function () {
                if ($(this).closest('tr').children('td:eq(0)').find('.rdoOfferList').is(':checked')) {
                    CheckSelectOferList();
                }
            });

            $('input[type=text]').each(function () {
                $(this).on('keydown', function (e) {
                    if (e.which == 13) {
                        e.preventDefault();
                    }

                });
            });

            $(".numberCtrl").keypress(function (event) {
                return isNumber(event);
            });
            CheckLengthCtrl();

        }
        function SaveFileUpload() {
            $('#<%= hdfFileUpload.ClientID %>').val('');
            var FileVal = "";
            $('.FileAttach').each(function () {
                FileVal += $(this).attr('value') + "|";
            });
            $('#<%= hdfFileUpload.ClientID %>').val(FileVal);

            $('#<%= hdfExplanFileUpload.ClientID %>').val('');
            var FileValExplan = "";
            $('.ExplanAttach').each(function () {
                var txtDesc = $(this).closest('tr').find('.txtUpload').val();
                FileValExplan += $(this).attr('value') + ":" + txtDesc + "|";
            });
            $('#<%= hdfExplanFileUpload.ClientID %>').val(FileValExplan);
        }
        function LoadFileUpload() {
            $('.removeAttach').each(function () {
                $(this).parent().parent().parent().remove();
            });
            $('.removeExplanAttach').each(function () {
                $(this).parent().parent().parent().parent().remove();
            });

            if ($('#<%= hdfFileUpload.ClientID %>').val()) {
                var ItemFile = $('#<%= hdfFileUpload.ClientID %>').val().split("|");
                for (var i = 0; i < ItemFile.length; i++) {
                    if (ItemFile[i] != "") {
                        $('#filesUploaded').append(MakeUploadTag(ItemFile[i]));
                        $('#txtAttach_File').show();
                    }
                }
                DeleteFileEventSet();
            }

            if ($('#<%= hdfExplanFileUpload.ClientID %>').val()) {
                var ItemFile = $('#<%= hdfExplanFileUpload.ClientID %>').val().split("|");
                for (var i = 0; i < ItemFile.length; i++) {
                    if (ItemFile[i] != "") {
                        var splitVal = ItemFile[i].split(":")
                        $('#ExplanfilesUploaded').append(MakeUploadTagExplan(splitVal[0], splitVal[1]));
                    }
                }
                DeleteFileEventSetExplan();
            }

            
        }
        function DeleteFileEventSetExplan() {
            $('.glyphicon-remove').click(function () {
                $(this).parent().parent().parent().parent().remove();
            });
        }
        function DeleteFileEventSet() {
            $('.glyphicon-remove').click(function () {
                $(this).parent().parent().parent().remove();
            });
        }

        

        function CheckSelectOferList() {
            $('.rdoOfferList').each(function () {

                if ($(this).is(':checked')) {
                    $('#<%= hdfOfferFinalPrice.ClientID %>').val($(this).val());
                    //($(this).closest('tr').children('td:eq(1)').find('.supplier').find("option:selected").text());
                    if ($(this).closest('tr').children('td:eq(1)').find('.supplier').length > 0) {
                        var suppli = $(this).closest('tr').children('td:eq(1)').find('.supplier').find("option:selected").text()
                        var _pos = $(this).closest('tr').children('td').length - 5;
                        var strChild = "td:eq(" + _pos + ")";
                        var finalPri = $(this).closest('tr').children(strChild).find('.final_price').val();
                        $('#<%= txtAwardPrice.ClientID %>').val(finalPri);
                        $('#<%= txtAwardTo.ClientID %>').val(suppli);
                        if ("<% =EditPageMode.ToString().ToUpper() %>" == "TRUE") {
                            $('#<%= txtAwardPricetmp.ClientID %>').val(finalPri);
                            $('#<%= txtAwardTotmp.ClientID %>').val(suppli);
                        }
                    }
                    else {
                        $('#<%= txtAwardTo.ClientID %>').val($(this).closest('tr').children('td:eq(1)').text());
                        var _pos = $(this).closest('tr').children('td').length - 5;
                        var strChild = "td:eq(" + _pos + ")";
                        if ("<% =EditPageMode.ToString().ToUpper() %>" == "TRUE") {
                            $('#<%= txtAwardPrice.ClientID %>').val($(this).closest('tr').children(strChild).text());
                            $('#<%= txtAwardPricetmp.ClientID %>').val($(this).closest('tr').children(strChild).text());
                        }
                    }
                }
            });
        }

        if ("<% =EditPageMode.ToString().ToUpper() %>" == "TRUE" || "<% = StatusPageMode.ToString().ToUpper() %>" == "VERIFIED") {
            $('#<%= txtPurchasedDate.ClientID %>').dateRangePicker(
                {

                    autoClose: true,
                    singleDate: true,
                    showShortcuts: false,
                    format: 'DD/MM/YYYY'
                });


            $('#<%= txtDeliveryDaterange.ClientID %>').dateRangePicker(
            {
                autoClose: true,
                singleDate: true,
                startOfWeek: 'monday',
                format: "DD/MM/YYYY"
            });

            tinymce.init({
                selector: '#<%= txtExplanation.ClientID %>',
                theme: 'modern',
                plugins: ['textcolor colorpicker  '],
                height: 200,
                menubar: false,
                toolbar: " undo redo | bold italic underline | numlist | forecolor | removeformat | spellchecker",
                statusbar: true,
                image_advtab: true,
                paste_data_images: true,
                textcolor_map: [
										"000000", "Black",
										"993300", "Burnt orange",
										"333300", "Dark olive",
										"003300", "Dark green",
										"003366", "Dark azure",
										"000080", "Navy Blue",
										"333399", "Indigo",
										"333333", "Very dark gray",
										"800000", "Maroon",
										"FF6600", "Orange",
										"808000", "Olive",
										"008000", "Green",
										"008080", "Teal",
										"0000FF", "Blue",
										"666699", "Grayish blue",
										"808080", "Gray",
										"FF0000", "Red",
										"FF9900", "Amber",
										"99CC00", "Yellow green",
										"339966", "Sea green",
										"33CCCC", "Turquoise",
										"3366FF", "Royal blue",
										"800080", "Purple",
										"999999", "Medium gray",
										"FF00FF", "Magenta",
										"FFCC00", "Gold",
										"FFFF00", "Yellow",
										"00FF00", "Lime",
										"00FFFF", "Aqua",
										"00CCFF", "Sky blue",
										"993366", "Red violet",
										"FFFFFF", "White",
										"FF99CC", "Pink",
										"FFCC99", "Peach",
										"FFFF99", "Light yellow",
										"CCFFCC", "Pale green",
										"CCFFFF", "Pale cyan",
										"99CCFF", "Light sky blue",
										"CC99FF", "Plum"
                ]
            });       
        } else {
            tinymce.init({
                selector: '#<%= txtExplanation.ClientID %>',
                height: 200,
                menubar: false,
                readonly: 1
            });
        }
        function SetClieckRefresh() {
            $('#<%= btnrefresh.ClientID %>').click();


        }
       

        


        function ReMoveValidateClass() {
           
            $("#<%=txtPurchasedDate.ClientID%>").removeClass("input-error");
            $("#<%=txtDeliveryDaterange.ClientID%>").removeClass("input-error");
            $("#<%=ddlVassel.ClientID%>").removeClass("input-error");
            $("#<%=txtVassel.ClientID%>").removeClass("input-error");
            $("#<%=txtVoyage.ClientID%>").removeClass("input-error");
            $("#<%=ddlTypeOfPurchase.ClientID%>").removeClass("input-error");
            $("#<%=txtTypeofpurchase.ClientID%>").removeClass("input-error");
            $("#<%=ddlSupplyingLocation.ClientID%>").removeClass("input-error");
            $("#<%=txtSupplyingLocation.ClientID%>").removeClass("input-error");
            $("#<%=ddlProduct.ClientID%>").removeClass("input-error");
            $("#<%=txtProductVolume.ClientID%>").removeClass("input-error");
            $("#<%=ddlContractType.ClientID%>").removeClass("input-error");
            $("#<%=txtAnyOtherSpecial.ClientID%>").removeClass("input-error");
            $("#<%=txtExplanation.ClientID%>").removeClass("input-error");
            $("#<%=ddlPaymentTerm.ClientID%>").removeClass("input-error");
            $("#<%=txtAwardTotmp.ClientID%>").removeClass("input-error");
            $("#<%=txtAwardPrice.ClientID%>").removeClass("input-error");
            $("#<%=txtAwardPricetmp.ClientID%>").removeClass("input-error");
            $("#<%=txtReason.ClientID%>").removeClass("input-error");
            $("#<%=txtNote.ClientID%>").removeClass("input-error");
            $("#<%=txtTripNo.ClientID%>").removeClass("input-error");
        }

        function ValidateBeforeSave() {
            ReMoveValidateClass();
            var FocusCtrl = "";
            var KeyOtherddlCheck = "Others";
            var i = 0;
            var MSGError = "";
            //CheckSuppliror
            var HaveDup = false; var count = 0;
            $('.supplier').each(function () {
                var Sup = $(this).find("option:selected").text();
                //console.log("Sup : " + Sup);
                count = 0;
                $('.supplier').each(function () {
                    //console.log("Sup selected : " + count.toString() +":" + $(this).find("option:selected").text());
                    if (Sup == $(this).find("option:selected").text()) {
                        //console.log("Done : " + count.toString());
                        count++;                        
                    }

                    //console.log("count : " + count.toString());
                    if (count > 1) { HaveDup = true; }
                });
            });
            if (HaveDup) {
                i++;
                if (FocusCtrl == "") FocusCtrl = '#htmlTag';
                if (MSGError == "") MSGError = "ไม่สามารถบันทึก Supplier ซ้ำกันได้";
            }

            if ($('.TrData').length <= 0) {
                i++;
                if (FocusCtrl == "") FocusCtrl = '#htmlTag';
                if (MSGError == "") MSGError = "ไม่สามารถบันทึกโดยไม่มี Offer ได้";
            }
            var CheckFinalPrice = false;
            $('.rdoOfferList').each(function () {
                if ($(this).is(':checked')) { CheckFinalPrice = true; }
            });
            if (!CheckFinalPrice) {
                i++;
                if (FocusCtrl == "") FocusCtrl = '#htmlTag';
                if (MSGError == "") MSGError = "ไม่สามารถบันทึก โดยไม่มี Final Price ได้";
            }
            //txtPurchasedDate
            if ($("#<%=txtPurchasedDate.ClientID %>").val() + "" == "") {i++;
                addValidMessageError($('.txtPurchasedDate'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtPurchasedDate.ClientID%>';
            }
            //txtDeliveryDaterange
             if ($("#<%=txtDeliveryDaterange.ClientID %>").val() + "" == "") {i++;
                 addValidMessageError($('.txtDeliveryDaterange'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtDeliveryDaterange.ClientID%>';
             }
            //ddlVassel
            if ($("#<%=ddlVassel.ClientID %>").find("option:selected").text() + "" == "") {
                i++;
                addValidMessageError($('.ddlVassel'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=ddlVassel.ClientID%>';
            }
            //txtVassel
            if ($("#<%=ddlVassel.ClientID %>").find("option:selected").text() + "" == KeyOtherddlCheck &&
                $("#<%=txtVassel.ClientID %>").val() + "" == "") {
                i++;
                addValidMessageError($('.txtVassel'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtVassel.ClientID%>';
            }
            //txtVoyage
            if ($("#<%=txtVoyage.ClientID %>").val() + "" == "") {i++;
                addValidMessageError($('.txtVoyage'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtVoyage.ClientID%>';
            }
            if ($("#<%=txtTripNo.ClientID %>").val() + "" == "TC") {i++;
                 addValidMessageError($('.txtTripNo'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtTripNo.ClientID%>';
             }
            //ddlTypeOfPurchase
            if ($("#<%=ddlTypeOfPurchase.ClientID %>").find("option:selected").text() + "" == "") {
                i++;
                addValidMessageError($('.ddlTypeOfPurchase'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=ddlTypeOfPurchase.ClientID%>';
            }
            //txtTypeofpurchase
            if ($("#<%=ddlTypeOfPurchase.ClientID %>").find("option:selected").text() + "" == KeyOtherddlCheck &&
                $("#<%=txtTypeofpurchase.ClientID %>").val() + "" == "") {
                i++;
                addValidMessageError($('.txtTypeofpurchase'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtTypeofpurchase.ClientID%>';
            }
            //ddlSupplyingLocation
            if ($("#<%=ddlSupplyingLocation.ClientID %>").find("option:selected").text() + "" == "") {
                i++;
                addValidMessageError($('.ddlSupplyingLocation'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=ddlSupplyingLocation.ClientID%>';
            }
            //txtSupplyingLocation
            if ($("#<%=ddlSupplyingLocation.ClientID %>").find("option:selected").text() + "" == KeyOtherddlCheck &&
                $("#<%=txtSupplyingLocation.ClientID %>").val() + "" == "") {
                i++;
                addValidMessageError($('.txtSupplyingLocation'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtSupplyingLocation.ClientID%>';
            }
            //ddlProduct
            if ($("#<%=ddlProduct.ClientID %>").find("option:selected").text() + "" == "") {
                i++;
                addValidMessageError($('.ddlProduct'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=ddlProduct.ClientID%>';
            }
            //txtProductVolume
            if ($("#<%=ddlProduct.ClientID %>").find("option:selected").text() + "" == KeyOtherddlCheck &&
                $("#<%=txtProductOthers.ClientID %>").val() + "" == "") {
                i++;
                addValidMessageError($('.txtProductOthers'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtProductOthers.ClientID%>';
            }
             //txtProductVolume
            if ( $("#<%=txtProductVolume.ClientID %>").val() + "" == "") {
                i++;
                addValidMessageError($('.txtProductVolume'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtProductVolume.ClientID%>';
            }
            //ddlContractType
            if ($("#<%=ddlContractType.ClientID %>").find("option:selected").text() + "" == "") {
                i++;
                addValidMessageError($('.ddlContractType'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=ddlContractType.ClientID%>';
            }
           
            //ddlPaymentTerm
            if ($("#<%=ddlPaymentTerm.ClientID %>").find("option:selected").text() + "" == "") {
                i++;
                addValidMessageError($('.ddlPaymentTerm'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=ddlPaymentTerm.ClientID%>';
            }
            //txtAwardTo
            if ($("#<%=txtAwardTotmp.ClientID %>").val() + "" == "") {i++;
                addValidMessageError($('.txtAwardTotmp'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=txtAwardTotmp.ClientID%>';
            }
            //txtAwardPrice
            if ($("#<%=txtAwardPricetmp.ClientID %>").val() + "" == "" || $("#<%=txtAwardPricetmp.ClientID %>").val() + "" == "0") {
                i++;
                addValidMessageError($('.final_price'), "Please input Value.");
                if ( $("#<%=txtAwardPricetmp.ClientID %>").val() + "" == "0") {
                    OpenBoxPopup("Award Price ไม่ถูกต้อง");
                }
                $('.final_price').each(function () {
                    if (FocusCtrl == "") FocusCtrl = '#'+this.id;
                })
                
            }
            //ddlReason
            if ($("#<%=ddlReason.ClientID %>").find("option:selected").text() + "" == "") {
                i++;
                addValidMessageError($('.ddlReason'), "Please input Value.");
                if (FocusCtrl == "") FocusCtrl = '#<%=ddlReason.ClientID%>';
            }
           
            if ($('#<%= ddlPaymentTerm.ClientID %> option:selected').text() == "Others") {
                if ($('#<%=txtPaymentTermDays.ClientID%>').val() == "") {
                    i++;
                    addValidMessageError($('.txtPaymentTermDays'), "Please input Value.");
                     if (FocusCtrl == "") FocusCtrl = '#<%=txtPaymentTermDays.ClientID%>';
                }                
            }
            else if ($('#<%= ddlPaymentTerm.ClientID %> option:selected').text().indexOf("payment") > 0) {                
            }
            else {
                if ($('#<%=txtPaymentTermDays.ClientID%>').val() == "") {
                    i++;
                    addValidMessageError($('.txtPaymentTermDays'), "Please input Value.");
                     if (FocusCtrl == "") FocusCtrl = '#<%=txtPaymentTermDays.ClientID%>';
                }
                
                if ($('#<%=hdfDayTerm.ClientID%>').val() == "") {
                    i++;
                    addValidMessageError($('.txtPaymentTermDays'), "Please input Value.");
                     if (FocusCtrl == "") FocusCtrl = '#<%=txtPaymentTermDays.ClientID%>';
                }
            }


            if (FocusCtrl != "") $(FocusCtrl).focus();
            if (i > 0) {
                if (MSGError != "") { OpenBoxPopup(MSGError); }
                return false;
            }
            return true;
        }
    </script>
    <script type="text/javascript">

        var type_ajax = '';
        var pgname_ajax = '';

        function addAttachFile(type, pgname) {
            type_ajax = type;
            pgname_ajax = pgname;
            $('#<%= upload_MultiFile.ClientID %>').click();
        }

        function addExplanAttachFile(type, pgname) {
            type_ajax = type;
            pgname_ajax = pgname;
            $('#<%= Explanupload_MultiFile.ClientID %>').click();
        }

        $('#<%= upload_MultiFile.ClientID %>').change(function (e) {
            var files = e.target.files;
            if (files.length > 0) {
                if (window.FormData !== undefined) {
                    for (var x = 0; x < files.length; x++) {
                        var data = new FormData();
                        data.append("file" + x, files[x]);
                        $.ajax({
                            type: "POST",
                            url: "../Web/Api/UploadFileAPI.aspx?Type=" + type_ajax + "|" + pgname_ajax,
                            contentType: false,
                            processData: false,
                            data: data,
                            success: function (response) {
                                var index = response.indexOf('<!DOCTYPE html>');
                                var json = response.substring(0, index);
                                var data = JSON.parse(json.trim());
                                $('#filesUploaded').append(MakeUploadTag(data.file_path));
                                DeleteFileEventSet();
                            },
                            error: function (event, queueID, fileObj, errorObj) {
                                alert(errorObj.type + ' Error: ' + errorObj.info);
                                if (fileObj.size > 4194304) { alert('File size too large.'); }
                                else { alert('Someting wrong.'); }
                            }
                        });
                        data = null;
                    }
                } else {
                    alert("This browser doesn't support HTML5 file uploads!");
                }
            }
            $(this).val('');
        });

        $('#<%= Explanupload_MultiFile.ClientID %>').change(function (e) {
            var files = e.target.files;
            if (files.length > 0) {
                if (window.FormData !== undefined) {
                    for (var x = 0; x < files.length; x++) {
                        var data = new FormData();
                        data.append("file" + x, files[x]);
                        $.ajax({
                            type: "POST",
                            url: "../Web/Api/UploadFileAPI.aspx?Type=" + type_ajax + "|" + pgname_ajax,
                            contentType: false,
                            processData: false,
                            data: data,
                            success: function (response) {
                                var index = response.indexOf('<!DOCTYPE html>');
                                var json = response.substring(0, index);
                                var data = JSON.parse(json.trim());
                                $('#ExplanfilesUploaded').append(MakeUploadTagExplan(data.file_path, ''));
                                DeleteFileEventSetExplan();
                            },
                            error: function (event, queueID, fileObj, errorObj) {
                                alert(errorObj.type + ' Error: ' + errorObj.info);
                                if (fileObj.size > 4194304) { alert('File size too large.'); }
                                else { alert('Someting wrong.'); }
                            }
                        });
                        data = null;
                    }
                } else {
                    alert("This browser doesn't support HTML5 file uploads!");
                }
            }
            $(this).val('');
        });
    </script>
</asp:Content>
