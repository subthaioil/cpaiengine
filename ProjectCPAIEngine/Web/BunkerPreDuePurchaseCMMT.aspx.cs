﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text;
using System.IO;
using System.Linq;
using ProjectCPAIEngine.DAL.Entity;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using System.Web.Script.Serialization;
using com.pttict.engine.utility;
using System.Net;
using ProjectCPAIEngine.DAL.DALMaster;
using System.Web.Services;
using System.Web.Script.Services;
using System.Threading;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.Web
{
    public partial class BunkerPreDuePurchaseCMMT : System.Web.UI.Page
    {
        public static DataTable dtVendor = new DataTable();
        public Setting setting
        {
            get
            {
                object o = Session["settingCMMT"];
                if (o == null)
                {
                    Setting s = new Setting();
                    return s;
                }
                else
                {
                    return (Setting)o;
                }

            }
            set
            {
                Session["settingCMMT"] = value;
            }

        }
        public List<OffersItemTmp> LstOffersItemDataSS
        {
            get
            {
                if (Session["LstOffersItemDataSSCMMT"] == null) return new List<OffersItemTmp>();
                else return (List<OffersItemTmp>)Session["LstOffersItemDataSSCMMT"];
            }
            set { Session["LstOffersItemDataSSCMMT"] = value; }
        }
        public List<Product> ProductSS
        {
            get
            {
                if (Session["ProductSSCMMT"] == null) return new List<Product>();
                else return (List<Product>)Session["ProductSSCMMT"];
            }
            set { Session["ProductSSCMMT"] = value; }
        }
        public StringBuilder HTMLTable = new StringBuilder();
        public StringBuilder HTMLProduct = new StringBuilder();
        public BunkerPurchase BunkerPurchaseSS//For PDF Report
        {
            get
            {
                if (Session["BunkerPurchaseSSCMMT"] == null) return new BunkerPurchase();
                else return (BunkerPurchase)Session["BunkerPurchaseSSCMMT"];
            }
            set { Session["BunkerPurchaseSSCMMT"] = value; }
        }
        public BunkerPurchase BunkerPurchaseTmp//For PDF Report
        {
            get
            {
                if (Session["BunkerPurchaseTmp"] == null) return new BunkerPurchase();
                else return (BunkerPurchase)Session["BunkerPurchaseTmp"];
            }
            set { Session["BunkerPurchaseTmp"] = value; }
        }
        public List<ButtonAction> ButtonListBunker
        {
            get
            {
                if (Session["ButtonListBunkerCMMT"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListBunkerCMMT"];
            }
            set { Session["ButtonListBunkerCMMT"] = value; }
        }
        ShareFn _FN = new ShareFn();
        Bunger _bungercls = new Bunger();
        GridMode _Mode = GridMode.Read;
        public bool EditPageMode = true;
        public string StatusPageMode = "";
        private string ReturnPage = "MainBoards.aspx";//"BunkerSearchList.aspx?Type=D9xSF83RP4U=";
        protected void Page_Load(object sender, EventArgs e)
        {
            string Class = this.GetType().Name.ToString();

            if (!Page.IsPostBack)
            {
                ButtonListBunker = null;
                LstOffersItemDataSS = null;
                ProductSS = null;
                BunkerPurchaseSS = null;
                LoadMaster();

                if (Request.QueryString["TranID"] != null)
                {
                    string TransID = Request.QueryString["TranID"].ToString();
                    //TransID = TransID.Encrypt();
                    LoadDataBunker(TransID.Decrypt());
                }
                else
                {
                    hdfDayTerm.Value = "DayOne";
                    txtTaskID.Text = "Draft";
                    ddlPaymentTerm.SelectedIndex = 1;
                    txtPaymentTermDays.Value = "30";
                    txtPurchasedDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                }


                LoadHTML();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CheckCtrl", "SetEventCtrl();", true);

        }


        #region---------------------------Make HTML Offer---------------------------

        private List<OffersItemTmp> MakeOffer()
        {
            List<OffersItemTmp> _lst = new List<OffersItemTmp>();
            OffersItemTmp _item = new OffersItemTmp();
            _item.KeyItem = ConstantPrm.EnginGetEngineID();
            _item.supplier = "";
            _item.contact_person = "";

            _item.purchasing_term = "Delivered";
            _item.others_cost_barge = "";
            _item.others_cost_surveyor = "";
            _item.final_price = "";
            _item.total_price = "";
            _item.round_items = new List<RoundItem>();

            RoundItem _itemD = new RoundItem();
            _itemD.round_no = "1";
            _itemD.round_info = new List<RoundInfo>();
            for (int i = 0; i < ProductSS.Count(); i++)
            {
                List<RoundValues> _lstrItemFix = new List<RoundValues>(); _lstrItemFix.Add(new RoundValues());
                _itemD.round_info.Add(new RoundInfo { round_type = string.Format("PRODUCT{0}", i + 1), round_value = _lstrItemFix });
            }

            _item.round_items.Add(_itemD);

            List<Quantity> LstQ = new List<Quantity>();
            for (int i = 0; i < ProductSS.Count(); i++)
            {
                Quantity _itemQ = new Quantity();
                _item.quantitys = new List<Quantity>();
                _itemQ.quantity_value = new List<string>();
                _itemQ.quantity_type = string.Format("PRODUCT{0}", i + 1);
                List<string> _lstrItem = new List<string>(); _lstrItem.Add("");
                _itemQ.quantity_value = _lstrItem;
                LstQ.Add(_itemQ);
            }
            _item.quantitys = LstQ;
            _lst.Add(_item);

            return _lst;
        }
        private string LoadHTML()
        {
            LstOffersItemDataSS = ReCheckProduct(LstOffersItemDataSS);
            StringBuilder HTMLResult = new StringBuilder();
            int NoofOffer = 1;
            string DateNote = "";
            GenHTMLProduct();
            int PSSCnt = (ProductSS != null && ProductSS.Count() > 0) ? ProductSS.Count() : 1;
            StringBuilder HTMLBody = MakePriceOfferHTMLData(ref NoofOffer, ref DateNote, LstOffersItemDataSS);

            //++++++++++++++Head Session Line No 1++++++++++++++
            HTMLResult.Append("<tr class=\"HeaderTable\"><td rowspan=\"3\">select</td>" +
                            "<td rowspan=\"3\">Supplier</td>" +
                            "<td rowspan=\"3\">Contact Person</td>" +
                            "<td style=\"text-align:center;\" colspan=\"" + (3 + (NoofOffer * PSSCnt)).ToString() + "\">Offered Price or Formula ( <label class=\"lbCurrncy\">$</label>/MT )</td>" +
                            "<td rowspan=\"3\" style=\"width: 100px;\">Total Cost (<label class=\"lbCurrncy\"> $ </label>)</td>" +
                            //"<td rowspan=\"3\" style=\"width: 170px;\">Benefit vs</br> Second low</td>" +
                            ((EditPageMode) ? "<td rowspan=\"3\">Delete</td>" : "") + "</tr>");

            //++++++++++++++Head Session Line No 2++++++++++++++
            HTMLResult.Append("<tr class=\"HeaderTable\"><td rowspan=\"2\">Purchasing Term</td>");
            string btndeleteoffer = "";
            for (int i = 0; i < NoofOffer; i++)
            {
                if (NoofOffer > 1 && i == (NoofOffer - 1)) btndeleteoffer = string.Format(" <input type=\"image\" onclick=\"CallItemEvent('{0}')\" value=\"{0}\" src=\"images/icon-delete.gif\" />", "DELETEOFFER");

                HTMLResult.Append(string.Format("<td colspan=\"" + ((PSSCnt > 0) ? PSSCnt.ToString() : "1") + "\" class=\"{1}\" >{0} Offer/Negotiation {2}</td>", _FN.GetORDINALNUMBERS((i + 1)), "roundOffer", btndeleteoffer));
            }
            HTMLResult.Append("<td colspan=\"2\">Other Costs (<label class=\"lbCurrncy\"> $ </label>)</td> </tr>");

            //++++++++++++++Head Session Line No 3++++++++++++++
            HTMLResult.Append("<tr class=\"HeaderTable\">");

            for (int i = 0; i < NoofOffer; i++)
            {
                if (PSSCnt > 0)
                {
                    foreach (var Product in ProductSS)
                    {
                        HTMLResult.Append("<td style=\"width:100px;\">" + _FN.GetProductShortName(Product.product_name, setting.products) + "</td>");
                    }
                }
                else
                {
                    HTMLResult.Append("<td style=\"width:100px;\"></td>");
                }
            }
            if (ProductSS != null && ProductSS.Count > 0)
            {
                HTMLResult.Append("<td style=\"width:70px;\">Barge</td>");
                HTMLResult.Append("<td style=\"width:70px;\">Bunker Survey</td>");
            }
            HTMLResult.Append(" </tr>");
            //HTMLResult += HTMLBody;
            if (HTMLTable != null) HTMLTable.Clear();
            HTMLTable.Append("<table id=\"tblNegotiation\" class=\"table table-bordered PriceOfferTable\" width=\"100 % \" >");
            HTMLTable.Append(HTMLResult);
            HTMLTable.Append(HTMLBody);
            HTMLTable.Append(" </table>");
            return HTMLTable.ToString();
        }
        private StringBuilder MakePriceOfferHTMLData(ref int NoOfferRoundItem, ref string DateNote, List<OffersItemTmp> _lstPriceOffer)
        {
            StringBuilder HTMLData = new StringBuilder();

            List<OffersItemTmp> lstPriceOffer = _bungercls.CloneBungerPropertiesLst(_lstPriceOffer);
            if (lstPriceOffer.Count <= 0)
            {
                NoOfferRoundItem = 1;
                HTMLData.Append("<tr><td colspan=\"12\"  style=\"text-align:center;\" >No Data</td></tr>");
                return HTMLData;
            }
            else
            {
                //MapData Edit


                int MaxRoundItem = lstPriceOffer.Max(x => x.round_items.Count);
                int MaxQuantity = lstPriceOffer.Max(x => x.quantitys.Count);
                foreach (OffersItemTmp _item in lstPriceOffer)
                {
                    if (DateNote == "") DateNote = _item.pricebasedate;
                    _Mode = GridMode.Read;
                    if (EditPageMode) _Mode = GridMode.Edit;


                    HTMLData.Append("<tr class=\"TrData\"><td>");
                    HTMLData.Append(string.Format("<div class=\"radio\"><input type=\"radio\" class=\"rdoOfferList\" value={0} {1} id=\"{0}\" name=\"rdoffer\"> <label for=\"{0}\"></label></div>", _item.KeyItem, (_item.final_flag == "Y") ? "checked" : ""));
                    HTMLData.Append("</td>  ");
                    if (_Mode == GridMode.Edit)
                        HTMLData.Append("<td>" + _FN.GenControlDropdown(setting.vendor.OrderBy(x => x.VND_NAME1).ToList(), "VND_NAME1", "VND_ACC_NUM_VENDOR", _item.supplier, "supplier", _Mode) + "</td>  ");
                    else
                    {
                        if (setting.vendor.Where(x => x.VND_ACC_NUM_VENDOR == _item.supplier).FirstOrDefault() != null)
                        {
                            HTMLData.Append("<td>" + _FN.GenControlDropdown(setting.vendor.OrderBy(x => x.VND_NAME1).ToList(), "VND_NAME1", "VND_ACC_NUM_VENDOR", _item.supplier, "supplier", _Mode) + "</td>  ");
                        }
                        else
                        {
                            HTMLData.Append("<td>" + _FN.GenControlGrid(TypeCcontrol.TextBox, _item.supplier, "supplier", _Mode) + "</td>  ");
                        }
                    }
                    HTMLData.Append("<td>" + _FN.GenControlGrid(TypeCcontrol.TextBox, _item.contact_person, "contact_person ValTableControl", _Mode, "100") + "</td>  ");
                    HTMLData.Append("<td>" + _FN.GenControlGrid(TypeCcontrol.TextBox, _item.purchasing_term, "ValTableControl purchasing_term", _Mode) + "</td>  ");

                    //+++++++++++++++++++++++++++Gen RoundItem Detail++++++++++++++++++++++++++++++++++++++++++++++++++
                    //Gen Offer RoundItem
                    foreach (RoundItem _detail in _item.round_items)
                    {
                        if (ProductSS.Count() > 0)
                        {
                            for (int ip = 1; ip < ProductSS.Count() + 1; ip++)
                            {
                                HTMLData.Append("<td style=\"width:100px;\">" + MakeTableRoundItem(string.Format("PRODUCT{0}", ip), _detail, _item.KeyItem) + "</td>  ");
                            }
                        }
                        else
                        {
                            HTMLData.Append("<td>" + MakeTableRoundItem(string.Format("PRODUCT1"), _detail, _item.KeyItem) + "</td>  ");
                        }
                        // "<td>" + MakeTableRoundItem("PRODUCT2", _detail, _item.KeyItem) + "</td>  ");
                    }
                    //Check And Gen Emtry Offer RoundItem
                    if (MaxRoundItem > _item.round_items.Count)
                    {
                        for (int i = 0; i < (MaxRoundItem - _item.round_items.Count); i++)
                        {
                            HTMLData.Append("<td></td><td></td>  ");
                        }
                    }
                    //+++++++++++++++++++++++++++++++++End++++++++++++++++++++++++++++++++++++++++++++
                    HTMLData.Append("<td>" + _FN.GenControlGrid(TypeCcontrol.TextBox, _item.others_cost_barge, "ValTableControl others_cost_barge", _Mode, "100") + "</td>  " +
                             "<td>" + _FN.GenControlGrid(TypeCcontrol.TextBox, _item.others_cost_surveyor, "ValTableControl others_cost_surveyor", _Mode, "100") + "</td>  " +
                             "<td>" + _FN.GenControlGrid(TypeCcontrol.TextBox, _item.final_price, "ValTableControl final_price", _Mode, "100") + "</td> ");
                    //td>" + _FN.GenControlGrid(TypeCcontrol.TextBox, _item.note, "note", _Mode, "170") + "</td> ");
                    if (EditPageMode)
                    {
                        HTMLData.Append("<td>");
                        HTMLData.Append(string.Format(" <input type=\"image\" onclick=\"CallOfferMethod('{0}','delete')\" value=\"{0}\" src=\"images/icon-delete.gif\" />", _item.KeyItem));
                        HTMLData.Append("</td>");
                    }

                    HTMLData.Append("</tr>");
                }
                NoOfferRoundItem = MaxRoundItem;
                return HTMLData;
            }
        }
        private string MakeTableRoundItem(string type, RoundItem _rndItem, string KeyItem)
        {
            string _htmlRet = "";
            var _lstR = _rndItem.round_info.Where(x => x.round_type == type).ToList();
            foreach (RoundInfo _item in _lstR)
            {
                foreach (RoundValues _str in _item.round_value)
                {
                    _htmlRet += "<tr><td class=\"roundoffer_value\" style =\"border: none;\">" + _FN.GenControlGrid(TypeCcontrol.TextBox, _str.value, "ValTableControl round_value_" + type + "_" + _rndItem.round_no, _Mode, "120") + "</td></tr>";
                }
            }
            if (_Mode == GridMode.Edit)
            {
                _htmlRet += string.Format("<tr><td style=\"text-align:center;border: none;\"><input type=\"image\" onclick=\"CallItemEvent('AddRound|{0}|{1}!{2}')\" value=\"{0}\" src=\"images/btn-add-member.png\" />"
                        + "<input type=\"image\" onclick=\"CallItemEvent('DeleteRound|{0}|{1}!{2}')\" value=\"{0}\" src=\"images/btn-delete.png\" /></td></tr>", KeyItem, type, _rndItem.round_no);
            }
            return "<table  width=\"100%\">" + _htmlRet + "</table>";
        }
        private string MakeTableQunatity(Quantity Qunality, string KeyItem)
        {
            string _htmlRet = "";
            foreach (string _item in Qunality.quantity_value)
            {
                _htmlRet += "<tr><td style=\"border: none;\">" + _FN.GenControlGrid(TypeCcontrol.TextBox, _item, "quantity_value_" + Qunality.quantity_type, _Mode, "120") + "</td></tr>";
            }
            if (_Mode == GridMode.Edit)
            {
                _htmlRet += string.Format("<tr><td style=\"text-align:center;border: none;\"><input type=\"image\" onclick=\"CallItemEvent('AddQuan|{0}|{1}')\" value=\"{0}\" src=\"images/btn-add-member.png\" />"
                            + "<input  type=\"image\" onclick=\"CallItemEvent('DeleteQuan|{0}|{1}')\" value=\"{0}\" src=\"images/btn-delete.png\" /></td></tr>", KeyItem, "QUANTITY_" + Qunality.quantity_type);
            }
            return "<table width=\"100%\" >" + _htmlRet + "</table>";
        }

        private void GenHTMLProduct()
        {
            StringBuilder _htmlProduct = new StringBuilder();
            if (setting.product_name != null)
            {
                string CheckItem = "";
                string CheckQty = "";
                string CheckSpec = "";
                string CheckGroup = "";
                string CheckTextOther = "";
                List<string> tmpProduct = new List<string>();
                for (int i = 0; i < setting.products.Count; i++)
                {
                    CheckItem = "";
                    CheckQty = "";
                    CheckSpec = "";
                    CheckTextOther = "";
                    CheckGroup = "";
                    if (setting.products[i].full_name.ToUpper().IndexOf("GAS") >= 0) { CheckGroup = "GAS"; }
                    else if (setting.products[i].full_name.ToUpper().IndexOf("FUEL") >= 0) { CheckGroup = "FUEL"; }
                    else CheckGroup = "OTHERS";
                    if (BunkerPurchaseSS.product != null)
                    {
                        var Item = BunkerPurchaseSS.product.Where(x => x.product_name == setting.products[i].full_name).ToList();
                        if (Item.Count > 0)
                        {
                            CheckItem = "checked"; CheckQty = Item[0].product_qty; CheckSpec = Item[0].product_spec;
                            tmpProduct.Add(Item[0].product_name);
                        }
                        else
                        {
                            if (BunkerPurchaseSS.product != null && setting.products[i].full_name.ToUpper() == "OTHERS")
                            {
                                foreach (var _item in BunkerPurchaseSS.product)
                                {
                                    var tmpCnt = tmpProduct.Where(x => x == _item.product_name).ToList();
                                    if (tmpCnt == null || tmpCnt.Count <= 0)
                                    {
                                        CheckItem = "checked"; CheckQty = _item.product_qty; CheckSpec = _item.product_spec;
                                        CheckTextOther = _item.product_name;
                                    }
                                }
                            }
                        }

                    }
                    else if (string.IsNullOrEmpty(CheckSpec)) CheckSpec = setting.products[i].spac_name;
                    if (setting.product_name[i] != null)
                    {
                        string TextBoxOther = (setting.product_name[i].ToUpper() == "OTHERS") ? string.Format("<input type=\"text\"  ID=\"txtProductOthers\" class=\"form-control txtProductOthers\" placeholder=\"Please enter detail\" value=\"{0}\" runat=\"server\"></input>", CheckTextOther.Trim()) : "";
                        _htmlProduct.Append(string.Format("<div class=\"row checkbox {0}\">", (!EditPageMode) ? "disabled" : ""));
                        _htmlProduct.Append("<div class=\"col-md-3\">");
                        _htmlProduct.Append(string.Format("<input id=\"chkProduct_{0}_{3}\" {2} class=\"chkProduct\" name=\"{3}\" value=\"{1}\"  type=\"checkbox\"><label for=\"chkProduct_{0}_{3}\">{1}</label>{4}", i.ToString(), setting.products[i].full_name, CheckItem, CheckGroup, TextBoxOther));
                        _htmlProduct.Append("</div>");
                        _htmlProduct.Append("<div class=\"col-md-1 \" style=\"text-align: center; font-weight: bold;\">Spec</div>");
                        _htmlProduct.Append("<div class=\"col-md-4\">");
                        _htmlProduct.Append(string.Format("<input type=\"text\" ID=\"txtProductSpec_{0}\" class=\"form-control txtProductSpec\" {2} value=\"{1}\" placeholder=\"Please enter detail\" runat=\"server\" />", i.ToString(), CheckSpec, (EditPageMode) ? "" : "readonly=\"readonly\""));
                        _htmlProduct.Append("</div>");
                        _htmlProduct.Append("<div class=\"col-md-1 \" style=\"text-align: center; font-weight: bold;\">Volume ( MT )</div>");
                        _htmlProduct.Append("<div class=\"col-md-2\">");
                        _htmlProduct.Append(string.Format("<input type=\"text\" ID=\"txtProductVal_{0}\" class=\"form-control numberCtrl txtProductVal\" {2} value=\"{1}\" placeholder=\"Please enter detail\" runat=\"server\" />", i.ToString(), CheckQty, (EditPageMode) ? "" : "readonly=\"readonly\""));
                        _htmlProduct.Append("</div>");
                        _htmlProduct.Append("</div>");
                    }
                }
            }
            HTMLProduct = _htmlProduct;
        }
        #endregion

        #region---------------------------Load Master Data---------------------------


        private void LoadMaster()
        {

            //LoadMaster From JSON
            setting = JSONSetting.getSetting("JSON_BUNKER_VESSEL");
            //load type of purchase
            _FN.LoadControlDropdown(ddlTypeOfPurchase, setting.type_of_purchase, true);
            //load supplying location
            _FN.LoadControlDropdown(ddlSupplyingLocation, setting.supplying_location, true, false);
            //load contract type
            _FN.LoadControlDropdown(ddlContractType, setting.contract_type, false);
            //load payment term
            foreach (var _payment in setting.payment_terms) { if (_payment.type.IndexOf("#") > 1) { _payment.type = _payment.type.Substring(0, _payment.type.IndexOf('#')); } }
            _FN.LoadControlDropdown(ddlPaymentTerm, setting.payment_terms, "type", "type", false);
            //load reason
            _FN.LoadControlDropdown(ddlReason, setting.proposal_reason, true);
            //load Vendor
            setting.vendor = VendorDAL.GetVendor("BUNSUPMT", "ACTIVE").ToList();
            //load VEHICLE
            setting.vehicle = VehicleDAL.GetVehicle(ConstantPrm.ENGINECONF.EnginAppID.ToUpper(), "ACTIVE", "PRODUCT").OrderBy(x => x.VEH_VEH_TEXT).ToList();
            _FN.LoadControlDropdown(ddlVassel, setting.vehicle, "VEH_ID", "VEH_VEH_TEXT", false);
        }


        #endregion------------------------------------------------------        

        #region-----------------Grid Offer Event-----------------

        [WebMethod]
        public static void SetEventRecord(string KeyID, string EventClick)
        {
            BunkerPreDuePurchaseCMMT _cls = new Web.BunkerPreDuePurchaseCMMT();
            if (EventClick == "delete")
            {
                var X = _cls.LstOffersItemDataSS.Where(x => x.KeyItem == KeyID).ToList();
                if (X.Count > 0) _cls.LstOffersItemDataSS.Remove(X[0]);
            }

        }

        [WebMethod]
        public static void CallSaveItemEvent(string EventClick, IEnumerable<OfferSaveData> jsonData)
        {
            BunkerPreDuePurchaseCMMT _cls = new BunkerPreDuePurchaseCMMT();
            string[] ValSplt = EventClick.Split('|');
            if (ValSplt[0].ToUpper() == "DELETEOFFER")
            {
                _cls.SaveTmpData(jsonData.ToList());
                _cls.DeleteLastOffer();
            }
            else if (ValSplt[0].ToUpper() == "SAVEOFFER")
            {
                _cls.SaveTmpData(jsonData.ToList());
            }
            else if (ValSplt[0].ToUpper() == "ADDSUP")
            {
                _cls.SaveTmpData(jsonData.ToList());
                _cls.AddNewSupplier();
            }
            else if (ValSplt[0].ToUpper() == "ADDOFF")
            {
                _cls.SaveTmpData(jsonData.ToList());
                _cls.AddNewOffer();
            }

            if (ValSplt.Length >= 3)
            {
                if (ValSplt[0].ToUpper() == "ADDROUND")
                {
                    _cls.SetValToSessionObj("ADD", ValSplt[2], ValSplt[1], jsonData.ToList());
                }
                else if (ValSplt[0].ToUpper() == "DELETEROUND")
                {
                    _cls.SetValToSessionObj("DELETE", ValSplt[2], ValSplt[1], jsonData.ToList());
                }
                else if (ValSplt[0].ToUpper() == "ADDQUAN")
                {
                    _cls.SetValToSessionObj("ADD", ValSplt[2], ValSplt[1], jsonData.ToList());
                }
                else if (ValSplt[0].ToUpper() == "DELETEQUAN")
                {
                    _cls.SetValToSessionObj("DELETE", ValSplt[2], ValSplt[1], jsonData.ToList());
                }
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static void CallUpdateProduct(string Product, IEnumerable<OfferSaveData> jsonData)
        {
            BunkerPreDuePurchaseCMMT _cls = new BunkerPreDuePurchaseCMMT();
            _cls.SaveTmpData(jsonData.ToList());
            string[] ProductArr = Product.Split('^');
            if (_cls.ProductSS != null) _cls.ProductSS.Clear();
            _cls.ProductSS = new List<Product>();
            for (int i = 0; i < ProductArr.Count(); i++)
            {
                string[] ProductItem = ProductArr[i].Split('|');
                if (ProductItem.Length >= 3)
                {
                    if (ProductItem[0] != "")
                    {
                        _cls.ProductSS.Add(new Model.Product() { product_order = i.ToString(), product_name = ProductItem[0], product_spec = ProductItem[2], product_qty = ProductItem[1], product_unit = _cls.setting.product_unit.First() });
                    }
                }
            }



            if (_cls.LstOffersItemDataSS != null && _cls.LstOffersItemDataSS != null)
            {
                foreach (var Item in _cls.LstOffersItemDataSS)
                {
                    if (Item.quantitys != null && Item.quantitys.Count > 0)
                    {
                        Item.quantitys.Clear();
                        foreach (var ItemPro in _cls.ProductSS)
                        {
                            Item.quantitys.Add(new Quantity() { quantity_type = ItemPro.product_name });
                        }
                    }
                }
            }


        }

        private void SaveTmpData(List<OfferSaveData> lstJsonData)
        {
            foreach (var _item in lstJsonData)
            {
                var OffItem = LstOffersItemDataSS.Where(x => x.KeyItem == _item.keyitem).ToList();
                if (OffItem != null && OffItem.Count > 0)
                {
                    OffItem[0] = MappDataToTempOffer(OffItem[0], _item);
                }
            }
        }

        private void SetValToSessionObj(string Event, string TypeEvent, string KeyItem, List<OfferSaveData> lstJsonData)
        {
            SaveTmpData(lstJsonData);

            List<OffersItemTmp> tmplst = new List<OffersItemTmp>();
            tmplst = LstOffersItemDataSS.Where(x => x.KeyItem == KeyItem).ToList();
            if (Event.ToUpper() == "DELETE")
            {
                if (tmplst != null && tmplst.Count != 0)
                {
                    string[] _type = TypeEvent.Split('!');
                    if (_type[0].ToUpper() == "QUANTITY_PRODUCT2" || _type[0].ToUpper() == "QUANTITY_PRODUCT1")
                    {
                        if (tmplst[0].quantitys.Count > 0)
                        {
                            var _Quantity = tmplst[0].quantitys.Where(x => x.quantity_type == _type[0].ToUpper().Replace("QUANTITY_", "")).ToList();
                            if (_Quantity != null && _Quantity.Count > 0)
                            {
                                _Quantity[0].quantity_value.Remove(_Quantity[0].quantity_value.Last());
                            }
                        }
                    }
                    else if (_type[0].ToUpper() == "PRODUCT2" || _type[0].ToUpper() == "PRODUCT1")
                    {

                        if (tmplst[0].round_items.Count > 0 && _type.Length >= 2)
                        {
                            var lstround = tmplst[0].round_items.Where(x => x.round_no == _type[1]).ToList();
                            var lstinfo = lstround[0].round_info.Where(x => x.round_type == _type[0]).ToList();
                            if (lstinfo.Count > 0 && lstinfo[0].round_value.Count > 0)
                            {
                                lstinfo[0].round_value.Remove(lstinfo[0].round_value.Last());
                            }
                        }
                    }

                }
            }
            else if (Event.ToUpper() == "ADD")
            {
                if (tmplst != null && tmplst.Count != 0)
                {
                    string[] _type = TypeEvent.Split('!');
                    if (_type[0].IndexOf("QUANTITY_QUANTITY_") >= 0) _type[0] = _type[0].Replace("QUANTITY_QUANTITY_", "QUANTITY_");
                    if (_type[0].ToUpper() == "QUANTITY_PRODUCT2" || _type[0].ToUpper() == "QUANTITY_PRODUCT1")
                    {
                        var _Quantity = tmplst[0].quantitys.Where(x => x.quantity_type == _type[0].ToUpper().Replace("QUANTITY_", "")).ToList();
                        if (_Quantity != null && _Quantity.Count > 0)
                        {
                            _Quantity[0].quantity_value.Add("");
                        }
                        else
                        {
                            Quantity _Qunatity = new Quantity();
                            List<string> Lst = new List<string>();
                            Lst.Add("");
                            _Qunatity.quantity_type = _type[0].ToUpper().Replace("QUANTITY_", "");
                            _Qunatity.quantity_value = Lst;
                            tmplst[0].quantitys.Add(_Qunatity);
                        }
                    }
                    else if (_type[0].ToUpper() == "PRODUCT2" || _type[0].ToUpper() == "PRODUCT1")
                    {
                        List<RoundItem> lstround = new List<RoundItem>();
                        List<RoundInfo> lstinfo = new List<RoundInfo>();
                        if (tmplst[0].round_items.Count >= 0 && _type.Length >= 2)
                        {
                            lstround = tmplst[0].round_items.Where(x => x.round_no == _type[1]).ToList();
                            lstinfo = lstround[0].round_info.Where(x => x.round_type == _type[0]).ToList();
                            if (lstinfo.Count == 0)
                            {
                                List<RoundValues> _str = new List<RoundValues>(); _str.Add(new RoundValues());
                                RoundInfo rd = new RoundInfo(); rd.round_type = _type[0]; rd.round_value = _str;
                                lstround[0].round_info.Add(rd);
                            }
                            else
                            {
                                if (lstinfo.Count >= 0 && lstinfo[0].round_value.Count >= 0)
                                {
                                    lstinfo[0].round_value.Add(new RoundValues());
                                }
                            }
                        }
                    }

                }
            }
        }

        private void AddNewSupplier()
        {
            if (ProductSS.Count() > 0)
            {
                foreach (var _item in ProductSS)
                {
                    if (_item.product_qty == null || _item.product_qty == "" || _item.product_qty == "0")
                    {
                        throw new Exception("กรุณาระบุ Volume ก่อน");
                    }
                }

                if (LstOffersItemDataSS == null || LstOffersItemDataSS.Count == 0)
                {
                    LstOffersItemDataSS = MakeOffer();
                    LstOffersItemDataSS[0].flagEdit = true;
                    CheckOfferRound(false);
                }
                else
                {
                    foreach (var _item in LstOffersItemDataSS)
                    {
                        _item.flagEdit = false;
                    }
                    RoundItem _roudnItem = new RoundItem();
                    _roudnItem.round_no = "1";
                    List<RoundValues> _lstStr = new List<RoundValues>();
                    _lstStr.Add(new RoundValues());
                    List<RoundInfo> _lstRInfo = new List<RoundInfo>();
                    for (int i = 0; i < ProductSS.Count(); i++)
                    {
                        _lstRInfo.Add(new RoundInfo { round_type = string.Format("PRODUCT{0}", i + 1), round_value = _lstStr });
                    }
                    _roudnItem.round_info = _lstRInfo;
                    List<RoundItem> lstround = new List<RoundItem>();
                    List<Quantity> lstQuantity = new List<Quantity>();

                    for (int i = 0; i < ProductSS.Count(); i++)
                    {
                        Quantity _itemQ = new Quantity();
                        _itemQ.quantity_value = new List<string>();
                        _itemQ.quantity_type = string.Format("PRODUCT{0}", i + 1);
                        List<string> _lstrItem = new List<string>(); _lstrItem.Add("");
                        _itemQ.quantity_value = _lstrItem;
                        lstQuantity.Add(_itemQ);
                    }

                    string key = ConstantPrm.EnginGetEngineID();
                    lstround.Add(_roudnItem);
                    LstOffersItemDataSS.Add(new OffersItemTmp
                    {
                        KeyItem = key,
                        flagEdit = true,
                        round_items = lstround,
                        quantitys = lstQuantity,
                        purchasing_term = "Delivered"
                    });
                    CheckOfferRound(false);
                }
            }
            else
            {
                throw new Exception("กรุณาเลือก Product ก่อน");
            }

            LoadHTML();
        }

        private void AddNewOffer()
        {
            if (LstOffersItemDataSS.Count > 0)
            {
                CheckOfferRound(true);
            }
            else
            {
                throw new Exception("กรุณาเพิ่ม Supplier ก่อน");
            }
            LoadHTML();
        }

        private void DeleteLastOffer()
        {
            if (LstOffersItemDataSS.Count > 0)
            {
                if (LstOffersItemDataSS.Count > 0)
                {
                    int MaxOffer = LstOffersItemDataSS.Max(x => x.round_items.Count);
                    for (int i = 0; i < LstOffersItemDataSS.Count; i++)
                    {
                        if (LstOffersItemDataSS[i].round_items.Count >= MaxOffer)
                        {
                            LstOffersItemDataSS[i].round_items.Remove(LstOffersItemDataSS[i].round_items[MaxOffer - 1]);
                        }
                    }
                }
            }
            else
            {
                _FN.MessageBoxShow(this.Page, "กรุณาเพิ่ม Supplier ก่อน");
            }
            LoadHTML();
        }

        private void CheckOfferRound(bool AddOffer)
        {
            if (LstOffersItemDataSS.Count > 0)
            {
                int RecMax = 0; int maxVal = 0;
                for (int i = 0; i < LstOffersItemDataSS.Count; i++)
                {
                    if (maxVal < LstOffersItemDataSS[i].round_items.Count)
                    {
                        RecMax = i;
                        maxVal = LstOffersItemDataSS[i].round_items.Count;
                    }
                }
                if (AddOffer)
                {
                    List<RoundInfo> _lstRInfo = new List<RoundInfo>();
                    List<RoundValues> _lstStr = new List<RoundValues>();
                    _lstStr.Add(new RoundValues());
                    _lstRInfo.Add(new RoundInfo { round_type = "PRODUCT1", round_value = _lstStr });
                    _lstRInfo.Add(new RoundInfo { round_type = "PRODUCT2", round_value = _lstStr });
                    LstOffersItemDataSS[RecMax].round_items.Add(new RoundItem { round_no = (LstOffersItemDataSS[RecMax].round_items.Count + 1).ToString(), round_info = _lstRInfo });
                    maxVal += 1;
                }
                //Set Another offer Round NO

                for (int i = 0; i < LstOffersItemDataSS.Count; i++)
                {
                    if (LstOffersItemDataSS[i].round_items.Count < maxVal)
                    {
                        while (LstOffersItemDataSS[i].round_items.Count < maxVal)
                        {
                            RoundItem _rit = new RoundItem();

                            List<RoundInfo> _lstRInfo = new List<RoundInfo>();
                            List<RoundValues> _lstStr = new List<RoundValues>();
                            _lstStr.Add(new RoundValues());
                            _lstRInfo.Add(new RoundInfo { round_type = "PRODUCT1", round_value = _lstStr });
                            _lstRInfo.Add(new RoundInfo { round_type = "PRODUCT2", round_value = _lstStr });
                            _rit.round_no = (LstOffersItemDataSS[i].round_items.Count + 1).ToString();
                            _rit.round_info = _lstRInfo;
                            LstOffersItemDataSS[i].round_items.Add(_rit);
                        }
                    }
                }
            }
        }

        public OffersItemTmp MappDataToTempOffer(OffersItemTmp EditOffers, OfferSaveData jsonData)
        {
            EditOffers.contact_person = jsonData.contact_person;
            EditOffers.final_price = jsonData.final_price;
            EditOffers.total_price = jsonData.final_price;
            EditOffers.others_cost_barge = jsonData.others_cost_barge;
            EditOffers.others_cost_surveyor = jsonData.others_cost_surveyor;
            EditOffers.purchasing_term = jsonData.purchasing_term;
            EditOffers.supplier = jsonData.supplier;
            EditOffers.note = jsonData.note;
            EditOffers.pricebasedate = _FN.ConvertDateFormat(jsonData.pricebasedate);
            if (EditOffers.quantitys.Count > 0) EditOffers.quantitys[0].quantity_value.Clear();

            foreach (var _item in EditOffers.round_items)
            {
                foreach (var info in _item.round_info)
                {
                    info.round_value.Clear();
                }
                _item.round_info.Clear();
                _item.round_info.Add(new RoundInfo() { round_type = "PRODUCT1", round_value = new List<RoundValues>() });
                _item.round_info.Add(new RoundInfo() { round_type = "PRODUCT2", round_value = new List<RoundValues>() });
            }
            foreach (var _item in EditOffers.quantitys)
            {
                _item.quantity_value.Clear();
            }

            string[] _arrItem = jsonData.item.Split('^');
            List<Quantity> _lstRoundQ1 = new List<Quantity>();
            List<Quantity> _lstRoundQ2 = new List<Quantity>();
            List<RoundItem> _lstRoundP1 = new List<RoundItem>();
            List<RoundItem> _lstRoundP2 = new List<RoundItem>();
            foreach (string _DataVal in _arrItem)
            {
                _lstRoundQ1.Clear();
                _lstRoundQ2.Clear();
                _lstRoundP1.Clear();
                _lstRoundP2.Clear();
                string[] _itemData = _DataVal.Split('|');
                int count = 1;
                if (_itemData.Length >= 2)
                {
                    if (_itemData[0].ToString().IndexOf("quantity_PRODUCT1") >= 0)
                    {
                        _lstRoundQ1 = EditOffers.quantitys.Where(x => x.quantity_type == _itemData[0].Replace("quantity_", "")).ToList();
                        if (_lstRoundQ1.Count > 0)
                            _lstRoundQ1[0].quantity_value.Add(_itemData[1]);
                    }
                    else if (_itemData[0].ToString().IndexOf("quantity_PRODUCT2") >= 0)
                    {
                        _lstRoundQ2 = EditOffers.quantitys.Where(x => x.quantity_type == _itemData[0].Replace("quantity_", "")).ToList();
                        if (_lstRoundQ2.Count > 0)
                            _lstRoundQ2[0].quantity_value.Add(_itemData[1]);
                    }
                    else if (_itemData[0].ToString().IndexOf("PRODUCT1_") >= 0)
                    {
                        _lstRoundP1 = EditOffers.round_items.Where(x => x.round_no == _itemData[0].Replace("PRODUCT1_", "")).ToList();
                        if (_lstRoundP1.Count > 0)
                        {
                            var rLstP1 = _lstRoundP1[0].round_info.Where(x => x.round_type == "PRODUCT1").ToList();
                            if (rLstP1.Count > 0)
                            {
                                RoundValues val = new RoundValues();
                                val.order = count++ + "";
                                val.value = _itemData[1];
                                rLstP1[0].round_value.Add(val);
                            }
                        }
                    }
                    else if (_itemData[0].ToString().IndexOf("PRODUCT2_") >= 0)
                    {
                        _lstRoundP2 = EditOffers.round_items.Where(x => x.round_no == _itemData[0].Replace("PRODUCT2_", "")).ToList();
                        if (_lstRoundP2.Count > 0)
                        {
                            var rLstP2 = _lstRoundP2[0].round_info.Where(x => x.round_type == "PRODUCT2").ToList();
                            if (rLstP2.Count > 0)
                            {
                                RoundValues val = new RoundValues();
                                val.order = count++ + "";
                                val.value = _itemData[1];
                                rLstP2[0].round_value.Add(val);
                            }
                        }
                    }
                }
            }
            return EditOffers;
        }

        protected void btnrefresh_Click(object sender, EventArgs e)
        {


            LoadHTML();
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "MakeDate", "LoadDateNote();", true);
        }



        #endregion-----------------------------------------------

        private BunkerPurchase GetObjPurchaseForSave()
        {
            BunkerPurchase purchase = new BunkerPurchase();
            purchase.contract_type = ddlContractType.SelectedValue;
            purchase.date_purchase = _FN.ConvertDateFormat(txtPurchasedDate.Text);
            purchase.delivery_date_range = new DeliveryDateRange();
            string[] Delivaeryrange = txtDeliveryDaterange.Value.SplitWord(" to ");
            purchase.delivery_date_range.date_from = (Delivaeryrange.Length >= 2) ? _FN.ConvertDateFormat(Delivaeryrange[0]) : "";
            purchase.delivery_date_range.date_to = (Delivaeryrange.Length >= 2) ? _FN.ConvertDateFormat(Delivaeryrange[1]) : "";
            purchase.explanation = txtExplanation.Text;
            purchase.explanationAttach = hdfExplanFileUpload.Value;
            purchase.notes = txtNote.Value;
            purchase.offers_items = MapTmptoLst(LstOffersItemDataSS);
            purchase.payment_terms = new PaymentTerms();
            purchase.payment_terms.payment_term = ddlPaymentTerm.SelectedValue;
            purchase.payment_terms.payment_term_detail = txtPaymentTermDays.Value;
            purchase.payment_terms.sub_payment_term = hdfDayTerm.Value;
            purchase.market_price = new MarketPrice();
            purchase.market_price.market_price_date = _FN.ConvertDateFormat(txtMerketPriceDate.Value);
            purchase.market_price.market_price_nsfo380cst = txtMerketPrice380.Value;
            purchase.market_price.market_price_nsfo180cst = txtMerketPrice180.Value;
            purchase.market_price.market_price_mgo = txtMerketPriceMGO.Value;
            purchase.product = ProductSS;
            purchase.proposal = new Proposal();
            purchase.proposal.award_price = txtAwardPrice.Value;
            purchase.proposal.award_to = txtAwardTo.Text;
            purchase.proposal.reason = ddlReason.SelectedValue;
            purchase.proposal.reasondetail = txtReason.Value;
            purchase.remark = txtAnyOtherSpecial.Value;
            purchase.request_date = DateTime.Now.ToString("dd/MM/yyyy");
            purchase.section_head_by = "";
            purchase.supplying_location = _FN.CheckDropdownVal(ddlSupplyingLocation, txtSupplyingLocation.Text);
            purchase.currency = (hdfCurrency.Value == "B") ? "Baht" : "$";
            purchase.currency_symbol = (hdfCurrency.Value == "B") ? "Baht" : "USD";
            purchase.trader_by = "";
            purchase.type_of_purchase = _FN.CheckDropdownVal(ddlTypeOfPurchase, txtTypeofpurchase.Value);
            //purchase.vessel = ddlVassel.SelectedValue;
            purchase.vessel = hdfVesselID.Value;
            purchase.vesselothers = txtVassel.Text;
            purchase.trip_no = txtTripNo.Text;
            purchase.voyage = (string.IsNullOrEmpty(txtVoyage.Text)) ? "N/A" : txtVoyage.Text;
            purchase.reason = hdfNoteAction.Value;
            foreach (var OffItem in purchase.offers_items)
            {
                if (OffItem.round_items.Count <= 0)
                {
                    throw new Exception("ไม่พบรายการ Offer");
                }
            }
            return purchase;
        }
        private string GetFileUploadJson()
        {
            string[] _split = hdfFileUpload.Value.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }

        }
        private void SaveDataBunker(DocumentActionStatus _status)
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_VERIFY; }



            if (DocumentActionStatus.DraftPreview == _status)
            {
                BunkerPurchase _tmp = new BunkerPurchase();
                BunkerPurchase _bunkerSS = new BunkerPurchase();
                _tmp = GetObjPurchaseForSave();
                BunkerPurchaseTmp = _tmp;
                //_bunkerSS = GetObjPurchaseForSave();
                //BunkerPurchaseSS = _bunkerSS;
                BunkerPurchaseSS = _tmp;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenTab", "OpenNewTabF('Report/BunkerPredueCMMTReport.aspx',SaveClick)", true);

            }

            var json = new JavaScriptSerializer().Serialize(GetObjPurchaseForSave());

            //add json to class for convert to xml.
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000001;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "current_action", V = CurrentAction });
            req.Req_parameters.P.Add(new P { K = "next_status", V = NextState });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.BUNKER });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.VESSEL });
            req.Req_parameters.P.Add(new P { K = "attach_items", V = GetFileUploadJson() });
            req.Req_parameters.P.Add(new P { K = "note", V = (hdfNoteAction.Value == "") ? "-" : hdfNoteAction.Value });
            req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
            req.Extra_xml = "";



            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            if (resData != null)
            {
                //if (DocumentActionStatus.Draft == _status && resData.resp_parameters != null && resData.resp_parameters.Where(x => x.k.ToLower() == "purchase_no").ToList().Count > 0)
                //{

                //    string currUrl = string.Format("BunkerPreDuePurchaseCMMT.aspx?TranID={1}&Tran_Req_ID={0}&PURNO={2}", resData.req_transaction_id.Encrypt(), resData.transaction_id.Encrypt(), resData.resp_parameters.Where(x => x.k.ToLower() == "purchase_no").ToList()[0].v.Encrypt());
                //    _FN.MessageBoxShowLink(this.Page, resData.response_message, currUrl);
                //}
                //else
                //{
                //    _FN.MessageBoxShowLink(this.Page, resData.response_message, ReturnPage);
                //    //LoadDataBunker(resData.transaction_id);
                //}
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string currUrl = string.Format("BunkerPreDuePurchaseCMMT.aspx?TranID={1}&Tran_Req_ID={0}&PURNO={2}", resData.req_transaction_id.Encrypt(), resData.transaction_id.Encrypt(), resData.resp_parameters.Where(x => x.k.ToLower() == "purchase_no").ToList()[0].v.Encrypt());
                    _FN.MessageBoxShowLink(this.Page, resData.response_message, currUrl);
                }
                else
                {
                    _FN.MessageBoxShow(this.Page, resData.response_message);
                    LoadHTML();
                }
            }
            else
            {
                _FN.MessageBoxShow(this.Page, resData.response_message);
                LoadHTML();
            }
        }

        private List<OffersItem> MapTmptoLst(List<OffersItemTmp> lstTmp)
        {
            List<OffersItem> Lst = new List<Model.OffersItem>();
            foreach (var _itmTmp in lstTmp)
            {
                if (_itmTmp.KeyItem == hdfOfferFinalPrice.Value) _itmTmp.final_flag = "Y";
                else { _itmTmp.final_flag = "N"; }
                OffersItem item = new Model.OffersItem();
                _FN.CopyObject(_itmTmp, item);
                int count = 1;
                for (int i = 0; i < item.round_items.Count; i++)
                {
                    var tmpItem = item.round_items[i].round_info.OrderBy(x => x.round_type).ToList();
                    for (int ii = 0; ii < item.round_items[i].round_info.Count; ii++)
                    {
                        tmpItem[ii].round_type = (ii >= 2) ? "" : (ii >= ProductSS.Count) ? "" : ProductSS[ii].product_name;
                        for (int iii = 0; iii < item.round_items[i].round_info[ii].round_value.Count; iii++)
                        {
                            tmpItem[ii].round_value[iii].order = count++ + "";
                        }
                    }
                }
                Lst.Add(item);

            }

            return Lst;
        }

        private List<OffersItemTmp> MaptoTmpLst(List<OffersItem> lst)
        {
            List<OffersItemTmp> Lst = new List<Model.OffersItemTmp>();
            if (lst != null)
            {
                foreach (var _itm in lst)
                {
                    OffersItemTmp item = new Model.OffersItemTmp();
                    _FN.CopyObject(_itm, item, true);
                    for (int i = 0; i < item.quantitys.Count; i++)
                    {
                        item.quantitys[i].quantity_type = "PRODUCT" + (i + 1).ToString();
                    }
                    for (int i = 0; i < item.round_items.Count; i++)
                    {
                        for (int ii = 0; ii < item.round_items[i].round_info.Count; ii++)
                        {
                            item.round_items[i].round_info[ii].round_type = "PRODUCT" + (ii + 1).ToString();
                        }
                    }

                    Lst.Add(item);
                    item.flagEdit = false;
                    item.KeyItem = ConstantPrm.EnginGetEngineID();
                    if (item.final_flag == "Y") hdfOfferFinalPrice.Value = item.KeyItem;
                }
            }
            return Lst;
        }

        private List<OffersItemTmp> ReCheckProduct(List<OffersItemTmp> lst)
        {
            List<OffersItemTmp> Lst = new List<Model.OffersItemTmp>();
            if (lst != null)
            {
                foreach (var _itm in lst)
                {
                    OffersItemTmp item = new Model.OffersItemTmp();
                    _FN.CopyObject(_itm, item, true);
                    for (int i = 0; i < item.quantitys.Count; i++)
                    {
                        item.quantitys[i].quantity_type = "PRODUCT" + (i + 1).ToString();
                    }
                    for (int i = 0; i < item.round_items.Count; i++)
                    {
                        for (int ii = 0; ii < item.round_items[i].round_info.Count; ii++)
                        {
                            item.round_items[i].round_info[ii].round_type = "PRODUCT" + (ii + 1).ToString();
                        }
                    }

                    Lst.Add(item);
                    //if (item.final_flag == "Y") hdfOfferFinalPrice.Value = item.KeyItem;
                }
            }
            return Lst;
        }

        protected void btnEvent_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdfEventClick.Value == "Draft")
                {
                    SaveDataBunker(DocumentActionStatus.Draft);
                }
                else if (hdfEventClick.Value == "Submit")
                {
                    SaveDataBunker(DocumentActionStatus.Submit);
                }
                else if (hdfEventClick.Value == "DraftPreview")
                {
                    SaveDataBunker(DocumentActionStatus.DraftPreview);
                }
                LoadHTML();
            }
            catch (Exception ex)
            {
                _FN.MessageBoxShow(this.Page, ex.Message);
                LoadHTML();
            }
        }

        private void LoadDataBunker(string TransactionID)
        {

            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.BUNKER });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.VESSEL });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                BindButtonPermission(_model.buttonDetail.Button);
                btnEventCommand.Visible = true;
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    btnSaveDrafttmp.Visible = false;
                    btnSavePreviewtmp.Visible = false;
                    btnSubmittmp.Visible = false;
                    _FN.DisableAllControl(this);
                    EditPageMode = false;
                }
                else
                {
                    txtTaskID.Text = "Draft";
                }
            }
            if (txtTaskID.Text != "Draft")
            {
                Button _btn = new Button();
                _btn.ID = "btnPrint";
                _btn.Text = "PRINT";
                _btn.Attributes.Add("class", _FN.GetDefaultButtonCSS("btnPrint"));
                _btn.Attributes.Add("OnClick", string.Format("OpenNewTab('Report/BunkerPredueCMMTReport.aspx?TranID={0}');return false;", TransactionID.Encrypt()));
                buttonDiv.Controls.Add(_btn);
            }
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att.attach_items.Count > 0)
                {
                    foreach (string _item in Att.attach_items)
                    {
                        hdfFileUpload.Value += _item + "|";
                    }
                }
            }
            if (_model.data_detail != null)
            {
                BunkerPurchaseSS = new JavaScriptSerializer().Deserialize<BunkerPurchase>(_model.data_detail);
                BindModelToControl(BunkerPurchaseSS, TransactionID);
                LoadHTML();
            }
            else
            {
                _FN.MessageBoxShowLink(this.Page, resData.response_message, ReturnPage);
            }

        }

        private void BindButtonPermission(List<ButtonAction> Button)
        {
            if (Request.QueryString["isDup"] == null)
            {
                btnSaveDrafttmp.Visible = false;
                btnSavePreviewtmp.Visible = false;
                btnSubmittmp.Visible = false;
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    _FN.DisableAllControl(this);
                    EditPageMode = false;
                }

                if (Button.Count >= 2)
                {
                    if (Button[0].name == "APPROVE" && Button[1].name == "REJECT")
                    {
                        ButtonAction _btn = new ButtonAction();
                        _btn = Button[0];
                        Button[0] = Button[1];
                        Button[1] = _btn;
                    }
                    else if (Button[1].name == "VERIFIED" || Button[1].name == "CERTIFIED")
                    {
                        txtNote.Attributes.Remove("readonly");
                        StatusPageMode = Button[1].name;
                    }
                }

                foreach (ButtonAction _button in Button)
                {
                    Button _btn = new Button();
                    _btn.ID = _button.name;
                    if (_button.name == "CERTIFIED")
                    {
                        _button.name = "ENDORSED";
                    }
                    _btn.Text = _button.name;
                    _btn.Attributes.Add("class", _FN.GetDefaultButtonCSS(_button.name));
                    _btn.Attributes.Add("OnClick", "OpenBoxBungerPrompt('" + _FN.GetMessageBTN(_button.name) + "','" + _button.name + "');return false;");
                    buttonDiv.Controls.Add(_btn);
                }

                ButtonListBunker = Button;
            }
            else
            {
                txtTaskID.Text = "Draft";
            }
        }

        protected void _btn_Click(object sender, EventArgs e)
        {
            try
            {
                if (ButtonListBunker != null)
                {
                    Button _btn = (Button)sender;
                    var _lstButton = ButtonListBunker.Where(x => x.name == hdfEventClick.Value).ToList();
                    if (_lstButton != null && _lstButton.Count > 0)
                    {
                        string _xml = _lstButton[0].call_xml;
                        List<ReplaceParam> _param = new List<ReplaceParam>();
                        string TranReqID = Request.QueryString["Tran_Req_ID"].ToString();
                        var json = new JavaScriptSerializer().Serialize(GetObjPurchaseForSave());
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = (hdfNoteAction.Value == "") ? "-" : hdfNoteAction.Value });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                        _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(GetFileUploadJson()) });
                        _xml = _FN.RaplceParamXML(_param, _xml);
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                        RequestData _req = new RequestData();
                        ResponseData resData = new ResponseData();
                        _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                        resData = service.CallService(_req);
                        if (resData != null)
                        {
                            //if (hdfEventClick.Value.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT.ToUpper())
                            //{
                            //    string currUrl = string.Format("BunkerPreDuePurchaseCMMT.aspx?TranID={1}&Tran_Req_ID={0}&PURNO={2}", Request.QueryString["Tran_Req_ID"], Request.QueryString["TranID"], Request.QueryString["PURNO"]);
                            //    _FN.MessageBoxShowLink(this.Page, resData.response_message, currUrl);
                            //}
                            //else
                            //{
                            //    _FN.MessageBoxShowLink(this.Page, (string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message), ReturnPage);
                            //    //LoadDataBunker(resData.transaction_id);
                            //}
                            if (resData.result_code == "1" || resData.result_code == "2")
                            {
                                using (var context = new EntityCPAIEngine())
                                {
                                    var tranID = resData.transaction_id;
                                    var bnkData = context.BNK_DATA.SingleOrDefault(a => a.BDA_ROW_ID == tranID);
                                    if (bnkData != null)
                                    {
                                        if (bnkData.BDA_EXPLANATION != txtExplanation.Text)
                                        {
                                            ReturnValue rtn = new ReturnValue();
                                            BunkerServiceModel serviceCMMT = new BunkerServiceModel();
                                            rtn = serviceCMMT.AddBunkerExplanation(txtExplanation.Text, bnkData.BDA_UPDATED_BY, tranID);
                                            if (rtn.Status == false)
                                            {
                                                throw new Exception("Explanation Error");
                                            }
                                        }
                                        if (bnkData.BDA_NOTE != txtNote.Value)
                                        {
                                            ReturnValue rtn = new ReturnValue();
                                            BunkerServiceModel serviceCMMT = new BunkerServiceModel();
                                            rtn = serviceCMMT.AddBunkerNote(txtNote.Value, bnkData.BDA_UPDATED_BY, tranID);
                                            if (rtn.Status == false)
                                            {
                                                throw new Exception("Note Error");
                                            }
                                        }
                                    }
                                }
                                string currUrl = string.Format("BunkerPreDuePurchaseCMMT.aspx?TranID={1}&Tran_Req_ID={0}&PURNO={2}", Request.QueryString["Tran_Req_ID"], Request.QueryString["TranID"], Request.QueryString["PURNO"]);
                                _FN.MessageBoxShowLink(this.Page, resData.response_message, currUrl);
                            }
                            else
                            {
                                _FN.MessageBoxShow(this.Page, (string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message));
                                LoadHTML();
                                if (ButtonListBunker != null && ButtonListBunker.Count > 0)
                                {
                                    BindButtonPermission(ButtonListBunker);
                                    btnEventCommand.Visible = true;
                                }
                                if (txtTaskID.Text != "Draft")
                                {
                                    _btn = new Button();
                                    _btn.ID = "btnPrint";
                                    _btn.Text = "PRINT";
                                    _btn.Attributes.Add("class", _FN.GetDefaultButtonCSS("btnPrint"));
                                    _btn.Attributes.Add("OnClick", string.Format("OpenNewTab('Report/BunkerPredueCMCSReport.aspx?TranID={0}');return false;", txtTaskID.Encrypt()));
                                    buttonDiv.Controls.Add(_btn);
                                }
                            }
                        }
                        else
                        {
                            _FN.MessageBoxShow(this.Page, (string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message));
                            LoadHTML();
                        }
                    }
                }
                else
                {
                    _FN.MessageBoxShow(this.Page, "เกิดข้อผิดพลาด");
                }
                LoadHTML();
            }
            catch (Exception ex)
            {
                _FN.MessageBoxShow(this.Page, ex.Message);
                LoadHTML();
            }
        }
        private void BindModelToControl(BunkerPurchase purchase, string TranID)
        {
            txtTaskID.Text = (txtTaskID.Text != "") ? txtTaskID.Text : TranID;
            ddlContractType.SelectedValue = purchase.contract_type;
            txtPurchasedDate.Text = _FN.ConvertDateFormatBack(purchase.date_purchase);
            txtExplanation.Text = purchase.explanation;
            hdfExplanFileUpload.Value = purchase.explanationAttach;
            txtNote.Value = purchase.notes;
            _FN.SetValDropDown(ddlTypeOfPurchase, purchase.type_of_purchase, txtTypeofpurchase);
            if (purchase.delivery_date_range != null && !string.IsNullOrEmpty(purchase.delivery_date_range.date_from))
            {
                txtDeliveryDaterange.Value = string.Format("{0} to {1}", _FN.ConvertDateFormatBack(purchase.delivery_date_range.date_from), _FN.ConvertDateFormatBack(purchase.delivery_date_range.date_to));
            }
            if (purchase.payment_terms != null)
            {
                ddlPaymentTerm.SelectedValue = purchase.payment_terms.payment_term;
                txtPaymentTermDays.Value = purchase.payment_terms.payment_term_detail;
                hdfDayTerm.Value = purchase.payment_terms.sub_payment_term;
            }
            if (purchase.market_price != null)
            {
                txtMerketPriceDate.Value = _FN.ConvertDateFormatBack(purchase.market_price.market_price_date);
                txtMerketPrice380.Value = purchase.market_price.market_price_nsfo380cst;
                txtMerketPrice180.Value = purchase.market_price.market_price_nsfo180cst;
                txtMerketPriceMGO.Value = purchase.market_price.market_price_mgo;
            }
            if (purchase.proposal != null)
            {
                txtAwardPrice.Value = purchase.proposal.award_price;
                txtAwardPricetmp.Value = purchase.proposal.award_price;
                txtAwardTo.Text = purchase.proposal.award_to;
                txtAwardTotmp.Value = purchase.proposal.award_to;
                ddlReason.SelectedValue = purchase.proposal.reason;
                txtReason.Value = purchase.proposal.reasondetail;

            }
            ProductSS = purchase.product;
            txtAnyOtherSpecial.Value = purchase.remark;
            LstOffersItemDataSS = MaptoTmpLst(purchase.offers_items);
            _FN.SetValDropDown(ddlSupplyingLocation, purchase.supplying_location, txtSupplyingLocation);
            ddlVassel.SelectedValue = purchase.vessel;
            txtVassel.Text = purchase.vesselothers;
            txtTripNo.Text = purchase.trip_no;
            txtVoyage.Text = purchase.voyage;
            hdfCurrency.Value = (purchase.currency.ToUpper() == "BAHT") ? "B" : "D";
            txtNoteReason.Value = (Request.QueryString["Reason"] != null) ? Request.QueryString["Reason"].ToString().Decrypt() : purchase.reason;
            txtPurchaseNo.Text = (txtTaskID.Text.ToUpper() == "DRAFT" || Request.QueryString["PURNO"] == null) ? "" : Request.QueryString["PURNO"].ToString().Decrypt();


        }
    }

}
