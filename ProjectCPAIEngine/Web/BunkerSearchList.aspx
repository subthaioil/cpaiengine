﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/MainSite.Master" AutoEventWireup="true"  CodeBehind="BunkerSearchList.aspx.cs" Inherits="ProjectCPAIEngine.Web.BunkerSearchList" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .table tbody tr {
            cursor: pointer;
        }
            .table tbody tr:hover {
                background-color: #f3f3f3;
            }
        
        .clearIcon {
            cursor: pointer;
            color:red;
        }
        .createByField {
            font-family: Arial,sans-serif;
            font-size: 12px;
        }
        .select.input-sm{
            width:50px;
        }
    </style>
    <script type="text/javascript" src="../Content/js/Common.js"></script>
    <script src="../Content/js/plugins/AdminLTE/dataTables.rowsGroup.js"></script>
     <script type="text/javascript" src="../Content/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Content/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            CheckDateFormat('<%= txtStartDate.ClientID %>');
        });
        function btnSearch_Click() {
            <%--var sdate = document.getElementById("<%= txtStartDate.ClientID %>").value; 
            var edate = document.getElementById("<%= txtEndDate.ClientID %>").value; 
            if (sdate != "" && edate != "") {
                if (Date.parse(sdate) > Date.parse(edate)) {
                    alert("กรุณาระบุวันที่เริ่มต้นน้อยกว่าหรือเท่ากับวันที่สิ้นสุด");
                    return false;
                }
            }--%>
            var StrUser = "";
            $('.select2-selection__choice').each(function () {
                if (StrUser == "") {
                    StrUser += $(this).attr("title");
                } else {
                    StrUser +="|"+ $(this).attr("title");
                }
            });
            $("#<%= hdfUserSelect.ClientID %>").val(StrUser)

        }

        function ClearCondition() {
            $("#<%= txtStartDate.ClientID %>").val('')
            $("#<%= ddlCreateby.ClientID %>").prop('selectedIndex', 0);
            $("#<%= ddlLocation.ClientID %>").prop('selectedIndex', 0);
            $("#<%= ddlProduct.ClientID %>").prop('selectedIndex', 0);
            $("#<%= ddlSupplier.ClientID %>").prop('selectedIndex', 0);
            $("#<%= ddlVessel.ClientID %>").prop('selectedIndex', 0);
            $('#<%= ddlCreateby.ClientID %>').select2("val", "");
            return false;
        }

        function OpenLinkLocaltion(link) {
            window.location.href = link;
        }
    </script>
  
    <div class="main-content-area">
        <asp:HiddenField runat="server" ID="hdfUserSelect" />
        <div class="wrapper-detail">
            <div>
                <h1><b>Search Bunker Purchase</b></h1>
            </div>
            <div class="form-horizontal">
                
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Date</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtStartDate" runat="server" class="form-control input-date-picker"></asp:TextBox>
                                <asp:HiddenField ID="hdStartDate" runat="server" />
                                <asp:HiddenField ID="hdEndDate" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Vessel</label>
                            <div class="col-md-7">
                                <asp:DropDownList ID="ddlVessel" runat="server" class="form-control">
                                    <asp:ListItem Text="--All--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Supplier</label>
                            <div class="col-md-7">
                                <asp:DropDownList ID="ddlSupplier" runat="server" class="form-control">
                                    <asp:ListItem Text="--All--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Product</label>
                            <div class="col-md-7">
                                <asp:DropDownList ID="ddlProduct" runat="server" class="form-control">
                                    <asp:ListItem Text="--All--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Location</label>
                            <div class="col-md-7">
                                <asp:DropDownList ID="ddlLocation" runat="server" class="form-control">
                                    <asp:ListItem Text="--All--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <div class="col-md-7" style="text-align: left;" title="Clear Filter All.">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-1 control-label">Created By</label>
                            <div class="col-md-10 createByField">
                                <asp:DropDownList ID="ddlCreateby" runat="server" name="multiselect[]" multiple="multiple" class="form-control"></asp:DropDownList>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="text-left">
                            <asp:Button ID="btnAdd" class="btn btn-success" runat="server" Text="Add" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text-center" style="vertical-align: middle;">
                            <asp:Button ID="btnSearch" class="btn btn-primary" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="return btnSearch_Click();" />
                            <input type="button" class="btn btn-default" onclick="ClearCondition(); return false;" value="Clear All" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text-right">
                            <asp:UpdatePanel runat="server" ID="upd">
                                <ContentTemplate>
                                    <asp:Button ID="btnDup" class="btn btn-info" runat="server" Text="Copy" OnClientClick="return SetValHdf();" OnClick="btnDup_Click" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="page-header">
                <h1>Search Result</h1>
            </div>
            <div class="task-box">

            
            <asp:UpdatePanel runat="server" ID="upn">
                <ContentTemplate>            
                    <div class="task-box">
                        <% =HTMLDataText.ToString() %>
                    </div>               
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
                <asp:HiddenField runat="server" id="hdfTransID" />
                 <asp:HiddenField runat="server" id="hdfTransReq" />
                 <asp:HiddenField runat="server" id="hdfType" />
</div>
        </div>

    </div>
    <script type="text/javascript">

        function SetValHdf() {           
            $('.chkDup').each(function () {               
                if ($(this).is(':checked')) {
                    //alert("xxx");
                    $('#<%= hdfTransID.ClientID %>').val($(this).closest('tr').children('td:eq(7)').find('.lblTransID').text());
                    $('#<%= hdfTransReq.ClientID %>').val($(this).closest('tr').children('td:eq(7)').find('.lblTransReq').text());
                    $('#<%= hdfType.ClientID %>').val($(this).closest('tr').children('td:eq(7)').find('.lblType').text());
                    return true;
                }
            });
            if($('#<%= hdfTransID.ClientID %>').val()=="" || $('#<%= hdfTransReq.ClientID %>').val()=="" || $('#<%= hdfType.ClientID %>').val()==""){
                OpenBoxPopup("กรุณาเลือกรายการที่ต้องการคัดลอก");
                return false;
            }
           
        }

        $('#<%= txtStartDate.ClientID %>').dateRangePicker(
        {
            autoClose: true,
            singleDate: false,
            showShortcuts: false,
            format: 'DD/MM/YYYY'                     
        });
       
        var UserSelect = $("#<%= hdfUserSelect.ClientID %>").val().replace('|', ',').split(',');;        
        $('#<%= ddlCreateby.ClientID %>').select2();
        //($("#<%= hdfUserSelect.ClientID %>").val());    
        $(document).ready(function () {
            $("#table-display").DataTable({
                rowsGroup: [0, 1, 2, 5, 6],
                "ordering": false,
                "bLengthChange": false,
                "bInfo": false,
            });
            
        });
        $('#<%= ddlCreateby.ClientID %>').change(function () {
            $('.select2-search__field').keydown(function (e) {

                if ($(this).val() == "") {
                    if (e.keyCode == 8) {
                        return false;
                    }
                }
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $("#table-display").DataTable({
                rowsGroup: [0, 1, 2, 5, 6],
                "ordering": false,
                "bLengthChange": false,
                "bInfo": false,
            });
        });
            
    </script>
</asp:Content>
