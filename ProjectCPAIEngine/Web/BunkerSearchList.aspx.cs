﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;



namespace ProjectCPAIEngine.Web
{
    public partial class BunkerSearchList : System.Web.UI.Page
    {
        //BunkerPurchase BunkerModel;
        ShareFn _FN = new ShareFn();
        public Setting settingSession
        {
            get
            {
                object o = Session["setting"];
                if (o == null)
                {
                    Setting s = new Setting();
                    return s;
                }
                else
                {
                    return (Setting)o;
                }

            }
            set
            {
                Session["setting"] = value;
            }

        }

        public StringBuilder HTMLDataText = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                btnDup.Visible = false;
                if (Request.QueryString["Type"] != null)
                {
                    string link = "";
                    if (Request.QueryString["Type"].Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
                    {
                        LoadMaster(ConstantPrm.SYSTEMTYPE.VESSEL);
                        link = "BunkerPreDuePurchaseCMMT.aspx";
                    }
                    else
                    {
                        LoadMaster(ConstantPrm.SYSTEMTYPE.CRUDE);
                        link = "BunkerPreDuePurchase.aspx";
                    }
                    btnAdd.Attributes.Add("OnClick", string.Format("OpenLinkLocaltion('{0}');return false;", link));
                }
                //lvSearchBunker.DataSource = new List<BungerTransaction>();
                //lvSearchBunker.DataBind
                LoadDataHTML(new List<BungerTransaction>());

            }
        }

        private void LoadMaster(string Type)
        {
            UsersDAL _user = new UsersDAL();
            Setting setting = new Setting();
            //LoadMaster From JSON
            if (Type.ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) setting = JSONSetting.getSetting("JSON_BUNKER_CRUDE");
            if (Type.ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) setting = JSONSetting.getSetting("JSON_BUNKER_VESSEL");
            if (Type.ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) Type = "PRODUCT";
            setting.vehicle = VehicleDAL.GetVehicle(ConstantPrm.ENGINECONF.EnginAppID.ToUpper(), "ACTIVE", Type).ToList();
            setting.vendor = VendorDAL.GetVendor((Type.ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "PRODUCT" : "CRUDE", "ACTIVE").ToList();
            if(settingSession.vendor != null)
            {
                setting.vendor = settingSession.vendor.OrderBy(x => x.VND_NAME1).ToList();
            }
            else
            {
                if (Request.QueryString["Type"].Decrypt().ToUpper() != ConstantPrm.SYSTEMTYPE.VESSEL)
                {
                    ////CMCS
                    setting.vendor = VendorDAL.GetVendor("BUNSUPCS", "ACTIVE").ToList();
                }
                else
                {
                    ////CMMT
                    setting.vendor = VendorDAL.GetVendor("BUNSUPMT", "ACTIVE").ToList();
                }
                
            }

            _FN.LoadControlDropdown(ddlVessel, setting.vehicle.OrderBy(x => x.VEH_VEH_TEXT).ToList(), "VEH_ID", "VEH_VEH_TEXT", false);
            _FN.LoadControlDropdown(ddlSupplier, setting.vendor.OrderBy(x=>x.VND_NAME1).ToList(), "VND_ACC_NUM_VENDOR", "VND_NAME1", false);
            

            if(Request.QueryString["Type"].Decrypt().ToUpper() != ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                ////CMCS
                _FN.LoadControlDropdown(ddlProduct, setting.product_name, false);
            }
            else
            {
                ////CMMT
                if (setting.products != null)
                {
                    List<string> newList = new List<string>();
                    foreach (var item in setting.products)
                    {
                        newList.Add(item.full_name);
                    }
                    _FN.LoadControlDropdown(ddlProduct, newList, false);
                }
            }            

            _FN.LoadControlDropdown(ddlLocation, setting.supplying_location, false,false);
            _FN.LoadControlDropdown(ddlCreateby, MasterData.GetAllUer(Const.User.UserName,ConstantPrm.SYSTEM.BUNKER), false);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            CheckFieldBeforeLoad();
        }

        protected void btnDup_Click(object sender, EventArgs e)
        {
            string urlPage = "";
            if (!string.IsNullOrEmpty(hdfType.Value) && !string.IsNullOrEmpty(hdfTransID.Value) && !string.IsNullOrEmpty(hdfTransReq.Value))
            {
                if (hdfType.Value.ToUpper().Equals("CRUDE"))
                {
                    urlPage = "BunkerPreDuePurchase.aspx";
                }
                else
                {
                    urlPage = "BunkerPreDuePurchaseCMMT.aspx";
                }
                string path = "~/web/" + urlPage + "?TranID=" + hdfTransID.Value.Encrypt() + "&Tran_Req_ID" + hdfTransReq.Value.Encrypt() + "&isDup=Y";
                Response.Redirect(path);
            }

        }
        public void CheckFieldBeforeLoad()
        {
            string StartDate = string.Empty;
            string EndDate = string.Empty;           
            string[] Delivaeryrange = txtStartDate.Text.SplitWord(" to ");
            if (Delivaeryrange.Length >= 2)
            {
                StartDate = _FN.ConvertDateFormat(Delivaeryrange[0], true);
                EndDate = _FN.ConvertDateFormat(Delivaeryrange[1], true);
            }
            else
            {
                EndDate = "";
                StartDate = "";
            }
            if ((!String.IsNullOrEmpty(StartDate)) && (!String.IsNullOrEmpty(EndDate)))
            {
                hdStartDate.Value = StartDate;
                hdEndDate.Value = EndDate;
                StartDate = _FN.ConvertDateFormat(StartDate) ;
                EndDate = _FN.ConvertDateFormat(EndDate) ;
            }
            LoadDataBunker(StartDate, EndDate);
        }

        private void LoadDataBunker(string sDate, string eDate)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000004;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel});
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.BUNKER });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = sDate });
            req.Req_parameters.P.Add(new P { K = "to_date", V = eDate });
            req.Req_parameters.P.Add(new P { K = "index_1", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_2", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_3", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_4", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_5", V = ddlVessel.SelectedValue });
            req.Req_parameters.P.Add(new P { K = "index_6", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_7", V = ddlLocation.SelectedValue });
            req.Req_parameters.P.Add(new P { K = "index_8", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_9", V = hdfUserSelect.Value });
            req.Req_parameters.P.Add(new P { K = "index_10", V = ddlProduct.SelectedValue });
            req.Req_parameters.P.Add(new P { K = "index_11", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_12", V = ddlSupplier.SelectedValue });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_Bunkertrx _model = ShareFunction.DeserializeXMLFileToObject<List_Bunkertrx>(_DataJson);
            if (_model != null && _model.BungerTransaction!=null && _model.BungerTransaction.Count>0)
            {
                if (_model.BungerTransaction.Count > 0)
                {
                    var lstByTransID = _model.BungerTransaction.Where(x=>x.Date_purchase!=null).ToList().OrderByDescending(x => x.Transaction_id).ToList();

                    foreach (var _item in lstByTransID)
                    {
                        string Parameter = "?TranID=" + _item.Transaction_id.Encrypt() + "&Tran_Req_ID=" + _item.Req_transaction_id.Encrypt() + "&PURNO=" + _item.Purchase_no.Encrypt() + "&Reason=" + _item.Reason.Encrypt();
                        if (_item.Type.ToUpper().Equals("CRUDE"))
                        {
                            _item.Function_desc = "BunkerPreDuePurchase.aspx" + Parameter;
                        }
                        else
                        {
                            _item.Function_desc = "BunkerPreDuePurchaseCMMT.aspx" + Parameter;
                        }

                        _item.Date_purchase = _FN.ConvertDateFormatBackFormat(_item.Date_purchase, "MMMM dd yyyy");
                        _item.Vessel = _item.Vessel + " " + _item.Trip_no;
                        _item.Products = (_item.Products == null) ? "" : _item.Products.Replace("|", "<p>");
                        _item.Volumes = (_item.Volumes == null) ? "" : _item.Volumes.Replace("|", "<p>");
                    }

                    //lvSearchBunker.DataSource = lstByTransID;
                    //lvSearchBunker.DataBind();
                    LoadDataHTML(lstByTransID);
                    btnDup.Visible = true;
                }
            }
            else
            {
                //lvSearchBunker.DataSource = new List<List_Bunkertrx>();
                //lvSearchBunker.DataBind();
                LoadDataHTML(new List<BungerTransaction>());
                btnDup.Visible = false;
            }
        }

        private void LoadDataHTML(List<BungerTransaction> lstDataByStatus)
        {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();

            _htmlDataHead.Clear();
            _htmlDataData.Clear();

            //-----------------Header-----------------
            _htmlDataHead.Append(string.Format("<table class=\"table table-hover table-striped table-bordered\" id = \"{0}\" > ",(lstDataByStatus.Count>0)? "table-display" : ""));
            _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
            _htmlDataHead.Append("<th>Purchase No</th>");
            // _htmlDataHead.Append("<th>Task ID</th>");
            _htmlDataHead.Append("<th>Vessel Name / Voyage No.</th>");
            _htmlDataHead.Append("<th>Product</th>");
            _htmlDataHead.Append("<th>Volume</th>");
            _htmlDataHead.Append("<th>Supply location</th>");
            _htmlDataHead.Append("<th>Created by</th>");
            _htmlDataHead.Append("<th style=\"width:50px;\">Copy</th>");
            _htmlDataHead.Append("</tr></thead>");

            //-----------------Data-----------------
            _htmlDataData.Append("<tbody>");
            string Link = "";
            foreach (var _item in lstDataByStatus)
            {
                DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (_item.System.ToUpper() == ConstantPrm.SYSTEM.BUNKER)
                {
                    Link = (_item.Type.ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "BunkerPreDuePurchaseCMMT.aspx" : "BunkerPreDuePurchase.aspx";
                }
                if (string.IsNullOrEmpty(_item.Products)) { _item.Products = ""; }
                var tmpproduct = _item.Products.Replace("<p>","|").Split('|');
                List<string> product = tmpproduct.Where(x => !string.IsNullOrEmpty(x)).ToList();
                if (string.IsNullOrEmpty(_item.Volumes)) { _item.Volumes = ""; }
                var Valume = _item.Volumes.Replace("<p>", "|").Split('|');
                string Cospan = ""; string Cickbtn = "";
                if (string.IsNullOrEmpty(_item.Products))
                {
                    product.Add("-");
                    Valume = new string[1]; Valume[0] = "";
                }
                for (int i = 0; i < product.Count; i++)
                {
                    var newStatus = _item.Status == "WAITING CERTIFIED" ? "WAITING ENDORSED" : _item.Status;
                    Cickbtn =string.Format( "onclick =\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}')\"", Link, _item.Transaction_id.Encrypt(), _item.Req_transaction_id.Encrypt(), _item.Purchase_no.Encrypt(), _item.Reason.Encrypt());
                    _htmlDataData.Append(string.Format("<tr>" ));
                    _htmlDataData.Append(string.Format("<td {4} {2} width=\"14%\">{0}<span>{1}</span> <label style=\"display:none;\">{2}</label></td>", _item.Date_purchase, newStatus, _item.Purchase_no, Cospan, Cickbtn));
                    _htmlDataData.Append(string.Format("<td {2} {1} width=\"8%\">{0}</td>", _item.Purchase_no, Cospan, Cickbtn));
                    _htmlDataData.Append(string.Format("<td {3} {2} width=\"13%\">{0} {1}</td>", _item.Vessel,(_item.Vessel.IndexOf(_item.Trip_no)>=0)?"": _item.Trip_no, Cospan, Cickbtn));
                    _htmlDataData.Append(string.Format("<td {1} width=\"19%\">{0}</td>", (_item.Products == null) ? "" : product[i], Cickbtn));
                    _htmlDataData.Append(string.Format("<td {1} width=\"8%\">{0}</td>", (_item.Volumes == null) ? "" : (Valume.Length > i) ? Valume[i] : "", Cickbtn));
                    _htmlDataData.Append(string.Format("<td {2} {1} width=\"13%\">{0}</td>", _item.Supplying_location, Cospan, Cickbtn));
                    _htmlDataData.Append(string.Format("<td {2} {1} width=\"8%\">{0}</td>", (_item.Create_by == null) ? "" : _item.Create_by, Cospan, Cickbtn));
                    if (i == 0)
                    {
                        _htmlDataData.Append(string.Format("<td width=\"9%\"><input class=\"chkDup\" type=\"checkbox\" onclick=\"javascript: CheckAll(this);\" />"));
                        _htmlDataData.Append(string.Format("<label  class=\"lblTransID\" style=\"display:none;\">{0}</label>", _item.Transaction_id));
                        _htmlDataData.Append(string.Format("<label  class=\"lblTransReq\"  style=\"display:none;\">{0}</label>", _item.Req_transaction_id));
                        _htmlDataData.Append(string.Format("<label  class=\"lblType\"  style=\"display:none;\">{0}</label>", _item.Type));
                        _htmlDataData.Append("</td>");
                    }
                    else _htmlDataData.Append(string.Format("<td></td>"));
                   
                    _htmlDataData.Append("</tr>");

                }
            }
            _htmlDataData.Append("</tbody>");

            if (lstDataByStatus.Count <= 0)
            {
                _htmlDataData.Append("<tbody><tr class=\"waiting\"><td colspan=\"8\">Waiting for search</td></tr></tbody>");
            }

            //-----------------Finish-----------------
            HTMLDataText.Append(_htmlDataHead);
            HTMLDataText.Append(_htmlDataData);
            HTMLDataText.Append("</table>");

        }

    }
}