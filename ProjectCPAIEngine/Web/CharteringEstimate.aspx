﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MainSite.Master" AutoEventWireup="true" CodeBehind="ChateringEstimate.aspx.cs" Inherits="ProjectCPAIEngine.Web.ChateringEstimate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-content-area">
                <div class="wrapper-detail">
                    <h2 class="title">Vessel Particular</h2>
                    <div class="form-horizontal">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Task ID</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="CHRT-1607-04 ds"  disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Date time</label>
                                    <div class="col-xs-4">
                                        <!-- if change ID please also change js command -->
                                        <input type="text" class="form-control input-date-picker date-single" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Vessel Name</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>DD.MM.YYYY - DD.MM.YYYY</option>
                                            <option>Test 1</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">DWT</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Type</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>Test 1</option>
                                            <option>Test 2</option>
                                            <option>Test 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Capacity (m3)</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Built</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>Test 1</option>
                                            <option>Test 2</option>
                                            <option>Test 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Max Draft (M)</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Age</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Coating</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="title">Payment Terms</h2>
                    <div class="form-horizontal">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Charterer</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>Test 1</option>
                                            <option>Test 2</option>
                                            <option>Test 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Dem</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Cargo</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>Test 1</option>
                                            <option>Test 2</option>
                                            <option>Test 3</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="" placeholder="Please enter detail">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Broker</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>Test 1</option>
                                            <option>Test 2</option>
                                            <option>Test 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Quantity</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Add Comm</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">BSS</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Broker Comm</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Load Port</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>Test 1</option>
                                            <option>Test 2</option>
                                            <option>Test 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Withold Tax</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Discharge port</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>Test 1</option>
                                            <option>Test 2</option>
                                            <option>Test 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Transit Loss</label>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="0.5" >
                                        <span class="input-add">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Laycan</label>
                                    <div class="col-xs-4">
                                        <!-- if change ID please also change js command -->
                                        <input type="text" id="date-range" class="form-control input-date-picker" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Payment Term</label>
                                    <div class="col-xs-4">
                                        <select class="form-control">
                                            <option>Test 1</option>
                                            <option>Test 2</option>
                                            <option>Test 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Laytime</label>
                                    <div class="col-xs-4">
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon blue">Hrs</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h2 class="title">Time for operation (Days)</h2>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Time spending at loading port</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Time spending at discharging port</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Time steaming (Ballast)</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Time steaming (Laden)</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Time for tank cleaning</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Totel duration per voyage</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-xs-8">
                                    <h2 class="title">Vessel speed (Knots)</h2>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-xs-3">
                                                <div class="radio">
                                                    <input id="radiobox1" name="test1" type="radio" checked >
                                                    <label for="radiobox1"> Full</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control paddingleft60">
                                                <span class="input-add-left">Ballast:</span>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control paddingleft60">
                                                <span class="input-add-left">Laden:</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-3">
                                                <div class="radio">
                                                    <input id="radiobox2" name="test1" type="radio">
                                                    <label for="radiobox2"> ECO</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control paddingleft60" readonly>
                                                <span class="input-add-left">Ballast:</span>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control paddingleft60"  readonly>
                                                <span class="input-add-left">Laden:</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <h2 class="title">Exchange rate</h2>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label"><span><b>1</b>USD</span>   =</label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control">
                                                <span class="input-add">Baht</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="title" style="padding-top: 20px;">Latest bunker price info</h2>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-xs-2 control-label">Grade FO</label>
                                    <div class="col-xs-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Date</span>
                                            <input type="text" class="form-control input-date-picker date-single" value="DD/MM/YY">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Place</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Price</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                        <span class="input-add">USD/MT</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-2 control-label">Grade MGO</label>
                                    <div class="col-xs-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Date</span>
                                            <input type="text" class="form-control input-date-picker date-single" value="DD/MM/YY">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Place</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Price</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                        <span class="input-add">USD/MT</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <h2 class="title">Port Charge (USD)</h2>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Port disbursement at load port</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Port disbursement at discharging port</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <h2 class="title"><span class="number">1</span>Bunker Cost (USD)</h2>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunk price (USD/MT)</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at loading</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at discharge port</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at steaming ballast</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at steaming laden</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at tank cleaning</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                            </div>
                            <h2 class="title"><span class="number">2</span>Bunker Cost (USD)</h2>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunk price (USD/MT)</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at loading</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at discharge port</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at steaming ballast</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at steaming laden</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Bunker at tank cleaning</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Ton / Day</span>
                                    </div>
                                </div>
                                <a href="#" class="btn-add"><span>+</span>Add more Bunker Cost</a>
                            </div>
                            <h2 class="title">Other costs (USD)</h2>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Fix cost per day</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Total commession deduction</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Withholding Tax</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Total expenses    USD</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Suggested adding margin (%)</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                </div>
                            </div>
                            <h2 class="title">Results</h2>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Offer freight</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Baht</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label paddingtop0">Total vaviable cost <em>(Bunker + Port charge + Comm + Tax)</em></label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Baht</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Profit over variable cost</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Baht</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">%</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Baht</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Total fix cost </label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Baht</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label paddingtop0">Total cost <em>(Variable + Fix)</em> </label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Baht</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label">Profit over total cost</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Baht</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-5 control-label text-blue">Market freight reference</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">USD</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" value="">
                                        <span class="input-add">Baht</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <h2 class="title">Bunker consumption <span class="sub">(Ton/Day)</span></h2>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Load</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Discharge</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Steaming Ballast</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Steaming Laden</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Idle</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Tank cleaning</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Heating</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">FO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">MGO</span>
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="title">Distance calculation from netpass</h2>
                            <div class="box-map"><img src="images/map.jpg"></div>
                            <h2 class="title">Remark</h2>
                            <textarea class="form-control" style="min-height: 190px;"></textarea>
                        </div>
                    </div>


                    <div class="wrapper-button row">
                        <div class="col-xs-6 text-right">
                            <button class="btn btn-success">Submit Form</button>
                            <button class="btn btn-default">Cancel</button>
                        </div>
                        <div class="col-xs-6 text-left paddingleft60">
                            <button class="btn btn-primary">Save & Preview</button>
                            <button class="btn btn-primary">Save Draft</button>
                        </div>
                    </div>
                </div>
            </div>


</asp:Content>
