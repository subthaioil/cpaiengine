﻿using com.pttict.engine.dal.dao;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace ProjectCPAIEngine.Web
{
  
    public partial class DirectActionPage : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //iFrameDir.Attributes.Add("onload", "autoResize('"+ iFrameDir.ClientID+ "');");

            if (!Page.IsPostBack)
            {
                string allParam = Request.QueryString.ToString();

                if (Request.QueryString["MEUROWID"] != null)
                {
                    string menuRowID = Request.QueryString["MEUROWID"].ToString();
                    GetDirectURL(menuRowID, allParam);
                }
            }
        }

        private AppUserDao appUserDao = new AppUserDaoImpl();
        private SpecialConditionDao specialConditionDao = new SpecialConditionDaoImpl();

        protected void GetDirectURL(string sMenuRowID, string allParam)
        {
            List<MENU> _lstMenu= MasterData.GetLinkDirect(sMenuRowID);
            string strURL = string.Empty;
            UserModel _user = Const.User;
            foreach (var _mainMenu in _lstMenu)
            {
                strURL = _mainMenu.MEU_URL_DIRECT;
            }
            //strURL = "http://www.w3schools.com";
            //strURL = "https://www.google.co.th";
            
            string user_id = _user.UserName;
            string password = _user.UserPassword;
            /*
            APP_USER appUser = appUserDao.findByAppId(ConstantPrm.ENGINECONF.EnginAppID);
            if (appUser != null)
            {
                SPECIAL_CONDITION scn = specialConditionDao.findByKeyTableRef(ConstantUtil.APP_USER_PREFIX, appUser.AUR_ROW_ID, ConstantUtil.PRIVATE_KEY_SCN_KEY);
                if (scn != null && scn.SCN_ROW_ID != null && scn.SCN_ROW_ID.Trim().Length > 0)
                {
                    String priKey = scn.SCN_VALUE;
                    password = RSAHelper.EncryptText(password, priKey);
                }
            }*/
            strURL += "?user=" + user_id + "&pwd=" + password;
            if (!string.IsNullOrEmpty(allParam))
                strURL += "&" + allParam;

            //if (strURL.IndexOf("http://") < 0) strURL = "http://" + strURL;
            iFrameDir.Attributes.Add("src", strURL);
            //iFrameDir.Attributes.Add("onload", " resizeFrame(this)");

        }
    }
}