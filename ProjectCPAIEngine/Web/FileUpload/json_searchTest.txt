{
"HedgingTicket_Search": {
		"sSearchData": [{
				"dDate": "22/06/2017",
				"dStatus": "BUY",
				"dTicketNo": "10001",
				"dDealNo": "DEAL17050001",
				"dDealType": "BUY",
				"dTool": "SWAP",
				"dUnderlying": "Underlying1",
				"dTrader": "AAA",
				"dCounterParty": "TOP"

			},
			{
				"dDate": "22/06/2017",
				"dStatus": "BUY",
				"dTicketNo": "10002",
				"dDealNo": "DEAL17050002",
				"dDealType": "BUY",
				"dTool": "SWAP",
				"dUnderlying": "Underlying1",
				"dTrader": "AAA",
				"dCounterParty": "TOP"
			},
			{
				"dDate": "22/06/2017",
				"dStatus": "BUY",
				"dTicketNo": "10003",
				"dDealNo": "DEAL17050003",
				"dDealType": "BUY",
				"dTool": "SWAP",
				"dUnderlying": "Underlying1",
				"dTrader": "AAA",
				"dCounterParty": "TOP"
			}
		]
	}
}