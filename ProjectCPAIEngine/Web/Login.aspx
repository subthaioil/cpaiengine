﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ProjectCPAIEngine.Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../Content/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../Content/css/thaioil.css" />
    <link rel="stylesheet" type="text/css" href="../Content/css/daterangepicker.css" />
    <style type="text/css">
        .login-logo,
        .register-logo {
            font-size: 35px;
            text-align: center;
            margin-bottom: 25px;
            font-weight: 300;
        }

            .login-logo a,
            .register-logo a {
                color: #444;
            }

        .login-page,
        .register-page {
            background: #d2d6de;
        }

        .login-box,
        .register-box {
            width: 360px;
            margin: 7% auto;
        }

        @media (max-width: 768px) {
            .login-box,
            .register-box {
                width: 90%;
                margin-top: 20px;
            }
        }

        .login-box-body,
        .register-box-body {
            background: #fff;
            padding: 20px;
            border-top: 0;
            color: #666;
        }

            .login-box-body .form-control-feedback,
            .register-box-body .form-control-feedback {
                color: #777;
            }

        .login-box-msg,
        .register-box-msg {
            margin: 0;
            text-align: center;
            padding: 0 20px 20px 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="login-box">
            <div class="login-logo">
                <asp:Image runat="server" ImageUrl="../content/images/logo-thaioil.png" />
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>
                    <div class="form-group has-feedback">
                        <input type="text" id="txtUsername" runat="server" class="form-control" placeholder="Username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" id="txtPassword" runat="server" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">                      
                        <div class="col-xs-11">
                            <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" CssClass="btn btn-primary btn-block btn-flat" Text="Sign In" />
                        </div>
                    </div>
            </div>
        </div>
    </form>
</body>
</html>
