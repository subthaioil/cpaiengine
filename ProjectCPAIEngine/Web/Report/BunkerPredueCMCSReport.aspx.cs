﻿using com.pttict.engine.utility;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;

namespace ProjectCPAIEngine.Web.Report
{
    public partial class BunkerPredueCMCSReport : System.Web.UI.Page
    {
        ShareFn _FN = new ShareFn();
        ReportFunction _RFN = new ReportFunction();
        Document document;
        Table table;
        Color TableBorder;
        Color TableGray;

        int FontSize = 7;
        string FontName = "Tahoma";
        string URL = "";
        List<string> images = new List<string>();
        Setting _setting = JSONSetting.getSetting("JSON_BUNKER_CRUDE");
        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadXMLFromTest();
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload")))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload"));
            }
            if (Request.QueryString["TranID"] != null)
            {
                LoadDataBunker(Request.QueryString["TranID"].ToString().Decrypt());
            }
            else
            {
                if (Session["BunkerPurchaseTmp"] != null)
                {
                    BunkerPurchase _Bunker = (BunkerPurchase)Session["BunkerPurchaseTmp"];
                    _setting.vendor = VendorDAL.GetVendor("BUNSUPCS", "ACTIVE").ToList();
                    _setting.vehicle = VehicleDAL.GetVehicle(ConstantPrm.ENGINECONF.EnginAppID.ToUpper(), "ACTIVE", "CRUDE").ToList();
                    GenPDF(_Bunker,true);
                    Session["BunkerPurchaseTmp"] = null;
                }
            }
        }
        
        public string LoadDataBunker(string TransactionID,bool redirect = true,string UserName="")
        {
            string pdfPath = "";
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V =(string.IsNullOrEmpty(UserName))? Const.User.UserName: UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.BUNKER });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                _setting.vendor = VendorDAL.GetVendor("BUNSUPCS", "ACTIVE").ToList();
                _setting.vehicle = VehicleDAL.GetVehicle(ConstantPrm.ENGINECONF.EnginAppID.ToUpper(), "ACTIVE", "CRUDE").ToList();
                BunkerPurchase _modelBunker = new JavaScriptSerializer().Deserialize<BunkerPurchase>(_model.data_detail);
                if (_modelBunker.offers_items.Count <= 0) _modelBunker.offers_items.Add(new OffersItem());
                if (!redirect)
                    URL = JSONSetting.getGlobalConfig("ROOT_URL");
                pdfPath = GenPDF(_modelBunker, redirect);
                if (!redirect)
                {
                    for (int i = 0; i < images.Count(); i++)
                    {
                        File.Delete(images[i]);
                    }
                }
            }
            else
            {
               if(redirect) _FN.MessageBoxShowLink(this.Page, resData.response_message, "MainBoards.aspx");
            }
            return pdfPath;
        }

        private void LoadXMLFromTest()
        {
            if (File.Exists(@"D:\CPAIExtraXML1.txt"))
            {
                StreamReader _sr = new StreamReader(@"D:\CPAIExtraXML1.txt");
                string Line = _sr.ReadToEnd();
                ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + Line + "</ExtraXML>");

                if (_model.data_detail != null)
                {
                    BunkerPurchase _modelBunker = new JavaScriptSerializer().Deserialize<BunkerPurchase>(_model.data_detail);
                    GenPDF(_modelBunker,true);
                }
            }

        }

        private string GenPDF(BunkerPurchase modelBunker,bool redirect)
        {
            Document document = CreateDocument(modelBunker);

            document.UseCmykColor = true;
            const bool unicode = true;
            //const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();
            // Save the document...
            string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("CMCS{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmssffff")));
            if(!Directory.Exists( Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile"))) { Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile")); }
            pdfRenderer.PdfDocument.Save(filename);
            if(redirect) Response.Redirect(string.Format("TmpFile/{0}", new FileInfo(filename).Name));
            // ...and start a viewer.
            //Process.Start(filename);
            return filename;
        }

        public Document CreateDocument(BunkerPurchase modelBunker)
        {
            TableGray = Colors.Blue;
            TableBorder = Colors.Black;
            TableGray = Colors.Gray;
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = "Predue Bunker CMCS";
            this.document.DefaultPageSetup.LeftMargin = 30;
            this.document.DefaultPageSetup.TopMargin = 30;
            this.document.DefaultPageSetup.BottomMargin = 30;
            DefineStyles();
            _RFN.document = document;
            _RFN.FontName = FontName;
            CreatePage(modelBunker);
            return this.document;
        }

        void DefineStyles()
        {
            MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
            style.Font.Name = FontName;

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = FontName;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 10;

            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        void CheckOfferItem(BunkerPurchase modelBunker)
        {
            for(int i =0;i< modelBunker.offers_items.Count; i++)
            {
                if (modelBunker.offers_items[i].round_items==null || modelBunker.offers_items[i].round_items.Count <= 0)
                {
                    List<RoundValues> lStr = new List<RoundValues>(); lStr.Add(new RoundValues());
                    List<RoundInfo> rInfo = new List<RoundInfo>();
                    rInfo.Add(new RoundInfo { round_type = "fix", round_value = lStr });
                    rInfo.Add(new RoundInfo { round_type = "floating", round_value = lStr });
                    List<RoundItem> rItem = new List<RoundItem>(); rItem.Add(new RoundItem { round_no = "1", round_info = rInfo });
                    modelBunker.offers_items[i].round_items = rItem;
                }
            }

            if (modelBunker.offers_items.Count <= 0)
            {
                List<RoundValues> lStr = new List<RoundValues>(); lStr.Add(new RoundValues());
                List<RoundInfo> rInfo = new List<RoundInfo>();
                rInfo.Add(new RoundInfo { round_type = "fix", round_value = lStr });
                rInfo.Add(new RoundInfo { round_type = "floating", round_value = lStr });
                List<RoundItem> rItem = new List<RoundItem>(); rItem.Add(new RoundItem { round_no = "1", round_info = rInfo });
                List<OffersItem> offer = new List<Model.OffersItem>(); offer.Add(new OffersItem { contact_person = "", final_flag = "", final_price = "", note = "", others_cost_barge = "", others_cost_surveyor = "", purchasing_term = "", supplier = "", round_items = rItem });
                modelBunker.offers_items = offer;
            }
        }

        void CreatePage(BunkerPurchase modelBunker)
        {
            Section section = this.document.AddSection();
            int StartOfferIndex = 0;
            CheckOfferItem(modelBunker);
            if (modelBunker.offers_items.Count > 0)
            {
                while (StartOfferIndex + 1 <= modelBunker.offers_items[0].round_items.Count)
                {
                    if (StartOfferIndex > 0) document.LastSection.AddPageBreak();
                    HeadingReport(section, modelBunker);
                    OfferTableZone(section, modelBunker, StartOfferIndex);
                    BottomZoneReport(section, modelBunker);
                    //document.LastSection.AddPageBreak();            
                    if (StartOfferIndex == 0) FooterReport(section, modelBunker);

                    StartOfferIndex += 2;
                }
                if (!string.IsNullOrEmpty(modelBunker.explanationAttach))
                {
                    document.LastSection.AddPageBreak();
                    HeadingReport(section, modelBunker,true);
                    ExplanImageReport(section, modelBunker);
                }
            }
            //-------------Table Offer-------------

            //-------------------------------------

        }
        
        
        private void HeadingReport(Section section, BunkerPurchase modelBunker,bool AttachPage = false)
        {

            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 13;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            // Create the header of the table
            Row row = table.AddRow();
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].AddParagraph("Bunker/Gas Oil Purchase Approval Form for Time Charter Vessel");
            row.Cells[icell].Format.Font.Size = 9;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 1;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            icell = 0;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 12;
            row.Cells[icell].Borders.Top.Color = Colors.White;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Purchased Date :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(modelBunker.date_purchase, "dd MMM yyyy"));
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;


            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("For Vessel :");
            row.Cells[0].MergeRight = 2;
            row.Cells[3].AddParagraph(_FN.GetTextValByObject(_setting.vehicle, "VEH_ID", "VEH_VEH_TEXT", modelBunker.vessel, modelBunker.vesselothers) +" "+modelBunker.trip_no);
            //row.Cells[4].AddParagraph());
            row.Cells[3].Format.Font.Bold = false;
            row.Cells[3].MergeRight = 15;
            bool HaveCheck = false;
            Table tbTmp = new Table(); tbTmp.AddColumn("4cm");
            tbTmp.Format.Font.Size = FontSize;
            row = tbTmp.AddRow();
            foreach (var _item in _setting.type_of_purchase)
            {
                if (modelBunker.type_of_purchase == _item)
                {
                    HaveCheck = true;
                    row.Cells[0].AddParagraph("  ").AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" "+_item, new Font(FontName, FontSize));//6e                    
                    //row.Cells[4].AddParagraph(_item).AddLineBreak();¢
                }
                else
                {
                    if (!HaveCheck && _item.ToUpper().Trim() == "OTHERS".Trim())
                    {
                        row.Cells[0].AddParagraph("  ").AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item + " : " + modelBunker.type_of_purchase, new Font(FontName, FontSize));
                    }
                    else
                    {
                        row.Cells[0].AddParagraph("  ").AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                    }
                }

            }

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Type of Purchase :");
            row.Cells[0].MergeRight = 2;
            row.Cells[3].Format.Font.Bold = false;
            row.Cells[3].MergeRight = 6;
            row.Cells[3].Elements.Add(tbTmp);
            row.Cells[10].AddParagraph("Supplying Location :");
            row.Cells[10].MergeRight = 3;
            HaveCheck = false;
            foreach (var _item in _setting.supplying_location)
            {
                if (modelBunker.supplying_location == _item)
                {
                    HaveCheck = true;
                    row.Cells[14].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                }
                else
                {
                    if (!HaveCheck && _item.ToUpper().Trim() == "OTHERS".Trim())
                    {
                        row.Cells[14].AddParagraph("  ").AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item + " : " + modelBunker.supplying_location, new Font(FontName, FontSize));
                    }
                    else
                    {
                        row.Cells[14].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                    }
                }
            }
            row.Cells[14].Format.Font.Bold = false;
            row.Cells[14].MergeRight = 4;
            bool HaveItem; string pdQty = ""; string _productName = "";
            HaveCheck = false;
            Table tbTmp_ = new Table();
            tbTmp_.Format.Font.Size = FontSize;
            tbTmp_.AddColumn("7cm"); 
            List<string> tmpProduct = new List<string>();
            pdQty = "";
            foreach (var item in _setting.product_name)
            {
                HaveItem = false;
                for (int i = 0; i < modelBunker.product.Count; i++)
                {
                    if (item.ToUpper().Trim() == modelBunker.product[i].product_name.ToUpper().Trim())
                    {
                        _productName = modelBunker.product[i].product_name;
                        HaveItem = true;
                        pdQty = modelBunker.product[i].product_qty;
                        tmpProduct.Add(item);
                    }
                    else
                    {
                        if (modelBunker.product != null && item.ToUpper() == "OTHERS")
                        {
                            foreach (var _item in modelBunker.product)
                            {
                                var tmpCnt = tmpProduct.Where(x => x == _item.product_name).ToList();
                                if (tmpCnt == null || tmpCnt.Count <= 0)
                                {
                                    HaveCheck = false; pdQty = _item.product_qty;
                                    _productName = _item.product_name;
                                }
                            }
                        }
                    }
                }
                row = tbTmp_.AddRow();
                row.Height = 8;
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Format.Font.Bold = true;
                if (HaveItem)
                {
                    HaveCheck = true;
                    row.Cells[0].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" " + item, new Font(FontName, FontSize));
                }
                else
                {
                    if (!HaveCheck && item.ToUpper().Trim() == "OTHERS".Trim())
                    {
                        HaveCheck = true;
                        row.Cells[0].AddParagraph("  ").AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" " + item + " : " + _productName, new Font(FontName, FontSize));
                    }
                    else
                    {
                        row.Cells[0].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + item, new Font(FontName, FontSize));
                    }
                }
                row.Cells[0].Format.Font.Bold = false;
                
            }
            row = table.AddRow();
            row.Cells[0].AddParagraph("Product :");
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].MergeRight = 2;
            row.Cells[3].MergeRight = 6;
            row.Cells[3].Elements.Add(tbTmp_);

            row.Cells[10].AddParagraph("Volume ( MT ) :");
            row.Cells[10].MergeRight = 3;
            row.Cells[10].Format.Font.Bold = true;
            row.Cells[14].AddParagraph(_FN.intTostrMoney(pdQty));
            row.Cells[14].Format.Font.Bold = false;
            row.Cells[14].MergeRight = 4;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Contract type :");
            row.Cells[0].MergeRight = 2;
            row.Cells[3].AddParagraph(modelBunker.contract_type);
            row.Cells[3].Format.Font.Bold = false;
            row.Cells[3].MergeRight = 6;
            row.Cells[10].AddParagraph("Any other special terms and conditions :");
            row.Cells[10].Borders.Bottom.Color = Colors.White;
            row.Cells[10].MergeRight = 8;

            string Daterange = "-";
            if (modelBunker.delivery_date_range != null)
            {
                Daterange = string.Format("{0}", _FN.ConvertDateFormatBackFormat(modelBunker.delivery_date_range.date_from, "dd MMM yyyy"));
            }
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Delivery Date :");
            row.Cells[0].MergeRight = 2;
            row.Cells[3].AddParagraph(Daterange);
            row.Cells[3].Format.Font.Bold = false;
            row.Cells[3].MergeRight = 6;
            row.Cells[10].AddParagraph(string.IsNullOrEmpty(modelBunker.remark) ? "" : modelBunker.remark);
            row.Cells[10].Borders.Top.Color = Colors.White;
            row.Cells[10].VerticalAlignment = VerticalAlignment.Top;
            row.Cells[10].MergeRight = 8;
            row.Cells[10].MergeDown = 1;
            row.Cells[10].Format.Font.Bold = false;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Voyage :");
            row.Cells[0].MergeRight = 2;
            row.Cells[3].AddParagraph(modelBunker.voyage == null ? "" : modelBunker.voyage);
            row.Cells[3].Format.Font.Bold = false;
            row.Cells[3].MergeRight = 6;
            if (!AttachPage)
            {
                row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Format.Font.Bold = true;
                row.Cells[0].AddParagraph("Explanation (As necessary)  :");
                row.Cells[0].Borders.Bottom.Color = Colors.White;
                row.Cells[0].MergeRight = 18;

                //List<htmlTagList> Lst = htmlRemove.SplitHTMLTag(modelBunker.explanation);
                //List<htmlTagList> list = htmlRemove.RearrangeHtmlTagList(Lst);
                //if (list.Count > 0)
                //{
                //    for (int i = 0; i < list.Count; i++)
                //    {
                //        row = table.AddRow();
                //        row.HeadingFormat = true;
                //        row.Format.Alignment = ParagraphAlignment.Left;
                //        row.VerticalAlignment = VerticalAlignment.Top;
                //        row.Format.Font.Bold = false;
                //        Paragraph _prg = row.Cells[0].AddParagraph();
                //        //_prg.AddFormattedText(HtmlRemoval.ValuetoParagraph(modelBunker.explanation));
                //        htmlRemove.ValuetoParagraph(list[i], _prg);
                //        row.Cells[0].AddParagraph();
                //        row.Cells[0].MergeRight = 18;
                //        row.Cells[0].Borders.Top.Color = Colors.White;
                //    }
                //} else
                //{
                //    row = table.AddRow();
                //    row.HeadingFormat = true;
                //    row.Format.Alignment = ParagraphAlignment.Left;
                //    row.VerticalAlignment = VerticalAlignment.Top;
                //    row.Format.Font.Bold = false;
                //    row.Cells[0].AddParagraph("");
                //    row.Cells[0].MergeRight = 18;
                //    row.Cells[0].Borders.Top.Color = Colors.White;
                //}

                if (!string.IsNullOrEmpty(modelBunker.explanation))
                {
                    Paragraph _prg = row.Cells[0].AddParagraph();
                    htmlRemove.ValuetoParagraph(modelBunker.explanation, _prg);
                }
                else
                {
                    row.Cells[0].AddParagraph("");
                }
            }
        }
        HtmlRemoval htmlRemove = new HtmlRemoval();
        private void OfferTableZone(Section section, BunkerPurchase modelBunker, int itemOfferIndex)
        {
            List<string> Quantity = new List<string>();
            List<string> FixFloting = new List<string>();
            FixFloting.Add("fix"); FixFloting.Add("floating");

            List<string> lstFix = new List<string>();
            lstFix.Add("Fix");
            List<string> lstFloting = new List<string>();
            lstFloting.Add("Floating");

            foreach (var _item in modelBunker.product)
            {
                Quantity.Add(_item.product_name);
            }
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 13;

            //var _offerItem = modelBunker.offers_items[itemOfferIndex];

            //check offer
            bool isOdd = false;

            if ((modelBunker.offers_items[0].round_items.Count - itemOfferIndex) % 2 != 0)
            {
                if ((modelBunker.offers_items[0].round_items.Count - itemOfferIndex) == 1)
                {
                    isOdd = true;
                }
            }

            if (isOdd)
            {
                #region 1Offer

                #region--------HeaddingTable-------------

                // Before you can add a row, you must define the columns
                Column column = this.table.AddColumn("0.5cm");
                for (int i = 0; i < 37; i++)
                {
                    column = this.table.AddColumn("0.5cm");
                }
                Row row = table.AddRow();

               // row.Cells[0].AddParagraph("Table1 : Applied for \"Fuel Oil/ Gas Oil price Offer\"");
               // row.Cells[0].MergeRight = 37;
               // _RFN.SetCellAlignment(row.Cells[0], ReportAlignment.LC);
               // row.Cells[0].VerticalAlignment = VerticalAlignment.Center;                
               //row = table.AddRow();

                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Format.Font.Bold = true;
                int icell = 0;
                row.Cells[0].AddParagraph("Supplier");
                row.Cells[0].MergeRight = 3;
                row.Cells[0].MergeDown = 2;
                icell += 4;
                row.Cells[icell].AddParagraph("Contact Person");
                row.Cells[icell].MergeRight = 2;
                row.Cells[icell].MergeDown = 2;
                icell += 3;
                //int icellQ = icell;
                //row.Cells[icell].AddParagraph("Quantity (MTON)");
                //row.Cells[icell].MergeRight = 5;
                //row.Cells[icell].MergeDown = 1;
                //icell += 6;
                int icelR2 = icell;
                //int ttSt = (2 * (Quantity.Count - 1));
                row.Cells[icell].AddParagraph("Offer Price or Formula ( $/MT )");
                row.Cells[icell].MergeRight = 14;
                icell += 15;
                row.Cells[icell].AddParagraph("Final Price").AddFormattedText(Environment.NewLine).AddFormattedText("( $/MT )");
                row.Cells[icell].MergeRight = 3;
                row.Cells[icell].MergeDown = 2;
                icell += 4;
                row.Cells[icell].AddParagraph("Est.Surveyor Cost").AddFormattedText(Environment.NewLine).AddFormattedText("( $/MT )");
                row.Cells[icell].MergeRight = 3;
                row.Cells[icell].MergeDown = 2;
                icell += 4;
                row.Cells[icell].AddParagraph("Total Price").AddFormattedText(Environment.NewLine).AddFormattedText("( $/MT )");
                row.Cells[icell].MergeRight = 3;
                row.Cells[icell].MergeDown = 2;
                icell += 4;
                row.Cells[icell].AddParagraph("Est.Benefit \nvs 2nd low").AddFormattedText(Environment.NewLine).AddFormattedText("( $/MT )");
                row.Cells[icell].MergeRight = 3;
                row.Cells[icell].MergeDown = 2;
                icell += 4;


                icell = icelR2;
                row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Purchasing Term");
                row.Cells[icell].MergeRight = 2;
                row.Cells[icell].MergeDown = 1;
                icell += 3;
                row.Cells[icell].AddParagraph(string.Format("{0}{1}Offer/Negotiation", _FN.GetORDINALNUMBERS((itemOfferIndex + 1)),Environment.NewLine));
                //row.Cells[icell].MergeRight = 5;
                //icell += 6;
                //row.Cells[icell].AddParagraph(string.Format("{0} Offer/Negotiation", (itemOfferIndex + 2).ToString()));
                row.Cells[icell].MergeRight = 11;
                icell += 12;
                //row.Cells[icell].AddParagraph("Others Cost");
                //row.Cells[icell].MergeRight = 5;


                row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                ////icell = icellQ;
                ////for (int i = 1; i <= Quantity.Count; i++)
                ////{
                ////    row.Cells[icell].AddParagraph(Quantity[i - 1]);
                ////    row.Cells[icell].MergeRight = Quantity.Count <= 1 ? 5 : 2;
                ////    icell += row.Cells[icell].MergeRight + 1;
                ////}

                icell = icelR2 + 3;
                for (int i = 1; i <= lstFix.Count; i++)
                {
                    row.Cells[icell].AddParagraph(lstFix[i - 1]);
                    row.Cells[icell].MergeRight = 5;// lstFix.Count <= 1 ? 5 : 2;
                    icell += row.Cells[icell].MergeRight + 1;
                }

                //icell = icelR2 + 3;
                for (int i = 1; i <= lstFloting.Count; i++)
                {
                    row.Cells[icell].AddParagraph(lstFloting[i - 1]);
                    row.Cells[icell].MergeRight = 5;// lstFloting.Count <= 1 ? 5 : 2;
                    icell += row.Cells[icell].MergeRight + 1;
                }
                //row.Cells[icell].AddParagraph("Barge");
                //row.Cells[icell].MergeRight = 2;
                //row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
                //row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
                //icell += row.Cells[icell].MergeRight + 1;
                //row.Cells[icell].AddParagraph("Surveyor");
                //row.Cells[icell].MergeRight = 2;
                //row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
                //row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;

                #endregion----------------------------------

                for (int ioffer = 0; ioffer < modelBunker.offers_items.Count; ioffer++)
                {
                    var _offerItem = modelBunker.offers_items[ioffer];

                    icell = 0;
                    row = table.AddRow();
                    row.Format.Font.Size = FontSize ;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.GetTextValByObject(_setting.vendor, "VND_ACC_NUM_VENDOR", "VND_NAME1", _offerItem.supplier), 3));
                    row.Cells[icell].MergeRight = 3;
                    _RFN.SetCellAlignment(row.Cells[icell],ReportAlignment.LC);
                    icell += 4;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.contact_person, 2));
                    row.Cells[icell].MergeRight = 2;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 3;
                    icell = icelR2;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.purchasing_term, 2));
                    row.Cells[icell].MergeRight = 2;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 3;

                    //icell = icelR2 + 3;

                    int StartI = itemOfferIndex;
                    //for (int i = 0; i < 2; i++)
                    //{
                    int offiNo = StartI;
                    //if (_offerItem.round_items.Count > offiNo)
                    //{
                    var offItem = _offerItem.round_items[offiNo];
                    for (int iq = 1; iq <= FixFloting.Count; iq++)
                    {
                        for (int rinfo = 0; rinfo < offItem.round_info.Count; rinfo++)
                        {
                            if (offItem.round_info[rinfo].round_type == FixFloting[iq - 1])
                            {
                                foreach (var _item in offItem.round_info[rinfo].round_value)
                                {
                                    string ValItem = _item.value;
                                    if (string.IsNullOrEmpty(ValItem)) ValItem = "-";
                                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(ValItem, true), FixFloting.Count <= 1 ? 5 : 2));
                                    row.Cells[icell].AddParagraph(_FN.intTostrMoney(ValItem, true));
                                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                                }

                            }
                        }
                        row.Cells[icell].MergeRight = 5;
                        _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                        icell += row.Cells[icell].MergeRight + 1;
                    }

                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.others_cost_barge, 2));
                    //row.Cells[icell].MergeRight = 2;
                    //_RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.LC);
                    //icell += 3;
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(_offerItem.others_cost_surveyor,true), 2));
                    string FinalPrice = (string.IsNullOrEmpty(_offerItem.final_price)) ? " " : _offerItem.final_price;
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn( row.Cells[icell], _FN.intTostrMoney(FinalPrice, true), 2));
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(FinalPrice, true), 5));
                    row.Cells[icell].MergeRight = 3;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 4;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(_offerItem.others_cost_surveyor, true), 5));
                    row.Cells[icell].MergeRight = 3;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 4;
                    string TotalPrice = (string.IsNullOrEmpty(_offerItem.total_price)) ? " " : _offerItem.total_price;
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn( row.Cells[icell], _FN.intTostrMoney(FinalPrice, true), 2));
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(TotalPrice, true), 5));
                    row.Cells[icell].MergeRight = 3;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 4;
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.note, 2));
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(_offerItem.note, true), 5));
                    row.Cells[icell].MergeRight = 3;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 4;
                }

                //row = table.AddRow();
                //row.Cells[0].AddParagraph("");
                //row.Cells[0].MergeRight = 37;
                #endregion

            }
            else
            {
                #region 2Offers

                #region--------HeaddingTable-------------

                // Before you can add a row, you must define the columns
                Column column = this.table.AddColumn("0.1cm");
                for (int i = 0; i < 189; i++)
                {
                    column = this.table.AddColumn("0.1cm");
                }
                //Row row = table.AddRow();
                //row.Cells[0].AddParagraph("Table1 : Applied for \"Fuel Oil/ Gas Oil price Offer\"");
                //row.Cells[0].MergeRight = 37;

                //row = table.AddRow();

                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Format.Font.Bold = true;
                int icell = 0;
                row.Cells[0].AddParagraph("Supplier");
                row.Cells[0].MergeRight = 17;
                row.Cells[0].MergeDown = 2;
                icell += 18;
                row.Cells[icell].AddParagraph("Contact Person");
                row.Cells[icell].MergeRight = 11;
                row.Cells[icell].MergeDown = 2;
                icell += 12;              
                int icelR2 = icell;
                row.Cells[icell].AddParagraph("Offer Price or Formula ($/MT )");
                row.Cells[icell].MergeRight = 87;
                icell += 88;
                row.Cells[icell].AddParagraph("Final Price").AddFormattedText(Environment.NewLine).AddFormattedText("( $/MT )");
                row.Cells[icell].MergeRight = 17;
                row.Cells[icell].MergeDown = 2;
                icell += 18;
                row.Cells[icell].AddParagraph("Est.Surveyor Cost").AddFormattedText(Environment.NewLine).AddFormattedText("( $/MT )");
                row.Cells[icell].MergeRight = 17;
                row.Cells[icell].MergeDown = 2;
                icell += 18;
                row.Cells[icell].AddParagraph("Total Price").AddFormattedText(Environment.NewLine).AddFormattedText("( $/MT )");
                row.Cells[icell].MergeRight = 17;
                row.Cells[icell].MergeDown = 2;
                icell += 18;
                row.Cells[icell].AddParagraph("Est. Benefit vs 2nd low").AddFormattedText(Environment.NewLine).AddFormattedText("( $/MT )");
                row.Cells[icell].MergeRight = 17;
                row.Cells[icell].MergeDown = 2;
                icell += 18;


                icell = icelR2;
                row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Purchasing Term");
                row.Cells[icell].MergeRight = 15;
                row.Cells[icell].MergeDown = 1;
                icell += 16;
                row.Cells[icell].AddParagraph(string.Format("{0}{1}Offer/Negotiation", _FN.GetORDINALNUMBERS((itemOfferIndex + 1)),Environment.NewLine));
                row.Cells[icell].MergeRight = 35;
                icell += 36;
                row.Cells[icell].AddParagraph(string.Format("{0}{1}Offer/Negotiation", _FN.GetORDINALNUMBERS((itemOfferIndex + 2)),Environment.NewLine));
                row.Cells[icell].MergeRight = 35;
                icell += 36;
                //row.Cells[icell].AddParagraph("Others Cost");
                //row.Cells[icell].MergeRight = 5;


                row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;

                icell = icelR2 + 16;
                for (int i = 0; i < 2; i++)
                {
                    for (int ii = 0; ii < FixFloting.Count; ii++)
                    {

                        row.Cells[icell].AddParagraph(FixFloting[ii].Replace("f","F"));
                        row.Cells[icell].MergeRight = FixFloting.Count <= 1 ? 35 : 17;
                        icell += row.Cells[icell].MergeRight + 1;
                    }
                }
                //row.Cells[icell].AddParagraph("Barge");
                //row.Cells[icell].MergeRight = 2;
                //row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
                //row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
                //icell += row.Cells[icell].MergeRight + 1;
                //row.Cells[icell].AddParagraph("Surveyor");
                //row.Cells[icell].MergeRight = 2;
                //row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
                //row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;

                #endregion----------------------------------

                for (int ioffer = 0; ioffer < modelBunker.offers_items.Count; ioffer++)
                {
                    var _offerItem = modelBunker.offers_items[ioffer];


                    icell = 0;
                    row = table.AddRow();
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.GetTextValByObject(_setting.vendor, "VND_ACC_NUM_VENDOR", "VND_NAME1", _offerItem.supplier), 17));
                    row.Cells[icell].MergeRight = 17;
                    icell += 18;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.contact_person, 11));
                    row.Cells[icell].MergeRight = 11;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 12;                   
                    icell = icelR2;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.purchasing_term, 15));
                    row.Cells[icell].MergeRight = 15;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 16;

                    int StartI = itemOfferIndex;
                    for (int i = 0; i < 2; i++)
                    {
                        int offiNo = i + StartI;
                        if (_offerItem.round_items.Count > offiNo)
                        {
                            var offItem = _offerItem.round_items[offiNo];
                            for (int iq = 1; iq <= FixFloting.Count; iq++)
                            {
                                for (int rinfo = 0; rinfo < offItem.round_info.Count; rinfo++)
                                {
                                    if (offItem.round_info[rinfo].round_type == FixFloting[iq - 1])
                                    {
                                        foreach (var _item in offItem.round_info[rinfo].round_value)
                                        {
                                            string ValItem = _item.value;
                                            if (string.IsNullOrEmpty(ValItem)) ValItem = "-";
                                            //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(ValItem, true), FixFloting.Count <= 1 ? 5 : 2));
                                            row.Cells[icell].AddParagraph(_FN.intTostrMoney(ValItem, true));
                                            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                                        }

                                    }
                                }
                                row.Cells[icell].MergeRight = FixFloting.Count <= 1 ? 35 : 17;
                                _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                                icell += row.Cells[icell].MergeRight + 1;
                            }
                        }
                        else
                        {
                            for (int iq = 1; iq <= FixFloting.Count; iq++)
                            {
                                row.Cells[icell].AddParagraph("");
                                row.Cells[icell].MergeRight = FixFloting.Count <= 1 ? 35 : 17;
                                _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                                icell += row.Cells[icell].MergeRight + 1;
                            }
                        }
                    }
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.others_cost_barge, 2));
                    //row.Cells[icell].MergeRight = 2;
                    //icell += 3;
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(_offerItem.others_cost_surveyor,true), 2));
                    string FinalPrice = (string.IsNullOrEmpty(_offerItem.final_price)) ? " " : _offerItem.final_price;
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn( row.Cells[icell], _FN.intTostrMoney(FinalPrice, true), 2));
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(FinalPrice, true), 22));
                    row.Cells[icell].MergeRight = 17;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 18;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(_offerItem.others_cost_surveyor, true), 22));
                    row.Cells[icell].MergeRight = 17;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 18;
                    string TotalPrice = (string.IsNullOrEmpty(_offerItem.total_price)) ? " " : _offerItem.total_price;
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn( row.Cells[icell], _FN.intTostrMoney(FinalPrice, true), 2));
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(TotalPrice, true), 22));
                    row.Cells[icell].MergeRight = 17;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 18;
                    //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(_offerItem.note, true), 2));
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(_offerItem.note, true), 22));
                    row.Cells[icell].MergeRight = 17;
                    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                    icell += 18;

                }
                
                #endregion
            }
        }

        private void BottomZoneReport(Section section, BunkerPurchase modelBunker)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            }

            //Start Create Table Payment Term 1
            Table _tmpPaymentTerm = new Table();
            _tmpPaymentTerm.Format.Font.Name = FontName;
            _tmpPaymentTerm.Format.Font.Size = FontSize;
            _tmpPaymentTerm.Rows.Height = 8;
            _tmpPaymentTerm.AddColumn("1cm");
            _tmpPaymentTerm.AddColumn("1cm");
            _tmpPaymentTerm.AddColumn("4cm");
            //End Create Payment Term

            Row row;
            bool chkPayment = false; bool chkPaymentSub = false;
            //foreach (var _item in _setting.payment_terms)
            //{
            for (int j = 0; j < 2; j++)
            {
                var _item = _setting.payment_terms[j];
                chkPayment = false; chkPaymentSub = false;
                string[] PayType = _item.type.ToString().Split('#');
                //string _termDay = (string.IsNullOrEmpty(modelBunker.payment_terms.payment_term_detail)) ? "________" : " " + modelBunker.payment_terms.payment_term_detail + " ";
                if (modelBunker.payment_terms.payment_term.IndexOf(PayType[0]) >= 0)
                {
                    chkPayment = true;
                }
                string _termDay = (string.IsNullOrEmpty(modelBunker.payment_terms.payment_term_detail) && !chkPayment) ? "________" : " " + modelBunker.payment_terms.payment_term_detail + " ";

                row = _tmpPaymentTerm.AddRow();
                //row.Cells[0].AddParagraph(_item.type);
                if (chkPayment) row.Cells[0].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" " + _item.type.Replace("#", _termDay) + ((_item.type.ToUpper() == "OTHERS") ? " : " + _termDay : ""), new Font(FontName, FontSize));
                else row.Cells[0].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item.type.Replace("#", "________"), new Font(FontName, FontSize));
                row.Cells[0].MergeRight = 2;
                if (_item.sub_type != null && _item.sub_type.Count > 0)
                {
                    for (int i = 0; i < _item.sub_type.Count; i++)
                    {
                        chkPaymentSub = false;
                        string ValSub = string.IsNullOrEmpty(modelBunker.payment_terms.sub_payment_term) ? "" : modelBunker.payment_terms.sub_payment_term.ToUpper().Replace("DAY", "");
                        if (ValSub != "" && _item.sub_type[i].ToUpper().IndexOf(ValSub) >= 0 && chkPayment) chkPaymentSub = true;
                        row = _tmpPaymentTerm.AddRow();
                        if (chkPaymentSub) row.Cells[1].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" " + _item.sub_type[i], new Font(FontName, FontSize));
                        else row.Cells[1].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item.sub_type[i], new Font(FontName, FontSize));
                        //row.Cells[1].AddParagraph(_item.sub_type[i]);
                        row.Cells[1].MergeRight = 1;
                    }
                }
            }
            //}

            //Start Create Table Payment Term 1
            Table _tmpPaymentTerm2 = new Table();
            _tmpPaymentTerm2.Format.Font.Name = FontName;
            _tmpPaymentTerm2.Format.Font.Size = FontSize;
            _tmpPaymentTerm2.Rows.Height = 8;
            _tmpPaymentTerm2.AddColumn("1cm");
            _tmpPaymentTerm2.AddColumn("1cm");
            _tmpPaymentTerm2.AddColumn("4cm");
            //End Create Payment Term

            Row row2;
            bool chkPayment2 = false; bool chkPaymentSub2 = false;
            for (int j = 2; j < 4; j++)
            {
                var _item = _setting.payment_terms[j];
                chkPayment2 = false; chkPaymentSub2 = false;
                string[] PayType = _item.type.ToString().Split('#');
                //string _termDay = (string.IsNullOrEmpty(modelBunker.payment_terms.payment_term_detail)) ? "________" : " " + modelBunker.payment_terms.payment_term_detail + " ";
                if (modelBunker.payment_terms.payment_term.IndexOf(PayType[0]) >= 0)
                {
                    chkPayment2 = true;
                }
                string _termDay = (string.IsNullOrEmpty(modelBunker.payment_terms.payment_term_detail) && !chkPayment2) ? "________" : " " + modelBunker.payment_terms.payment_term_detail + " ";

                row2 = _tmpPaymentTerm2.AddRow();
                //row.Cells[0].AddParagraph(_item.type);
                if (chkPayment2) row2.Cells[0].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" " + _item.type.Replace("#", _termDay) + ((_item.type.ToUpper() == "OTHERS") ? " : " + _termDay : ""), new Font(FontName, FontSize));
                else row2.Cells[0].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item.type.Replace("#", "________"), new Font(FontName, FontSize));
                row2.Cells[0].MergeRight = 2;
                if (_item.sub_type != null && _item.sub_type.Count > 0)
                {
                    for (int i = 0; i < _item.sub_type.Count; i++)
                    {
                        chkPaymentSub2 = false;
                        string ValSub = string.IsNullOrEmpty(modelBunker.payment_terms.sub_payment_term) ? "" : modelBunker.payment_terms.sub_payment_term.ToUpper().Replace("DAY", "");
                        if (ValSub != "" && _item.sub_type[i].ToUpper().IndexOf(ValSub) >= 0 && chkPayment2) chkPaymentSub2 = true;
                        row2 = _tmpPaymentTerm2.AddRow();
                        if (chkPaymentSub2) row2.Cells[1].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" " + _item.sub_type[i], new Font(FontName, FontSize));
                        else row2.Cells[1].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item.sub_type[i], new Font(FontName, FontSize));
                        //row.Cells[1].AddParagraph(_item.sub_type[i]);
                        row2.Cells[1].MergeRight = 1;
                    }
                }
            }

            

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Height = 14;
            row.Cells[0].AddParagraph("Payment Term : ");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].Borders.Bottom.Color = Colors.White;
            row.Cells[3].Elements.Add(_tmpPaymentTerm);
            row.Cells[3].MergeRight = 5;
            row.Cells[3].MergeDown = 1;
            row.Cells[3].Format.Font.Bold = false;
            row.Cells[3].Borders.Left.Color = Colors.White;
            row.Cells[3].Borders.Right.Color = Colors.White;            
            row.Cells[9].Elements.Add(_tmpPaymentTerm2);
            row.Cells[9].MergeRight = 9;
            row.Cells[9].MergeDown = 1;
            row = table.AddRow();
            row.Height = 26;
            row.Cells[0].MergeRight = 2;
            row.Cells[0].Borders.Top.Color = Colors.White;
            //row.Cells[0].Borders.Right.Color = Colors.White;            


            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Proposal");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].Borders.Bottom.Color = Colors.White;
            row.Cells[0].Borders.Right.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;

            row.Cells[3].AddParagraph("Award to :");
            row.Cells[3].MergeRight = 1;
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[3].Borders.Left.Color = Colors.White;
            row.Cells[3].Borders.Right.Color = Colors.White;

            row.Cells[5].AddParagraph(modelBunker.proposal.award_to == null ? "" : modelBunker.proposal.award_to);
            row.Cells[5].MergeRight = 3;
            row.Cells[5].Format.Font.Bold = false;
            row.Cells[5].Borders.Bottom.Color = Colors.White;
            row.Cells[5].Borders.Left.Color = Colors.White;
            row.Cells[5].Borders.Right.Color = Colors.White;

            row.Cells[9].AddParagraph("Award Price :");
            row.Cells[9].MergeRight = 2;
            row.Cells[9].Borders.Bottom.Color = Colors.White;
            row.Cells[9].Borders.Left.Color = Colors.White;
            row.Cells[9].Borders.Right.Color = Colors.White;

            row.Cells[12].AddParagraph(_FN.intTostrMoney( modelBunker.proposal.award_price,true));
            row.Cells[12].MergeRight = 6;
            row.Cells[12].Format.Font.Bold = false;
            row.Cells[12].Borders.Bottom.Color = Colors.White;
            row.Cells[12].Borders.Left.Color = Colors.White;


            Table _tmpReason = new Table();
            _tmpReason.Format.Font.Name = FontName;
            _tmpReason.Format.Font.Size = FontSize;
            _tmpReason.Rows.Height = 8;
            _tmpReason.AddColumn("1cm");
            _tmpReason.AddColumn("1cm");
            _tmpReason.AddColumn("4cm");
            Row rowReason;
            bool HaveCheck = false;            
            foreach (var _item in _setting.proposal_reason)
            {
                rowReason = _tmpReason.AddRow();
                if (modelBunker.proposal.reason == _item)
                {
                    HaveCheck = true; 
                    rowReason.Cells[0].AddParagraph("  ").AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" " + _item + (string.IsNullOrEmpty(modelBunker.proposal.reasondetail) ? "" : (" : " + modelBunker.proposal.reasondetail)), new Font(FontName, FontSize));
                    //row.Cells[4].AddParagraph(_item).AddLineBreak();
                }
                else
                {
                    if (!HaveCheck && _item.ToUpper().Trim() == "OTHERS".Trim())
                    {
                        rowReason.Cells[0].AddParagraph("  ").AddFormattedText("\u006e", new Font("Wingdings", FontSize+2)).AddFormattedText(" " + _item + " : " + modelBunker.proposal.reasondetail, new Font(FontName, FontSize));
                    }
                    else
                    {
                        rowReason.Cells[0].AddParagraph("  ").AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                    }//u00A8//u00fe
                }
                rowReason.Cells[0].MergeRight = 2;
            }

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].Borders.Top.Color = Colors.White;
            row.Cells[0].Borders.Right.Color = Colors.White;
            row.Cells[3].AddParagraph("Reason :");
            row.Cells[3].MergeRight = 1;
            row.Cells[3].Borders.Top.Color = Colors.White;
            row.Cells[3].Borders.Left.Color = Colors.White;
            row.Cells[3].Borders.Right.Color = Colors.White;
            row.Cells[5].Elements.Add(_tmpReason);
            row.Cells[5].MergeRight = 13;
            row.Cells[5].Format.Font.Bold = false;
            row.Cells[5].Borders.Top.Color = Colors.White;
            row.Cells[5].Borders.Left.Color = Colors.White;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Note :");
            row.Cells[0].MergeRight = 18;
            row.Cells[0].Borders.Bottom.Color = Colors.White;

            row = table.AddRow();
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Format.Font.Bold = false;
            row.Cells[0].AddParagraph(modelBunker.notes);
            row.Cells[0].AddParagraph();
            row.Cells[0].MergeRight = 18;
            row.Cells[0].Borders.Top.Color = Colors.White;

        }
        private void FooterReport(Section section, BunkerPurchase model)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            //row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy.");
            //row.Cells[icell].MergeRight = 37;

            //icell = 0;
            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            var approveName_1 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            }
            // _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            approveName_1 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            }
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve4
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            if (!string.IsNullOrEmpty(approveName))
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            //Approve3
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            //if (!string.IsNullOrEmpty(approveName))
            //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            //else
            //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            approveName = !String.IsNullOrEmpty(approveName_1) ? _FN.GetNameOffApprove(model.approve_items, "APPROVE_1") : _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = !String.IsNullOrEmpty(approveName_1) ? _FN.GetNameOffApprove(model.approve_items, "APPROVE_2") : _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

        }
        /*
        private void FooterReport(Section section, BunkerPurchase modelBunker)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            //row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy");
            //row.Cells[icell].MergeRight = 37;
            //icell += row.Cells[icell].MergeRight + 1;

            //icell = 0;
            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested By");
            row.Cells[icell].MergeRight = 17;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Certified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight =1;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelBunker.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
           
            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelBunker.approve_items, "APPROVE_1", ref height, ref width, true));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve2
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelBunker.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve3
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelBunker.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph(string.Format("( CMCS-{0} )", _FN.GetNameOffApprove(modelBunker.approve_items, "APPROVE_1")));
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;
            
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.Format("( CMCS-{0} )", _FN.GetNameOffApprove(modelBunker.approve_items, "APPROVE_1",true)));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;



        }

    */
        private void FooterReport2(Section section, BunkerPurchase modelBunker)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Requested, endorsed and approved can be done either via e-mail or hard copy");
            row.Cells[0].MergeRight = 18;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Requested By");
            row.Cells[0].MergeRight = 6;
            row.Cells[7].AddParagraph("Endorsed by");
            row.Cells[7].MergeRight = 5;
            row.Cells[13].AddParagraph("Approved by");
            row.Cells[13].MergeRight = 5;

            row = table.AddRow();
            row.Height = 18;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Right;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Trader :");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].MergeDown = 1;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[3].AddParagraph("____________________");
            double height = 0;double width = 0;
            _img = row.Cells[3].AddParagraph().AddImage(_FN.GetPathSignature(modelBunker.approve_items, "APPROVE_1",ref height,ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[3].MergeRight = 3;
            row.Cells[7].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[7].AddParagraph("________________________");
            height = 0; width = 0;
             _img = row.Cells[7].AddParagraph().AddImage(_FN.GetPathSignature(modelBunker.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[7].Borders.Bottom.Color = Colors.White;
            row.Cells[7].MergeRight = 5;
            row.Cells[7].MergeDown = 1;
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[13].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[13].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[13].AddParagraph("________________________");
            height = 0; width = 0;
            _img = row.Cells[13].AddParagraph().AddImage(_FN.GetPathSignature(modelBunker.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[13].Borders.Bottom.Color = Colors.White;
            row.Cells[13].MergeRight = 5;
            row.Cells[13].MergeDown = 1;

            row = table.AddRow();
            row.Height = 18;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[3].AddParagraph(string.Format("(CMCS-{0})", _FN.GetNameOffApprove(modelBunker.approve_items, "APPROVE_1")));
            row.Cells[3].Borders.Top.Color = Colors.White;
            row.Cells[3].MergeRight = 3;

            row = table.AddRow();
            row.Height = 18;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Right;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("S/H :");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].MergeDown = 1;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[3].AddParagraph("____________________");
            height = 0; width = 0;
            _img = row.Cells[3].AddParagraph().AddImage(_FN.GetPathSignature(modelBunker.approve_items, "APPROVE_1", ref height, ref width, true));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[3].MergeRight = 3;
            row.Cells[7].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[7].Borders.Top.Color = Colors.White;
            row.Cells[7].AddParagraph("");
            row.Cells[7].Borders.Bottom.Color = Colors.White;
            row.Cells[7].MergeRight = 5;
            row.Cells[13].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[13].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[13].Borders.Top.Color = Colors.White;
            row.Cells[13].Borders.Bottom.Color = Colors.White;
            row.Cells[13].AddParagraph("");
            row.Cells[13].MergeRight = 5;

            row = table.AddRow();
            row.Height = 18;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[3].AddParagraph(string.Format("(CMCS-{0})", _FN.GetNameOffApprove(modelBunker.approve_items, "APPROVE_1", true)));
            row.Cells[3].Borders.Top.Color = Colors.White;
            row.Cells[3].MergeRight = 3;
            row.Cells[7].AddParagraph("CMVP");
            row.Cells[7].Borders.Top.Color = Colors.White;
            row.Cells[7].MergeRight = 5;
            row.Cells[13].AddParagraph("EVPC");
            row.Cells[13].Borders.Top.Color = Colors.White;
            row.Cells[13].MergeRight = 5;

        }
        private void ExplanImageReport(Section section, BunkerPurchase modelBunker)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            }

            
            
            string[] _image = modelBunker.explanationAttach.Split('|');
            foreach (var _item in _image)
            {
                if (_item != "")
                {
                    string[] itemsplit = _item.ToString().Split(':');
                    Row row = table.AddRow();
                    int icell = 0;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Format.Font.Bold = true;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    icell += row.Cells[icell].MergeRight + 1;
                    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetExplanImage(itemsplit[0], URL));
                    if (!string.IsNullOrEmpty(URL))
                    {
                        images.Add(_img.Name);
                    }
                    _img.Width = new Unit(11, UnitType.Centimeter);
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].MergeRight = 12;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Borders.Bottom.Color = Colors.White;
                    if (itemsplit.Length > 0)
                    {
                        row = table.AddRow();
                        icell = 0;
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Format.Font.Bold = true;
                        row.Cells[icell].AddParagraph(itemsplit[1]);
                        row.Cells[icell].MergeRight = 18;
                    }
                }
            }
            

        }
    }
}