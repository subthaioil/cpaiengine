﻿using com.pttict.engine.utility;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;

namespace ProjectCPAIEngine.Web.Report
{
    public partial class CharteringInReport : System.Web.UI.Page
    {
        ShareFn _FN = new ShareFn();
        ReportFunction _RFN = new ReportFunction();
        Document document;
        MigraDoc.DocumentObjectModel.Tables.Table table;
        Color TableBorder;
        Color TableGray;
        const int defaultHeight = 10;
        bool isLumpsum = true;

        int FontSize = 8;
        string URL = "";
        List<string> images = new List<string>();
        string FontName = "Tahoma";
        string type = ConstantPrm.SYSTEMTYPE.CRUDE;
        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadXMLFromTest();
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload")))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload"));
            }
            if (Request.QueryString["TranID"] != null)
            {
                LoadDataCharter(Request.QueryString["TranID"].ToString().Decrypt());
            }
            else
            {
                if (Session["ChiRootObject"] != null)
                {
                    ChiRootObject _Bunker = (ChiRootObject)Session["ChiRootObject"];
                    GenPDF(_Bunker, true);
                    Session["ChiRootObject"] = null;
                }
            }
        }

        private void updateModel(ChiRootObject model, bool isTextMode = true)
        {
            //Assign loadport order and discharge port order
            //List<SelectListItem> port = DropdownServiceModel.getPortName(true, "", "PORT_CMCS");
            //List<SelectListItem> crude = DropdownServiceModel.getMaterial(true, "", "MAT_CMCS");
            //List<SelectListItem> suplier = DropdownServiceModel.getVendorFreight("CHISUPCS", true);


            using (DAL.Entity.EntityCPAIEngine context = new DAL.Entity.EntityCPAIEngine())
            {
                for (int i = 0; i < model.chi_cargo_detail.chi_load_ports.Count; i++)
                {
                    if (isTextMode)
                    {
                        //model.chi_cargo_detail.chi_load_ports[i].port = port.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].port).FirstOrDefault() == null ? "" : port.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].port).FirstOrDefault().Text;
                        //model.chi_cargo_detail.chi_load_ports[i].crude = crude.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].crude).FirstOrDefault() == null ? "" : crude.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].crude).FirstOrDefault().Text;
                        //model.chi_cargo_detail.chi_load_ports[i].supplier = suplier.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].supplier).FirstOrDefault() == null ? "" : suplier.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].supplier).FirstOrDefault().Text;

                        var portId = Convert.ToDecimal(model.chi_cargo_detail.chi_load_ports[i].port);
                        var chkPort = context.MT_PORT.SingleOrDefault(a => a.MLP_LOADING_PORT_ID == portId);
                        if (chkPort != null)
                        {
                            model.chi_cargo_detail.chi_load_ports[i].port = chkPort.MLP_LOADING_PORT_NAME;
                        }
                        var crudeId = model.chi_cargo_detail.chi_load_ports[i].crude;
                        var chkCrude = context.MT_MATERIALS.SingleOrDefault(a => a.MET_NUM == crudeId);
                        if (chkCrude != null)
                        {
                            model.chi_cargo_detail.chi_load_ports[i].crude = chkCrude.MET_MAT_DES_ENGLISH;
                        }
                        var SupplierId = model.chi_cargo_detail.chi_load_ports[i].supplier;
                        var chkSupplier = context.MT_VENDOR.SingleOrDefault(a => a.VND_ACC_NUM_VENDOR == SupplierId);
                        if (chkSupplier != null)
                        {
                            model.chi_cargo_detail.chi_load_ports[i].supplier = chkSupplier.VND_NAME1;
                        }
                    }
                }
            }                

            //Assign round_no, order no., order type and final flag
            List<SelectListItem> broker = DropdownServiceModel.getVendorFreight("CHIBROCS", true);
            List<SelectListItem> vehicle = DropdownServiceModel.getVehicle("CHIVESCS", true);
            if (model.chi_negotiation_summary != null)
            {
                for (int i = 0; i < model.chi_negotiation_summary.chi_offers_items.Count; i++)
                {
                    if (isTextMode)
                    {
                        model.chi_negotiation_summary.chi_offers_items[i].broker = broker.Where(x => x.Value == model.chi_negotiation_summary.chi_offers_items[i].broker).FirstOrDefault() == null ? "" : broker.Where(x => x.Value == model.chi_negotiation_summary.chi_offers_items[i].broker).FirstOrDefault().Text;
                        model.chi_negotiation_summary.chi_offers_items[i].vessel = vehicle.Where(x => x.Value == model.chi_negotiation_summary.chi_offers_items[i].vessel).FirstOrDefault() == null ? "" : vehicle.Where(x => x.Value == model.chi_negotiation_summary.chi_offers_items[i].vessel).FirstOrDefault().Text;
                    }
                }
            }

            if (isTextMode)
            {
                model.chi_proposed_for_approve.charterer_broker = broker.Where(x => x.Value == model.chi_proposed_for_approve.charterer_broker).FirstOrDefault() == null ? "" : broker.Where(x => x.Value == model.chi_proposed_for_approve.charterer_broker).FirstOrDefault().Text;
                model.chi_proposed_for_approve.vessel_name = vehicle.Where(x => x.Value == model.chi_proposed_for_approve.vessel_name).FirstOrDefault() == null ? "" : vehicle.Where(x => x.Value == model.chi_proposed_for_approve.vessel_name).FirstOrDefault().Text;
            }
        }

        public string LoadDataCharter(string TransactionID, bool redirect = true, string UserName= "")
        {
            string pdfPath = "";
            RequestData reqData = new RequestData();
            reqData.function_id = ConstantPrm.FUNCTION.F10000005;
            reqData.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            reqData.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            reqData.req_transaction_id = ConstantPrm.EnginGetEngineID();
            reqData.state_name = "";
            reqData.req_parameters = new req_parameters();
            reqData.req_parameters.p = new List<p>();
            reqData.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            reqData.req_parameters.p.Add(new p { k = "user", v = (string.IsNullOrEmpty(UserName)) ? Const.User.UserName : UserName });
            reqData.req_parameters.p.Add(new p { k = "transaction_id", v = TransactionID });
            reqData.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
            reqData.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            reqData.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(reqData);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                string pattern = "[\"](\\w+)[\"]:null";
                Regex rgx = new Regex(pattern);
                string replacement = "\"$1\":\"\"";
                string result = rgx.Replace(_model.data_detail, replacement);
                result = result.Replace("\"approve_items\":\"\"", "\"approve_items\":null");
                ChiRootObject _modelReport = new JavaScriptSerializer().Deserialize<ChiRootObject>(result);
                if (_modelReport.chi_negotiation_summary.chi_offers_items.Count <= 0) _modelReport.chi_negotiation_summary.chi_offers_items.Add(new ChiOffersItem());
                this.updateModel(_modelReport);
                if (!redirect)
                    URL = JSONSetting.getGlobalConfig("ROOT_URL");
                pdfPath = GenPDF(_modelReport, redirect);
                if (!redirect)
                {
                    for (int i = 0; i < images.Count(); i++)
                    {
                        File.Delete(images[i]);
                    }
                }
            }
            else
            {
                if (redirect) _FN.MessageBoxShowLink(this.Page, resData.response_message, "MainBoards.aspx");
            }
            return pdfPath;
        }

        private void LoadXMLFromTest()
        {
            string PathTest = @"D:\Json.txt";
            if (File.Exists(PathTest))
            {
                StreamReader _sr = new StreamReader(PathTest);
                string Line = _sr.ReadToEnd();
                ChiRootObject _modelReport = new JavaScriptSerializer().Deserialize<ChiRootObject>(Line);
                GenPDF(_modelReport, true);
            }


        }

        private string GenPDF(ChiRootObject modelReport, bool redirect)
        {
            Document document = CreateDocument(modelReport);

            document.UseCmykColor = true;
            const bool unicode = true;
            //const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();
            // Save the document...
            string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("{0}{1}.pdf", type, DateTime.Now.ToString("yyyyMMddHHmmssffff")));
            pdfRenderer.PdfDocument.Save(filename);
            if (redirect) Response.Redirect(string.Format("TmpFile/{0}", new FileInfo(filename).Name));
            // ...and start a viewer.
            //Process.Start(filename);
            return filename;
        }

        public Document CreateDocument(ChiRootObject modelReport)
        {
            TableGray = Colors.Blue;
            TableBorder = Colors.Black;
            TableGray = Colors.Gray;
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = "Charter In";
            this.document.DefaultPageSetup.LeftMargin = 30;
            this.document.DefaultPageSetup.TopMargin = 30;
            this.document.DefaultPageSetup.BottomMargin = 30;
            DefineStyles();
            _RFN.document = document;
            _RFN.FontName = FontName;
            CreatePage(modelReport);
            return this.document;
        }

        void DefineStyles()
        {
            MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
            style.Font.Name = FontName;

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = FontName;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 10;

            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        void CreatePage(ChiRootObject modelReport)
        {
            Section section = this.document.AddSection();
            int StartOfferIndex = 0;
            while (StartOfferIndex + 1 <= modelReport.chi_negotiation_summary.chi_offers_items[0].chi_round_items.Count)
            {
                if (StartOfferIndex > 0) document.LastSection.AddPageBreak();
                HeadingReport(section, modelReport);
                OfferTableZone(section, modelReport, StartOfferIndex);
                BottomZoneReport(section, modelReport);
                //document.LastSection.AddPageBreak();            
                FooterReport(section, modelReport);

                StartOfferIndex += 5;
            }
            if (!string.IsNullOrEmpty(modelReport.explanationAttach))
            {
                document.LastSection.AddPageBreak();
                HeadingReport(section, modelReport, true);
                ExplanImageReport(section, modelReport);
            }

            //-------------Table Offer-------------

            //-------------------------------------

        }
        Setting _setting = JSONSetting.getSetting("JSON_CHARTER_IN_CRUDE");
        private void HeadingReport(Section section, ChiRootObject modelReport, bool AttachPage = false)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            // Create the header of the table
            Row row = table.AddRow();
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].AddParagraph("Spot Chartering In Vessel  Memorandum");
            row.Cells[icell].Format.Font.Size = 9;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 1;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("For Vessel:");
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.chi_proposed_for_approve.vessel_name.ToUpper());
            row.Cells[icell].Format.LeftIndent = new Unit(1, UnitType.Millimeter);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("On Subject Date");
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(modelReport.chi_evaluating.date_purchase, "dd MMM yyyy"));
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;


            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].AddParagraph("EVALUATION :");
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            Table tmpTable = new Table();
            MakeTableEvaluation(modelReport.chi_evaluating, tmpTable);
            row.Cells[icell].Elements.Add(tmpTable);
            row.Cells[icell].MergeRight = 15;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].AddParagraph("CARGO DETAIL :");
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            tmpTable = new Table();
            MakeTableCargoDetail(modelReport.chi_cargo_detail, tmpTable);
            row.Cells[icell].Elements.Add(tmpTable);
            row.Cells[icell].MergeRight = 15;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Font.Size = FontSize- 1;
            row.Height = 12;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].AddParagraph("Type of Fixing :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            foreach (var item in modelReport.chi_chartering_method.chi_type_of_fixings)
            {
                if (item.type_flag.ToUpper() == "Y")
                {
                    row.Cells[icell].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + item.type_value, new Font(FontName, FontSize-1));
                }
                else
                {
                    row.Cells[icell].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + item.type_value, new Font(FontName, FontSize-1));

                }
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].Borders.Color = Colors.White;
                row.Cells[icell].Borders.Top.Color = Colors.Black;
                row.Cells[icell].MergeRight = 2;
                icell += row.Cells[icell].MergeRight + 1;
            }
            row.Format.Font.Bold = false;
            row.Cells[icell].MergeRight = 9;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.White;

            row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Font.Size = FontSize - 1;
            row.Height = 12;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].AddParagraph("Reason (optional for Private Order) :");
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Top.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.White;

            //List<htmlTagList> Lst = htmlRemove.SplitHTMLTag(modelReport.chi_chartering_method.reason);
            //List<htmlTagList> list = htmlRemove.RearrangeHtmlTagList(Lst);
            //if (list.Count > 0)
            //{
            //    for (int i = 0; i < list.Count; i++)
            //    {
            //        row = table.AddRow(); icell = 0;
            //        row.Format.Font.Size = FontSize - 1;
            //        row.Height = 12;
            //        row.Format.Font.Bold = true;
            //        row.Format.Alignment = ParagraphAlignment.Left;
            //        row.VerticalAlignment = VerticalAlignment.Top;
            //        Paragraph _prg = row.Cells[0].AddParagraph();
            //        //_prg.AddFormattedText(HtmlRemoval.ValuetoParagraph(modelReport.explanation));
            //        htmlRemove.ValuetoParagraph(list[i], _prg);
            //        row.Cells[icell].MergeRight = 18;
            //        row.Cells[icell].Borders.Top.Color = Colors.White;
            //    }
            //} else
            //{
            //    row = table.AddRow(); icell = 0;
            //    row.Format.Font.Size = FontSize - 1;
            //    row.Height = 12;
            //    row.Format.Font.Bold = true;
            //    row.Format.Alignment = ParagraphAlignment.Left;
            //    row.VerticalAlignment = VerticalAlignment.Top;
            //    row.Cells[0].AddParagraph();
            //    row.Cells[icell].MergeRight = 18;
            //    row.Cells[icell].Borders.Top.Color = Colors.White;
            //}
            if (!string.IsNullOrEmpty(modelReport.chi_chartering_method.reason))
            {
                Paragraph _prg = row.Cells[0].AddParagraph();
                htmlRemove.ValuetoParagraph(modelReport.chi_chartering_method.reason, _prg);
            }
            else
            {
                row.Cells[0].AddParagraph("");
            }

        }
        HtmlRemoval htmlRemove = new HtmlRemoval();
        private void OfferTableZone(Section section, ChiRootObject modelReport, int itemOfferIndex)
        {
            
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize - 2;
            this.table.Rows.Height = defaultHeight;
            this.table.Format.Alignment = ParagraphAlignment.Center;

            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }
            #region--------HeaddingTable-------------

            
            Row row = table.AddRow();
            row.Cells[0].AddParagraph("Table1 : Applied for \"Fixing Counter\"");
            row.Cells[0].MergeRight = 37;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            //row = table.AddRow();
            //1 -> 16, 7
            //2 -> 8, 5
            //3 -> 6, 5
            //4 -> 4, 7
            //5 -> 4, 3
            int offer_count = 0;
            int offer_width = 0;
            int fixing_width = 0;
            if (itemOfferIndex + 5 <= modelReport.chi_negotiation_summary.chi_offers_items[0].chi_round_items.Count)
            {
                offer_count = 5;
            } else
            {
                offer_count = modelReport.chi_negotiation_summary.chi_offers_items[0].chi_round_items.Count % 5;
            }
            switch (offer_count)
            {
                case 1: offer_width = 16; fixing_width = 7; break;
                case 2: offer_width = 8; fixing_width = 7; break;
                case 3: offer_width = 6; fixing_width = 5; break;
                case 4: offer_width = 4; fixing_width = 7; break;
                case 5: offer_width = 4; fixing_width = 3; break;
                default: offer_width = 4; fixing_width = 3; break;
            }

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            row.Cells[icell].AddParagraph("Broker");
            row.Cells[icell].MergeRight = 2;
            row.Cells[0].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Targeted Vessel");
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;
            int icellQ = icell;
            row.Cells[icell].AddParagraph("Owner");
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;
            int icelR2 = icell;
            //int ttSt = (2 * (Quantity.Count - 1));
            row.Cells[icell].AddParagraph("Offer and Counter");
            row.Cells[icell].MergeRight = 22;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Final Deal");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].MergeDown = 2;            
            icell += row.Cells[icell].MergeRight + 1;


            icell = icelR2;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Fixing Items");
            row.Cells[icell].MergeRight = fixing_width - 1;
            row.Cells[icell].MergeDown = 1;
            icell += row.Cells[icell].MergeRight + 1;
            icelR2 = icell;
            for (int i = 0; i < offer_count; i++)
            {
                row.Cells[icell].AddParagraph(string.Format("{0}", _FN.GetORDINALNUMBERS(itemOfferIndex + (i + 1))));
                row.Cells[icell].MergeRight = offer_width - 1;
                if (i != offer_count - 1)
                    icell += row.Cells[icell].MergeRight + 1;
            }

            icell = icelR2;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            for (int i = 0; i < offer_count; i++)
            {
                row.Cells[icell].AddParagraph("Offer");
                row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("Counter");
                row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                icell += row.Cells[icell].MergeRight + 1;
            }

            #endregion----------------------------------

            //Loop Bind Data
            for(int i = 0; i < modelReport.chi_negotiation_summary.chi_offers_items.Count; i++)
            {
                
                var _offerItem = modelReport.chi_negotiation_summary.chi_offers_items[i];
                int MergeDown =  _offerItem.chi_round_items[itemOfferIndex].chi_round_info.Count < 3 ? 2 : _offerItem.chi_round_items[itemOfferIndex].chi_round_info.Count - 1;
                icell = 0;
                row = table.AddRow();
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.broker, 2));
                row.Cells[icell].MergeRight = 2;
                row.Cells[icell].MergeDown = MergeDown;
                int vesselcol = row.Cells[icell].MergeRight + 1;
                icell += row.Cells[icell].MergeRight + 4;
                //row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.vessel, 4));
                //row.Cells[icell].MergeRight = 4;
                //row.Cells[icell].MergeDown =  MergeDown;
                //icell += row.Cells[icell].MergeRight + 1;

                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.owner, 2));
                row.Cells[icell].MergeRight = 2;
                row.Cells[icell].MergeDown =  MergeDown;
                icell += row.Cells[icell].MergeRight + 1;
                int rcell = icell;bool firstrow = true;string tmpVal = "";
                int count = 0;
                foreach (var item in _offerItem.chi_round_items[itemOfferIndex].chi_round_info)
                {
                    tmpVal = "";
                    if (!firstrow) {
                        row = table.AddRow();
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                    }

                    if (count == 0)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.vessel, 2));
                        row.Cells[vesselcol].MergeRight = 2;
                        row.Cells[vesselcol].MergeDown = MergeDown - 2;
                        count++;
                    }
                    else if (count == (MergeDown - 1))
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.year, 2));
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }
                    else if (count == MergeDown)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.KDWT , 3)).AddFormattedText(" KDWT",TextFormat.Bold);
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    } else
                    {
                        row.Cells[vesselcol].AddParagraph("");
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }

                    icell = rcell;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], item.type, fixing_width - 1));
                    row.Cells[icell].MergeRight = fixing_width - 1;
                    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
                    icell += row.Cells[icell].MergeRight + 1;

                    for (int j = 0; j < offer_count; j++)
                    {
                        tmpVal = _offerItem.chi_round_items[itemOfferIndex + j].chi_round_info.Where(x => x.type == item.type).First().offer;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(tmpVal), (offer_width / 2)));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                        tmpVal = _offerItem.chi_round_items[itemOfferIndex + j].chi_round_info.Where(x => x.type == item.type).First().counter;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(tmpVal), (offer_width / 2)));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                    }
                    if (firstrow)
                    {
                        
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.final_deal, 5));
                        row.Cells[icell].MergeRight = 5;
                        row.Cells[icell].MergeDown = MergeDown;
                        firstrow = false;
                    }

                }

                for (int j = count; j < 3; j++)
                {
                    if (j>= 1)
                    {
                        row = table.AddRow();
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                    }
                    if (j == 0)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.vessel, 2));
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }
                    else if (j == 1)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.year, 2));
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }
                    else if (j == 2)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.KDWT, 3)).AddFormattedText(" KDWT", TextFormat.Bold); ;
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }

                    icell = rcell;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], "", fixing_width - 1));
                    row.Cells[icell].MergeRight = fixing_width - 1;
                    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
                    icell += row.Cells[icell].MergeRight + 1;

                    for(int k = 0; k < offer_count; k++)
                    {
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], "", 2));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], "", 2));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                    }
                }

            }


        }

        private void BottomZoneReport(Section section, ChiRootObject modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;
            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }
            int icell = 0;
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].Format.Font.Size = 12;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 37;
            row.Cells[icell].AddParagraph("Final Fixing Detail");

            row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            Table tmpTable = new Table();
            MakeTableFinalFixDetail(modelReport.chi_proposed_for_approve, modelReport.chi_cargo_detail, tmpTable, true);
            row.Cells[icell].MergeRight = 14;
            row.Cells[icell].Elements.Add(tmpTable);
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            tmpTable = new Table();
            MakeTableFinalFixDetail(modelReport.chi_proposed_for_approve, modelReport.chi_cargo_detail, tmpTable, false);
            row.Cells[icell].MergeRight = 22;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Elements.Add(tmpTable);

            if (!isLumpsum)
            {
                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Height = 13;
                row.Format.Font.Size = FontSize - 1;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Flat rate " + modelReport.chi_evaluating.year + " (USD/MT)");
                row.Cells[icell].MergeRight = 14;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(modelReport.chi_proposed_for_approve.flat_rate);
                row.Cells[icell].MergeRight = 22;
            }

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Estimated total freight cost (USD)");
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(modelReport.chi_proposed_for_approve.est_total));
            row.Cells[icell].MergeRight = 22; 

            if(!String.IsNullOrEmpty(modelReport.chi_proposed_for_approve.address_com_percent))
            {
                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Height = 13;
                row.Format.Font.Size = FontSize - 1;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Address Commission (%)");
                row.Cells[icell].MergeRight = 14;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(modelReport.chi_proposed_for_approve.address_com_percent));
                row.Cells[icell].MergeRight = 22;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Height = 13;
                row.Format.Font.Size = FontSize - 1;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Address Commission (USD)");
                row.Cells[icell].MergeRight = 14;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(modelReport.chi_proposed_for_approve.address_com));
                row.Cells[icell].MergeRight = 22;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Height = 13;
                row.Format.Font.Size = FontSize - 1;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Net Freight (USD)");
                row.Cells[icell].MergeRight = 14;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(modelReport.chi_proposed_for_approve.net_freight));
                row.Cells[icell].MergeRight = 22;
            }

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Estimated freight per unit BBL (USD/BBL)");
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(modelReport.chi_proposed_for_approve.est_freight));
            row.Cells[icell].MergeRight = 22;
        }

        private void FooterReport2(Section section, ChiRootObject modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            //row.Cells[0].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy");
            //row.Cells[0].MergeRight = 37;

            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Requested By");
            row.Cells[0].MergeRight = 14;
            row.Cells[15].AddParagraph("Endorsed by");
            row.Cells[15].MergeRight = 10;
            row.Cells[26].AddParagraph("Approved by");
            row.Cells[26].MergeRight = 11;

            row = table.AddRow();
            row.Height = 18;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Right;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Charterer :");
            row.Cells[0].MergeRight = 5;
            row.Cells[0].MergeDown = 1;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[3].AddParagraph("____________________");
            _img = row.Cells[6].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1"));
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[6].Borders.Bottom.Color = Colors.White;
            row.Cells[6].MergeRight = 8;
            row.Cells[15].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[15].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[7].AddParagraph("________________________");
            _img = row.Cells[15].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2"));
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[15].Borders.Bottom.Color = Colors.White;
            row.Cells[15].MergeRight = 10;
            row.Cells[15].MergeDown = 1;
            row.Cells[15].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[26].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[26].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[13].AddParagraph("________________________");
            _img = row.Cells[26].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3"));
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[26].Borders.Bottom.Color = Colors.White;
            row.Cells[26].MergeRight = 11;
            row.Cells[26].MergeDown = 1;

            row = table.AddRow();
            row.Height = 18;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[6].AddParagraph(string.Format("( CMCS-{0} )", _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1")));
            row.Cells[6].Borders.Top.Color = Colors.White;
            row.Cells[6].MergeRight = 8;

            row = table.AddRow();
            row.Height = 18;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Right;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("S/H :");
            row.Cells[0].MergeRight = 5;
            row.Cells[0].MergeDown = 1;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[3].AddParagraph("____________________");
            _img = row.Cells[6].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", true));
            _img.Width = new Unit(3, UnitType.Centimeter);

            row.Cells[6].Borders.Bottom.Color = Colors.White;
            row.Cells[6].MergeRight = 8;
            row.Cells[15].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[15].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[15].Borders.Top.Color = Colors.White;
            row.Cells[15].AddParagraph("");
            row.Cells[15].Borders.Bottom.Color = Colors.White;
            row.Cells[15].MergeRight = 10;
            row.Cells[26].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[26].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[26].Borders.Top.Color = Colors.White;
            row.Cells[26].Borders.Bottom.Color = Colors.White;
            row.Cells[26].AddParagraph("");
            row.Cells[26].MergeRight = 11;

            row = table.AddRow();
            row.Height = 18;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[6].AddParagraph(string.Format("( CMCS-{0} )", _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1", true)));
            row.Cells[6].Borders.Top.Color = Colors.White;
            row.Cells[6].MergeRight = 8;
            row.Cells[15].AddParagraph("CMVP");
            row.Cells[15].Borders.Top.Color = Colors.White;
            row.Cells[15].MergeRight = 10;
            row.Cells[26].AddParagraph("EVPC");
            row.Cells[26].Borders.Top.Color = Colors.White;
            row.Cells[26].MergeRight = 11;

        }

        private void FooterReport(Section section, ChiRootObject model)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;

            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            var approveName_1 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            }
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            approveName_1 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            }
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve4
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            if (!string.IsNullOrEmpty(approveName))
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
           // _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            //Approve3
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            //if (!string.IsNullOrEmpty(approveName))
            //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            //else
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            approveName = !String.IsNullOrEmpty(approveName_1) ? _FN.GetNameOffApprove(model.approve_items, "APPROVE_1") : _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = !String.IsNullOrEmpty(approveName_1) ? _FN.GetNameOffApprove(model.approve_items, "APPROVE_2") : _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            // approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

        }

        /*
        private void FooterReport(Section section, ChiRootObject modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            //row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy");
            //row.Cells[icell].MergeRight = 37;
            //icell += row.Cells[icell].MergeRight + 1;

            //icell = 0;
            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested By");
            row.Cells[icell].MergeRight = 17;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Certified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Charterer");
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", ref height, ref width, true));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve2
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve3
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph(string.Format("( CMCS-{0} )", _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1")));
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.Format("( CMCS-{0} )", _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1"), true));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

        }
        */

        private void ExplanImageReport(Section section, ChiRootObject modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            }



            string[] _image = modelReport.explanationAttach.Split('|');
            foreach (var _item in _image)
            {
                if (_item != "")
                {
                    string[] itemsplit = _item.ToString().Split(':');
                    Row row = table.AddRow();
                    int icell = 0;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Format.Font.Bold = true;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    icell += row.Cells[icell].MergeRight + 1;
                    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetExplanImage(itemsplit[0], URL));
                    if (!string.IsNullOrEmpty(URL))
                    {
                        images.Add(_img.Name);
                    }
                    _img.Width = new Unit(11, UnitType.Centimeter);
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].MergeRight = 12;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Borders.Bottom.Color = Colors.White;
                    if (itemsplit.Length > 0)
                    {
                        row = table.AddRow();
                        icell = 0;
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Format.Font.Bold = true;
                        row.Cells[icell].AddParagraph(itemsplit[1]);
                        row.Cells[icell].MergeRight = 18;
                    }
                }
            }
        }

    public void MakeTableOfferItem(List<ChiRoundItem> Detail, Table table, int indexOffer)
    {
        table.Borders.Color = Colors.Black;
        table.Format.Font.Name = FontName;
        table.Format.Font.Size = FontSize - 1;
        table.Rows.Height = 8;
        for (int i = 0; i < 17; i++)
        {
            table.AddColumn("0.5cm");
        }
        var _RoundItem = new List<ChiRoundItem>();
        for (int i=0;i< 2;i++)
        {
            if (Detail.Where(x => x.round_no == (indexOffer+i).ToString()).ToList().Count > 0)
            {
                _RoundItem.Add(Detail.Where(x => x.round_no == (indexOffer + i).ToString()).ToList()[0]);
            }
        }
        if (_RoundItem.Count > 0)
        {
            foreach (var _item in _RoundItem[0].chi_round_info)
            {
                int icell = 0;
                Row row = table.AddRow();
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;

                row.Cells[icell].AddParagraph(_item.type);
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_item.offer);
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_item.counter);
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_item.offer);
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_item.offer);
                icell += row.Cells[icell].MergeRight + 1;
            }
        }

    }

        public void MakeTableEvaluation(ChiEvaluating Detail, Table table)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 1;
            table.Rows.Height = 8;
            table.AddColumn("2.5cm");
            table.AddColumn("2.5cm");
            table.AddColumn("2.5cm");
            table.AddColumn("2.5cm");
            table.AddColumn("2.5cm");
            table.AddColumn("2.5cm");
            int icell = 0;
            Row row = table.AddRow();
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;

            row.Cells[icell].AddParagraph("Loading Month:");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(Detail.loading_month + "-" + Detail.loading_year);
            row.Cells[icell].Format.Font.Bold = false;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Vessel Laycan:");
            icell += row.Cells[icell].MergeRight + 1;
            string laycan_date = "";
            if (!string.IsNullOrEmpty(Detail.laycan_from))
            {
                laycan_date = _FN.ConvertDateFormatBackFormat(Detail.laycan_from, "dd MMM yyyy") + " to " + _FN.ConvertDateFormatBackFormat(Detail.laycan_to, "dd MMM yyyy");
            }
            row.Cells[icell].AddParagraph(laycan_date);
            row.Cells[icell].Format.Font.Bold = false;
            row.Cells[icell].MergeRight = 2;


            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Freight Type:");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(Detail.freight);
            row.Cells[icell].Format.Font.Bold = false;
            icell += row.Cells[icell].MergeRight + 1;
            if (Detail.freight != "LUMPSUM")
            {
                row.Cells[icell].AddParagraph("Flat rate:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.flat_rate);
                row.Cells[icell].Format.Font.Bold = false;
                icell += row.Cells[icell].MergeRight + 1;
                isLumpsum = false;
            }
            row.Cells[icell].AddParagraph("at year:");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(Detail.year);
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Vessel Size:");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(Detail.vessel_size);
            row.Cells[icell].Format.Font.Bold = false;
            row.BottomPadding = new Unit(1, UnitType.Millimeter);
            row.Cells[icell].MergeRight = 4;
        }

        public void MakeTableCargoDetail(ChiCargoDetail Detail, Table table)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 1;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = 8;
            table.AddColumn("1cm");
            table.AddColumn("3cm");
            table.AddColumn("2.5cm");
            table.AddColumn("2.5cm");
            table.AddColumn("1cm");
            table.AddColumn("1cm");
            table.AddColumn("2cm");
            table.AddColumn("2cm");
            int icell = 0;
            Row row = table.AddRow();
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;

            row.Cells[icell].AddParagraph(string.Format("Route: {0}", Detail.route));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Loading Port");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Grade");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Supplier");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("KB");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("KT");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Tolerance(%)");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Loading Date");
            row.Cells[icell].Format.Font.Underline = Underline.Single;

            int i = 1;
            foreach (var item in Detail.chi_load_ports)
            {
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph(_FN.GetORDINALNUMBERS(i));
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(item.port);
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(item.crude);
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(item.supplier);
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], item.kbbls));
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], item.ktons));
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], item.tolerance));
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], item.date));
                icell += row.Cells[icell].MergeRight + 1;
                i++;
            }

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Discharging Port");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            i = 1;
            foreach (var item in Detail.discharge_ports)
            {
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph(_FN.GetORDINALNUMBERS(i));
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(item.dis_port);
                row.BottomPadding = new Unit(1, UnitType.Millimeter);
                i++;
            }
        }

        public void MakeTableFinalFixDetail(ChiProposedForApprove Detail, ChiCargoDetail Cargo, Table table,bool Table1)
        {
            table.Borders.Color = Colors.Black;
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 1;
            table.Rows.Height = 8;
            table.AddColumn("3cm");
            table.AddColumn(new Unit(4.35, UnitType.Centimeter));
            int icell = 0;
            Row row = table.AddRow();
            if (Table1)
            {
                #region-----------------Table1----------------------
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Owner:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.owner);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Charter Party:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.charter_patry);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Owner's Broker:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.owner_broker);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Charterer's Broker:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.charterer_broker);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Vessel's Name:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.vessel_name);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Laycan:");
                icell += row.Cells[icell].MergeRight + 1;
                string laycan_date = "";
                if (!string.IsNullOrEmpty(Detail.laycan_from))
                {
                    laycan_date = _FN.ConvertDateFormatBackFormat(Detail.laycan_from, "dd MMM yyyy") + " to " + _FN.ConvertDateFormatBackFormat(Detail.laycan_to, "dd MMM yyyy");
                }
                row.Cells[icell].AddParagraph(laycan_date);
                row.Cells[icell].Format.Font.Bold = false;

                foreach(var item in Cargo.chi_load_ports)
                {
                    row = table.AddRow(); icell = 0;
                    row.Format.Font.Bold = true;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[icell].AddParagraph(string.Format("Loading Port {0}:", item.order_port));
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(item.port);
                    row.Cells[icell].Format.Font.Bold = false;
                }

                foreach(var item in Cargo.discharge_ports)
                {
                    row = table.AddRow(); icell = 0;
                    row.Format.Font.Bold = true;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[icell].AddParagraph(string.Format("Discharging Port {0}:", item.dis_order_port));
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(item.dis_port);
                    row.Cells[icell].Format.Font.Bold = false;
                }

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Laytime:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.laytime);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Crude oil washing:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.crude_oil_washing);
                row.Cells[icell].Format.Font.Bold = false;

                #endregion-------------------------------------
            }
            else
            {
                #region-----------------Table2----------------------
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Vessel's Built:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.vessel_built);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Capacity:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.capacity).AddFormattedText(" KDWT", TextFormat.Bold);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Laden Speed:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.laden_speed);
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Arm Guard:");
                row.Cells[icell].Borders.Bottom.Color = Colors.White;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(Detail.exten_cost);
                row.Cells[icell].Format.Font.Bold = false;
                row.Cells[icell].MergeDown = 2;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("War Risk:");
                row.Cells[icell].Borders.Bottom.Color = Colors.White;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("");
                row.Cells[icell].Format.Font.Bold = false;

                row = table.AddRow(); icell = 0;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph("Deviation:");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("");
                row.Cells[icell].Format.Font.Bold = false;
                #endregion-------------------------------------
            }
        }


    }
}