﻿using com.pttict.engine.utility;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Newtonsoft.Json;
using PdfSharp.Pdf;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALMaster;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;

namespace ProjectCPAIEngine.Web.Report
{
    public partial class CharteringOutCMMTReport : System.Web.UI.Page
    {
        ShareFn _FN = new ShareFn();
        ReportFunction _RFN = new ReportFunction();
        Document document;
        MigraDoc.DocumentObjectModel.Tables.Table table;
        Color TableBorder;
        Color TableGray;
        const int defaultHeight = 10;
        HtmlRemoval _htmlRemove = new HtmlRemoval();
        int FontSize = 8;
        string URL = "";
        List<string> images = new List<string>();
        string FontName = "Tahoma";
        string type = ConstantPrm.SYSTEMTYPE.VESSEL;
        string unit = "USD";

        #region -- Report New ---

        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadTest("201707261639430146309");
            //LoadXMLFromTest();
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload")))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload"));
            }
            if (Request.QueryString["TranID"] != null)
            {
                LoadDataCharter(Request.QueryString["TranID"].ToString().Decrypt());
            }
            else
            {
                if (Session["chot_object_root"] != null)
                {
                    ChotObjectRoot _model = (ChotObjectRoot)Session["chot_object_root"];
                    GenPDF(_model, true);
                    Session["chot_object_root"] = null;
                }
            }
        }

        private void LoadXMLFromTest()
        {
            string PathTest = @"C:\Users\CHOLLAPHAS\Documents\Visual Studio 2015\Chartering Out CMMT.json";
            if (File.Exists(PathTest))
            {

                try
                {
                    StreamReader _sr = new StreamReader(PathTest);
                    string Line = _sr.ReadToEnd();
                    //Line = Line.Replace("\r\n ", "");
                    //Line = Line.Replace("\"", "");

                    //DataTable dt = (DataTable)JsonConvert.DeserializeObject(Line, (typeof(DataTable)));
                    //DataSet data = JsonConvert.DeserializeObject<DataSet>(Line);
                    //ChosRootObject obj = new ChosRootObject();
                    //var json = JsonConvert.SerializeObject(Line);
                    //var json = new JavaScriptSerializer().Serialize(obj);


                    ChotObjectRoot _modelReport = JsonConvert.DeserializeObject<ChotObjectRoot>(Line);

                    GenPDF(_modelReport, true);

                }
                catch (Exception e)
                {

                }


            }
        }

        public string LoadDataCharter(string TransactionID, bool redirect = true, string UserName = "")
        {
            string pdfPath = "";
            RequestData reqData = new RequestData();
            reqData.function_id = ConstantPrm.FUNCTION.F10000005;
            reqData.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            reqData.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            reqData.req_transaction_id = ConstantPrm.EnginGetEngineID();
            reqData.state_name = "";
            reqData.req_parameters = new req_parameters();
            reqData.req_parameters.p = new List<p>();
            reqData.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            reqData.req_parameters.p.Add(new p { k = "user", v = (string.IsNullOrEmpty(UserName)) ? Const.User.UserName : UserName });
            reqData.req_parameters.p.Add(new p { k = "transaction_id", v = TransactionID });
            reqData.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
            reqData.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.VESSEL });
            reqData.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(reqData);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                string pattern = "[\"](\\w+)[\"]:null";
                Regex rgx = new Regex(pattern);
                string replacement = "\"$1\":\"\"";
                string result = rgx.Replace(_model.data_detail, replacement);
                result = result.Replace("\"approve_items\":\"\"", "\"approve_items\":null");
                result = result.Replace("\"chot_charter_out_case_a_other_costs\":\"\"", "\"chot_charter_out_case_a_other_costs\":[]");
                result = result.Replace("\"chot_est_expense_b_two_point_one\":\"\"", "\"chot_est_expense_b_two_point_one\":null");
                result = result.Replace("\"chot_est_expense_b_two_point_two\":\"\"", "\"chot_est_expense_b_two_point_two\":null");
                result = result.Replace("\"chot_cargo\":\"\"", "\"chot_cargo\":[]");
                result = result.Replace("\"chot_loading_port\":\"\"", "\"chot_loading_port\":[]");
                result = result.Replace("\"chot_discharging_port\":\"\"", "\"chot_discharging_port\":[]");
                result = result.Replace("\"chot_saving\":\"\"", "\"chot_saving\":null");
                result = result.Replace("\"profit\":\"\"", "\"profit\":null");
                ChotObjectRoot _modelRoot = new JavaScriptSerializer().Deserialize<ChotObjectRoot>(result);
                if (!redirect)
                    URL = JSONSetting.getGlobalConfig("ROOT_URL");
                pdfPath = GenPDF(_modelRoot, redirect);
                if (!redirect)
                {
                    for (int i = 0; i < images.Count(); i++)
                    {
                        File.Delete(images[i]);
                    }
                }
            }
            else
            {
                if (redirect) _FN.MessageBoxShowLink(this.Page, resData.response_message, "MainBoards.aspx");
            }

            return pdfPath;
        }

        private void LoadTest(string TranID)
        {
            LoadData(TranID);
        }

        public string LoadData(string TransactionID, bool redirect = true, string UserName = "")
        {
            string pdfPath = "";

            string _model = CharterOutServiceModel.getTransactionCMMTByID(TransactionID);
            if (_model != null)
            {
                string pattern = "[\"](\\w+)[\"]:null";
                Regex rgx = new Regex(pattern);
                string replacement = "\"$1\":\"\"";
                string result = rgx.Replace(_model, replacement);
                result = result.Replace("\"approve_items\":\"\"", "\"approve_items\":null");
                //_model = _model.Replace("null", "\"\"");
                ChotObjectRoot _modelReport = new JavaScriptSerializer().Deserialize<ChotObjectRoot>(result);
                pdfPath = GenPDF(_modelReport, redirect);
            }
            else
            {
                if (redirect) _FN.MessageBoxShowLink(this.Page, "Error", "MainBoards.aspx");
            }
            return pdfPath;
        }

        void DefineStyles()
        {
            MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
            style.Font.Name = FontName;

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = FontName;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 10;

            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        private string GenPDF(ChotObjectRoot modelReport, bool redirect)
        {
            Document document = CreateDocument(modelReport);

            document.UseCmykColor = true;
            const bool unicode = true;
            const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, embedding);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();
            // Save the document...
            string filename = "";
            string sVessel = "VESSEL";
            string sDate = DateTime.Now.ToString("ddMMyyyy");

            if (!string.IsNullOrEmpty(modelReport.vessel_id))
            {
                sVessel = VehicleServiceModel.GetNameInitial(modelReport.vessel_id);
            }
            if (!string.IsNullOrEmpty(modelReport.chot_date))
            {
                sDate = modelReport.chot_date.Replace("/", "");
            }
            if (!string.IsNullOrEmpty(modelReport.vessel_id))
            {
                filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile",
                    string.Format("{0}{1}-{2}-{3}.pdf", "CHRO", sDate.Substring(4, 4), sVessel, sDate));
            }
            else
            {
                filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile",
                    string.Format("{0}{1}-{2}-{3}.pdf", "CHRO", DateTime.Now.Year, sVessel, sDate));
            }
            pdfRenderer.PdfDocument.Save(filename);
            if (redirect) Response.Redirect(string.Format("TmpFile/{0}", new FileInfo(filename).Name));
            // ...and start a viewer.
            //Process.Start(filename);
            return filename;
        }

        public Document CreateDocument(ChotObjectRoot modelReport)
        {
            TableGray = Colors.Blue;
            TableBorder = Colors.Black;
            TableGray = Colors.Gray;
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = "Charter Out";
            this.document.DefaultPageSetup.LeftMargin = 30;
            this.document.DefaultPageSetup.TopMargin = 30;
            this.document.DefaultPageSetup.BottomMargin = 30;
            DefineStyles();
            _RFN.document = document;
            _RFN.FontName = FontName;
            CreatePage(modelReport);
            return this.document;
        }

        void CreatePage(ChotObjectRoot modelReport)
        {
            Section section = this.document.AddSection();
            HeadingReport(section, modelReport);
            ReasonDetail(section, modelReport);
            NoCharterOutCaseA_Report(section, modelReport);


            CharterOutCaseB_Report(section, modelReport);
            CharterOutSummary_Report(section, modelReport);
            CharterOutResult_Report(section, modelReport);
            CharterOutNote_Report(section, modelReport);

            FooterReport(section, modelReport);

            if (!string.IsNullOrEmpty(modelReport.explanationAttach))
            {
                document.LastSection.AddPageBreak();
                HeadingReport(section, modelReport, true);
                ExplanImageReport(section, modelReport);
            }

        }
        Setting _setting = JSONSetting.getSetting("JSON_CHARTER_OUT_VESSEL");
        HtmlRemoval htmlRemove = new HtmlRemoval();
        private void HeadingReport(Section section, ChotObjectRoot modelReport, bool AttachPage = false)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            // Create the header of the table
            Row row = table.AddRow();
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;

            int icell = 0;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            // Header
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].AddParagraph("Spot Chartering Out");
            row.Cells[icell].Format.Font.Size = 10;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 1;
            // Logo
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;

            // For Vessel
            row.Cells[icell].AddParagraph("For Vessel :");
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph(nullToEmtry(VehicleServiceModel.GetName(modelReport.vessel_id)));
            row.Cells[icell].Format.LeftIndent = new Unit(1, UnitType.Millimeter);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            // On Subject Date
            row.Cells[icell].AddParagraph("Date : " + nullToEmtry(modelReport.chot_date));
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;
            icell += row.Cells[icell].MergeRight + 1;
        }

        private void ReasonDetail(Section section, ChotObjectRoot modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }
            //Reason 
            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;

            int icell = 0;

            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].AddParagraph("Reason for Chartering Out :");
            row.Cells[icell].MergeRight = 18;

            if (!string.IsNullOrEmpty(modelReport.chot_reason_explanation))
            {
                Paragraph _prg = row.Cells[0].AddParagraph();
                htmlRemove.ValuetoParagraph(modelReport.chot_reason_explanation, _prg);
            }
            else
            {
                row.Cells[0].AddParagraph("");
            }
        }

        private void NoCharterOutCaseA_Report(Section section, ChotObjectRoot modelReport)
        {

            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Left.Color = Colors.Black;
            this.table.Borders.Right.Color = Colors.Black;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("0.5cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = this.table.AddColumn("9.25cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = this.table.AddColumn("9.25cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            Row row = table.AddRow(); int icell = 0;
            row.Cells[icell].AddParagraph("A) No Charter Out Case");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 2;

            // Detail
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Right.Color = Colors.White;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            Table tmpTable = new Table();
            MakeTableCharterOutCaseA_Sub_Report(modelReport, tmpTable, unit);
            row.Cells[icell].Elements.Add(tmpTable);


            // Footer
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;




        }

        public void MakeTableCharterOutCaseA_Sub_Report(ChotObjectRoot modelReport, Table table, string unit)
        {

            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 8;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 17; c++)
            {
                column = table.AddColumn("0.5cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Top.Color = Colors.Black;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Est. Expense");
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Size = FontSize - 1;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            Table tmpTable = new Table();
            MakeTableNoCharterOutCaseA_SubDetail_Report(modelReport, tmpTable, unit);
            row.Cells[icell].Elements.Add(tmpTable);
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;


            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

        }

        public void MakeTableNoCharterOutCaseA_SubDetail_Report(ChotObjectRoot modelReport, Table table, string unit)
        {

            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 6;

            Column column = table.AddColumn("0.25cm");
            for (int c = 0; c < 36; c++)
            {
                column = table.AddColumn("0.25cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;

            row = table.AddRow(); icell = 1;
            row.Cells[icell].AddParagraph("Period");
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            //row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_no_charter_out_case_a.period));
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 26;

            row = table.AddRow(); icell = 1;
            row.Cells[icell].AddParagraph("Time Charter Cost");
            row.Cells[icell].MergeRight = 15;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("No. Days");
            row.Cells[icell].MergeRight = 4;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow(); icell = 1;
            row.Cells[icell].AddParagraph("TC Hire");
            row.Cells[icell].MergeRight = 4;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_no_charter_out_case_a.tc_hire)));
            row.Cells[icell].MergeRight = 4;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("USD/Day");
            row.Cells[icell].MergeRight = 4;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(modelReport.chot_no_charter_out_case_a.no_day));
            row.Cells[icell].MergeRight = 4;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_no_charter_out_case_a.tc_hire_val)));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 1;
            row.Cells[icell].AddParagraph("Port Charge + Agency Fee (Idling)");
            row.Cells[icell].MergeRight = 19;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_no_charter_out_case_a.idling)));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 1;
            row.Cells[icell].AddParagraph("Bunker");
            row.Cells[icell].MergeRight = 19;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_no_charter_out_case_a.bunker)));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            for (int i = 0; i < modelReport.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs.Count(); i++)
            {
                if (modelReport.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs.Count() == 1)
                {
                    row = table.AddRow(); icell = 1;
                    row.Cells[icell].AddParagraph("Other Costs : ");
                    row.Cells[icell].MergeRight = 7;
                    icell += row.Cells[icell].MergeRight + 1;
                } else
                {
                    row = table.AddRow(); icell = 1;
                    row.Cells[icell].AddParagraph("Other Costs " + (i + 1) + " : ");
                    row.Cells[icell].MergeRight = 7;
                    icell += row.Cells[icell].MergeRight + 1;
                }

                row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs[i].detail));
                row.Cells[icell].MergeRight = 11;
                icell += row.Cells[icell].MergeRight + 1;

                string value_costs = string.IsNullOrEmpty(modelReport.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs[i].value) ? "-" : _FN.intTostrMoney(modelReport.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs[i].value);
                row.Cells[icell].AddParagraph(value_costs);
                row.Cells[icell].MergeRight = 8;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;

                row.Cells[icell].AddParagraph(unit);
                row.Cells[icell].MergeRight = 2;
            }
            row = table.AddRow();
            row = table.AddRow(); icell = 1;
            row.Cells[icell].AddParagraph("Total TC Expense (A)");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 19;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_no_charter_out_case_a.total_expense)));
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;
        }

        private void CharterOutCaseB_Report(Section section, ChotObjectRoot modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Left.Color = Colors.Black;
            this.table.Borders.Right.Color = Colors.Black;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("0.5cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = this.table.AddColumn("9.25cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = this.table.AddColumn("9.25cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            Row row = table.AddRow(); int icell = 0;
            row.Cells[icell].AddParagraph("B) Charter Out Case");
            //row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 2;

            // Detail
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Right.Color = Colors.White;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            Table tmpTable = new Table();
            MakeTableCharterOutCaseBOne_Sub_Report(modelReport, unit, tmpTable);
            row.Cells[icell].Elements.Add(tmpTable);

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            Table tmpTable2 = new Table();
            MakeTableChosCharterOutCaseBTwo_Sub_Report(modelReport, unit, tmpTable2);
            row.Cells[icell].Elements.Add(tmpTable2);

            // Footer
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        public void MakeTableCharterOutCaseBOne_Sub_Report(ChotObjectRoot obj, string unit, Table table)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 1;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = 8;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 17; c++)
            {
                column = table.AddColumn("0.5cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Center;

            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("B1) Est. Income");
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            row = table.AddRow();

            Table tmpTable = new Table();
            MakeTableCharterOutCaseBOne_SubDetail_Report(obj, tmpTable, unit);
            row.Cells[icell].Elements.Add(tmpTable);

            //row = table.AddRow();
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        public void MakeTableCharterOutCaseBOne_SubDetail_Report(ChotObjectRoot modelReport, Table table, string unit)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 6;

            Column column = table.AddColumn("0.25cm");
            for (int c = 0; c < 32; c++)
            {
                column = table.AddColumn("0.25cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;

            //Row 1
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Period");
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 3;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_charter_out_case_b.chot_est_income_b_one.period)); //after edit
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 12;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Laycan");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 3;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(modelReport.chot_charter_out_case_b.chot_est_income_b_one.laycan));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 11;

            //Row 2
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Exchange Rate");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 6;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(modelReport.chot_charter_out_case_b.chot_est_income_b_one.ex_rate));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("THB");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 3;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("As of");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 3;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(modelReport.chot_charter_out_case_b.chot_est_income_b_one.ex_date));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 11;

            //Row 3
            if (modelReport.chot_charter_out_case_b.chot_est_income_b_one.type.ToUpper() == "LUMPSUM")
            {
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph().AddFormattedText("\u00A2", new Font("Wingdings 2", FontSize - 2)).AddFormattedText(" " + "Lumpsum", new Font(FontName, FontSize - 2));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 5;

                icell += row.Cells[icell].MergeRight + 1;
                if (modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer.Contains("THB"))
                {
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_income_b_one.freight));
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[icell].MergeRight = 6;

                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("THB");
                }
                else if (modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer.Contains("USD"))
                {
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_income_b_one.freight));
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[icell].MergeRight = 6;

                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("USD");
                }

                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 4;

                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph().AddFormattedText("\u00A3", new Font("Wingdings 2", FontSize - 2)).AddFormattedText(" " + "Unit Price", new Font(FontName, FontSize - 2));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 5;
            }
            else if (modelReport.chot_charter_out_case_b.chot_est_income_b_one.type.ToUpper() == "UNIT PRICE")
            {
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph().AddFormattedText("\u00A3", new Font("Wingdings 2", FontSize - 2)).AddFormattedText(" " + "Lumpsum", new Font(FontName, FontSize - 2));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 5;

                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph().AddFormattedText("\u00A2", new Font("Wingdings 2", FontSize - 2)).AddFormattedText(" " + "Unit Price", new Font(FontName, FontSize - 2));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 5;

                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(modelReport.chot_charter_out_case_b.chot_est_income_b_one.freight);
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 4;

                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer);
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 4;

                icell += row.Cells[icell].MergeRight + 1;
                if (modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer.Contains("THB"))
                {
                    double totalValue = double.Parse(modelReport.chot_charter_out_case_b.chot_est_income_b_one.total_freight) / double.Parse(modelReport.chot_charter_out_case_b.chot_est_income_b_one.ex_rate);
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(totalValue.ToString()));
                }
                else
                {
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_income_b_one.total_freight));
                }
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 4;

                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("USD");
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 3;

                icell += row.Cells[icell].MergeRight + 1;
                if (modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer.Contains("THB"))
                {
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_income_b_one.total_freight));
                }
                else
                {
                    double totalValue = double.Parse(modelReport.chot_charter_out_case_b.chot_est_income_b_one.total_freight) * double.Parse(modelReport.chot_charter_out_case_b.chot_est_income_b_one.ex_rate);
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(totalValue.ToString()));
                }
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 4;

                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("THB");
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 2;
            }
            else
            {
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph().AddFormattedText("\u00A3", new Font("Wingdings 2", FontSize - 2)).AddFormattedText(" " + "Lumpsum", new Font(FontName, FontSize - 2));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 5;

                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph().AddFormattedText("\u00A3", new Font("Wingdings 2", FontSize - 2)).AddFormattedText(" " + "Unit Price", new Font(FontName, FontSize - 2));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].MergeRight = 5;
            }

            //blank Row

            row = table.AddRow();
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Charter Out : Total Income (B1)");
            // row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_income_b_one != null)
            {
                row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_income_b_one.total_income));

            }
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
        }

        public void MakeTableChosCharterOutCaseBTwo_Sub_Report(ChotObjectRoot modelReport, string unit, Table table)
        {

            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 8;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 17; c++)
            {
                column = table.AddColumn("0.5cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Size = FontSize - 1;
            row.Cells[icell].AddParagraph("B2) Est. Expense");
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            Table tmpTable = new Table();
            MakeTableCharterOutCaseBTwo_SubDetail_Report(modelReport, tmpTable, unit);
            row.Cells[icell].Elements.Add(tmpTable);

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        public void MakeTableCharterOutCaseBTwo_SubDetail_Report(ChotObjectRoot modelReport, Table table, string unit)
        {
            double port_charge = 0;
            double bunker = 0;
            double commission_total = 0;
            double withholding_tax_persent = 0;
            double withholding_tax_total = 0;
            double other = 0;
            double total_b_two_point_one = 0;

            double fix_costs = 0;
            double no_day = 0;
            double total_fixed_costs = 0;

            double total_val = 0;

            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 6;

            Column column = table.AddColumn("0.25cm");
            for (int c = 0; c < 36; c++)
            {
                column = table.AddColumn("0.25cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Period");
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.period));
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 26;

            row = table.AddRow(); icell = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("B2.1) Total Variable Cost");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 15;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 16;

            row = table.AddRow(); icell = 5;
            row.Cells[icell].AddParagraph("Port Charge");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 4;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 4;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.port_charge)));
                if (!string.IsNullOrEmpty(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.port_charge))
                    port_charge = Double.Parse(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.port_charge);
            }
            row.Cells[icell].MergeRight = 4;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 5;
            row.Cells[icell].AddParagraph("Bunker");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.bunker)));
                if (!string.IsNullOrEmpty(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.bunker))
                    bunker = Double.Parse(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.bunker);
            }
            row.Cells[icell].MergeRight = 14;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 5;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph("Total Commission " + nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_persent)) + " %");
            }
            else
            {
                row.Cells[icell].AddParagraph("Total Commission");
            }
            row.Cells[icell].MergeRight = 13;

            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_total)));
                if (!string.IsNullOrEmpty(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_total))
                    commission_total = Double.Parse(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_total);
            }
            row.Cells[icell].MergeRight = 10;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 5;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph("Withholding Tax " + nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_persent)) + " %");
                if (!string.IsNullOrEmpty(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_persent))
                    withholding_tax_persent = Double.Parse(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_persent);
            }
            else
            {
                row.Cells[icell].AddParagraph("Withholding Tax");
            }
            row.Cells[icell].MergeRight = 13;

            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_total)));
                if (!string.IsNullOrEmpty(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_total))
                    withholding_tax_total = Double.Parse(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_total);
            }
            row.Cells[icell].MergeRight = 10;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 5;
            row.Cells[icell].AddParagraph("Other");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.other)));
                if (!string.IsNullOrEmpty(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.other))
                    other = Double.Parse(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.other);
            }
            row.Cells[icell].MergeRight = 14;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 5;
            row.Cells[icell].AddParagraph("Total Variable Cost");            
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                total_b_two_point_one = port_charge + bunker + commission_total + withholding_tax_persent + withholding_tax_total + other;
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(total_b_two_point_one.ToString())));
            }
            //row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 14;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            //row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;


            row = table.AddRow(); icell = 0;


            row = table.AddRow(); icell = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("B2.2) Total Fixed Cost");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 16;

            row = table.AddRow(); icell = 5;
            row.Cells[icell].AddParagraph("Fixed Cost");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.fix_costs)));
                if (!string.IsNullOrEmpty(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.fix_costs))
                    fix_costs = double.Parse(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.fix_costs);
            }
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit + "/Day");
            row.Cells[icell].Format.Font.Size = FontSize - 3;
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 5;
            row.Cells[icell].AddParagraph("No.");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.no)));
                if (!string.IsNullOrEmpty(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.no))
                    no_day = double.Parse(modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.no);
            }
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Day");
            row.Cells[icell].MergeRight = 2;


            row = table.AddRow(); icell = 5;
            row.Cells[icell].AddParagraph("Total Fixed Cost");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                total_fixed_costs = fix_costs * no_day;
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(_FN.intTostrMoney2(total_fixed_costs.ToString()))));
            }
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;


            row = table.AddRow();


            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Charter Out : Total Expense (B2)");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one != null)
            {
                total_val = total_b_two_point_one + total_fixed_costs;
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(total_val.ToString())));
            }
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 14;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;
        }

        private void CharterOutSummary_Report(Section section, ChotObjectRoot modelReport)

        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Left.Color = Colors.Black;
            this.table.Borders.Right.Color = Colors.Black;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("0.25cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = this.table.AddColumn("9.25cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = this.table.AddColumn("9.50cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            Row row = table.AddRow(); int icell = 0;
            row.Cells[icell].AddParagraph("Summary");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;

            // Detail
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Right.Color = Colors.White;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            Table tmpTable = new Table();
            CharterOutSummary_Sub_Report_One(modelReport, tmpTable, unit);
            row.Cells[icell].Elements.Add(tmpTable);

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            Table tmpTable2 = new Table();
            CharterOutSummary_Sub_Report_Two(modelReport, tmpTable2, unit);
            row.Cells[icell].Elements.Add(tmpTable2);


            // Footer
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        public void CharterOutSummary_Sub_Report_One(ChotObjectRoot modelReport, Table table, string unit)
        {

            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 8;

            Column column = table.AddColumn("0.25cm");
            for (int c = 0; c < 36; c++)
            {
                column = table.AddColumn("0.25cm");
            }

            Row row = table.AddRow(); int icell = 0;

            //Charterer
            //row 1 col 1
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Charterer");
            row.Cells[icell].MergeRight = 10;

            //row 1 col 2
            icell += row.Cells[icell].MergeRight + 1;
            if (!string.IsNullOrEmpty(modelReport.chot_summary.charterer))
            {
                string mt_cust_detail = MT_CUST_DETAIL_DAL.GetCUSTDetailName(modelReport.chot_summary.charterer);
                row.Cells[icell].AddParagraph(mt_cust_detail);
            }
            else
            {
                row.Cells[icell].AddParagraph("");
            }
            row.Cells[icell].MergeRight = 19;

            //row 1 col 3
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 5;

            //CP Date
            //row 2 col 1
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("CP Date");
            row.Cells[icell].MergeRight = 10;

            //row 2 col 2
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.cp_date));
            row.Cells[icell].MergeRight = 14;

            //row 2 col 3
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 10;

            //Charterer Party Form
            //row 3 col 1
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Charter Party Form");
            row.Cells[icell].MergeRight = 10;

            //row 3 col 2
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.charterer_party_form));
            row.Cells[icell].MergeRight = 14;

            //row 3 col 3
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 10;

            if (modelReport.chot_summary.chot_cargo != null)
            {
                if (modelReport.chot_summary.chot_cargo.Count == 1)
                {
                    //Cargo 1
                    //row 4 col 1
                    row = table.AddRow(); icell = 0;
                    row.Cells[icell].AddParagraph("Cargo");
                    row.Cells[icell].MergeRight = 10;

                    //row 4 col 2
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.chot_cargo[0].cargo_name));
                    row.Cells[icell].MergeRight = 14;

                    //row 4 col 3
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 10;

                    //Quantity  1
                    //row 4 col 1
                    row = table.AddRow(); icell = 0;
                    row.Cells[icell].AddParagraph("Quantity");
                    row.Cells[icell].MergeRight = 10;

                    //row 4 col 2
                    icell += row.Cells[icell].MergeRight + 1;
                    if (!string.IsNullOrEmpty(modelReport.chot_summary.chot_cargo[0].cargo_unit_other))
                    {
                        row.Cells[icell].AddParagraph(nullToEmtry(_FN.intTostrMoney2(modelReport.chot_summary.chot_cargo[0].cargo_qty)) + " " + modelReport.chot_summary.chot_cargo[0].cargo_unit_other);
                    }
                    else
                    {
                        row.Cells[icell].AddParagraph(nullToEmtry(_FN.intTostrMoney2(modelReport.chot_summary.chot_cargo[0].cargo_qty)) + " " + modelReport.chot_summary.chot_cargo[0].cargo_unit);
                    }

                    row.Cells[icell].MergeRight = 14;

                    //row 4 col 3
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 10;

                    //Tolerance  1
                    //row 4 col 1
                    
                    row = table.AddRow(); icell = 0;
                    row.Cells[icell].AddParagraph("Tolerance (+/-)");
                    row.Cells[icell].MergeRight = 10;

                    //row 4 col 2
                    icell += row.Cells[icell].MergeRight + 1;
                    if (!string.IsNullOrEmpty(modelReport.chot_summary.chot_cargo[0].cargo_tolerance))
                    {
                        row.Cells[icell].AddParagraph(modelReport.chot_summary.chot_cargo[0].cargo_tolerance + " %");
                    }
                    else
                    {
                        row.Cells[icell].AddParagraph("-");
                    }
                    row.Cells[icell].MergeRight = 14;
                    
                    //row 4 col 3
                    //icell += row.Cells[icell].MergeRight + 1;
                    //row.Cells[icell].AddParagraph("");
                    //row.Cells[icell].MergeRight = 10;
                }
                else
                {
                    int cargoNo = 1;
                    foreach (var item in modelReport.chot_summary.chot_cargo)
                    {
                        //Cargo 1
                        //row 4 col 1
                        row = table.AddRow(); icell = 0;
                        row.Cells[icell].AddParagraph("Cargo " + cargoNo.ToString());
                        row.Cells[icell].MergeRight = 10;

                        //row 4 col 2
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(nullToEmtry(item.cargo_name));
                        row.Cells[icell].MergeRight = 14;

                        //row 4 col 3
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph("");
                        row.Cells[icell].MergeRight = 10;

                        //Quantity  1
                        //row 4 col 1
                        row = table.AddRow(); icell = 0;
                        row.Cells[icell].AddParagraph("Quantity " + cargoNo.ToString());
                        row.Cells[icell].MergeRight = 10;

                        //row 4 col 2
                        icell += row.Cells[icell].MergeRight + 1;
                        if (!string.IsNullOrEmpty(item.cargo_unit_other))
                        {
                            row.Cells[icell].AddParagraph(nullToEmtry(_FN.intTostrMoney2(item.cargo_qty)) + " " + item.cargo_unit_other);
                        }
                        else
                        {
                            row.Cells[icell].AddParagraph(nullToEmtry(_FN.intTostrMoney2(item.cargo_qty)) + " " + item.cargo_unit);
                        }

                        row.Cells[icell].MergeRight = 14;

                        //row 4 col 3
                        //icell += row.Cells[icell].MergeRight + 1;
                        //row.Cells[icell].AddParagraph("");
                        //row.Cells[icell].MergeRight = 10;

                        //Tolerance  1
                        //row 4 col 1
                       
                        row = table.AddRow(); icell = 0;
                        row.Cells[icell].AddParagraph("Tolerance " + cargoNo.ToString() + " (+/-)");
                        row.Cells[icell].MergeRight = 10;

                        //row 4 col 2
                        icell += row.Cells[icell].MergeRight + 1;
                        if (!string.IsNullOrEmpty(item.cargo_tolerance))
                        {
                            row.Cells[icell].AddParagraph(item.cargo_tolerance + " %");
                        }
                        else
                        {
                            row.Cells[icell].AddParagraph("-");
                        }
                        row.Cells[icell].MergeRight = 14;
                        
                        //row 4 col 3
                        //icell += row.Cells[icell].MergeRight + 1;
                        //row.Cells[icell].AddParagraph("");
                        //row.Cells[icell].MergeRight = 10;
                        cargoNo++;
                    }
                }            
            }
            else
            {
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph("Cargo");
                row.Cells[icell].MergeRight = 10;

                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph("Quantity");
                row.Cells[icell].MergeRight = 10;

                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph("Tolerance (+/-)");
                row.Cells[icell].MergeRight = 10;
            }

            //Laycan
            //row 1 col 1
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Laycan");
            row.Cells[icell].MergeRight = 10;

            //row 1 col 2
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.laycan));
            row.Cells[icell].MergeRight = 14;

            //row 1 col 3
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 10;

            //BSS
            //row 1 col 1
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("BSS");
            row.Cells[icell].MergeRight = 10;

            //row 1 col 2
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.bss));
            row.Cells[icell].MergeRight = 14;

            //row 1 col 3
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 10;

            //Loading Port 1 
            //row 1 col 1
            if (modelReport.chot_summary.chot_loading_port != null)
            {
                if (modelReport.chot_summary.chot_loading_port.Count() == 1)
                {
                    row = table.AddRow(); icell = 0;
                    row.Cells[icell].AddParagraph("Loading Port ");
                    row.Cells[icell].MergeRight = 10;

                    //row 1 col 2
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.chot_loading_port[0].port_full_name));
                    row.Cells[icell].MergeRight = 14;

                    //row 1 col 3
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 10;
                } else
                {
                    for (int i = 0; i < modelReport.chot_summary.chot_loading_port.Count(); i++)
                    {
                        row = table.AddRow(); icell = 0;
                        row.Cells[icell].AddParagraph("Loading Port " + Convert.ToString(i + 1));
                        row.Cells[icell].MergeRight = 10;

                        //row 1 col 2
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.chot_loading_port[i].port_full_name));
                        row.Cells[icell].MergeRight = 14;

                        //row 1 col 3
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph("");
                        row.Cells[icell].MergeRight = 10;
                    }
                }                
            }
            else
            {
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph("Loading Port");
                row.Cells[icell].MergeRight = 10;

                //row 1 col 2
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("");
                row.Cells[icell].MergeRight = 14;

                //row 1 col 3
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("");
                row.Cells[icell].MergeRight = 10;
            }

            if (modelReport.chot_summary.chot_discharging_port != null)
            {
                if (modelReport.chot_summary.chot_discharging_port.Count == 1)
                {
                    row = table.AddRow(); icell = 0;
                    row.Cells[icell].AddParagraph("Discharging Port");
                    row.Cells[icell].MergeRight = 10;

                    //row 1 col 2
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.chot_discharging_port[0].port_full_name));
                    row.Cells[icell].MergeRight = 14;

                    //row 1 col 3
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 10;
                } else
                {
                    for (int i = 0; i < modelReport.chot_summary.chot_discharging_port.Count(); i++)
                    {
                        //Discharging Port 1 
                        //row 1 col 1
                        row = table.AddRow(); icell = 0;
                        row.Cells[icell].AddParagraph("Discharging Port " + Convert.ToString(i + 1));
                        row.Cells[icell].MergeRight = 10;

                        //row 1 col 2
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.chot_discharging_port[i].port_full_name));
                        row.Cells[icell].MergeRight = 14;

                        //row 1 col 3
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph("");
                        row.Cells[icell].MergeRight = 10;
                    }
                }                
            }
            else
            {
                //Discharging Port 1 
                //row 1 col 1
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph("Discharging Port 1");
                row.Cells[icell].MergeRight = 10;

                //row 1 col 2
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("");
                row.Cells[icell].MergeRight = 14;

                //row 1 col 3
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("");
                row.Cells[icell].MergeRight = 10;
            }
        }

        public void CharterOutSummary_Sub_Report_Two(ChotObjectRoot modelReport, Table table, string unit)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 8;

            Column column = table.AddColumn("0.25cm");
            for (int c = 0; c < 36; c++)
            {
                column = table.AddColumn("0.25cm");
            }

            Row row = table.AddRow(); int icell = 0;

            //row = table.AddRow(); icell = 0;
            //row.Cells[icell].MergeRight = 36;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Freight");
            if (modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer == "THB")
            {
                row.Cells[icell].MergeRight = 5;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("(" + modelReport.chot_charter_out_case_b.chot_est_income_b_one.type + " = " + nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_income_b_one.total_freight)) + " " + modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer + " )");
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 17;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.freight)));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 9;
            }
            else if (modelReport.chot_charter_out_case_b.chot_est_income_b_one.type == "UNIT PRICE" && modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer.Contains("THB"))
            {
                row.Cells[icell].MergeRight = 5;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("(" + modelReport.chot_charter_out_case_b.chot_est_income_b_one.type + " = " + nullToZero(_FN.intTostrMoney2(modelReport.chot_charter_out_case_b.chot_est_income_b_one.freight)) + " " + modelReport.chot_charter_out_case_b.chot_est_income_b_one.offer + " )");
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 17;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.freight)));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 9;
            }
            else
            {
                row.Cells[icell].MergeRight = 11;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.freight)));
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 21;
            }
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Laytime");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.laytime)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Hrs");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Demurrage (" + modelReport.chot_summary.demurrage_unit + ")");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.demurrage)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Payment Term");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.chot_summary.payment_term);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Charterer's Broker");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.charterer_broker));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Charterer's Broker Commission");
            row.Cells[icell].MergeRight = 12;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.charterer_broker_comm)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 20;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("%");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Owner's Broker");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chot_summary.owner_broker));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Owner's Broker Commission");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.owner_broker_comm)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("%");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Address Commission");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.addr_comm)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("%");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Total Commission");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.total_comm)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("%");
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("Withholding Tax");
            row.Cells[icell].MergeRight = 11;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(_FN.intTostrMoney2(modelReport.chot_summary.withholding_tax)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 21;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("%");
            row.Cells[icell].MergeRight = 2;
        }

        private void CharterOutResult_Report(Section section, ChotObjectRoot modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Left.Color = Colors.Black;
            this.table.Borders.Right.Color = Colors.Black;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("19cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            //column = this.table.AddColumn("10.50cm");
            //column.Format.Alignment = ParagraphAlignment.Left;

            //column = this.table.AddColumn("8.25cm");
            //column.Format.Alignment = ParagraphAlignment.Left;

            Row row = table.AddRow(); int icell = 0;
            row.Cells[icell].AddParagraph("Result");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            //row.Cells[icell].MergeRight = 2;

            // Detail
            row = table.AddRow(); icell = 0;

            //icell += row.Cells[icell].MergeRight + 1;
            Table tmpTable = new Table();
            CharterOutResult_Sub_Report(modelReport, tmpTable, unit);
            row.Cells[icell].Elements.Add(tmpTable);


            // Footer
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        public void CharterOutResult_Sub_Report(ChotObjectRoot modelReport, Table table, string unit)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 8;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 36; c++)
            {
                column = table.AddColumn("0.5cm");
            }

            Row row = table.AddRow(); int icell = 0;

            //row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 20;
            
            if (modelReport.chot_result != null)
            {
                if (modelReport.chot_result.flag == "Saving")
                {
                    if (modelReport.chot_result.chot_saving != null)
                    {
                        row = table.AddRow(); icell = 2;
                        row.Cells[icell].AddParagraph("Saving");
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].Format.Font.Underline = Underline.Single;

                        row = table.AddRow();

                        row = table.AddRow(); icell = 4;
                        //row.Cells[icell].AddParagraph("No Charter out case").AddFormattedText();
                        row.Cells[icell].AddParagraph().AddFormattedText("\u0076", new Font("Wingdings", FontSize)).AddFormattedText(" No Charter out case", new Font(FontName, FontSize));
                        row.Cells[icell].MergeRight = 6;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(": Total TC Expense");
                        row.Cells[icell].MergeRight = 8;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.chot_result.chot_saving.saving_total_expense) ? "-" : _FN.intTostrMoney2(modelReport.chot_result.chot_saving.saving_total_expense));
                        row.Cells[icell].MergeRight = 4;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(unit);
                        row.Cells[icell].MergeRight = 5;

                        row = table.AddRow(); icell = 4;
                        //row.Cells[icell].AddParagraph("Charter out case");
                        row.Cells[icell].AddParagraph().AddFormattedText("\u0076", new Font("Wingdings", FontSize)).AddFormattedText(" Charter out case", new Font(FontName, FontSize));
                        row.Cells[icell].MergeRight = 6;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(": Total Expense – Total Income");
                        row.Cells[icell].MergeRight = 8;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.chot_result.chot_saving.saving_exp_inc) ? "-" : _FN.intTostrMoney2(modelReport.chot_result.chot_saving.saving_exp_inc));
                        row.Cells[icell].MergeRight = 4;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(unit);
                        row.Cells[icell].MergeRight = 5;

                        row = table.AddRow(); icell = 4;
                        //row.Cells[icell].AddParagraph("Total Saving");
                        row.Cells[icell].AddParagraph().AddFormattedText("\u0076", new Font("Wingdings", FontSize)).AddFormattedText(" Total Saving", new Font(FontName, FontSize));
                        row.Cells[icell].MergeRight = 15;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.chot_result.chot_saving.saving_total) ? "-" : _FN.intTostrMoney2(modelReport.chot_result.chot_saving.saving_total));
                        row.Cells[icell].MergeRight = 4;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(unit);
                        row.Cells[icell].MergeRight = 5;
                    }
                }
                else if (modelReport.chot_result.flag == "Profit")
                {
                    if (modelReport.chot_result.chot_profit != null)
                    {
                        row = table.AddRow(); icell = 2;
                        row.Cells[icell].AddParagraph("Profit");
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].Format.Font.Underline = Underline.Single;

                        row = table.AddRow();

                        row = table.AddRow(); icell = 4;
                        //row.Cells[icell].AddParagraph("No Charter out case");
                        row.Cells[icell].AddParagraph().AddFormattedText("\u0076", new Font("Wingdings", FontSize)).AddFormattedText(" No Charter out case", new Font(FontName, FontSize));
                        row.Cells[icell].MergeRight = 6;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(": Total TC Expense");
                        row.Cells[icell].MergeRight = 8;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.chot_result.chot_profit.profit_total_expense) ? "-" : _FN.intTostrMoney2(modelReport.chot_result.chot_profit.profit_total_expense));
                        row.Cells[icell].MergeRight = 4;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(unit);
                        row.Cells[icell].MergeRight = 5;

                        row = table.AddRow(); icell = 4;
                        //row.Cells[icell].AddParagraph("Charter out case");
                        row.Cells[icell].AddParagraph().AddFormattedText("\u0076", new Font("Wingdings", FontSize)).AddFormattedText(" Charter out case", new Font(FontName, FontSize));
                        row.Cells[icell].MergeRight = 6;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(": Total Income - Total Variable Cost");
                        row.Cells[icell].MergeRight = 9;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.chot_result.chot_profit.profit_inc_var) ? "-" : _FN.intTostrMoney2(modelReport.chot_result.chot_profit.profit_inc_var));
                        row.Cells[icell].MergeRight = 3;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(unit);
                        row.Cells[icell].MergeRight = 5;

                        row = table.AddRow(); icell = 4;
                        //row.Cells[icell].AddParagraph("Total Profit");
                        row.Cells[icell].AddParagraph().AddFormattedText("\u0076", new Font("Wingdings", FontSize)).AddFormattedText(" Total Profit", new Font(FontName, FontSize));
                        row.Cells[icell].MergeRight = 15;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.chot_result.chot_profit.profit_total) ? "-" : _FN.intTostrMoney2(modelReport.chot_result.chot_profit.profit_total));
                        row.Cells[icell].MergeRight = 4;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(unit);
                        row.Cells[icell].MergeRight = 5;
                    }                    
                }
            }            
        }
        
        private void CharterOutNote_Report(Section section, ChotObjectRoot modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }


            Row row = table.AddRow(); int icell = 0;
            row.Cells[icell].AddParagraph("Note");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Color.Empty;
            row.Cells[icell].Borders.Bottom.Color = Color.Empty;

            row = table.AddRow(); icell = 0;
            if (!string.IsNullOrEmpty(modelReport.chot_note))
            {
                row.Cells[icell].AddParagraph(modelReport.chot_note);
            }
            else
            {
                row.Cells[icell].AddParagraph("");
            }
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Color.Empty;
            row.Cells[icell].Borders.Bottom.Color = Color.Empty;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Color.Empty;
        }

        private string nullToEmtry(string value)
        {
            return string.IsNullOrEmpty(value) ? "-" : value;
        }

        private string nullToZero(string value)
        {
            return string.IsNullOrEmpty(value) ? "-" : value;
        }

        private void FooterReport(Section section, ChotObjectRoot model)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;

            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Chartering");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            double height = 0; double width = 0;
            if (!string.IsNullOrEmpty(approveName))
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(2, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve4
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            if (!string.IsNullOrEmpty(approveName))
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            //Approve3
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            //if (!string.IsNullOrEmpty(approveName))
            //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            //else
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            row.Cells[icell].AddParagraph(string.Format("( CMMT{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            row.Cells[icell].AddParagraph(string.Format("( CMMT{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
        }

        private void ExplanImageReport(Section section, ChotObjectRoot modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            }

            string[] _image = modelReport.explanationAttach.Split('|');
            foreach (var _item in _image)
            {
                if (_item != "")
                {
                    string[] itemsplit = _item.ToString().Split(':');
                    Row row = table.AddRow();
                    int icell = 0;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Format.Font.Bold = true;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    icell += row.Cells[icell].MergeRight + 1;
                    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetExplanImage(itemsplit[0], URL));
                    if (!string.IsNullOrEmpty(URL))
                    {
                        images.Add(_img.Name);
                    }
                    _img.Width = new Unit(11, UnitType.Centimeter);
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].MergeRight = 12;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Borders.Bottom.Color = Colors.White;
                    if (itemsplit.Length > 0)
                    {
                        row = table.AddRow();
                        icell = 0;
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Format.Font.Bold = true;
                        row.Cells[icell].AddParagraph(itemsplit[1]);
                        row.Cells[icell].MergeRight = 18;
                    }
                }
            }
        }
        #endregion
    }
}


