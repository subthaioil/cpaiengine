﻿using com.pttict.engine.utility;
using Common.Logging;
using log4net.Appender;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;
using log4net;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Web.Report
{
    public partial class CrudePurchaseReport : System.Web.UI.Page
    {
        ShareFn _FN = new ShareFn();
        ReportFunction _RFN = new ReportFunction();
        Document document;
        Table table;
        Color TableBorder;
        Color TableGray;
        const int defaultHeight = 10;

        int FontSize = 8;
        string URL = "";
        List<string> images = new List<string>();
        string FontName = "Tahoma";
        Setting _setting = JSONSetting.getSetting("JSON_CRUDE_PURCHASE");
        string unit = "($/bbl)";

        HtmlRemoval htmlRemove = new HtmlRemoval();

        protected void Page_Load(object sender, EventArgs e)
        {
            //var stringPath = "";
            try
            {
                if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload")))
                {
                    Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload"));
                }
                if (Request.QueryString["TranID"] != null)
                {
                  //stringPath = LoadData(Request.QueryString["TranID"].ToString().Decrypt());
                    LoadData(Request.QueryString["TranID"].ToString().Decrypt());
                }
                else
                {
                    if (Session["CdpRootObject"] != null)
                    {
                        CdpRootObject _CrudePurchase = (CdpRootObject)Session["CdpRootObject"];
                        GenPDF(_CrudePurchase, true);
                        Session["CdpRootObject"] = null;
                    }
                }
            }
            catch (Exception ex)
            {
                //log4net.ILog log ;
                //log.Error("# Error :: Exception >>>  " + ex.Message);

                var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message); 
                showErrMag.Text = ex.Message +"|"+ LineNumber;
            }
           // Response.Redirect(stringPath, false);
        }
        public string GetRequesterFullName(string pUserName)
        {
            string rslt = "";
            USERS_DAL userDal = new USERS_DAL();
            DAL.Entity.USERS tUser = userDal.GetUserByUserName(pUserName);
            if (tUser != null)
            {
                rslt = tUser.USR_FIRST_NAME_EN + " " + tUser.USR_LAST_NAME_EN;
            }
            return rslt;
        }
        public string LoadData(string TransactionID, bool redirect = true, string UserName = "")
        {
            string pdfPath = "";
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            //req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "user", V = (string.IsNullOrEmpty(UserName)) ? Const.User.UserName : UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                CdpRootObject _modelCrude = new JavaScriptSerializer().Deserialize<CdpRootObject>(_model.data_detail);
                if (_modelCrude.offers_items.Count <= 0) _modelCrude.offers_items.Add(new CdpOffersItem());
                if (!redirect)
                    URL = JSONSetting.getGlobalConfig("ROOT_URL");
                pdfPath = GenPDF(_modelCrude, redirect);
                if (!redirect)
                {
                    for (int i = 0; i < images.Count(); i++)
                    {
                        File.Delete(images[i]);
                    }
                }
            }
            else
            {
                if (redirect) _FN.MessageBoxShowLink(this.Page, resData.response_message, "MainBoards.aspx");
            }
            return pdfPath;
            //return Path.GetFileName(pdfPath);
        }

        private string GenPDF(CdpRootObject model, bool redirect)
        {
            Document document = CreateDocument(model);

            document.UseCmykColor = true;
            const bool unicode = true;
            //const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();
            if (model.explanationAttach != null)
            {
                string[] filePath = model.explanationAttach.Split('|');
                foreach (string path in filePath)
                {
                    string newPath = path.Split(':')[0];
                    string pdfFilename = newPath.Replace("Web/FileUpload/", "");
                    if (pdfFilename.Split('.').Last().ToUpper() != "PDF")
                        continue;
                    string pdfFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", pdfFilename);
                    if (!File.Exists(pdfFile))
                        continue;
                    try
                    {
                        PdfDocument inputPDFDocument = PdfReader.Open(pdfFile, PdfDocumentOpenMode.Import);
                        pdfRenderer.PdfDocument.Version = inputPDFDocument.Version;
                        foreach (PdfPage page in inputPDFDocument.Pages)
                        {
                            pdfRenderer.PdfDocument.AddPage(page);
                        }
                    } catch (Exception ex)
                    {
                        Document doc = new Document();
                        Section sec = doc.AddSection();
                        Table table = sec.AddTable();
                        table.Style = "Table";
                        table.Format.Font.Name = FontName;
                        table.Borders.Color = TableBorder;
                        table.Format.Font.Size = FontSize + 3;
                        table.Rows.Height = 18;
                        Column column = table.AddColumn("1cm");
                        for (int i = 0; i < 18; i++)
                        {
                            column = table.AddColumn("1cm");
                        }
                        Row row = table.AddRow();
                        row.Cells[0].AddParagraph(pdfFilename + " can't be loaded.");
                        row.Cells[0].MergeRight = 18;
                        row.Cells[0].Borders.Color = Colors.Transparent;

                        doc.UseCmykColor = true;
                        //const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
                        PdfDocumentRenderer newpdfRenderer = new PdfDocumentRenderer(unicode);
                        newpdfRenderer.Document = doc;
                        newpdfRenderer.RenderDocument();

                        MemoryStream stream = new MemoryStream();
                        newpdfRenderer.Save(stream, false);

                        PdfDocument inputPDFDocument = PdfReader.Open(stream, PdfDocumentOpenMode.Import);
                        pdfRenderer.PdfDocument.Version = inputPDFDocument.Version;
                        pdfRenderer.PdfDocument.AddPage(inputPDFDocument.Pages[0]);

                    }
                }
            }
            // Save the document...
            string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("CDPH{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmssffff")));
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile")))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile"));
            }
            pdfRenderer.PdfDocument.Save(filename);
            if (redirect) Response.Redirect(string.Format("TmpFile/{0}", new FileInfo(filename).Name));
            // ...and start a viewer.
            //Process.Start(filename);

            return filename;
            //return File(filename, "application/pdf", Server.UrlEncode(filename));
        }

        public Document CreateDocument(CdpRootObject model)
        {
            TableGray = Colors.Blue;
            TableBorder = Colors.Black;
            TableGray = Colors.Gray;
            // Create a new MigraDoc document
            this.document = new Document();
            // this.document.Info.Title = "Crude/Feedstock Purchase Approval Form";
            this.document.Info.Title = "Report";
            this.document.DefaultPageSetup.LeftMargin = 30;
            this.document.DefaultPageSetup.TopMargin = 30;
            this.document.DefaultPageSetup.BottomMargin = 30;
            DefineStyles();
            _RFN.document = document;
            _RFN.FontName = FontName;
            CreatePage(model);
            return this.document;
        }

        void DefineStyles()
        {
            MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
            style.Font.Name = FontName;

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = FontName;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 10;

            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        void CreatePage(CdpRootObject model)
        {
            Section section = this.document.AddSection();
            int StartOfferIndex = 0;
            int MaxOfferItem = model.offers_items != null ? model.offers_items.Count > 0 ? model.offers_items[0].round_items != null ? model.offers_items[0].round_items.Count : 0 : 0 : 0;
            int MaxCompetOfferItem = model.compet_offers_items != null ? model.compet_offers_items.Count > 0 ? model.compet_offers_items[0].round_items != null ? model.compet_offers_items[0].round_items.Count : 0 : 0 : 0;
            this.unit = model.feedstock == "CRUDE" ? "($/bbl)" : "($/ton)";
            int OfferItem = 0;
            if (MaxOfferItem >= MaxCompetOfferItem)
            {
                OfferItem = MaxOfferItem;
            }
            else if (MaxOfferItem < MaxCompetOfferItem)
            {
                OfferItem = MaxCompetOfferItem;
            }
            else
            {
                OfferItem = 1;
            }

            while (StartOfferIndex + 1 <= OfferItem)
            {
                if (StartOfferIndex > 0)
                    document.LastSection.AddPageBreak();
                HeadingReport(section, model);
                OfferTableZone(section, model, StartOfferIndex, OfferItem - StartOfferIndex > 3 ? 3 : OfferItem - StartOfferIndex);
                BottomZoneReport1(section, model);
                BottomZoneReport2(section, model);

                FooterReport(section, model);

                StartOfferIndex += 3;
            }
            if (OfferItem == 0)
            {
                HeadingReport(section, model);
                BottomZoneReport1(section, model);
                BottomZoneReport2(section, model);
                FooterReport(section, model);
            }
            if (!string.IsNullOrEmpty(model.explanationAttach))
            {
                bool isOnlyPDF = true;
                string[] _image = model.explanationAttach.Split('|');
                foreach (var _item in _image)
                {
                    if (_item != "")
                    {
                        string[] itemsplit = _item.ToString().Split(':');
                        if (itemsplit[0].Split('.').Last().ToUpper() != "PDF")
                        {
                            isOnlyPDF = false;
                            break; 
                        }
                    }
                }
                if (!isOnlyPDF)
                {
                    document.LastSection.AddPageBreak();
                    HeadingReport(section, model, true);
                    ExplanImageReport(section, model);
                }
            }
        }

        private void HeadingReport(Section section, CdpRootObject model, bool AttachPage = false)
        {
            #region Set
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 13;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            #endregion

            #region Row 1 (Header Page)
            // Create the header of the table
            Row row = table.AddRow();
            
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Borders.Color = Colors.Black;

            //for (int i = 0; i <= 18; i++)
            //{
            //    row.Cells[i].AddParagraph(i.ToString());
            //}
            //row = table.AddRow();
            int icell = 0;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Color = Colors.Transparent;
            row.Cells[icell].AddParagraph("Crude/Feedstock Purchase Approval Form");
            row.Cells[icell].Format.Font.Size = 9;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;

            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            #endregion

            #region Row 2 (Date)
            row = table.AddRow();
            icell = 0;
            row.Cells[icell].AddParagraph("Requested by : " +GetRequesterFullName( model.requested_by));
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Color = Colors.Transparent;
            row.Cells[icell].Borders.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight+1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("Date Purchase : " + _FN.ConvertDateFormatBackFormat(model.date_purchase, "dd MMM yyyy"));
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.Black;

            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;

            #endregion

            #region Row 3 (For feedstock, Feedstock)

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;


            // For Feed stock
            icell = 0;
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            Paragraph paragraph = new Paragraph();
            paragraph.AddFormattedText("For : ");
            foreach (var item in _setting.for_feedstock)
            {
                if (model.for_feedStock == item)
                {
                    paragraph.AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + item + " ", new Font(FontName, FontSize));

                }
                else
                {
                    paragraph.AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + item + " ", new Font(FontName, FontSize));
                }
            }
            row.Cells[icell].Add(paragraph);

            // Feed stock
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].MergeRight = 11;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            paragraph = new Paragraph();
            paragraph.AddFormattedText("Feed stock : ");
            foreach (var item in _setting.feedstock)
            {
                if (model.feedstock.ToUpper().Trim() == item.key.ToUpper().Trim())
                {
                    if (item.key.ToUpper().Trim() == "OTHERS".Trim())
                    {
                        paragraph.AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + item.key + " : " + model.feedstock_others + " ", new Font(FontName, FontSize));
                    }
                   else
                    {
                        paragraph.AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + item.key + " ", new Font(FontName, FontSize));
                    }
                }
                else
                {
                    paragraph.AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + item.key + " ", new Font(FontName, FontSize));
                }
            }
            row.Cells[icell].Add(paragraph);

           // paragraph = new Paragraph();
            //icell += row.Cells[icell].MergeRight + 1;
            //Hide PTT Offer
            //if (string.IsNullOrEmpty(model.offered_by_ptt)) model.offered_by_ptt = "X";
            //paragraph.AddFormattedText("PTT Offer: ", new Font(FontName, FontSize))
            //    .AddFormattedText(model.offered_by_ptt.Equals("Y") ? "\u006e" : "\u00A8", new Font("Wingdings", FontSize + 2))
            //    .AddFormattedText(" Yes  ", new Font(FontName, FontSize))
            //    .AddFormattedText(model.offered_by_ptt.Equals("N") ? "\u006e" : "\u00A8", new Font("Wingdings"))
            //    .AddFormattedText(" NO", new Font(FontName, FontSize));
            //row.Cells[icell].Add(paragraph);
           // row.Cells[icell].MergeRight = 4;
            #endregion

            #region Row 4 (Crude, Origin, Term, Purchase)
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].MergeRight = 6;
            if (model.feedstock.ToUpper() == "CRUDE")
            {
                row.Cells[icell].AddParagraph("Crude Name : " + model.crude_name);
            } else
            {
                row.Cells[icell].AddParagraph("Name : " + model.crude_name_others);
            }
            row.Cells[icell].AddParagraph("Origin : " + model.origin);
            row.Cells[icell].AddParagraph("TPC Plan : " + model.plan_month + " " + model.plan_year);

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].MergeRight = 2;
            foreach (var _item in _setting.term)
            {
                if (model.term.ToUpper().Trim() == _item.ToUpper().Trim())
                {
                    if (_item.ToUpper().Trim() == "OTHERS".Trim())
                    {
                        row.Cells[icell].AddParagraph("  ").AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item + " : " + model.term_others, new Font(FontName, FontSize));
                    }
                    else
                    {
                        row.Cells[icell].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                    }
                }
                else
                {
                    row.Cells[icell].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                }
            }

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].MergeRight = 3;
            foreach (var _item in _setting.purchase)
            {
                if (model.purchase == _item)
                {
                    row.Cells[icell].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                }
                else
                {
                    row.Cells[icell].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                }
            }

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].MergeRight = 4;
            foreach (var _item in _setting.supply_source)
            {
                if (model.supply_source == _item)
                {
                    row.Cells[icell].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                }
                else
                {
                    row.Cells[icell].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                }
            }
            //row.Cells[icell].AddParagraph("Sole Source Purchase");
            #endregion

            //// Row PTT Offer Hide
            BottomZoneReport_PTT_Offer(section, model);            

            #region Row 5 Brief Market Situation / Reason for purchasing
            if (!AttachPage)
            {
                row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Format.Font.Bold = true;
                row.Cells[0].AddParagraph("Brief Market Situation / Reason for purchasing");
                row.Cells[0].Borders.Bottom.Color = Colors.White;
                row.Cells[0].MergeRight = 18;

                //List<htmlTagList> Lst = htmlRemove.SplitHTMLTag(model.explanation == null ? "" : model.explanation);
                //List<htmlTagList> list = htmlRemove.RearrangeHtmlTagList(Lst);
                //if (list.Count > 0)
                //{
                //    for (int i = 0; i < list.Count; i++)
                //    {
                //        row = table.AddRow();
                //        row.HeadingFormat = true;
                //        row.Format.Alignment = ParagraphAlignment.Left;
                //        row.VerticalAlignment = VerticalAlignment.Top;
                //        row.Format.Font.Bold = false;
                //        Paragraph _prg = row.Cells[0].AddParagraph();
                //        if (!string.IsNullOrEmpty(model.explanation))
                //            htmlRemove.ValuetoParagraph(list[i], _prg);
                //        row.Cells[0].AddParagraph();
                //        row.Cells[0].MergeRight = 18;
                //        row.Cells[0].Borders.Top.Color = Colors.White;
                //    }
                //} else
                //{
                //    row = table.AddRow();
                //    row.HeadingFormat = true;
                //    row.Format.Alignment = ParagraphAlignment.Left;
                //    row.VerticalAlignment = VerticalAlignment.Top;
                //    row.Format.Font.Bold = false;
                //    row.Cells[0].AddParagraph();
                //    row.Cells[0].MergeRight = 18;
                //    row.Cells[0].Borders.Top.Color = Colors.White;
                //}

                if (!string.IsNullOrEmpty(model.explanation))
                {
                    Paragraph _prg = row.Cells[0].AddParagraph();
                    htmlRemove.ValuetoParagraph(model.explanation, _prg);
                }
                else
                {
                    row.Cells[0].AddParagraph("");
                }
            }
            #endregion
        }

        private void OfferTableZone(Section section, CdpRootObject model, int itemOfferIndex, int maxOfferItem)
        {
            #region Set
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize - 1;
            this.table.Rows.Height = defaultHeight;
            this.table.Format.Alignment = ParagraphAlignment.Center;

            Column column = this.table.AddColumn("0.1cm");
            for (int i = 0; i < 189; i++)
            {
                column = this.table.AddColumn("0.1cm");
            }
            #endregion

            #region--------HeaddingTable-------------

            Row row = table.AddRow();
            row.Borders.Color = Colors.Black;
            row.Cells[0].AddParagraph("Table1: Applied for \"Crude / Feedstock price offer\"");
            row.Cells[0].MergeRight = 189;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            if (model.feedstock.ToUpper() == "CRUDE")
            {
                row.Cells[icell].AddParagraph("Crude");
            }
            else
            {
                row.Cells[icell].AddParagraph("Name");
            }
            //row.Cells[icell].AddParagraph("Crude");
            row.Cells[icell].MergeRight = 19;    //0-3
            row.Cells[icell].MergeDown = 2;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Supplier");
            row.Cells[icell].MergeRight = 14;    //4-6
            row.Cells[icell].MergeDown = 2;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Contact").AddFormattedText(Environment.NewLine).AddFormattedText("Person");
            row.Cells[icell].MergeRight = 14;    //7-9
            row.Cells[icell].MergeDown = 2;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Quantity").AddFormattedText(Environment.NewLine).AddFormattedText("(" + _setting.feedstock.Where(x => x.key == model.feedstock).FirstOrDefault().unit + ")");
            row.Cells[icell].MergeRight = 14;    //10-13
            row.Cells[icell].MergeDown = 2;

            icell += row.Cells[icell].MergeRight + 1;
            int icelR2 = icell;
            row.Cells[icell].AddParagraph("Offer Price or Formula " + unit);
            row.Cells[icell].MergeRight = 97;   //14-30

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Rank/").AddFormattedText(Environment.NewLine).AddFormattedText("Round");
            row.Cells[icell].MergeRight = 11;    //31-33
            row.Cells[icell].MergeDown = 2;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Margin").AddFormattedText(Environment.NewLine).AddFormattedText("vs LP").AddFormattedText(Environment.NewLine).AddFormattedText(unit);
            row.Cells[icell].MergeRight = 14;    //34-37
            row.Cells[icell].MergeDown = 2;


            icell = icelR2;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Inco-").AddFormattedText("terms");
            row.Cells[icell].MergeRight = 8;    //14-16
            row.Cells[icell].MergeDown = 1;

            icell += row.Cells[icell].MergeRight + 1;
            int icelR3 = icell;
            row.Cells[icell].AddParagraph("Offer / Negotiation round");
            row.Cells[icell].MergeRight = 44;    //17-22

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Market/").AddFormattedText(Environment.NewLine).AddFormattedText("Source");
            row.Cells[icell].MergeRight = 13;    //23-24
            row.Cells[icell].MergeDown = 1;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("LP Max").AddFormattedText(Environment.NewLine).AddFormattedText("Purchase Price");
            row.Cells[icell].MergeRight = 13;    //25-26
            row.Cells[icell].MergeDown = 1;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Benchmark price");
            row.Cells[icell].MergeRight = 15;    //27-30
            row.Cells[icell].MergeDown = 1;

            icell = icelR3;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;

            int indexOfferMax = itemOfferIndex + maxOfferItem;

            for (int indexOffer = itemOfferIndex + 1; indexOffer <= indexOfferMax; indexOffer++)
            {
                row.Cells[icell].AddParagraph(string.Format("{0}", _FN.GetORDINALNUMBERS((indexOffer))));
                if (maxOfferItem == 2 && maxOfferItem == indexOffer)
                    row.Cells[icell].MergeRight = (42 / maxOfferItem);
                else
                    row.Cells[icell].MergeRight = (42 / maxOfferItem) + (3 - maxOfferItem);
                icell += row.Cells[icell].MergeRight + 1;
            }

            #endregion

            #region ------Offer Data-----

            if (model.offers_items != null)
            {
                if (model.offers_items.Count > 0)
                {
                    foreach (var _offeritem in model.offers_items.OrderByDescending(x => x.final_flag))
                    {
                        if (_offeritem.final_flag == "Y")
                        {
                            List<string> lstrOfferRound = new List<string>();
                            for (int indexOffer = itemOfferIndex + 1; indexOffer <= indexOfferMax; indexOffer++)
                            {
                                var roundValue = (from p in _offeritem.round_items
                                                  where p.round_no == indexOffer.ToString()
                                                  select p).ToList();

                                if (roundValue.Count > 0)
                                {
                                    foreach (var value in roundValue)
                                    {
                                        if (value != null)
                                        {
                                            lstrOfferRound.Add(value.round_value.First());
                                        }
                                    }
                                }
                                else
                                {
                                    lstrOfferRound.Add("-");
                                }
                            }

                            while (lstrOfferRound.Count < 3)
                            {
                                lstrOfferRound.Add("-");
                            }

                            row = table.AddRow();
                            row.Cells[0].MergeDown = model.offers_items.Count - 1;
                            CreateRowOffer(ref row, model.crude_name, _offeritem.supplier_name
                                , _offeritem.contact_person
                                , _offeritem.quantity
                                , _offeritem.quantity_max
                                , _offeritem.incoterms
                                , lstrOfferRound[0]
                                , lstrOfferRound[1]
                                , lstrOfferRound[2]
                                , _offeritem.market_source
                                , _offeritem.latest_lp_plan_price
                                , _offeritem.bechmark_price
                                , _offeritem.rank_round
                                , _offeritem.margin_lp
                                , maxOfferItem
                                , "");
                        }
                        else if (_offeritem.final_flag == "N")
                        {
                            List<string> lstrOfferRound = new List<string>();
                            for (int indexOffer = itemOfferIndex + 1; indexOffer <= indexOfferMax; indexOffer++)
                            {
                                var roundValue = (from p in _offeritem.round_items
                                                  where p.round_no == indexOffer.ToString()
                                                  select p).ToList();

                                if (roundValue.Count > 0)
                                {
                                    foreach (var value in roundValue)
                                    {
                                        if (value != null)
                                        {
                                            lstrOfferRound.Add(value.round_value.First());
                                        }
                                    }
                                }
                                else
                                {
                                    lstrOfferRound.Add("-");
                                }
                            }

                            while (lstrOfferRound.Count < 3)
                            {
                                lstrOfferRound.Add("-");
                            }

                            row = table.AddRow();
                            CreateRowOffer(ref row, model.crude_name, _offeritem.supplier_name
                                , _offeritem.contact_person
                                , _offeritem.quantity
                                , _offeritem.quantity_max
                                , _offeritem.incoterms
                                , lstrOfferRound[0]
                                , lstrOfferRound[1]
                                , lstrOfferRound[2]
                                , _offeritem.market_source
                                , _offeritem.latest_lp_plan_price
                                , _offeritem.bechmark_price
                                , _offeritem.rank_round
                                , _offeritem.margin_lp
                                , maxOfferItem
                                , "");
                        }
                    }
                }
            }
            
            //row = table.AddRow();
            //CreateRowOffer(ref row, "","", "", "", "", "_", "_", "_", "", "", "", "", "");
            //row = table.AddRow();
            //CreateRowOffer(ref row, "", "", "", "", "", "_", "_", "_", "", "", "", "", "");


            #endregion

            row = table.AddRow();
            CreateRowOffer(ref row, "Competitive crude in market:", "", "", "", "", "", "_", "_", "_", "", "", "", "", "", maxOfferItem, "");
            row.Cells[0].MergeRight = 49;


            #region ------Compet Offer Data-----

            if (model.compet_offers_items != null)
            {
                if (model.compet_offers_items.Count > 0)
                {
                    foreach (var _offeritem in model.compet_offers_items)
                    {
                        List<string> lstrOfferRound = new List<string>();
                        for (int indexOffer = itemOfferIndex + 1; indexOffer <= indexOfferMax; indexOffer++)
                        {
                            var roundValue = (from p in _offeritem.round_items
                                              where p.round_no == indexOffer.ToString()
                                              select p).ToList();

                            if (roundValue.Count > 0)
                            {
                                foreach (var value in roundValue)
                                {
                                    if (value != null)
                                    {
                                        lstrOfferRound.Add(value.round_value.First());
                                    }
                                }
                            }
                            else
                            {
                                lstrOfferRound.Add("-");
                            }
                        }

                        while (lstrOfferRound.Count < 3)
                        {
                            lstrOfferRound.Add("-");
                        }

                        row = table.AddRow();
                        //row.Cells[0].MergeDown = 2;
                        if(_offeritem.crude_name == "")
                        {
                            CrudePurchaseViewModel vm = new CrudePurchaseViewModel();
                            vm.ddlCrude = MaterialsServiceModel.getMaterialsDDL();
                            var nameDDl = vm.ddlCrude.SingleOrDefault(a => a.Value == _offeritem.crude_id);
                            if(nameDDl != null)
                            {
                                _offeritem.crude_name = nameDDl.Text;
                            }
                        }         

                            CreateRowOffer(ref row, _offeritem.crude_name, _offeritem.supplier_name
                            , _offeritem.contact_person
                            , _offeritem.quantity
                            , _offeritem.quantity_max
                            , _offeritem.incoterms
                            , lstrOfferRound[0]
                            , lstrOfferRound[1]
                            , lstrOfferRound[2]
                            , _offeritem.market_source
                            , _offeritem.latest_lp_plan_price
                            , _offeritem.bechmark_price
                            , _offeritem.rank_round
                            , _offeritem.margin_lp
                            , maxOfferItem
                            , _offeritem.crude_name_others);

                    }
                }
            }
            
            #endregion
        }

        private void CreateRowOffer(ref Row row
            , string pCrude, string pSupplier, string pContactPerson, string pQuantity, string pQuantityMax
            , string pIncoterms, string pOffer1, string pOffer2, string pOffer3, string pMarketSource, string pLatestLP, string pBechmarkPrice
            , string pRankRound, string pMarginLP, int maxOffer, string pCrudeNameOther)
        {
           
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            int icell = 0;

            string crude_name = "";
            if (string.IsNullOrEmpty(pCrude))
            {
                crude_name = pCrudeNameOther;
            }
            else
            {
                crude_name = pCrude;
            }

            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(crude_name) == true ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], crude_name, 18));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].MergeRight = 19;    //0-3

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pSupplier) == true ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], pSupplier, 13));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].MergeRight = 14;    //4-6

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pContactPerson) == true ? "": _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], pContactPerson, 13));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].MergeRight = 14;    //7-9

            icell += row.Cells[icell].MergeRight + 1;
            string quantity = string.IsNullOrEmpty(pQuantity) == true ? "" : _FN.intTostrMoney(pQuantity);
            string quantity_max = string.IsNullOrEmpty(pQuantityMax) == true ? "" : " - " + _FN.intTostrMoney(pQuantityMax);
            row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], quantity + quantity_max, 15));
            row.Cells[icell].MergeRight = 14;    //10-13

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pIncoterms) == true ? "" : pIncoterms);
            row.Cells[icell].MergeRight = 8;   //14-16

            // Offer
            int count_offer = 1;
            int length_offer = (42 / maxOffer) + (3 - maxOffer);
            if (count_offer <= maxOffer)
            {
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pOffer1) == true ? "-" : pOffer1 == "_" ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(pOffer1, true), length_offer - 1));
                row.Cells[icell].MergeRight = length_offer;    //17-18
            }
            count_offer++;

            if (count_offer <= maxOffer)
            {
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pOffer2) == true ? "-" : pOffer2 == "_" ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(pOffer2, true), length_offer - 1));
                row.Cells[icell].MergeRight = maxOffer == 2 ? length_offer - 1 : length_offer;    //19-20
            }
            count_offer++;

            if (count_offer <= maxOffer)
            {
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pOffer3) == true ? "-" : pOffer3 == "_" ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(pOffer3, true), length_offer - 1));
                row.Cells[icell].MergeRight = length_offer;    //21-22
            }
            count_offer++;


            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pMarketSource) == true ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(pMarketSource), 13));
            row.Cells[icell].MergeRight = 13;    //23-24

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pLatestLP) == true ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(pLatestLP), 13));
            row.Cells[icell].MergeRight = 13;    //25-26

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pBechmarkPrice) == true ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], pBechmarkPrice, 14));
            row.Cells[icell].MergeRight = 15;    //27-30

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pRankRound) == true ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], pRankRound, 10));
            row.Cells[icell].MergeRight = 11;    //31-33

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(pMarginLP) == true ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(pMarginLP), 14));
            if (!String.IsNullOrEmpty(pMarginLP))
            {
                if(Convert.ToDecimal(pMarginLP)<0)
                    row.Cells[icell].Format.Font.Color = Colors.Red;
            }
            
            row.Cells[icell].MergeRight = 14;    //34-37
        }

        private void ExplanImageReport(Section section, CdpRootObject model)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            }



            string[] _image = model.explanationAttach.Split('|');
            foreach (var _item in _image)
            {
                if (_item != "")
                {
                    string[] itemsplit = _item.ToString().Split(':');
                    if (itemsplit[0].Split('.').Last().ToUpper() == "PDF")
                        continue;
                    Row row = table.AddRow();
                    int icell = 0;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Format.Font.Bold = true;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    icell += row.Cells[icell].MergeRight + 1;
                    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetExplanImage(itemsplit[0], URL));
                    if (!string.IsNullOrEmpty(URL))
                    {
                        images.Add(_img.Name);
                    }
                    _img.Width = new Unit(11, UnitType.Centimeter);
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].MergeRight = 12;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Borders.Bottom.Color = Colors.White;
                    if (itemsplit.Length > 0)
                    {
                        row = table.AddRow();
                        icell = 0;
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Format.Font.Bold = true;
                        row.Cells[icell].AddParagraph(itemsplit[1]);
                        row.Cells[icell].MergeRight = 18;
                    }
                }
            }
        }

        private void BottomZoneReport1(Section section, CdpRootObject model)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 13;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            string Daterange = "-";
            if (model.term.ToUpper().Contains("TERM"))
            {
                row.Cells[icell].AddParagraph("Contract Period");
                row.Cells[icell].MergeRight = 4;

                icell += row.Cells[icell].MergeRight + 1;
                if (model.contract_period != null)
                {
                    Daterange = string.Format("{0} - {1}", _FN.ConvertDateFormatBackFormat(model.contract_period.date_from, "dd MMM yyyy"), _FN.ConvertDateFormatBackFormat(model.contract_period.date_to, "dd MMM yyyy"));
                }
                row.Cells[icell].AddParagraph(Daterange);
                row.Cells[icell].Format.Font.Bold = false;
                row.Cells[icell].MergeRight = 3;
            } else
            {
                row.Cells[icell].MergeRight = 8;
            }
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("Any other special terms and conditions :");
            row.Cells[icell].Format.Font.Bold = false;
            row.Cells[icell].Borders.Top.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 9;
            row.Cells[icell].AddParagraph(String.IsNullOrEmpty(model.other_condition) ? "": _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], model.other_condition, 11));
            row.Cells[icell].Borders.Top.Color = Colors.White;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].MergeRight = 9;
            row.Cells[icell].MergeDown = 3;
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].AddParagraph("Loading Period");
            row.Cells[icell].MergeRight = 4;
            

            icell += row.Cells[icell].MergeRight + 1;
            if (model.loading_period != null)
            {
                Daterange = string.Format("{0} - {1}", _FN.ConvertDateFormatBackFormat(model.loading_period.date_from, "dd MMM yyyy"), _FN.ConvertDateFormatBackFormat(model.loading_period.date_to, "dd MMM yyyy"));
            }
            else
            {
                Daterange = "-";
            }
            row.Cells[icell].AddParagraph(Daterange);
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].AddParagraph("Discharging Period");
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            if (model.discharging_period != null)
            {
                Daterange = string.Format("{0} - {1}", _FN.ConvertDateFormatBackFormat(model.discharging_period.date_from, "dd MMM yyyy"), _FN.ConvertDateFormatBackFormat(model.discharging_period.date_to, "dd MMM yyyy"));
            }
            else
            {
                Daterange = "-";
            }
            row.Cells[icell].AddParagraph(Daterange);
            row.Cells[icell].Format.Font.Bold = false;
            row.Cells[icell].MergeRight = 3;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].AddParagraph("GT&C");
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(model.gtc) ? "-" : _RFN.AdjustIfTooWideToFitIn(row.Cells[icell], model.gtc, 3));
            row.Cells[icell].Format.Font.Bold = false;
            row.Cells[icell].MergeRight = 3;
        }

        private Table CreateTablePaymentTerms(string ColumnOrder, CdpRootObject model)
        {
            //Start Create Table Payment Term 1
            Table _tmpPaymentTerm = new Table();
            _tmpPaymentTerm.Format.Font.Name = FontName;
            _tmpPaymentTerm.Format.Font.Size = FontSize;
            _tmpPaymentTerm.Rows.Height = 8;
            _tmpPaymentTerm.AddColumn("1cm");
            _tmpPaymentTerm.AddColumn("6cm");

            //_tmpPaymentTerm.Borders.Color = Colors.Black;
            //_tmpPaymentTerm.AddColumn("4cm");
            //End Create Payment Term

            Row row = new Row();

            string strPaymentType = string.IsNullOrEmpty(model.payment_terms.payment_term) ? "" : model.payment_terms.payment_term.ToUpper();
            string strPaymentDetail = string.IsNullOrEmpty(model.payment_terms.payment_term_detail) ? "" : model.payment_terms.payment_term_detail.ToUpper();
            string strPaymentSub = string.IsNullOrEmpty(model.payment_terms.sub_payment_term) ? "" : model.payment_terms.sub_payment_term.ToUpper();
            string strPaymentOthers = string.IsNullOrEmpty(model.payment_terms.payment_term_others) ? "-" : model.payment_terms.payment_term_others;

            var paymenttemrs = from p in _setting.payment_terms
                               select p;

            if (ColumnOrder == "1")
            {
                paymenttemrs = paymenttemrs.Where(p => p.type.ToUpper() == "Pre-payment".ToUpper() || p.type.ToUpper() == "Credit".ToUpper()).ToList();
            }
            else
            {
                paymenttemrs = paymenttemrs.Where(p => p.type.ToUpper() == "L/C".ToUpper() || p.type.ToUpper() == "Others".ToUpper()).ToList();
            }

            foreach (var _item in paymenttemrs)
            {
                string blCheck = "";
                blCheck = strPaymentType == _item.type.ToUpper() ? "\u006e" : "\u00A8";

                if (_item.flag == "Y")
                {
                    strPaymentDetail = strPaymentType == _item.type.ToUpper() ? strPaymentDetail : "-";

                    row = _tmpPaymentTerm.AddRow();
                    string type = strPaymentSub.IndexOf("NOR") > -1 ? "NOR" : "B/L";
                    row.Cells[0].AddParagraph().AddFormattedText(blCheck, new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item.type + "  " + strPaymentDetail + "  days from " + type + " date", new Font(FontName, FontSize));
                    row.Cells[0].MergeRight = 1;

                    string blDateCheckZero = "";
                    string blDateCheckOne = "";
                    string norDateCheckZero = "";
                    string norDateCheckOne = "";
                    string dayOther = "";

                    blDateCheckZero = strPaymentSub == "DAYZERO" && strPaymentType == _item.type.ToUpper() ? "\u006e" : "\u00A8";
                    blDateCheckOne = strPaymentSub == "DAYONE" && strPaymentType == _item.type.ToUpper() ? "\u006e" : "\u00A8";
                    norDateCheckZero = strPaymentSub == "NORDAYZERO" && strPaymentType == _item.type.ToUpper() ? "\u006e" : "\u00A8";
                    norDateCheckOne = strPaymentSub == "NORDAYONE" && strPaymentType == _item.type.ToUpper() ? "\u006e" : "\u00A8";
                    dayOther = strPaymentSub == "DAYOTHER" && strPaymentType == _item.type.ToUpper() ? "\u006e" : "\u00A8";

                    if (type == "B/L")
                    {
                        row = _tmpPaymentTerm.AddRow();
                        row.Cells[1].AddParagraph().AddFormattedText(blDateCheckZero, new Font("Wingdings", FontSize + 2)).AddFormattedText(" B/L date = day zero", new Font(FontName, FontSize));

                        row = _tmpPaymentTerm.AddRow();
                        row.Cells[1].AddParagraph().AddFormattedText(blDateCheckOne, new Font("Wingdings", FontSize + 2)).AddFormattedText(" B/L date = day one", new Font(FontName, FontSize));
                    } else
                    {
                        row = _tmpPaymentTerm.AddRow();
                        row.Cells[1].AddParagraph().AddFormattedText(norDateCheckZero, new Font("Wingdings", FontSize + 2)).AddFormattedText(" NOR date = day zero", new Font(FontName, FontSize));

                        row = _tmpPaymentTerm.AddRow();
                        row.Cells[1].AddParagraph().AddFormattedText(norDateCheckOne, new Font("Wingdings", FontSize + 2)).AddFormattedText(" NOR date = day one", new Font(FontName, FontSize));
                    }

                    row = _tmpPaymentTerm.AddRow();
                    row.Cells[1].AddParagraph().AddFormattedText(dayOther, new Font("Wingdings", FontSize + 2)).AddFormattedText(" Others: " + (strPaymentType == _item.type.ToUpper() ? strPaymentOthers : "-"), new Font(FontName, FontSize));
                }
                else
                {
                    row = _tmpPaymentTerm.AddRow();
                    row.Cells[0].MergeRight = 1;
                    
                    if (strPaymentType.ToUpper() == "OTHERS".Trim() && _item.type != "Pre-payment")
                    {
                        row.Cells[0].AddParagraph().AddFormattedText(blCheck, new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item.type + " : " + model.payment_terms.payment_term_detail, new Font(FontName, FontSize));
                    }
                    else
                    {
                        row.Cells[0].AddParagraph().AddFormattedText(blCheck, new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item.type, new Font(FontName, FontSize));
                    }
                }

              
            }

            return _tmpPaymentTerm;
        }

        private void BottomZoneReport_PTT_Offer(Section section, CdpRootObject model)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 13;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            } 

            Row row = table.AddRow(); 
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Borders.Color = Colors.Transparent;
            row.Cells[0].AddParagraph("PTT Offer :");
            row.Cells[0].MergeRight = 1;
            row.Cells[0].Borders.Left.Color = Colors.Black;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            model.offered_by_ptt = String.IsNullOrEmpty(model.offered_by_ptt) ? "N" : model.offered_by_ptt;
            if (string.IsNullOrEmpty(model.offered_by_ptt)) model.offered_by_ptt = "X";
            row.Cells[2].AddParagraph(" ").AddFormattedText(model.offered_by_ptt.Equals("Y") ? "\u006e" : "\u00A8", new Font("Wingdings"))
            .AddFormattedText(" Yes  ", new Font(FontName, FontSize))
            //row.Cells[2].MergeRight = 1;
            .AddFormattedText(model.offered_by_ptt.Equals("N") ? "\u006e" : "\u00A8", new Font("Wingdings"))
            .AddFormattedText(" No", new Font(FontName, FontSize));
            row.Cells[2].MergeRight = 1;
            row.Cells[4].AddParagraph("Reason :");
            row.Cells[4].MergeRight = 1;
            //row.Cells[4].Borders.Left.Color = Colors.Black;
            row.Cells[6].AddParagraph(model.reasonPTToffer == null ? "" : model.reasonPTToffer);
            row.Cells[6].MergeRight = 4;
            row.Cells[6].Format.Font.Bold = false;
            row.Cells[18].Borders.Right.Color = Colors.Black;                         

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Borders.Color = Colors.Transparent;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Offer via :");
            row.Cells[0].MergeRight = 1;
            row.Cells[0].Borders.Left.Color = Colors.Black;
            row.Cells[0].Borders.Bottom.Color = Colors.Black;
            row.Cells[2].AddParagraph(model.offerVia == null ? "" : model.offerVia);
            row.Cells[2].MergeRight = 1;
            row.Cells[2].Format.Font.Bold = false;
            row.Cells[4].AddParagraph("Reason :");
            row.Cells[4].MergeRight = 1;
            row.Cells[6].AddParagraph(model.reasonOfferVia == null ? "" : model.reasonOfferVia);
            row.Cells[6].MergeRight = 4;
            row.Cells[6].Format.Font.Bold = false;
            row.Cells[14].AddParagraph("Offer Date :");
            row.Cells[14].MergeRight = 1;

            string newOfferDate = "";
            if(!String.IsNullOrEmpty(model.offerDate)) 
            {
                var convertNewDate = DateTime.ParseExact(model.offerDate,
                       "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                newOfferDate = convertNewDate.ToString("dd-MMM-yyyy");
            }
            row.Cells[16].AddParagraph(model.offerDate == null ? "" : newOfferDate);
            row.Cells[16].MergeRight = 1;
            row.Cells[16].Format.Font.Bold = false;
            row.Cells[18].Borders.Right.Color = Colors.Black; 
        }

        private void BottomZoneReport2(Section section, CdpRootObject model)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            }
   
            Row row = table.AddRow();
            //row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Height = 14;
            row.Cells[0].AddParagraph("Payment Term : ");
            row.Cells[0].MergeRight = 4;
            row.Cells[0].Borders.Bottom.Color = Colors.White;
            row.Cells[5].Elements.Add(CreateTablePaymentTerms("1",model));
            row.Cells[5].MergeRight = 6;
            row.Cells[5].MergeDown = 1;
            row.Cells[5].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Top;

            row.Cells[5].Format.Font.Bold = false;
            row.Cells[5].Borders.Left.Color = Colors.White;
            row.Cells[5].Borders.Right.Color = Colors.Transparent;
            row.Cells[12].Elements.Add(CreateTablePaymentTerms("2", model));
            row.Cells[12].MergeRight = 6;
            row.Cells[12].MergeDown = 1;
            row.Cells[12].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[12].VerticalAlignment = VerticalAlignment.Top;

            row = table.AddRow();
            row.Height = 26;
            row.Cells[0].MergeRight = 4;
            row.Cells[0].Borders.Top.Color = Colors.White;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Borders.Color = Colors.Transparent;
            row.Cells[0].AddParagraph("Proposal");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].Borders.Left.Color = Colors.Black;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;

            row.Cells[3].AddParagraph("Award to :");
            row.Cells[3].MergeRight = 1;

            row.Cells[5].AddParagraph(model.proposal.supplier == null ? "" : model.proposal.supplier);
            row.Cells[5].MergeRight = 13;
            row.Cells[5].Format.Font.Bold = false;
            row.Cells[18].Borders.Right.Color = Colors.Black;

            Table _tmpReason = new Table();
            _tmpReason.Format.Font.Name = FontName;
            _tmpReason.Format.Font.Size = FontSize;
            _tmpReason.Rows.Height = 8;
            _tmpReason.AddColumn("1cm");
            _tmpReason.AddColumn("1cm");
            _tmpReason.AddColumn("12cm");
            Row rowReason;
            foreach (var _item in _setting.proposal_reason)
            {
                string blCheck = "";
                blCheck = model.proposal.reason.ToUpper() == _item.ToUpper() ? "\u006e" : "\u00A8";
                rowReason = _tmpReason.AddRow();
              
                if (_item.ToUpper().Trim() == "OTHERS".Trim())
                {
                    rowReason.Cells[0].AddParagraph("  ").AddFormattedText(blCheck, new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item + " : " + Convert.ToString(model.proposal.reason_others), new Font(FontName, FontSize));
                    rowReason.Cells[0].MergeRight = 2;
                }
                else
                {
                    rowReason.Cells[0].AddParagraph("  ").AddFormattedText(blCheck, new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item, new Font(FontName, FontSize));
                    rowReason.Cells[0].MergeRight = 1;
                    string unit = model.feedstock == "CRUDE" ? "  $/bbl or   " : "  $/ton or   ";
                    rowReason.Cells[2].AddParagraph("  ").AddFormattedText(" Lower than market price by          " + _FN.intTostrMoney(Convert.ToString(model.proposal.reason_ref1_bbl), true) + unit + _FN.intTostrMoney(Convert.ToString(model.proposal.reason_ref1_us), true) + "  $", new Font(FontName, FontSize));
                    rowReason.Cells[0].MergeRight = 1;
                    rowReason = _tmpReason.AddRow();
                    rowReason.Cells[2].AddParagraph("  ").AddFormattedText(" Lower than LP Max Purchase Price by " + _FN.intTostrMoney(Convert.ToString(model.proposal.reason_ref2_bbl), true) + unit + _FN.intTostrMoney(Convert.ToString(model.proposal.reason_ref2_us), true) + "  $", new Font(FontName, FontSize));
                    rowReason.Cells[0].MergeRight = 1;
                }

            }

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Borders.Color = Colors.Transparent;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].Borders.Left.Color = Colors.Black;
            row.Cells[3].AddParagraph("Reason :");
            row.Cells[3].MergeRight = 1;
            row.Cells[5].Elements.Add(_tmpReason);
            row.Cells[5].MergeRight = 13;
            row.Cells[5].Format.Font.Bold = false;
            row.Cells[18].Borders.Right.Color = Colors.Black;

            string Daterange = "";
            if (model.proposal != null)
            {
                if (!string.IsNullOrEmpty(model.proposal.price_period_from) && !string.IsNullOrEmpty(model.proposal.price_period_to))
                {
                    Daterange = string.Format("{0} - {1}", _FN.ConvertDateFormatBackFormat(model.proposal.price_period_from, "dd MMM yyyy"), _FN.ConvertDateFormatBackFormat(model.proposal.price_period_to, "dd MMM yyyy"));
                } else
                {
                    Daterange = "-";
                }
            }
            else
            {
                Daterange = "-";
            }
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Borders.Color = Colors.Transparent;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].Borders.Left.Color = Colors.Black;
            row.Cells[3].AddParagraph("Pricing Period :");
            row.Cells[3].MergeRight = 2;
            row.Cells[6].AddParagraph(Daterange);
            row.Cells[6].MergeRight = 3;
            row.Cells[6].Format.Font.Bold = false;
            //Hide Performance Bond
            row.Cells[10].AddParagraph("Credit condition :");
            row.Cells[10].MergeRight = 2;
            row.Cells[13].AddParagraph(string.IsNullOrEmpty(model.proposal.performance_bond) || model.proposal.performance_bond == "-" ? "-" : "Performance Bond " + model.proposal.performance_bond);
            row.Cells[13].MergeRight = 5;
            row.Cells[13].Format.Font.Bold = false;
            row.Cells[18].Borders.Right.Color = Colors.Black;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Borders.Color = Colors.Transparent;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("");
            row.Cells[0].MergeRight = 2;
            row.Cells[0].Borders.Left.Color = Colors.Black;
            row.Cells[3].AddParagraph("Quantity (" + _setting.feedstock.Where(x => x.key == model.feedstock).FirstOrDefault().unit + ") :");
            row.Cells[3].MergeRight = 2;
            row.Cells[6].AddParagraph(string.IsNullOrEmpty(model.proposal.volume) ? "-" : model.proposal.volume);
            row.Cells[6].MergeRight = 3;
            row.Cells[6].Format.Font.Bold = false;
            row.Cells[10].AddParagraph("Tolerance (%) :");
            row.Cells[10].MergeRight = 2;
            row.Cells[13].AddParagraph(string.IsNullOrEmpty(model.proposal.tolerance) ? "-" : model.proposal.tolerance_type + "" + model.proposal.tolerance + " " + model.proposal.tolerance_option);
            row.Cells[13].MergeRight = 5;
            row.Cells[13].Format.Font.Bold = false;
            row.Cells[18].Borders.Right.Color = Colors.Black;

            //row = table.AddRow();
            //row.HeadingFormat = true;
            //row.Format.Alignment = ParagraphAlignment.Left;
            //row.VerticalAlignment = VerticalAlignment.Center;
            //row.Borders.Color = Colors.Transparent;
            //row.Format.Font.Bold = true;
            //row.Cells[0].AddParagraph("");
            //row.Cells[0].MergeRight = 2;
            //row.Cells[0].Borders.Left.Color = Colors.Black;
            //row.Cells[0].Borders.Bottom.Color = Colors.Black;
            //row.Cells[3].AddParagraph("Tolerance (%) :");
            //row.Cells[3].MergeRight = 2;
            //row.Cells[6].AddParagraph(string.IsNullOrEmpty(model.proposal.tolerance) ? "-" : model.proposal.tolerance_type + "" + model.proposal.tolerance + " " + model.proposal.tolerance_option);
            //row.Cells[6].MergeRight = 12;
            //row.Cells[6].Format.Font.Bold = false;
            //row.Cells[18].Borders.Right.Color = Colors.Black;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Note :");
            row.Cells[0].MergeRight = 18;
            row.Cells[0].Borders.Top.Color = Colors.Black;
            row.Cells[0].Borders.Bottom.Color = Colors.White;

            row = table.AddRow();
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Format.Font.Bold = false;
            row.Cells[0].AddParagraph(string.IsNullOrEmpty(model.notes) ? "" : _RFN.AdjustIfTooWideToFitIn(row.Cells[0], model.notes, 22));
            row.Cells[0].MergeRight = 18;
            row.Cells[0].Borders.Top.Color = Colors.White;

        }

        private void FooterReport(Section section, CdpRootObject model)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            //row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy.");
            //row.Cells[icell].MergeRight = 37;

            //icell = 0;
            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            var approveName_1 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            double height = 0; double width = 0;
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            }

            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            //var approveName_2 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            approveName_1 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            }
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));

            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve4
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            if (!string.IsNullOrEmpty(approveName))
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            //Approve3
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            //if(!string.IsNullOrEmpty(approveName))
            //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            //else
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;



            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            approveName = !String.IsNullOrEmpty(approveName_1) ? _FN.GetNameOffApprove(model.approve_items, "APPROVE_1") : _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = !String.IsNullOrEmpty(approveName_1) ? _FN.GetNameOffApprove(model.approve_items, "APPROVE_2") : _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            // approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;



        }

        private void FooterReportTmp(Section section, CdpRootObject model)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            row.Cells[icell].AddParagraph("Requested, endorsed and approved can be done either via e-mail or hard copy.");
            row.Cells[icell].MergeRight = 37;

            icell = 0;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verifyed by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width, true));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve2
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve3
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            string approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;



        }

    }
}