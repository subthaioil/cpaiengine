﻿using com.pttict.engine.utility;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;

namespace ProjectCPAIEngine.Web.Report
{
    public partial class FreightCalculationReport : System.Web.UI.Page
    {
        ShareFn _FN = new ShareFn();
        ReportFunction _RFN = new ReportFunction();
        Document document;
        MigraDoc.DocumentObjectModel.Tables.Table table;
        Color TableBorder;
        Color TableGray;
        HtmlRemoval _htmlRemove = new HtmlRemoval();
        int FontSize = 7;
        string FontName = "Tahoma";
        List<string> images = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadXMLFromTest();
            string tTranId = Request.QueryString["TranID"];
            if (Request.QueryString["TranID"] != null)
            {
                //LoadDataFreight(Request.QueryString["TranID"].ToString().Decrypt());
                LoadDataFreight(Request.QueryString["TranID"].ToString());
            }
            else
            {
                if (Session["FclRootObject"] != null)
                {
                    FclRootObject _Freight = (FclRootObject)Session["FclRootObject"];
                    GenPDF(_Freight);
                    Session["FclRootObject"] = null;
                }
            }
        }

        private void LoadDataFreight(string TransactionID)
        {
            RequestData reqData = new RequestData();
            reqData.function_id = ConstantPrm.FUNCTION.F10000005;
            reqData.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            reqData.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            reqData.req_transaction_id = ConstantPrm.EnginGetEngineID();
            reqData.state_name = "";
            reqData.req_parameters = new req_parameters();
            reqData.req_parameters.p = new List<p>();
            reqData.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            reqData.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            reqData.req_parameters.p.Add(new p { k = "transaction_id", v = TransactionID });
            reqData.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.FREIGHT });
            reqData.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            reqData.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(reqData);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                FclRootObject _modelBunker = new JavaScriptSerializer().Deserialize<FclRootObject>(_model.data_detail);
                GenPDF(_modelBunker);
            }
            else
            {
                _FN.MessageBoxShowLink(this.Page, resData.response_message, "MainBoards.aspx");
            }
        }

        private void LoadXMLFromTest()
        {
            string PathTest = @"C:\Users\PimasutA\Documents\Json.txt";
            if (File.Exists(PathTest))
            {
                StreamReader _sr = new StreamReader(PathTest);
                string Line = _sr.ReadToEnd();
                FclRootObject _modelReport = new JavaScriptSerializer().Deserialize<FclRootObject>(Line);
                GenPDF(_modelReport);
            }


        }

        private void GenPDF(FclRootObject modelReport)
        {
            Document document = CreateDocument(modelReport);

            document.UseCmykColor = true;
            const bool unicode = true;
            //const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();
            // Save the document...
            //string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("CMCS{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmssffff")));
            string year = "";
            if (string.IsNullOrEmpty(modelReport.fcl_vessel_particular.date_time))
            {
                year = DateTime.Now.ToString("yyyy");
            }
            else
            {
                year = modelReport.fcl_vessel_particular.date_time.SplitWord("/")[2];
            }

            string vessel = "VESSEL";
            if (!string.IsNullOrEmpty(modelReport.fcl_vessel_particular.vessel_id))
            {
                vessel = VehicleServiceModel.GetNameInitial(modelReport.fcl_vessel_particular.vessel_id);
            }

            string purchase_no = "";
            if (string.IsNullOrEmpty(modelReport.fcl_vessel_particular.purchase_no))
            {
                purchase_no = EstimateFreightServiceModel.getLastFreightNo(DateTime.Now.ToString("yyMM"));
            } else {
                purchase_no = modelReport.fcl_vessel_particular.purchase_no;
            }
            string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("Freight{0}-{1} ({2}).pdf", year, vessel, purchase_no));
            pdfRenderer.PdfDocument.Save(filename);
            Response.Redirect(string.Format("TmpFile/{0}", new FileInfo(filename).Name));
            // ...and start a viewer.
            //Process.Start(filename);
        }

        public Document CreateDocument(FclRootObject modelReport)
        {
            TableBorder = Colors.Black;
            TableGray = Colors.LightGray;
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = "Freight";
            this.document.DefaultPageSetup.LeftMargin = 30;
            this.document.DefaultPageSetup.TopMargin = 30;
            this.document.DefaultPageSetup.BottomMargin = 30;
            DefineStyles();
            _RFN.document = document;
            _RFN.FontName = FontName;
            CreatePage(modelReport);
            return this.document;
        }

        void DefineStyles()
        {
            MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
            style.Font.Name = FontName;

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = FontName;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 10;

            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        void CreatePage(FclRootObject modelReport)
        {
            Section section = this.document.AddSection();
            //HeaderReport(section, modelReport);
            HeadingReport(section, modelReport);
            BottomZoneReport(section, modelReport);

            if (modelReport.fcl_button_event != "SaveAndPreview")
            {
                if (!string.IsNullOrEmpty(modelReport.fcl_pic_path))
                {
                    document.LastSection.AddPageBreak();
                    HeaderReport(section, modelReport);
                    ExplanImageReport(section, modelReport);
                }
            }
            
            //--/Web/FileUpload/FREIGHT/IMAGE/20170721151423_F10000008_FREIGHT.png
            //-------------------------------------

        }

        private void HeaderReport(Section section, FclRootObject modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;

            this.table.AddColumn("3cm");
            this.table.AddColumn("13cm");
            this.table.AddColumn("3cm");

            // Create the header of the table
            Row row = table.AddRow();
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            row.Cells[icell].AddParagraph("");
            //row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].AddParagraph("ESTIMATED FREIGHT CALCULATION");
            row.Cells[icell].Format.Font.Size = 9;
            icell += row.Cells[icell].MergeRight + 1;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
        }

        private void HeadingReport(Section section, FclRootObject modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;

            this.table.AddColumn("3cm");
            this.table.AddColumn("13cm");
            this.table.AddColumn("3cm");

            int row_hight_main_term = 13;

            // Create the header of the table
            Row row = table.AddRow();
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            row.Cells[icell].AddParagraph("");
            //row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].AddParagraph("Estimated Freight Calculation");
            row.Cells[icell].Format.Font.Size = 9;
            icell += row.Cells[icell].MergeRight + 1;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;

            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.TopPadding = 2;
            this.table.BottomPadding = 2;
            this.table.Rows.Height = 18;

            this.table.AddColumn("2.5cm");
            this.table.AddColumn("3.5cm");
            this.table.AddColumn("3cm");
            this.table.AddColumn("3cm");
            this.table.AddColumn("3cm");
            this.table.AddColumn("1.5cm");
            this.table.AddColumn("1.5cm");
            this.table.AddColumn("1.5cm");

            //row 1
            row = table.AddRow();
            icell = 0;
            row.Format.Font.Bold = true;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Vessel Particular");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].MergeRight = 3;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Vessel Speed (Knots)");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("select");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Ballast");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Laden");
            row.Cells[icell].Shading.Color = Colors.LightGray;

            //row 2
            row = table.AddRow(); icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Calculation Date");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_particular.date_time);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Capacity 98% (m3)");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_vessel_particular.capacity));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Full");
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.fcl_vessel_speed.fcl_full.flag == "Y")
            {
                row.Cells[icell].AddParagraph("x");
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            }
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_speed.fcl_full.ballast);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_speed.fcl_full.laden);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;

            //row 3
            row = table.AddRow(); icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Vessel Name");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_particular.vessel_name);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("DWT");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_vessel_particular.dwt));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Eco");
            icell += row.Cells[icell].MergeRight + 1;
            if (modelReport.fcl_vessel_speed.fcl_eco.flag == "Y")
            {
                row.Cells[icell].AddParagraph("x");
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            }
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_speed.fcl_eco.ballast);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_speed.fcl_eco.laden);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;

            //row 4
            row = table.AddRow(); icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Type");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_particular.type);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Displacement (MT)");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_vessel_particular.displacement));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("Max Draft (M)");
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_vessel_particular.max_draft));
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Lastest bunker price info");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Date");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Place");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Price");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].Format.Font.Bold = true;

            //row 5
            row = table.AddRow(); icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Built");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_particular.built);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Max Draft (M)");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_vessel_particular.max_draft));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("Cargo Tanks Coating");
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph(modelReport.fcl_vessel_particular.coating);
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            //icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Grade FO");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_latest_bunker_price.fcl_grade_fo.date);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], modelReport.fcl_latest_bunker_price.fcl_grade_fo.place, 0.5));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney2(modelReport.fcl_latest_bunker_price.fcl_grade_fo.price), 0.5));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;

            //row 6
            row = table.AddRow(); icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Age");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_particular.age);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Cargo Tanks Coating");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_vessel_particular.coating);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("Displacement (MT)");
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_vessel_particular.displacement));
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Grade MGO");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_latest_bunker_price.fcl_grade_mgo.date);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], modelReport.fcl_latest_bunker_price.fcl_grade_mgo.place, 0.5));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney2(modelReport.fcl_latest_bunker_price.fcl_grade_mgo.price), 0.5));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;

            //row 7
            row = table.AddRow();
            icell = 0;
            row.Format.Font.Bold = true;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Main Terms");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].MergeRight = 3;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Exchange rate");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(modelReport.fcl_exchange_rate == null ? "" : _FN.intTostrMoney2(modelReport.fcl_exchange_rate));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            //row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("As of");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.fcl_exchange_date) ? "" : modelReport.fcl_exchange_date);

            List<string> list_key_cargo = new List<string>();
            List<string> list_value_cargo = new List<string>();
            list_key_cargo.Add("Charterer");
            list_value_cargo.Add(modelReport.fcl_payment_terms.charterer);

            if (modelReport.fcl_payment_terms.Cargoes != null)
            {
                for (int cargo_idx = 0; cargo_idx < modelReport.fcl_payment_terms.Cargoes.Count; cargo_idx++)
                {                    
                    if(modelReport.fcl_payment_terms.Cargoes.Count == 1)
                    {
                        list_key_cargo.Add("Cargo ");
                    }
                    else
                    {
                        list_key_cargo.Add("Cargo " + (cargo_idx + 1));
                    }
                    list_value_cargo.Add(MaterialsServiceModel.GetName(modelReport.fcl_payment_terms.Cargoes[cargo_idx].cargo));
                    if(modelReport.fcl_payment_terms.Cargoes.Count == 1)
                    {
                        list_key_cargo.Add("Quantity " + " (" + modelReport.fcl_payment_terms.Cargoes[cargo_idx].unit + ")");

                    }
                    else
                    {
                        list_key_cargo.Add("Quantity " + (cargo_idx + 1) + " (" + modelReport.fcl_payment_terms.Cargoes[cargo_idx].unit + ")");
                    }                    
                    list_value_cargo.Add(_FN.intTostrMoney2(modelReport.fcl_payment_terms.Cargoes[cargo_idx].quantity));
                    if(modelReport.fcl_payment_terms.Cargoes.Count == 1)
                    {
                        list_key_cargo.Add("Tolerance " + " (+/-) (%)");
                    }
                    else
                    {
                        list_key_cargo.Add("Tolerance " + (cargo_idx + 1) + " (+/-) (%)");
                    }                    
                    list_value_cargo.Add(_FN.intTostrMoney2(modelReport.fcl_payment_terms.Cargoes[cargo_idx].tolerance));
                }
            }
            else
            {
                list_key_cargo.Add("Cargo");
                list_value_cargo.Add("");
                list_key_cargo.Add("Quantity");
                list_value_cargo.Add("");
                list_key_cargo.Add("Tolerance (+/-) (%)");
                list_value_cargo.Add("");
            }

            list_key_cargo.Add("Laycan");
            list_value_cargo.Add(modelReport.fcl_payment_terms.laycan_from + " to " + modelReport.fcl_payment_terms.laycan_to);
            list_key_cargo.Add("Laytime (Hrs)");
            list_value_cargo.Add(modelReport.fcl_payment_terms.laytime);
            list_key_cargo.Add("BSS");
            list_value_cargo.Add(modelReport.fcl_payment_terms.bss);
            if (modelReport.fcl_payment_terms.ControlPort != null)
            {
                for (int idx = 0; idx < modelReport.fcl_payment_terms.ControlPort.Count; idx++)
                { 
                    if(modelReport.fcl_payment_terms.ControlPort.Count == 1)
                    {
                        list_key_cargo.Add("Load Port ");
                    }
                    else
                    {
                        list_key_cargo.Add("Load Port " + (idx + 1));
                    }
                    string port_name = "";
                    string jetty_name = string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[idx].PortName) ? "" : modelReport.fcl_payment_terms.ControlPort[idx].PortName.ToUpper() == "OTHER" ? "" : modelReport.fcl_payment_terms.ControlPort[idx].PortName;
                    string location = string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[idx].PortLocation) ? "" : modelReport.fcl_payment_terms.ControlPort[idx].PortLocation;
                    string country = string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[idx].PortCountry) ? "" : modelReport.fcl_payment_terms.ControlPort[idx].PortCountry;
                    if (!string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[idx].PortOtherDesc))
                    {
                        jetty_name = modelReport.fcl_payment_terms.ControlPort[idx].PortOtherDesc;
                    }

                    string[] arr_port = { jetty_name, location, country };
                    for (int i = 0; i < arr_port.Length; i++)
                    {
                        if (arr_port[i] != "")
                        {
                            port_name += arr_port[i] + ", ";
                        }
                    }
                    list_value_cargo.Add(port_name.Substring(0, port_name.Length - 2));
                }
            }
            else
            {
                list_key_cargo.Add("Load Port");
                list_value_cargo.Add("");
            }

            if (modelReport.fcl_payment_terms.ControlDischarge != null)
            {
                for (int idx = 0; idx < modelReport.fcl_payment_terms.ControlDischarge.Count; idx++)
                {
                    if(modelReport.fcl_payment_terms.ControlDischarge.Count == 1)
                    {
                        list_key_cargo.Add("Discharge Port ");
                    }
                    else
                    {
                        list_key_cargo.Add("Discharge Port " + (idx + 1));
                    } 

                    string port_name = "";
                    string jetty_name = string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[idx].DischargeName) ? "" : modelReport.fcl_payment_terms.ControlDischarge[idx].DischargeName.ToUpper() == "OTHER" ? "" : modelReport.fcl_payment_terms.ControlDischarge[idx].DischargeName;
                    string location = string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[idx].PortLocation) ? "" : modelReport.fcl_payment_terms.ControlDischarge[idx].PortLocation;
                    string country = string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[idx].PortCountry) ? "" : modelReport.fcl_payment_terms.ControlDischarge[idx].PortCountry;
                    if (!string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[idx].PortOtherDesc))
                    {
                        jetty_name = modelReport.fcl_payment_terms.ControlDischarge[idx].PortOtherDesc;
                    }

                    string[] arr_port = { jetty_name, location, country };
                    for (int i = 0; i < arr_port.Length; i++)
                    {
                        if (arr_port[i] != "")
                        {
                            port_name += arr_port[i] + ", ";
                        }
                    }
                    list_value_cargo.Add(port_name.Substring(0, port_name.Length - 2));
                }
            }
            else
            {
                list_key_cargo.Add("Discharge Port");
                list_value_cargo.Add("");
            }            

            List<string> list_key_term = new List<string>();
            //list_key_term.Add("Unit");
            list_key_term.Add("Demurrage (" + modelReport.fcl_payment_terms.dem_unit + ")");
            list_key_term.Add("Payment Term");
            list_key_term.Add("Transit Loss (%)");
            list_key_term.Add("Broker Name");
            list_key_term.Add("Broker Commission (%)");
            list_key_term.Add("Address Commission (%)");
            list_key_term.Add("Total Commission (%)");
            list_key_term.Add("Witholding Tax (%)");

            List<string> list_value_term = new List<string>();
            //list_value_term.Add(modelReport.fcl_payment_terms.unit);
            list_value_term.Add(_FN.intTostrMoney2(modelReport.fcl_payment_terms.dem));
            list_value_term.Add(modelReport.fcl_payment_terms.payment_term);
            list_value_term.Add(_FN.intTostrMoney2(modelReport.fcl_payment_terms.transit_loss));
            list_value_term.Add(modelReport.fcl_payment_terms.broker);
            list_value_term.Add(_FN.intTostrMoney2(modelReport.fcl_payment_terms.broker_comm));
            list_value_term.Add(_FN.intTostrMoney2(modelReport.fcl_payment_terms.add_comm));
            list_value_term.Add(_FN.intTostrMoney2(modelReport.fcl_payment_terms.total_comm));
            list_value_term.Add(_FN.intTostrMoney2(modelReport.fcl_payment_terms.withold_tax));

            List<string> list_key_term_exchange = new List<string>();
            list_key_term_exchange.Add("Bunker Consumption\n(MT/Day)");
            list_key_term_exchange.Add("Load");
            list_key_term_exchange.Add("Discharge");
            list_key_term_exchange.Add("Streaming Ballast");
            list_key_term_exchange.Add("Steaming Laden");
            list_key_term_exchange.Add("Idle");
            list_key_term_exchange.Add("Tank cleaning");
            list_key_term_exchange.Add("Heating");

            List<string> list_value_term_exchange_fo = new List<string>();
            list_value_term_exchange_fo.Add("FO");
            list_value_term_exchange_fo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_load.fo));
            list_value_term_exchange_fo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_discharge.fo));
            list_value_term_exchange_fo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_steaming_ballast.fo));
            list_value_term_exchange_fo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_steaming_laden.fo));
            list_value_term_exchange_fo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_idle.fo));
            list_value_term_exchange_fo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_tack_cleaning.fo));
            list_value_term_exchange_fo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_heating.fo));

            List<string> list_value_term_exchange_mgo = new List<string>();
            list_value_term_exchange_mgo.Add("MGO");
            list_value_term_exchange_mgo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_load.mgo));
            list_value_term_exchange_mgo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_discharge.mgo));
            list_value_term_exchange_mgo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_steaming_ballast.mgo));
            list_value_term_exchange_mgo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_steaming_laden.mgo));
            list_value_term_exchange_mgo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_idle.mgo));
            list_value_term_exchange_mgo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_tack_cleaning.mgo));
            list_value_term_exchange_mgo.Add(_FN.intTostrMoney2(modelReport.fcl_bunker_consumption.fcl_heating.mgo));

            int[] arr_row = { list_key_cargo.Count, list_key_term.Count, list_key_term_exchange.Count };
            int max_row = arr_row.Max();
            for (int i = 0; i < max_row; i++)
            {
                row = table.AddRow(); icell = 0;
                row.Height = row_hight_main_term;
                row.Format.Font.Size = FontSize - 1;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Center;

                row.Cells[icell].AddParagraph(getValueFromList(list_key_cargo, list_key_cargo.Count, i));
                icell += row.Cells[icell].MergeRight + 1;
                string value1 = getValueFromList(list_value_cargo, list_value_cargo.Count, i);
                row.Cells[icell].AddParagraph(value1);
                row.Cells[icell].Format.Alignment = getParagraphAlignment(value1);
                icell += row.Cells[icell].MergeRight + 1;

                row.Cells[icell].AddParagraph(getValueFromList(list_key_term, list_key_term.Count, i));
                icell += row.Cells[icell].MergeRight + 1;
                string value2 = getValueFromList(list_value_term, list_value_term.Count, i);
                row.Cells[icell].AddParagraph(value2);
                row.Cells[icell].Format.Alignment = getParagraphAlignment(value2);
                icell += row.Cells[icell].MergeRight + 1;

                row.Cells[icell].AddParagraph(getValueFromList(list_key_term_exchange, list_key_term_exchange.Count, i));
                if (i == 0)
                {
                    row.Cells[icell].Shading.Color = Colors.LightGray;
                    row.Cells[icell].Format.Font.Bold = true;
                }
                icell += row.Cells[icell].MergeRight + 1;
                string value3 = getValueFromList(list_value_term_exchange_fo, list_value_term_exchange_fo.Count, i);
                row.Cells[icell].AddParagraph(value3);
                row.Cells[icell].Format.Alignment = getParagraphAlignment(value3);
                if (i == 0)
                {
                    row.Cells[icell].Shading.Color = Colors.LightGray;
                    row.Cells[icell].Format.Font.Bold = true;
                }
                icell += row.Cells[icell].MergeRight + 1;
                string value4 = getValueFromList(list_value_term_exchange_mgo, list_value_term_exchange_mgo.Count, i);
                row.Cells[icell].AddParagraph(value4);
                row.Cells[icell].Format.Alignment = getParagraphAlignment(value4);
                if (i == 0)
                {
                    row.Cells[icell].Shading.Color = Colors.LightGray;
                    row.Cells[icell].Format.Font.Bold = true;
                }
                icell += row.Cells[icell].MergeRight + 1;
            }
        }

        public string GetPathImg(string path)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
        }

        private void BottomZoneReport(Section section, FclRootObject modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.TopPadding = 2;
            this.table.BottomPadding = 2;
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;

            this.table.AddColumn("4cm");
            this.table.AddColumn("2cm");
            this.table.AddColumn("3cm");
            this.table.AddColumn("3cm");
            this.table.AddColumn("3cm");
            this.table.AddColumn("3cm");
            this.table.AddColumn("1.5cm");
            //this.table.AddColumn("1.5cm");

            if (modelReport.fcl_bunker_cost == null)
            {
                modelReport.fcl_bunker_cost = new List<FclBunkerCost>();
                modelReport.fcl_bunker_cost.Add(new FclBunkerCost());
            }

            // Create the header of the table
            Row row = table.AddRow();
            int icell = 0;
            row.Format.Font.Bold = true;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Time for operation (Days)");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].MergeRight = 3;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Bunker Cost (USD)");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].MergeRight = 2;

            //row 1
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Time spending at loading port");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_time_for_operation.loading_port));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Distance (Laden) NM");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_time_for_operation.distance_laden));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Bunker at loading");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_bunker_cost[0].loading));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            //row 2
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Time spending at discharge port");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_time_for_operation.discharge_port));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Time steaming (Laden)");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_time_for_operation.laden));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Bunker at discharge port");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_bunker_cost[0].discharge_port));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            //row 3
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Distance (Ballast) NM");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_time_for_operation.distance_ballast));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Time for tank cleaning");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_time_for_operation.tank_cleaning));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Bunker at steaming ballast");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_bunker_cost[0].steaming_ballast));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            //row 4
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Time steaming (Ballast)");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_time_for_operation.ballast));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Total duration per voyage");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_time_for_operation.total_duration));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Bunker at steaming laden");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_bunker_cost[0].steaming_laden));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            //row 4
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Port Charge (USD)");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 3;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Bunker at tank cleaning");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_bunker_cost[0].tank_cleaning));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            //////////////////////////////////////////////////////////////////////////////
            int maxRowPort = modelReport.fcl_payment_terms.ControlPort.Count() + modelReport.fcl_payment_terms.ControlDischarge.Count();
            int portNumMax = modelReport.fcl_payment_terms.ControlPort.Count();
            int discNumMax = modelReport.fcl_payment_terms.ControlDischarge.Count();
            int portNum = 0;
            int discNum = 0;
            for (int i = 0; i < maxRowPort; i++)
            {
                string portName = "";
                string portIndex = "";
                string portValue = "";
                if (portNum < portNumMax)
                {
                    if (!string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[portNum].PortName))
                    {
                        if (modelReport.fcl_payment_terms.ControlPort[portNum].PortName == "OTHER")
                        {
                            if (!string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[portNum].PortOtherDesc))
                            {
                                portName = modelReport.fcl_payment_terms.ControlPort[portNum].PortOtherDesc + ", ";
                            }
                        }
                        else
                        {
                            portName = modelReport.fcl_payment_terms.ControlPort[portNum].PortName + ", ";
                        }
                    }

                    //portName = !string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[portNum].PortName) ? modelReport.fcl_payment_terms.ControlPort[portNum].PortName + ", " : "";
                    portName += !string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[portNum].PortLocation) ? modelReport.fcl_payment_terms.ControlPort[portNum].PortLocation + ", " : "";
                    portName += !string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlPort[portNum].PortCountry) ? modelReport.fcl_payment_terms.ControlPort[portNum].PortCountry : "";
                    if(modelReport.fcl_payment_terms.ControlPort != null)
                    {
                        if(modelReport.fcl_payment_terms.ControlPort.Count == 1)
                        {
                            portIndex = "Port disbursement at load port " + " (" + portName + ")";
                        }
                        else
                        {
                            portIndex = "Port disbursement at load port " + (portNum + 1) + " (" + portName + ")";
                        }
                    }
                    else
                    {
                        portIndex = "Port disbursement at load port " + " (" + portName + ")";
                    } 
                    portValue = modelReport.fcl_payment_terms.ControlPort[portNum].PortValue;
                    portNum += 1;
                }
                else if (discNum < discNumMax)
                {
                    if (!string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[discNum].DischargeName))
                    {
                        if (modelReport.fcl_payment_terms.ControlDischarge[discNum].DischargeName == "OTHER")
                        {
                            if (!string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[discNum].PortOtherDesc))
                            {
                                portName = modelReport.fcl_payment_terms.ControlDischarge[discNum].PortOtherDesc + ", ";
                            }
                        }
                        else
                        {
                            portName = modelReport.fcl_payment_terms.ControlDischarge[discNum].DischargeName + ", ";
                        }
                    }

                    // = !string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[discNum].DischargeName) ? modelReport.fcl_payment_terms.ControlDischarge[discNum].DischargeName + ", " : "";
                    portName += !string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[discNum].PortLocation) ? modelReport.fcl_payment_terms.ControlDischarge[discNum].PortLocation + ", " : "";
                    portName += !string.IsNullOrEmpty(modelReport.fcl_payment_terms.ControlDischarge[discNum].PortCountry) ? modelReport.fcl_payment_terms.ControlDischarge[discNum].PortCountry : "";
                    if(modelReport.fcl_payment_terms.ControlDischarge != null)
                    {
                        if (modelReport.fcl_payment_terms.ControlDischarge.Count == 1)
                        {
                            portIndex = "Port disbursement at discharging port " + " (" + portName + ")";
                        }
                        else
                        {
                            portIndex = "Port disbursement at discharging port " + (discNum + 1) + " (" + portName + ")";
                        }
                    }
                    else
                    {
                        portIndex = "Port disbursement at discharging port " + " (" + portName + ")";
                    }                    
                    
                    portValue = modelReport.fcl_payment_terms.ControlDischarge[discNum].DischargeValue;
                    discNum += 1;
                }

                if (i == 0)
                {
                    //row 12
                    row = table.AddRow(); icell = 0;
                    row.Height = 13;
                    row.Format.Font.Size = FontSize - 1;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[icell].AddParagraph(portIndex);
                    row.Cells[icell].MergeRight = 2;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(portValue));
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("Total Bunker Cost");
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.total_bunker_cost));
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                    //row.Cells[icell].MergeRight = 1;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
                    icell += row.Cells[icell].MergeRight + 1;
                }
                else if (i == 1)
                {
                    //row 13
                    row = table.AddRow(); icell = 0;
                    row.Height = 13;
                    row.Format.Font.Size = FontSize - 1;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[icell].AddParagraph(portIndex);
                    row.Cells[icell].MergeRight = 2;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney2(portValue));
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
                    icell += row.Cells[icell].MergeRight + 1;
                }
                else
                {
                    row = table.AddRow(); icell = 0;
                    row.Height = 13;
                    row.Format.Font.Size = FontSize - 1;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[icell].AddParagraph(portIndex);
                    row.Cells[icell].MergeRight = 2;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(portValue);
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
                    icell += row.Cells[icell].MergeRight + 1;
                }
            }

            row = table.AddRow(); icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Total Port Charge");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.total_port_charge));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");

            //row 7
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Total costs (USD)");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;

            //row 8
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Fixed cost per day (USD/Day)");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.fix_cost));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Others");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.others));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //row 9
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Total Fixed cost");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.total_fix_cost_per_day));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("Total costs");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.total_expenses));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            //row.Cells[icell].AddParagraph("Suggested adding margin (%)");
            ////row.Cells[icell].Format.Font.Size = FontSize - 2;
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.suggested_margin));
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //icell += row.Cells[icell].MergeRight + 1;

            //row 10
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Total commission deduction");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.total_commession));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Suggested adding margin (%)");
            row.Cells[icell].Format.Font.Size = FontSize - 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.suggested_margin));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("Suggested Freight after adding margin");
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.suggested_margin_value));
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //icell += row.Cells[icell].MergeRight + 1;

            //row 11
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Withholding Tax");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.withold_tax));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Suggested Freight after adding margin");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.suggested_margin_value));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("Total costs");
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_other_cost.total_expenses));
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //icell += row.Cells[icell].MergeRight + 1;

            //row 12
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Results");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("USD");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("THB");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            string unit = "";
            if (modelReport.fcl_results.offer_freight != null)
            {
                unit = modelReport.fcl_results.offer_freight.offer_by + "/" + (modelReport.fcl_payment_terms.Cargoes[0].unit == "OTHER" ? modelReport.fcl_payment_terms.Cargoes[0].unit_other : modelReport.fcl_payment_terms.Cargoes[0].unit);
            }
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("%");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Remark");
            row.Cells[icell].Shading.Color = Colors.LightGray;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;

            //row 13
            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Offer freight " + " (Offer By: " + modelReport.fcl_results.offer_freight.offer_by + ")");
            row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.offer_freight.usd));
            if (modelReport.fcl_results.offer_freight.offer_by == "USD")
                row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.offer_freight.baht));
            if (modelReport.fcl_results.offer_freight.offer_by == "THB")
                row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.offer_freight.mt));//USD/MT
            if (modelReport.fcl_results.offer_freight.offer_by == "USD/MT")
                row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].VerticalAlignment = VerticalAlignment.Top;
            if (!string.IsNullOrEmpty(modelReport.fcl_remark))
            {
                Paragraph _prg = row.Cells[icell].AddParagraph();
                _htmlRemove.ValuetoParagraph(modelReport.fcl_remark, _prg);
            }
            else
            {
                row.Cells[icell].AddParagraph("");
            }

            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].MergeDown = 6;

            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Total variable cost\n(Bunker + Port charge + Comm + Tax + Other)");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.total_variable_cost.usd));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.total_variable_cost.baht));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Profit over variable cost");
            row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.profit_over.usd));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            if (modelReport.fcl_results.offer_freight.offer_by == "USD")
                row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.profit_over.baht));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            if (modelReport.fcl_results.offer_freight.offer_by == "THB")
                row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 2;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.profit_over.percent));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Total fixed cost");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.total_fix_cost.usd));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.total_fix_cost.baht));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Total costs (Variable + Fixed)");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.total_cost.usd));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.total_cost.baht));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("NET Profit over total cost");
            row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.profit_over_total.usd));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            if (modelReport.fcl_results.offer_freight.offer_by == "USD")
                row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.profit_over_total.baht));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            if (modelReport.fcl_results.offer_freight.offer_by == "THB")
                row.Cells[icell].Format.Font.Bold = true;
            icell += row.Cells[icell].MergeRight + 2;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney2(modelReport.fcl_results.profit_over_total.percent));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            icell = 0;
            row.Height = 13;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Market freight reference");
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.fcl_results.market_ref.usd) ? "" : _FN.intTostrMoney2(modelReport.fcl_results.market_ref.usd));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.IsNullOrEmpty(modelReport.fcl_results.market_ref.baht) ? "" : _FN.intTostrMoney2(modelReport.fcl_results.market_ref.baht));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
        }

        private void ExplanImageReport(Section section, FclRootObject modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 450;
            Column column = this.table.AddColumn("19cm");
            string url = JSONSetting.getGlobalConfig("ROOT_URL") + "Web/FileUpload/FREIGHT/IMAGE/";
            Row row = table.AddRow();
            int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("");
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetExplanImage(modelReport.fcl_pic_path, url));
            images.Add(_img.Name);            
            _img.Width = new Unit(17, UnitType.Centimeter);            
        }

        // -------- Util ---------
        private ParagraphAlignment getParagraphAlignment(string value)
        {
            double dd = 0;
            if (double.TryParse(value, out dd))
            {
                return ParagraphAlignment.Right;
            }
            else
            {
                if (value == "FO" || value == "MGO" || value == "-")
                {
                    return ParagraphAlignment.Center;
                }
                else
                {
                    return ParagraphAlignment.Center;
                }
            }
        }

        private string getValueFromList(List<string> paStr, int pnMax, int pnIdx)
        {
            if (pnMax > pnIdx)
            {
                return paStr[pnIdx];
            }
            else
            {
                return "";
            }
        }
    }
}