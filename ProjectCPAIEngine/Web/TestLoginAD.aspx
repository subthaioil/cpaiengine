﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestLoginAD.aspx.cs" Inherits="ProjectCPAIEngine.Web.LoginAD" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="ldappathlb" runat="server" Text="LDAP Path : "></asp:Label>
            <asp:TextBox ID="ldappathtb" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="usernamelb" runat="server" Text="username : "></asp:Label>
            <asp:TextBox ID="usernametb" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="passwordlb" runat="server" Text="password : "></asp:Label>
            <asp:TextBox ID="passwordtb" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="domainlb" runat="server" Text="domain : "></asp:Label>
            <asp:TextBox ID="domaintb" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="loginbtn" runat="server" Text="Loing(ok)" OnClick="sendBtn_Click" />
        </div>
        <div>
            <asp:Button ID="loginbtn1" runat="server" Text="Loing1" OnClick="sendBtn_Click1" />
        </div>
        <div>         
            <asp:TextBox ID="error" runat="server" Height="48px" Width="1081px"></asp:TextBox>
        </div>
    </form>
</body>
</html>
