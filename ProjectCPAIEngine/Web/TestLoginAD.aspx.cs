﻿using DSAd.service;
using ProjectCPAIEngine.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectCPAIEngine.Web
{
    public partial class LoginAD : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void sendBtn_Click(object sender, EventArgs e)
        {
            string username = usernametb.Text;
            string password = passwordtb.Text;
            string domain = domaintb.Text;
            string path = ldappathtb.Text;

            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + path
                                                                             + " : " + username
                                                                             + " : " + password
                                                                             + " : " + domain
                                                                             + "');", true);
            ADService adService = new ADService();
            //string result = adService.adLoginTest(path,username, domain, password);
            bool t = adService.adLogin(path, username, domain, password);
            if (t) error.Text = "success" + adService.resp_code + adService.resp_desc; else error.Text = "fail" + adService.resp_code + adService.resp_desc;
            //ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + error.Text + "');", true);
        }


        protected void sendBtn_Click1(object sender, EventArgs e)
        {
            string username = usernametb.Text;
            string password = passwordtb.Text;
            string domain = domaintb.Text;
            string path = ldappathtb.Text;

            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + path
                                                                             + " : " + username
                                                                             + " : " + password
                                                                             + " : " + domain
                                                                             + "');", true);
            ADService adService = new ADService();
            string result = adService.adLoginTest(path,username, domain, password);
             error.Text = result;
            //ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + error.Text + "');", true);
        }
    }
}