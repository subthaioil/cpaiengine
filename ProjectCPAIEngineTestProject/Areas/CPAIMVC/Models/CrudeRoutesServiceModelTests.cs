﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class CrudeRoutesServiceModelTests
    {
        [TestMethod()]
        public void AddTest()
        {
            try
            {

                CrudeRoutesViewModel_Detail model = new CrudeRoutesViewModel_Detail();
                CrudeRoutesServiceModel service = new CrudeRoutesServiceModel();
                ReturnValue rtn = new ReturnValue();


                model.Date = "";
                model.TD2 = "";
                model.TD3 = "";
                model.TD15 = "";
                model.Status = "";

                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/225/2016"; // Valid Format

                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                // TB Blank
                model.TD2 = "";
                model.TD3 = "";
                model.TD15 = "";
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                model.TD2 = "222a.325sss"; // Valid Format
                model.TD3 = "";
                model.TD15 = "";
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                model.TD2 = "222.325";
                model.TD3 = "aa222.325";    // Valid Format
                model.TD15 = "";
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                model.TD2 = "222.325";
                model.TD3 = "11.325";
                model.TD15 = "aas";         // Valid Format
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                model.TD2 = "222.325";
                model.TD3 = "11.325";
                model.TD15 = "55.55";
                model.Status = ""; // Blank
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                // Data already in the database.
                model.Date = "23/11/2016";
                model.TD2 = "222.325";
                model.TD3 = "11.325";
                model.TD15 = "55.55";
                model.Status = "ACTIVE";

                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                // Data is holiday
                model.Date = "28/08/2016";
                model.TD2 = "222.325";
                model.TD3 = "11.325";
                model.TD15 = "55.55";
                model.Status = "ACTIVE";

                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                // Add Complete
                model.Date = "24/11/2016";
                model.TD2 = "222.325";
                model.TD3 = "11.325";
                model.TD15 = "55.55";
                model.Status = "ACTIVE";

                rtn = service.Add(model, "ADMIN");

                Assert.AreEqual(true, rtn.Status, rtn.Message);

                return;
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void EditTest()
        {
            try
            {

                CrudeRoutesViewModel_Detail model = new CrudeRoutesViewModel_Detail();
                CrudeRoutesServiceModel service = new CrudeRoutesServiceModel();
                ReturnValue rtn = new ReturnValue();


                model.Date = "";
                model.TD2 = "";
                model.TD3 = "";
                model.TD15 = "";
                model.Status = "";

                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/225/2016"; // Valid Format

                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                // TB Blank
                model.TD2 = "";
                model.TD3 = "";
                model.TD15 = "";
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                model.TD2 = "222a.325sss"; // Valid Format
                model.TD3 = "";
                model.TD15 = "";
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                model.TD2 = "222.325";
                model.TD3 = "aa222.325";    // Valid Format
                model.TD15 = "";
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                model.TD2 = "222.325";
                model.TD3 = "11.325";
                model.TD15 = "aas";         // Valid Format
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.Date = "23/11/2016";
                model.TD2 = "222.325";
                model.TD3 = "11.325";
                model.TD15 = "55.55";
                model.Status = ""; // Blank
                rtn = service.Add(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                // Data is holiday
                model.Date = "28/08/2016";
                model.TD2 = "222.325";
                model.TD3 = "11.325";
                model.TD15 = "55.55";
                model.Status = "ACTIVE";

                // Can not be edit the date
                model.Date = "23/11/2017";
                model.TD2 = "111.444";
                model.TD3 = "222.55555";
                model.TD15 = "333.66";
                model.Status = "ACTIVE";

                rtn = service.Edit(model, "ADMIN");

                Assert.AreEqual(false, rtn.Status, rtn.Message);

                // Edit Complete
                model.Date = "23/11/2016";
                model.TD2 = "111.444";
                model.TD3 = "222.55555";
                model.TD15 = "333.66";
                model.Status = "ACTIVE";

                rtn = service.Edit(model, "ADMIN");

                Assert.AreEqual(true, rtn.Status, rtn.Message);

                return;
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void CheckHaveDataTest()
        {
            try
            {
                bool rtn = false;
                rtn = CrudeRoutesServiceModel.CheckHaveData("25/11/2016");
                Assert.AreEqual(false, rtn, "ไม่พบข้อมูล");

                rtn = CrudeRoutesServiceModel.CheckHaveData("23/11/2016");
                Assert.AreEqual(true, rtn, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetTest()
        {
            CrudeRoutesViewModel_Detail model = new CrudeRoutesViewModel_Detail();
            CrudeRoutesServiceModel service = new CrudeRoutesServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model = service.Get("23/11/2017");
                Assert.AreEqual(null, model.Date, "ไม่ใส่รหัส");

                model = service.Get("23/11/2016");
                Assert.AreNotEqual(null, model, "ไม่พบข้อมูล");


            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void SearchTest()
        {
            CrudeRoutesViewModel_Seach model = new CrudeRoutesViewModel_Seach();
            CrudeRoutesServiceModel service = new CrudeRoutesServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {

                // Search All
                model.sDateFromTo = "";
                model.sStatus = "";

                rtn = service.Search(ref model);
                Assert.AreEqual(true, rtn.Status, rtn.Message);

                // Search by condition
                model.sDateFromTo = "01/11/2016 to 30/11/2016";
                model.sStatus = "ACTIVE";

                rtn = service.Search(ref model);
                Assert.AreEqual(true, rtn.Status, rtn.Message);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

    }
}