﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class HedgingDealServiceModelTests
    {
        [TestMethod()]
        public void getTransactionByIDTest()
        {
            HedgingDealServiceModel service = new HedgingDealServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                string id = "201706231817110155980";
                var test = HedgingDealServiceModel.getTransactionByID(id);
            }
            catch (Exception ex)
            {
                return;
            }
        }

        [TestMethod()]
        public void AddTicketTest()
        {
            HedgingDealServiceModel service = new HedgingDealServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                rtn = service.AssignTicketNo("TCKT-1706-0026", "", "DEAL-1706-0031", "Test", new EntityCPAIEngine());
            }
            catch
            {
                return;
            }
        }
    }
}