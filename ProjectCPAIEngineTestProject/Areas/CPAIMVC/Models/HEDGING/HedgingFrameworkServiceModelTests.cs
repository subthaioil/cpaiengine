﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class HedgingFrameworkServiceModelTests
    {
        [TestMethod()]
        public void SearchTest()
        {
            HedgingFrameworkViewModel_Search model = new HedgingFrameworkViewModel_Search();
            HedgingFrameworkServiceModel service = new HedgingFrameworkServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model.sActiveDate = "24/05/2017 to 26/05/2017";
                model.sUnderlying = "Y900SN";
                model.sStatus = "Status";
                rtn = service.Search(ref model);

                //model.sOrder = "160e682dc12e43129f75abe924d658c0";
                //rtn = service.SearchDetail(ref model);

                List<SelectListItem> lst = DropdownServiceModel.getUnderlying(false, string.Empty, "CPAI", true);
            }
            catch (Exception ex)
            {
                return;
            }
        }

        [TestMethod()]
        public void AddTest()
        {
            HedgingFrameworkViewModel_Detail model = new HedgingFrameworkViewModel_Detail();
            HedgingFrameworkServiceModel service = new HedgingFrameworkServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model.ActiveDate = "24/05/2017 to 26/05/2017";
                model.Underlying = "YD150SW";
                model.Description = "Description";
                model.Remark = "Remark";
                model.Status = "SUBMIT";
                model.DetailData = new List<HedgingFrameworkViewModel_DetailData>();
                model.DetailData.Add(new HedgingFrameworkViewModel_DetailData
                {
                    Order = "1",
                    HedgeType = "HedgeType",
                    Type = "Type",
                    Title = "Title",
                    ValueType = "ValueType",
                    Volume = "Volume",
                    VolumeUnit = "VolumeUnit",
                    Price1 = "Price1",
                    Price1Remark = "Price1Remark",
                    Price2 = "Price2",
                    Price2Remark = "Price2Remark",
                    Price3 = "Price3",
                    Price3Remark = "Price3Remark",
                    PriceUnit = "PriceUnit",
                });

                rtn = service.Add(ref model, "Test User");
            }
            catch (Exception ex)
            {
                return;
            }
        }

        [TestMethod()]
        public void EditTest()
        {
            HedgingFrameworkViewModel_Detail model = new HedgingFrameworkViewModel_Detail();
            HedgingFrameworkServiceModel service = new HedgingFrameworkServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model.OrderID = "8143eb2323574f00a3348db65a56a920";
                model.ActiveDate = "24/05/2017 to 26/05/2017";
                model.Underlying = "Y900SN";
                model.Description = "Description";
                model.Remark = "Remark";
                model.Status = ConstantPrm.ACTION.DRAFT;
                model.DetailData = new List<HedgingFrameworkViewModel_DetailData>();
                model.DetailData.Add(new HedgingFrameworkViewModel_DetailData
                {
                    RowID = "f3c7359154fe466187b062ba98631b93",
                    Order = "1",
                    HedgeType = "HTD",
                    Type = "VOLUME",
                    Title = "Title 1",
                    ValueType = "FIX",
                    Volume = "563",
                    VolumeUnit = "KBBL",
                    Price1 = "10",
                    Price1Remark = "Price1Remark",
                    Price2 = "20",
                    Price2Remark = "Price2Remark",
                    Price3 = "30",
                    Price3Remark = "Price3Remark",
                    PriceUnit = "$/BBL",
                });

                model.DetailData.Add(new HedgingFrameworkViewModel_DetailData
                {
                    RowID = "",
                    Order = "2",
                    HedgeType = "HTD",
                    Type = "VOLUME",
                    Title = "Title 2",
                    ValueType = "ACT",
                    Volume = "123",
                    VolumeUnit = "KBBL",
                    Price1 = "100",
                    Price1Remark = "Price1Remark1",
                    Price2 = "200",
                    Price2Remark = "Price2Remark2",
                    Price3 = "300",
                    Price3Remark = "Price3Remark3",
                    PriceUnit = "$/BBL",
                });

                rtn = service.Edit(model, "Test User");
            }
            catch (Exception ex)
            {
                return;
            }
        }

        [TestMethod()]
        public void GetDetailTest()
        {
            HedgingFrameworkViewModel_Detail model = new HedgingFrameworkViewModel_Detail();
            HedgingFrameworkServiceModel service = new HedgingFrameworkServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model = service.GetDetail("");
                Assert.AreEqual(null, model.OrderID, "ไม่ใส่รหัส");

                model = service.GetDetail("1000000420");
                Assert.AreEqual(null, model.OrderID, "ไม่พบข้อมูล");

                model = service.GetDetail("160e682dc12e43129f75abe924d658c0");
                Assert.AreNotEqual(null, model.OrderID, "ไม่พบข้อมูล");


            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void CancelTest()
        {
            HedgingFrameworkViewModel_Detail model = new HedgingFrameworkViewModel_Detail();
            HedgingFrameworkServiceModel service = new HedgingFrameworkServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model.OrderID = "776ed2954a0845f998fa1ab0d7dad1ba";
                model.Status = ConstantPrm.ACTION.CANCEL;
                

                rtn = service.Cancel(model, "Test User", "Test cancel");
            }
            catch (Exception ex)
            {
                return;
            }
        }
    }
}