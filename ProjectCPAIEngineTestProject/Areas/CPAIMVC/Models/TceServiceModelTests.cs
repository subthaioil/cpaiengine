﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class TceServiceModelTests
    {
        [TestMethod()]
        public void getTransactionByIDTest()
        {
            try
            {
                string json = TceServiceModel.getTransactionByID("201701061625000053852");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }
    }
}