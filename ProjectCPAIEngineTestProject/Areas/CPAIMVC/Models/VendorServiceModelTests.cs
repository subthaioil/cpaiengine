﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class VendorServiceModelTests
    {
        [TestMethod()]
        public void AddTest()
        {
            try
            {

                VendorViewModel_Detail model = new VendorViewModel_Detail();
                VendorServiceModel service = new VendorServiceModel();
                ReturnValue rtn = new ReturnValue();


                model.VendorName = "";
                model.CountryKey = "";
                model.Contract = "";
                model.Control = null;

                rtn = service.Add(ref model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);



                //model.VendorName = "PTT Aekkasit (Edit vendor)";
                //model.CountryKey = "SG";
                //model.Contract = "aekchainat@hotmail.com";
                //model.Control = new List<VendorViewModel_Control>();
                //// System is empty
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "ACTIVE" });
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "2", System = "", Type = "VV", Color = "#808080", Status = "ACTIVE" });
                //rtn = service.Add(model, "ADMIN");
                //Assert.AreEqual(false, rtn.Status, rtn.Message);

                //model.VendorName = "PTT Aekkasit (Edit vendor)";
                //model.CountryKey = "SG";
                //model.Contract = "aekchainat@hotmail.com";
                //model.Control = new List<VendorViewModel_Control>();
                //// Type is empty
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "ACTIVE" });
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "2", System = "CPAI", Type = "", Color = "#808080", Status = "ACTIVE" });
                //rtn = service.Add(model, "ADMIN");
                //Assert.AreEqual(false, rtn.Status, rtn.Message);

                //model.VendorName = "PTT Aekkasit (Edit vendor)";
                //model.CountryKey = "SG";
                //model.Contract = "aekchainat@hotmail.com";
                //model.Control = new List<VendorViewModel_Control>();
                //// Color is empty
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BOKER", Color = "", Status = "ACTIVE" });
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "2", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "ACTIVE" });
                //rtn = service.Add(model, "ADMIN");
                //Assert.AreEqual(false, rtn.Status, rtn.Message);

                //model.VendorName = "PTT Aekkasit (Edit vendor)";
                //model.CountryKey = "SG";
                //model.Contract = "aekchainat@hotmail.com";
                //model.Control = new List<VendorViewModel_Control>();
                //// Status is empty
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "" });
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "2", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "ACTIVE" });
                //rtn = service.Add(model, "ADMIN");
                //Assert.AreEqual(false, rtn.Status, rtn.Message);

                //model.VendorName = "PTT Aekkasit (Edit vendor)";
                //model.CountryKey = "SG";
                //model.Contract = "aekchainat@hotmail.com";
                //model.Control = new List<VendorViewModel_Control>();
                //// System is Duplicate
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "ACTIVE" });
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "2", System = "POR", Type = "AAA", Color = "#808080", Status = "ACTIVE" });
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "3", System = "CPAI", Type = "VVV", Color = "#808080", Status = "ACTIVE" });
                //model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "4", System = "BBC", Type = "BBB", Color = "#808080", Status = "ACTIVE" });

                //rtn = service.Add(model, "ADMIN");

                //Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.VendorName = "PTT Aekkasit (Edit Test dup)";
                model.CountryKey = "SG";
                model.Contract = "aekchainat@hotmail.com";
                model.Control = new List<VendorViewModel_Control>();
                // System and Type is Duplicate
                model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "ACTIVE" });
                model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "2", System = "CPsAI", Type = "BOKERa", Color = "#808080", Status = "ACTIVE" });
                model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "3", System = "AACT", Type = "vV", Color = "#808080", Status = "ACTIVE" });
                model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "4", System = "CPaI", Type = "BOKeR", Color = "#808080", Status = "ACTIVE" });

                rtn = service.Add(ref model, "ADMIN");

                Assert.AreEqual(false, rtn.Status, rtn.Message);




                model.VendorName = "PTT Aekkasit (Add Type)";
                model.CountryKey = "SG";
                model.Contract = "aekchainat@hotmail.com";
                model.Control = new List<VendorViewModel_Control>();
                model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "ACTIVE" });

                rtn = service.Add(ref model, "ADMIN");

                Assert.AreEqual(true, rtn.Status, rtn.Message);

                return;
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void EditTest()
        {
            VendorViewModel_Detail model = new VendorViewModel_Detail();
            VendorServiceModel service = new VendorServiceModel();
            ReturnValue rtn = new ReturnValue();

            try
            {
                // for check require field
                model.VendorName = "";
                model.CountryKey = "";
                model.Contract = "";
                model.CreateType = "";
                model.Control = null;

                rtn = service.Edit(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                // Edit CPAI
                model.VendorCode = "CPAI16111715322668";
                model.VendorName = "Aekkasit (Edit add type)";
                model.CountryKey = "TH";
                model.Contract = "aekchainat@hotmail.com";
                model.CreateType = "CPAI";
                model.Control = new List<VendorViewModel_Control>();
                model.Control.Add(new VendorViewModel_Control { RowID = "bb2a48632b6b4587b2305afd6b140cfe", Order = "1", System = "CPAI", Type = "BOKER", Color = "#808080", Status = "ACTIVE" });
                model.Control.Add(new VendorViewModel_Control { RowID = "c90b41312ee64add83c1ea0acc329b60", Order = "2", System = "OPER", Type = "ACT", Color = "#ff55aa", Status = "INACTIVE" });
                model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "3", System = "BOKER", Type = "BBC", Color = "#ff55bb", Status = "ACTIVE" });

                rtn = service.Edit(model, "ADMIN");

                Assert.AreEqual(true, rtn.Status, rtn.Message);


            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void SearchTest()
        {
            VendorViewModel_Seach model = new VendorViewModel_Seach();
            VendorServiceModel service = new VendorServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                // Search All
                //model.sVendorCode = "";
                //model.sVendorName = "";
                //model.sCountry = "";
                //model.sStatus = "";
                //model.sCreateType = "";

                rtn = service.Search(ref model);
                Assert.AreEqual(true, rtn.Status, rtn.Message);

                // Search by condition
                model.sVendorCode = "";
                model.sVendorName = "Aek";
                model.sCountry = "";
                model.sStatus = "";
                model.sType = "";
                model.sCreateType = "";

                rtn = service.Search(ref model);
                Assert.AreEqual(true, rtn.Status, rtn.Message);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetTest()
        {
            VendorViewModel_Detail model = new VendorViewModel_Detail();
            VendorServiceModel service = new VendorServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {


                model = service.Get("CPAI16111715322668");
                Assert.AreNotEqual(null, model, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetSystemJSONTest()
        {

            //VendorServiceModel service = new VendorServiceModel();

            try
            {
                string json = VendorServiceModel.GetSystemJSON("");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetSystemForDDLTest()
        {
            VendorServiceModel service = new VendorServiceModel();
            List<DropdownServiceModel_Data> data = new List<DropdownServiceModel_Data>();
            try
            {
                data = VendorServiceModel.GetSystemForDDL("ACTIVE");
                Assert.AreNotEqual(null, data, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetTypeJSONTest()
        {
            try
            {
                string json = VendorServiceModel.GetTypeJSON("");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetTypeForDDLTest()
        {
            VendorServiceModel service = new VendorServiceModel();
            List<DropdownServiceModel_Data> data = new List<DropdownServiceModel_Data>();
            try
            {
                data = VendorServiceModel.GetTypeForDDL("ACTIVE");
                Assert.AreNotEqual(null, data, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetNameTest()
        {
            try
            {
                string str = VendorServiceModel.GetName("CPAI1000002");
                Assert.AreNotEqual("", str, "ไม่พบข้อมูล");

                str = VendorServiceModel.GetName("CPAI1000002sss");
                Assert.AreEqual("", str, "ไม่ถูกต้อง");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void getVendorDDLJsonTest()
        {
            try
            {
                string json = VendorServiceModel.getVendorDDLJson("CPAI", "VVV|VV");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

      
    }
}