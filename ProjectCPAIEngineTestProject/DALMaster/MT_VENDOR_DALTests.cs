﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster.Tests
{
    [TestClass()]
    public class MT_VENDOR_DALTests
    {
        [TestMethod()]
        public void GetVerndorDataTest()
        {
            try
            {
               
                IList<MT_VENDOR> ent = new List<MT_VENDOR>();

                string sCode = "";
                string sName = "";
                string sCountry = "";
                string sCreateType = "";
                string sStatus = "";

                ent = MT_VENDOR_DAL.GetVerndorData("CPAI",sCode,sName,sCountry, sCreateType, sStatus);
                Assert.AreNotEqual(null, ent, "มีข้อมูล");

                sCode = "CPAI1000002";
                sName = "";
                sCountry = "";
                sCreateType = "";
                sStatus = "";

                ent = MT_VENDOR_DAL.GetVerndorData("CPAI", sCode, sName, sCountry, sCreateType, sStatus);
                Assert.AreNotEqual(null, ent, "มีข้อมูล");

                sCode = "";
                sName = "PT";
                sCountry = "TH";
                sCreateType = "";
                sStatus = "";

                ent = MT_VENDOR_DAL.GetVerndorData("CPAI",sCode, sName, sCountry, sCreateType, sStatus);
                Assert.AreNotEqual(null, ent, "มีข้อมูล");

                return;
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "เกิดความผิดพลาด");
                return;
            }
        }
    }
}