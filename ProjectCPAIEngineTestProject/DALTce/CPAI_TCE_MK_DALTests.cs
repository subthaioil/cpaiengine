﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.DAL.DALTce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALTce.Tests
{
    [TestClass()]
    public class CPAI_TCE_MK_DALTests
    {
        [TestMethod()]
        public void CalEverageWSTest()
        {
            try
            {
                // Case is holiday
                DateTime DateStart = new DateTime(2015, 3, 15);
                CPAI_TCE_MK_DAL dal = new CPAI_TCE_MK_DAL();
                double? rtn;


                rtn = dal.CalEverageWS(DateStart, "TD2", "UK");

                Assert.AreNotEqual(null, rtn, "ผิดพาด");


            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }
    }
}