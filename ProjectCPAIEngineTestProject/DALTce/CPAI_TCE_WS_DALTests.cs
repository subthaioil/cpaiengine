﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.DAL.DALTce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALTce.Tests
{
    [TestClass()]
    public class CPAI_TCE_WS_DALTests
    {
        [TestMethod()]
        public void CalTargetDateTest()
        {
            try
            {
                // Case is holiday
                DateTime LaycanFrom = new DateTime(2016, 12, 31);
                CPAI_TCE_WS_DAL dal = new CPAI_TCE_WS_DAL();

                List<DateTime> rtn = new List<DateTime>();

                rtn = dal.CalTargetDate(LaycanFrom, "Thai");

                Assert.AreNotEqual(0, rtn.Count, "ผิดพาด");


                // Case is not holiday
                LaycanFrom = new DateTime(2016, 12, 22);
                dal = new CPAI_TCE_WS_DAL();

                rtn = new List<DateTime>();

                rtn = dal.CalTargetDate(LaycanFrom, "Thai");

                Assert.AreNotEqual(0, rtn.Count, "ผิดพาด");



            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void CalEverAgeWSTest()
        {
            try
            {
                // Case is holiday
                DateTime LaycanFrom = new DateTime(2016, 12, 31);
                CPAI_TCE_WS_DAL dal = new CPAI_TCE_WS_DAL();
                double? rtn;
                string rtnMessage = "";


                var pair = dal.CalEverAgeWS(LaycanFrom,"TD15", "Thai");
                rtn = pair.Key;
                Assert.AreEqual(null, rtn, "ผิดพาด");

                // Case is holiday
                LaycanFrom = new DateTime(2016, 12, 22);
                dal = new CPAI_TCE_WS_DAL();
                rtn = null;
                pair = dal.CalEverAgeWS(LaycanFrom, "TD2", "Thai");
                rtn = pair.Key;
                //dal.CalEverAgeWS(LaycanFrom, "TD2", "Thai", out rtn, out rtnMessage);

                Assert.AreNotEqual(null, rtn, "ผิดพาด");



            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }
    }
}