﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;

namespace ProjectCPAIEngineTestProject.TDD
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSearchPIT()
        {
            try
            {
                CrudePurchaseServiceModel service = new CrudePurchaseServiceModel();
                string loadingPeriodFrom = "01/06/2017 00:00"; // dd/MM/yyyy HH:mm
                string loadingPeriodTo = "30/06/2017 23:59";
                string dischargingPeriodFrom = "";
                string dischargingPeriodTo = "";
                string crude = "ORIENTE";
                string supplier = "Brightoil Petroleum (Singapore) Pte Ltd";
                string incoterm = "CFR";
                CrudePurchaseReportViewModel_Search_PIT result = service.getDataCrudePurchase( loadingPeriodFrom,  loadingPeriodTo,  dischargingPeriodFrom,  dischargingPeriodTo,  crude,  supplier,  incoterm);
                CrudePurchaseReportViewModel_SearchData_PIT t = result.sSearchData[0]; 
                var t1 = t.dSupplier;
                var t2 = t.dIncoterms;
                Assert.AreNotEqual(false, result.Status, "done");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod]
        public void TestHedgDate()
        {
            try
            {
                DateTime date_from = new DateTime(2017,1,1);
                DateTime date_to = new DateTime(2017,12,31);
                string result = ProjectCPAIEngine.Utilities.DateTimeHelper.getDataHEDG(date_from, date_to);
                Console.Write(date_from +" - " + date_to + " >> " + result);
                Assert.IsNotNull(result);

                date_from = new DateTime(2017, 1, 1);
                date_to = new DateTime(2018, 12, 31);
                string result1 = ProjectCPAIEngine.Utilities.DateTimeHelper.getDataHEDG(date_from, date_to);
                Console.Write(date_from + " - " + date_to + " >> " + result1);
                Assert.IsNotNull(result);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }
    }
}
