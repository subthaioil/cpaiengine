﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Utilities;
using com.pttict.engine.utility;

namespace ProjectCPAIEngineTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            
            string x = "abc";
            string s = string.Format("{0:N}", double.Parse(x));
            
        }

        [TestMethod()]
        public void encryptTest()
        {
            int key = 512;
            EncryptorRSAKeys d = RSAHelper.GenerateKeys(key);

        }
        [TestMethod()]
        public void appuserTest()
        {
            string pass = "android@132"; //android@132
            string p = Cryptography.GetMD5(pass).ToLower();

        }

        [TestMethod()]
        public void encryptDecryptTest()
        {

            //string originalText = "Min load = 11 K Ton/r/n1 WS/r/n13 WS/r/nDemurrage rate = 15 K USD/ day";

            //152
            string originalText = "Min load = 11 K Ton|1 WS|13 WS|Demurrage rate = 15 K USD/day|";
            string publicKey = "NTEyITxSU0FLZXlWYWx1ZT48TW9kdWx1cz54elVMMTJhWHl3MVFTZDNwWDduQ0VpZ3g3NmNFbHZJUFQ5eE9NYWFhQjFKSmtnZUdNR3daM3RkTnVxODczU041OVdxTitOaTFEb1VFNWxHajJ1aUFldz09PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48L1JTQUtleVZhbHVlPg==";
            string privateKey = "NTEyITxSU0FLZXlWYWx1ZT48TW9kdWx1cz54elVMMTJhWHl3MVFTZDNwWDduQ0VpZ3g3NmNFbHZJUFQ5eE9NYWFhQjFKSmtnZUdNR3daM3RkTnVxODczU041OVdxTitOaTFEb1VFNWxHajJ1aUFldz09PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48UD43VVZQVk0wRjQxSW1rUmN5bkk1RzBzeGR0UDhwbWZmUGMzQzM3MThPV1hjPTwvUD48UT4xdTZQbUMwMWhqTnZiQXNZdDVvTkxNSk0zTWdNaXM5UzBMeVJCZXR5RWgwPTwvUT48RFA+YVk2QU1KWFZuTUFiY0VrRnR5aUUwWnNoeFRnUFpmUWRVcG8rK2pzcnd4az08L0RQPjxEUT5lT1czMytHT09lNjB2aEYvMWNiUm9sdW80SWVtaG00WUoxSHFRV291d0FFPTwvRFE+PEludmVyc2VRPjBzUmpXdUtpQmNzcHp4ZUw4QWwzcEpKN1FVdkNlYVB5d1M3L01RbUw1WkE9PC9JbnZlcnNlUT48RD5BQXh6UkVveUgwcVJpbk1UYTRLUUdaRUk4aFlJV2tFVTMrQWQ1TVUxcEdyUW5ZVkZCRTVldktiWGJqSVlER05BbEdYWG1ldUFQWVE0cDlMb3VBQk9nUT09PC9EPjwvUlNBS2V5VmFsdWU+";

            //1024
            //string publicKey = "MTAyNCE8UlNBS2V5VmFsdWU+PE1vZHVsdXM+dnFCOW0vdmN3WFRzTG9JTE5ScFZuK3l3RGI1a3N2THNadzkyTS9La3VXQXY2SUlRckhtRzZKTmVPZVlVQU5FQjJTRFFBVmtyamYxaTZXSWVCSzZ6UFUxQXRyOHoyZUNRSUlTTFd6YThuc2ZOcDVzc3NsdGRUd0crUGUvNXJYUUZQMlFpbXRXRVFlQjFQVXZhd1JIUEIxMFh6TmhnUk5UZVVOQjZNNjhWWWRjPTwvTW9kdWx1cz48RXhwb25lbnQ+QVFBQjwvRXhwb25lbnQ+PC9SU0FLZXlWYWx1ZT4=";
            //string privateKey =  "MTAyNCE8UlNBS2V5VmFsdWU+PE1vZHVsdXM+dnFCOW0vdmN3WFRzTG9JTE5ScFZuK3l3RGI1a3N2THNadzkyTS9La3VXQXY2SUlRckhtRzZKTmVPZVlVQU5FQjJTRFFBVmtyamYxaTZXSWVCSzZ6UFUxQXRyOHoyZUNRSUlTTFd6YThuc2ZOcDVzc3NsdGRUd0crUGUvNXJYUUZQMlFpbXRXRVFlQjFQVXZhd1JIUEIxMFh6TmhnUk5UZVVOQjZNNjhWWWRjPTwvTW9kdWx1cz48RXhwb25lbnQ+QVFBQjwvRXhwb25lbnQ+PFA+NVgvVWszTG5iVzhFY1FHcFdkZkpjVUY5QnJhNzlaSlhYMURKdTdSOXp1MWphK0JRdTZzTWg4ME1LTWlNckRZNittZmQ4bHJTdnRPSVJQbW5BRnVKUlE9PTwvUD48UT4xS09RR0I1cUV5bm81OXZGc0lwNzZGbmNHdDZuaDlNQkNISnpsYzR3VGNROVp6WlZyczB6eFN0aUcwaUxjd1VGTGNLZkJrYXFrV2RxQm1hZXFub2Fhdz09PC9RPjxEUD5FaklNWEpSam5OMk1jSms5SUE1bnZqNys5cVZPU2E0TWwxWUVhSWxxNEJrYUpOZG9EemtFaVUvTWd2OURoL1BVd1QyeTd5QWNUVnpnRlBmbU1iVVFNUT09PC9EUD48RFE+aDZ2Z3FYTG1iVm5GTUFLS2pReWU2RzVGUUtPbHNzVFBtT3Nsa29TZlQwOFJlb1F4Z1RtT2VaUW5vWTdVNHN0bU5YOUM1Y2M2MTFGclZiQXVOa2w0Qnc9PTwvRFE+PEludmVyc2VRPjR4SjlCbnV5a3pTVDFkbXFwQzdLaFNLUDRNNFpsUHR1L2lWaEg1SnZwRS9mbmZKNXpwbDIweXE2Szh0bmVkaGxwYitkNjlVb1dyRzFLcnppYmtPRkt3PT08L0ludmVyc2VRPjxEPk1mdERwQVA0UUZNbllCUmgydVVTUVZIZVlXUWo1QlcwclZzTSs1YnA0TjhiQUtRbHVndE5ONnI0SUVjMnhIWUl2WVVRZE9JTC9QbE1sM1BOK2gxU1NsTUs4NHZKYUdkOUdkSWdrcFdZaE8xVFRHY3VLSlFqb1dtRE9nalNtNldxZTU5OVduTTgzU2J1S1J4YUlDMm1XS3VFYzVadlg4SGowZk1sTnRhdlhlMD08L0Q+PC9SU0FLZXlWYWx1ZT4=";


            string encryptText = RSAHelper.EncryptText(originalText, publicKey);
            string decryptText = RSAHelper.DecryptText(encryptText, privateKey);
            StringAssert.Equals(originalText, decryptText);

        }

        [TestMethod()]
        public void NetWorkIPAddressTest()
        {
           string a =  ProjectCPAIEngine.Utilities.NetWorkIPAddress.getIpAddress();

        }

    }
}
