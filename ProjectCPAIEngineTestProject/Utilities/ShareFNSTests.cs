﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.Utilities.Tests
{
    [TestClass()]
    public class ShareFNSTests
    {
        [TestMethod()]
        public void EncryptTest()
        {
           

            string strEncrypt = "";
            string strTest = "TOP";
            strEncrypt = strTest.Encrypt();
            
            Assert.AreNotEqual(strTest, strEncrypt, "ไม่ถูกกต้อง");

            strTest = "OTHER";
            strEncrypt = strTest.Encrypt();
            Assert.AreNotEqual(strTest, strEncrypt, "ไม่ถูกกต้อง");
            return;
        }
    }
}