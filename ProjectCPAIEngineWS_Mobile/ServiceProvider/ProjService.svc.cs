﻿using com.pttict.engine;
using com.pttict.engine.utility;
using ProjectCPAIEngine.ServiceProvider;

namespace ProjectCPAIEngineWS_Mobile.ServiceProvider
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ProjService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ProjService.svc or ProjService.svc.cs at the Solution Explorer and start debugging.
    public class ProjService : IProjService
    {
        EngineReceive _xml = new EngineReceive();
        QuartzService _Quartz = new QuartzService();
        ProjService()
        {

        }

        public ResponseData CallService(RequestData _request)
        {
            return _xml.ResponseReceive(_request);
        }

        public string CallSchedule()
        {
            //_xml.StartSheduler();
            return "Start Schecdule";
        }

        public string StartQuartz()
        {
            //_Quartz.StartQuartz();
            return "Start Quartz";
        }

        public ResponseData CallService2(RequestData _request)
        {
            string _response = string.Empty;
            ResponseData ret = new ResponseData();
            ret.extra_xml = ShareFunction.XMLSerialize(_request);
            ret.result_desc = "Success";
            ret.result_code = "1";
            return ret;
        }
    }
}
