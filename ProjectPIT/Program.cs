﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectPIT
{
    static class Program
    {
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            StringBuilder step = new StringBuilder();

            try
            {
                step.AppendLine("Start Process : [" + DateTime.Now.ToString() + "]");
                step.AppendLine();
                string temp = new Guid().ToString();
                //ExportData.RunExportData(ref step);

                step.AppendLine();
                step.Append("End Process : [" + DateTime.Now.ToString() + "]");
                Console.WriteLine("Process Complete.");

            }
            catch (Exception ex)
            {
                step.AppendLine();
                step.AppendLine("--- Exception Occur ! ---");
                step.AppendLine();
                //LoggerService.WriteError(ex);
                Console.WriteLine("Process Error.");
            }
            finally
            {
                //LoggerService.WriteInfo(step.ToString());
                step = null;
            }
        }
    }
}
