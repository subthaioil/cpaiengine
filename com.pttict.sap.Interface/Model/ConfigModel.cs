﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class ConfigModel
    {
        public string sap_url { get; set; }
        public string sap_user { get; set; }
        public string sap_pass { get; set; }
        public string connect_time_out { get; set; }
    }
}
