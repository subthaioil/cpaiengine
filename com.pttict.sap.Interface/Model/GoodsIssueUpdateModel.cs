﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class GoodsIssueUpdateModel
    {

        public List<LIKP> mDELIVERY_HEADER { get; set; }
        public List<ZDO_ITEM> mDELIVERY_ITEM { get; set; }
        public List<LIPSO2> mDELIVERY_ITEM_ADD_QTY { get; set; }
        public string ACT_GI_DATE { get; set; }//GI_Date

        public class LIKP
        {
            public string VBELN { get; set; }//DO No
            public string TRATY { get; set; }//Sale Type: SWAP=Z005, Clearline=Z008, Borrow=Z009
        }
        public class ZDO_ITEM
        {
            public string CustomerCode { get; set; }
            public string SALES_ORG { get; set; }
            public string POSNR_VL { get; set; }//Item No.
            public string MATNR { get; set; }//Material Code
            public string WERKS { get; set; }//Plant
            public string LIANP { get; set; }//Global Config
            public decimal LFIMG { get; set; }//Quantity
            public string LGORT { get; set; }//Global Config
            public decimal ZTEMP { get; set; }//Temp
            public decimal ZDENS_15C { get; set; }//Density
        }
        public class LIPSO2
        {
            public string POSNR { get; set; }//Item No
            public string MSEHI { get; set; } //QTY Unit in BBL,MT,other
            public double ADQNT { get; set; } //QTY in BBL
            public string MANEN { get; set; }//Global Config

        }
    }
}
