﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.sap.Interface.sap.intansit.cancel;


namespace com.pttict.sap.Interface.Model
{
    public class IntansitCancelModel
    {
        public List<BAPI2017_GM_ITEM_04> GM_ITEM_04 { get; set; }
        public List<BAPIRET2> RETURN { get; set; }
        public BAPI2017_GM_HEAD_RET GM_HEAD_RET { get; set; }
        public MORE_DETAI more_detai { get; set; }
    }
    public class MORE_DETAI
    {
        public string USER_NAME { get; set; }
        public string MATERIAL_DOCUMENT { get; set; }
        public string YEAR { get; set; }
        public string DATE { get; set; }
    }
}
