﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class MemoRecordCreateModel
    {
        public string AVDAT { get; set; } //Expiration Date 
        public string BUKRS { get; set; } //Company Code
        public string DSART { get; set; } //Planning (Type) 
        public string DISPW { get; set; } //USD or THB
        public string DATUM { get; set; } //Planning Date
        public string GRUPP { get; set; } //Planning (Grupp)
        public string GVALT { get; set; } //Delivery Date
        public string KURST { get; set; } //Exchange Rate Type
        public string MERKM { get; set; } //Item(Text) 
        public string REFER { get; set; } //Characteristics
        public string SGTXT { get; set; } //Planning Type
        public string TESTRUN { get; set; }
        public string VOART { get; set; } //FinalPrepayment 
        public decimal WRSHB { get; set; } //Foreign Dispw Wrshb with Signs(+/-)
        public bool WRSHBSpecified { get; set; }
        public string XINVR { get; set; } //Posting with inverse exchange rates possible
        public string ZUONR { get; set; } //Assignment no
        public string E_IDENR { get; set; } //Return MEMO Number Or Messes Error with SAP
    }
}
