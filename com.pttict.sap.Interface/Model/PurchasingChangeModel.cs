﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class PurchasingChangeModel
    {
        public sap.po.change.BAPIMEPOHEADER POHEADER { get; set; }
        public sap.po.change.BAPIMEPOHEADERX POHEADERX { get; set; }
        public string PURCHASEORDER { get; set; }
        public List<sap.po.change.BAPIMEPOITEM> POITEM { get; set; }
        public List<sap.po.change.BAPIMEPOITEMX> POITEMX { get; set; }
        public List<sap.po.change.BAPIRET2> RETURN { get; set; }
        public string MEMORY_COMPLETE { get; set; }
        public string MEMORY_UNCOMPLETE { get; set; }
        public string NO_AUTHORITY { get; set; }
        public string NO_MESSAGE_REQ { get; set; }
        public string NO_MESSAGING { get; set; }
        public string NO_PRICE_FROM_PO { get; set; }
        public string PARK_COMPLETE { get; set; }
        public string PARK_UNCOMPLETE { get; set; }
        public sap.po.change.BAPIMEPOADDRVENDOR POADDRVENDOR { get; set; }
        public sap.po.change.BAPIEIKP POEXPIMPHEADER { get; set; }
        public sap.po.change.BAPIEIKPX POEXPIMPHEADERX { get; set; }
        public string TESTRUN { get; set; }
        public sap.po.change.BAPIMEDCM VERSIONS { get; set; }
        public List<sap.po.change.BAPIMEDCM_ALLVERSIONS> ALLVERSIONS { get; set; }
        public List<sap.po.change.BAPIPAREX> EXTENSIONIN { get; set; }
        public List<sap.po.change.BAPIPAREX> EXTENSIONOUT { get; set; }
        public List<sap.po.change.BAPI_INVOICE_PLAN_HEADER> INVPLANHEADER { get; set; }
        public List<sap.po.change.BAPI_INVOICE_PLAN_HEADERX> INVPLANHEADERX { get; set; }
        public List<sap.po.change.BAPI_INVOICE_PLAN_ITEM> INVPLANITEM { get; set; }
        public List<sap.po.change.BAPI_INVOICE_PLAN_ITEMX> INVPLANITEMX { get; set; }
        public List<sap.po.change.BAPIMEPOACCOUNT> POACCOUNT { get; set; }
        public List<sap.po.change.BAPIMEPOACCOUNTPROFITSEGMENT> POACCOUNTPROFITSEGMENT { get; set; }
        public List<sap.po.change.BAPIMEPOACCOUNTX> POACCOUNTX { get; set; }
        public List<sap.po.change.BAPIMEPOADDRDELIVERY> POADDRDELIVERY { get; set; }
        public List<sap.po.change.BAPIMEPOCOMPONENT> POCOMPONENTS { get; set; }
        public List<sap.po.change.BAPIMEPOCOMPONENTX> POCOMPONENTSX { get; set; }
        public List<sap.po.change.BAPIMEPOCOND> POCOND { get; set; }
        public List<sap.po.change.BAPIMEPOCONDHEADER> POCONDHEADER { get; set; }
        public List<sap.po.change.BAPIMEPOCONDHEADERX> POCONDHEADERX { get; set; }
        public List<sap.po.change.BAPIMEPOCONDX> POCONDX { get; set; }
        public List<sap.po.change.BAPIEKES> POCONFIRMATION { get; set; }
        public List<sap.po.change.BAPIESUCC> POCONTRACTLIMITS { get; set; }
        public List<sap.po.change.BAPIEIPO> POEXPIMPITEM { get; set; }
        public List<sap.po.change.BAPIEIPOX> POEXPIMPITEMX { get; set; }
        public List<sap.po.change.BAPIEKBE> POHISTORY { get; set; }
        public List<sap.po.change.BAPIEKBE_MA> POHISTORY_MA { get; set; }
        public List<sap.po.change.BAPIEKBES> POHISTORY_TOTALS { get; set; }
        public List<sap.po.change.BAPIESUHC> POLIMITS { get; set; }
        public List<sap.po.change.BAPIEKKOP> POPARTNER { get; set; }
        public List<sap.po.change.BAPIMEPOSCHEDULE> POSCHEDULE { get; set; }
        public List<sap.po.change.BAPIMEPOSCHEDULX> POSCHEDULEX { get; set; }
        public List<sap.po.change.BAPIESLLC> POSERVICES { get; set; }
        public List<sap.po.change.BAPIESLLTX> POSERVICESTEXT { get; set; }
        public List<sap.po.change.BAPIITEMSHIP> POSHIPPING { get; set; }
        public List<sap.po.change.BAPIMEPOSHIPPEXP> POSHIPPINGEXP { get; set; }
        public List<sap.po.change.BAPIITEMSHIPX> POSHIPPINGX { get; set; }
        public List<sap.po.change.BAPIESKLC> POSRVACCESSVALUES { get; set; }
        public List<sap.po.change.BAPIMEPOTEXTHEADER> POTEXTHEADER { get; set; }
        public List<sap.po.change.BAPIMEPOTEXT> POTEXTITEM { get; set; }
        public List<sap.po.change.BAPIMEPOSERIALNO> SERIALNUMBER { get; set; }
        public List<sap.po.change.BAPIMEPOSERIALNOX> SERIALNUMBERX { get; set; }
        public sap.po.change.BAPIEIKP EXPPOEXPIMPHEADER { get; set; }
    }
}
