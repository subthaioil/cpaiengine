﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class PurchasingCreateModel
    {
        
        public sap.po.create.ZPOHEADER ZPOHEADER { get; set; }
        public sap.po.create.ZPOHEADERX ZPOHEADERX { get; set; }
        public List<sap.po.create.ZPOITEM> ZPOITEM { get; set; }
        public List<sap.po.create.ZPOITEMX> ZPOITEMX { get; set; }
        public List<sap.po.create.ZPOACCOUNT> ZPOACCOUNT { get; set; }
        public List<sap.po.create.ZPOACCOUNTX> ZPOACCOUNTX { get; set; }
        public List<sap.po.create.BAPIRET2> RETURN { get; set; }
    }
}
