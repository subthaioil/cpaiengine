﻿using com.pttict.sap.Interface.sap.vatpostingMIRO.cerate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class VatMIROInvoiceServiceModel
    {
        public List<ZITEMDATE> itemData { get; set; }
        public List<ZHEADERDATA> headerdata { get; set; }

        public class ZHEADERDATA//headerdata
        {
            public string INVOICE_IND { get; set; }
            public string DOC_TYPE { get; set; }
            public string DOC_DATE { get; set; }
            public string PSTNG_DATE { get; set; }
            public string REF_DOC_NO { get; set; }
            public string COMP_CODE { get; set; }
            public string DIFF_INV { get; set; }
            public string CURRENCY { get; set; }
            public decimal EXCH_RATE { get; set; }
            public decimal GROSS_AMOUNT { get; set; }
            public string HEADER_TXT { get; set; }
            public string BUSINESS_PLACE { get; set; }
            public string ALLOC_NMBR { get; set; }
            public string PMNTTRMS { get; set; }
        }

        public class ZITEMDATE//itemData
        {
            //public string INVOICE_DOC_ITEM { get; set; }
            public string PO_NUMBER { get; set; }
            public string PO_ITEM { get; set; }
            public string TAX_CODE { get; set; }
            public decimal ITEM_AMOUNT { get; set; }
            public decimal QUANTITY { get; set; }
            public string PO_UNIT { get; set; }
            public string ITEM_TEXT { get; set; }
        }
    }
    





}
