﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.pttict.sap.Interface.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://sappi-dev.thaioilgroup.com:8021/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=PurchasingCreateRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_purchaseorder%2Fpurchasing%2Fpurchasing_create%2F")]
        public string com_pttict_sap_Interface_sap_po_create_PurchasingCreateRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_po_create_PurchasingCreateRequest_Sync_Out_SIService" +
                    ""]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://sappi-dev.thaioilgroup.com:8021/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=PurchasingChangeRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_purchaseorder%2Fpurchasing%2Fpurchasing_change%2F")]
        public string com_pttict_sap_Interface_sap_po_change_PurchasingChangeRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_po_change_PurchasingChangeRequest_Sync_Out_SIService" +
                    ""]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=GoodsInTransitPostRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_purchaseorder%2Fpurchasing%2Fgoodsintransit_post%2F")]
        public string com_pttict_sap_Interface_sap_intansit_post_GoodsInTransitPostRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_intansit_post_GoodsInTransitPostRequest_Sync_Out_SIS" +
                    "ervice"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=MemorecordCreateRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_accounting%2Fmemo%2Fmemorecord_create%2F")]
        public string com_pttict_sap_Interface_sap_memorecord_create_MemorecordCreateRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_memorecord_create_MemorecordCreateRequest_Sync_Out_S" +
                    "IService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=MemorecordDeleteRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_accounting%2Fmemo%2Fmemorecord_delete%2F")]
        public string com_pttict_sap_Interface_sap_memorecord_delete_MemorecordDeleteRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_memorecord_delete_MemorecordDeleteRequest_Sync_Out_S" +
                    "IService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=MemorecordChangeRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_accounting%2Fmemo%2Fmemorecord_change%2F")]
        public string com_pttict_sap_Interface_sap_memorecord_change_MemorecordChangeRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_memorecord_change_MemorecordChangeRequest_Sync_Out_S" +
                    "IService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://sappi-dev.thaioilgroup.com:8021/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PIT&receiverParty=&receiverService=&interface=BillingInfoSend_Async_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_salesorder%2Fsales%2Fbilling_info%2F")]
        public string com_pttict_sap_Interface_sap_billinginfo_send_BillingInfoSend_Async_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_billinginfo_send_BillingInfoSend_Async_Out_SIService" +
                    ""]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=AccountingPostRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_accounting%2Faccrual%2Faccounting_post%2F")]
        public string com_pttict_sap_Interface_sap_accrual_post_AccountingPostRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_accrual_post_AccountingPostRequest_Sync_Out_SIServic" +
                    "e"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=AccountingCreate2Request_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_accounting%2Faccrual%2Faccounting_create2%2F")]
        public string com_pttict_sap_Interface_sap_accrual_create_AccountingCreate2Request_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_accrual_create_AccountingCreate2Request_Sync_Out_SIS" +
                    "ervice"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://sappi-dev.thaioilgroup.com:8021/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=FIDocumentReverseRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2Fi_finance%2Ffi%2Ffidocument_reverse%2F")]
        public string com_pttict_sap_Interface_sap_reversefidocument_post_FIDocumentReverseRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_reversefidocument_post_FIDocumentReverseRequest_Sync" +
                    "_Out_SIService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=VATPostingCreateRequest2_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_accounting%2Fvat%2Fvat_posting2%2F")]
        public string com_pttict_sap_Interface_sap_vatposting_create_VATPostingCreateRequest2_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_vatposting_create_VATPostingCreateRequest2_Sync_Out_" +
                    "SIService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=VATPostingCreateRequest1_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_accounting%2Fvat%2Fvat_posting1%2F")]
        public string com_pttict_sap_Interface_sap_vatpostingMIRO_cerate_VATPostingCreateRequest1_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_vatpostingMIRO_cerate_VATPostingCreateRequest1_Sync_" +
                    "Out_SIService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SurveyorChangeRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_purchaseorder%2Fpurchasing%2Fsurveyor_change%2F")]
        public string com_pttict_sap_Interface_sap_posurveyor_change_SurveyorChangeRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_posurveyor_change_SurveyorChangeRequest_Sync_Out_SIS" +
                    "ervice"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SurveyorCreateRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_purchaseorder%2Fpurchasing%2Fsurveyor_create%2F")]
        public string com_pttict_sap_Interface_sap_posurveyor_create_SurveyorCreateRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_posurveyor_create_SurveyorCreateRequest_Sync_Out_SIS" +
                    "ervice"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=PriceCreateRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_salesorder%2Fprice%2Fprice_create%2F")]
        public string com_pttict_sap_Interface_sap_salesprice_create_PriceCreateRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_salesprice_create_PriceCreateRequest_Sync_Out_SIServ" +
                    "ice"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SalesOrderCreateRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_salesorder%2Fsales%2Fsalesorder_create%2F")]
        public string com_pttict_sap_Interface_sap_saleorder_create_SalesOrderCreateRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_saleorder_create_SalesOrderCreateRequest_Sync_Out_SI" +
                    "Service"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SalesOrderChangeRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_salesorder%2Fsales%2Fsalesorder_change%2F")]
        public string com_pttict_sap_Interface_sap_saleorder_change_SalesOrderChangeRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_saleorder_change_SalesOrderChangeRequest_Sync_Out_SI" +
                    "Service"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=GoodMovementCancelRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_oil%2Fproduct%2Fgoodsmovement_cancel%2F")]
        public string com_pttict_sap_Interface_sap_intansit_cancel_GoodMovementCancelRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_intansit_cancel_GoodMovementCancelRequest_Sync_Out_S" +
                    "IService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute(@"http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=DeliveryQtyUpdateRequest_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_oil%2Fproduct%2Fdeliveryqty_update%2F")]
        public string com_pttict_sap_Interface_sap_gi_update_DeliveryQtyUpdateRequest_Sync_Out_SIService {
            get {
                return ((string)(this["com_pttict_sap_Interface_sap_gi_update_DeliveryQtyUpdateRequest_Sync_Out_SIServic" +
                    "e"]));
            }
        }
    }
}
