﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.sap.Interface.sap.accrual.post;

namespace com.pttict.sap.Interface.Service
{
    public class AccrualService : BasicBean
    {
        //Type C
        public string Post(string jsonConfig, BAPIACHE09 DocumentHeader, List<BAPIACGL09> AccountGl, List<BAPIACCR09> CurrencyAmount, List<BAPIPAREX> Extension2, List<ZFI_GL_MAPPING> zfi_GL_MAPPING)
        {
            var accountGlArr = AccountGl.ToArray();
            var currencyAmountArr = CurrencyAmount.ToArray();
            var extension2Arr = Extension2.ToArray();
            var zfi_GL_MAPPINGArr = zfi_GL_MAPPING.ToArray();
            BAPIRET2[] returnValue = new BAPIRET2[1];
            returnValue[0] = new BAPIRET2();
            try
            {
                sap.accrual.post.AccountingPostRequest_Sync_Out_SIService SAPService = new AccountingPostRequest_Sync_Out_SIService();
                SAPService.Credentials = AuthServiceConnect(jsonConfig);

                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jConfig = javaScriptSerializer.Deserialize<ConfigModel>(jsonConfig);
                SAPService.Url = jConfig.sap_url;

                string comcode = "";
                BAPIACCAHD contractHeader = new BAPIACCAHD();
                BAPIACPA09 customerCrd = new BAPIACPA09();
                string document = "";
                string fiscalYear = "";

                BAPIACAP09[] accountPayable = null;
                BAPIACAR09[] accountReceivable = null;
                BAPIACTX09[] accountTax = null;
                BAPIACCAIT[] contractItem = null;
                BAPIACKEC9[] criteria = null;

                BAPIACEXTC[] extension1 = null;

                BAPIACPC09[] paymentCard = null;
                BAPIACRE09[] realEstate = null;

                BAPIACKEV9[] valueField = null;

                string sysObj = "";
                string objType = "";
                BKPF objBkpf = new BKPF();
                string result = SAPService.AccountingPostRequest_Sync_Out_SI(
                    null, //COMCODE
                    null, //CONTRACTHEADER
                    null, //CUSTOMERCRD
                    null, //DOCUMENT
                    DocumentHeader, //DOCUMENTHEADER
                    null, //FISCALYEAR
                    ref accountGlArr, //ACCOUNTGL
                    ref accountPayable, //ACCOUNTPAYABLE
                    ref accountReceivable, //ACCOUNTRECEIVABLE
                    ref accountTax, //ACCOUNTTAX
                    ref contractItem, //CONTRACTITEM
                    ref criteria, //CRITERIA
                    ref currencyAmountArr, //CURRENCYAMOUNT
                    ref extension1, //EXTENSION1
                    ref extension2Arr, //EXTENSION2
                    ref paymentCard, //PAYMENTCARD
                    ref realEstate, //REALESTATE
                    ref returnValue, //RETURN
                    ref valueField, //VALUEFIELD
                    ref zfi_GL_MAPPINGArr, //ZFI_GL_MAPPING
                    out sysObj, //OBJ_SYS
                    out objType, //OBJ_TYPE
                    out objBkpf //OUT_BKPF
                    );

                return result;
            }
            catch(Exception ex)
            {
                return "Error: " + ex.Message;
            }
        }

        //Type A
        public string Create(string jsonConfig, 
                             sap.accrual.create.BAPIACHE09 DocumentHeader, 
                             List<sap.accrual.create.BAPIACGL09> AccountGl, 
                             List<sap.accrual.create.BAPIACCR09> CurrencyAmount, 
                             List<sap.accrual.create.BAPIPAREX> Extension2, 
                             List<sap.accrual.create.ZFI_GL_MAPPING> zfi_GL_MAPPING, 
                             string reverseDate)
        {
            var accountGlArr = AccountGl.ToArray();
            var currencyAmountArr = CurrencyAmount.ToArray();
            var extension2Arr = Extension2.ToArray();
            var zfi_GL_MAPPINGArr = zfi_GL_MAPPING.ToArray();
            sap.accrual.create.BAPIRET2[] returnValue = new sap.accrual.create.BAPIRET2[1];
            returnValue[0] = new sap.accrual.create.BAPIRET2();

            try
            {
                sap.accrual.create.AccountingCreate2Request_Sync_Out_SIService SAPService = new sap.accrual.create.AccountingCreate2Request_Sync_Out_SIService();
                SAPService.Credentials = AuthServiceConnect(jsonConfig);

                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jConfig = javaScriptSerializer.Deserialize<ConfigModel>(jsonConfig);
                SAPService.Url = jConfig.sap_url;

                sap.accrual.create.BAPIACCAHD contractHeader = new sap.accrual.create.BAPIACCAHD();
                sap.accrual.create.BAPIACPA09 customerCpd = new sap.accrual.create.BAPIACPA09();

                sap.accrual.create.BAPIACAP09[] accountPayable = null;
                sap.accrual.create.BAPIACAR09[] accountReceivable = null;
                sap.accrual.create.BAPIACTX09[] accountTax = null;
                sap.accrual.create.BAPIACCAIT[] contractItem = null;
                sap.accrual.create.BAPIACKEC9[] criteria = null;
                sap.accrual.create.BAPIACEXTC[] extension1 = null;
                sap.accrual.create.BAPIACPC09[] paymentCard = null;
                sap.accrual.create.BAPIACRE09[] realEstate = null;
                sap.accrual.create.BAPIACKEV9[] valueField = null;

                string objSys = "";
                string objType = "";

                try
                {
                    string result = SAPService.AccountingCreate2Request_Sync_Out_SI(
                        contractHeader,
                        customerCpd,
                        DocumentHeader,
                        reverseDate,
                        ref accountGlArr,
                        ref accountPayable,
                        ref accountReceivable,
                        ref accountTax,
                        ref contractItem,
                        ref criteria,
                        ref currencyAmountArr,
                        ref extension1,
                        ref extension2Arr,
                        ref paymentCard,
                        ref realEstate,
                        ref returnValue,
                        ref valueField,
                        ref zfi_GL_MAPPINGArr,
                        out objSys,
                        out objType
                        );

                    if (returnValue.Count() > 0)
                    {
                        string sapFiDocNo = returnValue[returnValue.Count() - 1].MESSAGE_V1;
                        return sapFiDocNo;
                    }
                }
                catch(Exception ex2)
                {

                }

                return "";
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
    }
}
