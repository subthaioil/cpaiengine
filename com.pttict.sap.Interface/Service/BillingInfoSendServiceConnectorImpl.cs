﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common.utilities;

namespace com.pttict.sap.Interface.Service
{
    public class BillingInfoSendServiceConnectorImpl : BasicBean
    {
        public void connect(string config, string jsonContent)
        {
            try
            {
                BillingInfoService service = new BillingInfoService();
                
                service.Send(config, jsonContent);                
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
