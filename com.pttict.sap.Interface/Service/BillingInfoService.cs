﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using Newtonsoft.Json.Linq;
using com.pttict.sap.Interface.sap.billinginfo.send;

namespace com.pttict.sap.Interface.Service
{
    public class BillingInfoService : BasicBean
    {
        public void Send(string jsonConfig, string jsonContent)
        {
            try
            {
                bool errorConvertDate = false;
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                sap.billinginfo.send.BillingInfoSend_Async_Out_SIService SAPService = new sap.billinginfo.send.BillingInfoSend_Async_Out_SIService();
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                JObject json = JObject.Parse(String.IsNullOrEmpty(jsonContent) ? "" : jsonContent);
                BillingInfoConfig config = (BillingInfoConfig)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(BillingInfoConfig));

                string url = "";
                SAPService.Credentials = AuthServiceConnect(jsonConfig, out url);
                SAPService.Url = url;

                foreach (var salesOrg in config.SALES_ORG)
                {
                    BillingInfoRange_DTItem[] distributionChannel = new BillingInfoRange_DTItem[1];
                    distributionChannel[0] = new BillingInfoRange_DTItem();
                    distributionChannel[0].SIGN = "I";
                    distributionChannel[0].OPTION = "EQ";
                    distributionChannel[0].LOW = config.DISTRIBUTION_CHANNEL;

                    BillingInfoRange_DTItem[] shippingPoint = new BillingInfoRange_DTItem[1];
                    shippingPoint[0] = new BillingInfoRange_DTItem();
                    shippingPoint[0].SIGN = "I";
                    shippingPoint[0].OPTION = "EQ";
                    shippingPoint[0].LOW = salesOrg.SHIPPING_POINT;

                    BillingInfoRange_DTItem[] createdDate = new BillingInfoRange_DTItem[1];
                    createdDate[0] = new BillingInfoRange_DTItem();
                    createdDate[0].SIGN = "I";
                    createdDate[0].OPTION = "BT";

                    if (config.CREATED_DATE != null)
                    {
                        createdDate[0].LOW = DateTime.ParseExact(config.CREATED_DATE[0].FROM, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                        createdDate[0].HIGH = DateTime.ParseExact(config.CREATED_DATE[0].TO, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        if(config.RunFromWeb)
                        {
                            createdDate = new List<BillingInfoRange_DTItem>().ToArray();
                        }
                        else
                        {
                            createdDate[0].LOW = DateTime.Now.AddDays(-6).ToString("yyyy-MM-dd");
                            createdDate[0].HIGH = DateTime.Now.ToString("yyyy-MM-dd");
                        }                        
                    }

                    BillingInfoRange_DTItem[] billingDate;
                    if (config.BILLING_DATE != null)
                    {
                        billingDate = new BillingInfoRange_DTItem[1];
                        billingDate[0] = new BillingInfoRange_DTItem();
                        billingDate[0].SIGN = "I";
                        billingDate[0].OPTION = "BT";
                        billingDate[0].LOW = DateTime.ParseExact(config.BILLING_DATE[0].FROM, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                        billingDate[0].HIGH = DateTime.ParseExact(config.BILLING_DATE[0].TO, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                    }     
                    else
                    {
                        billingDate = new List<BillingInfoRange_DTItem>().ToArray();
                    }

                    BillingInfoSend_DT dt = new BillingInfoSend_DT
                    {
                        SALES_ORG = salesOrg.SALES_CODE,
                        DISTRIBUTION_CHANNEL = distributionChannel,
                        BILLING_TYPE = (new List<BillingInfoRange_DTItem>()).ToArray(),
                        BILLING_DATE = billingDate,
                        PRICING_DATE = (new List<BillingInfoRange_DTItem>()).ToArray(),
                        CHANGED_DATE = new List<BillingInfoRange_DTItem>().ToArray(),
                        CUSTOMER_CODE = (new List<BillingInfoRange_DTItem>()).ToArray(),
                        SHIPPING_POINT = shippingPoint,
                        BILLING_DOC = (new List<BillingInfoRange_DTItem>()).ToArray(),
                        BILLING_STATUS = (new List<BillingInfoRange_DTItem>()).ToArray(),
                        CREATED_DATE = createdDate,
                        CREATED_BY = (new List<BillingInfoRange_DTItem>()).ToArray()
                    };

                    SAPService.BillingInfoSend_Async_Out_SI(dt);

                    if(errorConvertDate)
                    {
                        throw new Exception("Error occurred while converting date, use the default value instead (6 days ago to today)");
                    }
                }                
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig, out string url)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            url = jsonConfig.sap_url;

            return NC;
        }
    }

    [Serializable]
    public class BillingInfoConfig
    {
        public List<SALES_ORG> SALES_ORG { get; set; }
        public string DISTRIBUTION_CHANNEL { get; set; }
        public List<CREATED_DATE> CREATED_DATE { get; set; }
        public List<CREATED_DATE> CHANGED_DATE { get; set; }
        public List<CREATED_DATE> BILLING_DATE { get; set; }
        public bool RunFromWeb { get; set; }
    }

    public class SALES_ORG
    {
        public string SALES_CODE { get; set; }
        public string SHIPPING_POINT { get; set; }
    }

    public class CREATED_DATE
    {
        public string FROM { get; set; }
        public string TO { get; set; }
    }
}
