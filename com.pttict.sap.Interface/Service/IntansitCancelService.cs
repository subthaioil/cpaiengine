﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace com.pttict.sap.Interface.Service
{
    public class IntansitCancelService : BasicBean
    {
        public string Cancel(string JsonConfig, string JsonContent, ref string msg)
        {
            string result = "";
            sap.intansit.cancel.GoodMovementCancelRequest_Sync_Out_SIService SAPService = new sap.intansit.cancel.GoodMovementCancelRequest_Sync_Out_SIService();

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonContent = javaScriptSerializer.Deserialize<IntansitCancelModel>(JsonContent);
            
            sap.intansit.cancel.BAPI2017_GM_ITEM_04[] ZGOODSMVT_CODE = jsonContent.GM_ITEM_04.ToArray();
            sap.intansit.cancel.BAPIRET2[] RETURN = jsonContent.RETURN.ToArray();
            sap.intansit.cancel.BAPI2017_GM_HEAD_RET GM_HEAD_RET = jsonContent.GM_HEAD_RET;

            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            SAPService.Url = jsonConfig.sap_url;
            SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);

            GM_HEAD_RET = SAPService.GoodMovementCancelRequest_Sync_Out_SI(jsonContent.more_detai.USER_NAME, jsonContent.more_detai.DATE, jsonContent.more_detai.YEAR, jsonContent.more_detai.MATERIAL_DOCUMENT, ref ZGOODSMVT_CODE , ref RETURN);
            if (RETURN != null) {
                int rcount = RETURN.Length;
                for (int i = 0; i < rcount; i++)
                {
                    if (RETURN[i].TYPE == "E")
                        msg += RETURN[i].MESSAGE + " " + RETURN[i].MESSAGE_V1 + " " + RETURN[i].MESSAGE_V2 + " " + RETURN[i].MESSAGE_V3 + " " + RETURN[i].MESSAGE_V4;
                }
            }
           

            return result;

        }

        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }

        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;            

            return NC;
        }

    }
}
