﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Service
{
    public class MemoRecordService : BasicBean
    {
        public sap.memorecord.create.BAPIRETURN1 Create(string JsonConfig, string JsonContent)
        {
            sap.memorecord.create.MemorecordCreateRequest_Sync_Out_SIService SAPService = new sap.memorecord.create.MemorecordCreateRequest_Sync_Out_SIService();

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonContent = javaScriptSerializer.Deserialize<MemoRecordCreateModel>(JsonContent);

            SAPService.Credentials = AuthServiceConnect(JsonConfig);
            var config = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
            SAPService.Url = config.sap_url;
            
            sap.memorecord.create.BAPIRETURN1 bapiReturn = new sap.memorecord.create.BAPIRETURN1();

            try
            {
                string result = SAPService.MemorecordCreateRequest_Sync_Out_SI(
                        jsonContent.AVDAT,
                        jsonContent.BUKRS,
                        jsonContent.DATUM,
                        jsonContent.DISPW,
                        jsonContent.DSART,
                        jsonContent.GRUPP,
                        jsonContent.GVALT,
                        jsonContent.KURST,
                        jsonContent.MERKM,
                        jsonContent.REFER,
                        jsonContent.SGTXT,
                        jsonContent.TESTRUN,
                        jsonContent.VOART,
                        jsonContent.WRSHB,
                        jsonContent.XINVR,
                        jsonContent.ZUONR,
                        out bapiReturn
                    );

                return bapiReturn;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public sap.memorecord.delete.BAPIRETURN1 Delete(string JsonConfig, string JsonContent)
        {
            sap.memorecord.delete.MemorecordDeleteRequest_Sync_Out_SIService SAPService = new sap.memorecord.delete.MemorecordDeleteRequest_Sync_Out_SIService();

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonContent = javaScriptSerializer.Deserialize<MemoRecordDeleteModel>(JsonContent);

            SAPService.Credentials = AuthServiceConnect(JsonConfig);
            var config = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
            SAPService.Url = config.sap_url;

            sap.memorecord.delete.BAPIRETURN1 bapiReturn = new sap.memorecord.delete.BAPIRETURN1();

            string result = SAPService.MemorecordDeleteRequest_Sync_Out_SI(
                    jsonContent.BUKRS,
                    jsonContent.IDENR,
                    jsonContent.TESTRUN,
                    out bapiReturn
                );

            return bapiReturn;
        }

        public sap.memorecord.change.BAPIRETURN1 Change(string JsonConfig, string JsonContent)
        {
            sap.memorecord.change.MemorecordChangeRequest_Sync_Out_SIService SAPService = new sap.memorecord.change.MemorecordChangeRequest_Sync_Out_SIService();

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonContent = javaScriptSerializer.Deserialize<MemoRecordChangeModel>(JsonContent);

            SAPService.Credentials = AuthServiceConnect(JsonConfig);
            var config = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
            SAPService.Url = config.sap_url;

            sap.memorecord.change.BAPIRETURN1 bapiReturn = new sap.memorecord.change.BAPIRETURN1();
            
            string result = SAPService.MemorecordChangeRequest_Sync_Out_SI(
                    jsonContent.AVDAT,
                    jsonContent.BUKRS,
                    jsonContent.DATUM,
                    jsonContent.DISPW,
                    jsonContent.DMSHB,
                    jsonContent.DMSHBSpecified,
                    jsonContent.GRUPP,
                    jsonContent.GVALT,
                    jsonContent.IDENR,
                    jsonContent.KURST,
                    jsonContent.MERKM,
                    jsonContent.REFER,
                    jsonContent.SGTXT,
                    jsonContent.TESTRUN,
                    jsonContent.VOART,
                    jsonContent.WRSHB,
                    jsonContent.WRSHBSpecified,
                    jsonContent.XINVR,
                    jsonContent.ZUONR,
                    out bapiReturn
                );

            return bapiReturn;
        }

        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
    }
}
