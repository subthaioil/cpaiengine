﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
//using static com.pttict.sap.Interface.Model.POSuveyorChangeModel;
//using static com.pttict.sap.Interface.Model.POSuveyorCreateModel;
//using com.pttict.sap.Interface.sap.posurveyor.create;
//using com.pttict.sap.Interface.sap.vatposting.create;

namespace com.pttict.sap.Interface.Service
{
    public class POSuveyorService : BasicBean
    {
        #region "Create"
       // public void Create(string jsonConfig, string content)
       public string Create(string JsonConfig, string content, ref string msg)
        {
            try
            {
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<POSuveyorModel>(content);
                string res = "";
                sap.posurveyor.create.SurveyorCreateRequest_Sync_Out_SIService SAPService = new sap.posurveyor.create.SurveyorCreateRequest_Sync_Out_SIService();
                sap.posurveyor.create.BAPIMEPOHEADER POHEADER = new sap.posurveyor.create.BAPIMEPOHEADER();
                POHEADER.PO_NUMBER = null;
                POHEADER.COMP_CODE = jsonContent.mPOHEADER[0].COMP_CODE;//"1100";
                POHEADER.DOC_TYPE = jsonContent.mPOHEADER[0].DOC_TYPE;// "ZYR";//GLOBAL_CONFIG
                POHEADER.DELETE_IND = null;
                POHEADER.VENDOR = jsonContent.mPOHEADER[0].VENDOR;// "0000000013";
                POHEADER.PMNTTRMS = jsonContent.mPOHEADER[0].PMNTTRMS;// "Z023";//GC                
                POHEADER.PURCH_ORG = jsonContent.mPOHEADER[0].PURCH_ORG;// "1100";
                POHEADER.PUR_GROUP = jsonContent.mPOHEADER[0].PUR_GROUP;// "501";//GC 

                sap.posurveyor.create.BAPIMEPOHEADERX POHEADERX = new sap.posurveyor.create.BAPIMEPOHEADERX();
                POHEADERX.PO_NUMBER = null;
                POHEADERX.COMP_CODE = "X";
                POHEADERX.DOC_TYPE = "X";
                POHEADERX.DELETE_IND = null;
                POHEADERX.VENDOR = "X";
                POHEADERX.PMNTTRMS = "X";
                POHEADERX.PURCH_ORG = "X";
                POHEADERX.PUR_GROUP = "X";

                int index1 = 0;
                sap.posurveyor.create.BAPIMEPOACCOUNT[] POACCOUNT = new sap.posurveyor.create.BAPIMEPOACCOUNT[jsonContent.mPOACCOUNT.Count];
                sap.posurveyor.create.BAPIMEPOACCOUNTX[] POACCOUNTX = new sap.posurveyor.create.BAPIMEPOACCOUNTX[jsonContent.mPOACCOUNT.Count];

                foreach (com.pttict.sap.Interface.Model.POSuveyorModel.BAPIMEPOACCOUNT jsPOACCOUNT in jsonContent.mPOACCOUNT)///Loop ให้วนแถววนคอรัม
                {
                    POACCOUNT[index1] = new sap.posurveyor.create.BAPIMEPOACCOUNT();
                    POACCOUNT[index1].PO_ITEM = jsPOACCOUNT.PO_ITEM;//(indexmode1).ToString("00000"); //"00010";
                    POACCOUNT[index1].SERIAL_NO = jsPOACCOUNT.SERIAL_NO;// "01";
                    POACCOUNT[index1].DELETE_IND = null;
                    POACCOUNT[index1].QUANTITY = jsPOACCOUNT.QUANTITY;//1;
                    POACCOUNT[index1].QUANTITYSpecified = true;
                    POACCOUNT[index1].GL_ACCOUNT = jsPOACCOUNT.GL_ACCOUNT;// "0005000230";
                    POACCOUNT[index1].COSTCENTER = jsPOACCOUNT.COSTCENTER; //"0000113002";
                    POACCOUNT[index1].ORDERID = jsPOACCOUNT.ORDERID;// "D11C-ARLI";
                    POACCOUNT[index1].CO_AREA = jsPOACCOUNT.CO_AREA;// "1000";
                    //x
                    POACCOUNTX[index1] = new sap.posurveyor.create.BAPIMEPOACCOUNTX();
                    POACCOUNTX[index1].PO_ITEM = jsPOACCOUNT.PO_ITEM;//(indexmode1).ToString("00000");
                    POACCOUNTX[index1].SERIAL_NO = jsPOACCOUNT.SERIAL_NO;// "01";
                    POACCOUNTX[index1].DELETE_IND = null;
                    POACCOUNTX[index1].QUANTITY = "X";
                    POACCOUNTX[index1].GL_ACCOUNT = "X";
                    POACCOUNTX[index1].COSTCENTER = "X";
                    POACCOUNTX[index1].ORDERID = "X";
                    POACCOUNTX[index1].CO_AREA = "X";

                    index1 += 1;
                    //indexmode1 += 10;
                }


                index1 = 0;
                sap.posurveyor.create.BAPIMEPOCOND[] POCOND = new sap.posurveyor.create.BAPIMEPOCOND[jsonContent.mPOCOND.Count];
                sap.posurveyor.create.BAPIMEPOCONDX[] POCONDX = new sap.posurveyor.create.BAPIMEPOCONDX[jsonContent.mPOCOND.Count];
                foreach (com.pttict.sap.Interface.Model.POSuveyorModel.BAPIMEPOCOND jsPOCOND in jsonContent.mPOCOND)///Loop ให้วนแถววนคอรัม
                {

                    POCOND[index1] = new sap.posurveyor.create.BAPIMEPOCOND();
                    POCOND[index1].ITM_NUMBER = jsPOCOND.ITM_NUMBER;//(indexmode1).ToString("00000"); //"00010";
                    POCOND[index1].COND_ST_NO = jsPOCOND.COND_ST_NO;//"001";
                    POCOND[index1].COND_VALUE = jsPOCOND.COND_VALUE;//47112.57M;
                    POCOND[index1].COND_VALUESpecified = true;
                    POCOND[index1].CURRENCY = jsPOCOND.CURRENCY;// "THB";
                    POCOND[index1].CONDCOUNT = jsPOCOND.CONDCOUNT;// "01";
                    POCOND[index1].CHANGE_ID = jsPOCOND.CHANGE_ID;// "U";

                    POCONDX[index1] = new sap.posurveyor.create.BAPIMEPOCONDX();
                    POCONDX[index1].ITM_NUMBER = jsPOCOND.ITM_NUMBER;//(indexmode1).ToString("00000");//"00010";
                    POCONDX[index1].COND_ST_NO = jsPOCOND.COND_ST_NO;//"001";
                    POCONDX[index1].ITM_NUMBERX = "X";
                    POCONDX[index1].COND_ST_NOX = "X";
                    POCONDX[index1].COND_VALUE = "X";
                    POCONDX[index1].CURRENCY = "X";
                    POCONDX[index1].CHANGE_ID = "X";

                    index1 += 1;
                    //indexmode1 += 10;
                }





               // indexmode1 = 10;
                index1 = 0;
                sap.posurveyor.create.BAPIMEPOITEM[] POITEM = new sap.posurveyor.create.BAPIMEPOITEM[jsonContent.mPOITEM.Count];
                sap.posurveyor.create.BAPIMEPOITEMX[] POITEMX = new sap.posurveyor.create.BAPIMEPOITEMX[jsonContent.mPOITEM.Count];
                foreach (com.pttict.sap.Interface.Model.POSuveyorModel.BAPIMEPOITEM jsPOITEM in jsonContent.mPOITEM)///Loop ให้วนแถววนคอรัม
                {

                    POITEM[index1] = new sap.posurveyor.create.BAPIMEPOITEM();
                    POITEM[index1].PO_ITEM = jsPOITEM.PO_ITEM; //(indexmode1).ToString("00000"); //"00010";
                    POITEM[index1].DELETE_IND = null;
                    POITEM[index1].SHORT_TEXT = jsPOITEM.SHORT_TEXT;//"loading AL 100%";
                    POITEM[index1].PLANT = jsPOITEM.PLANT;//"1100";
                    POITEM[index1].TRACKINGNO = jsPOITEM.TRACKINGNO;// "CF11170501";
                    POITEM[index1].MATL_GROUP = jsPOITEM.MATL_GROUP;// "IA5310";
                    POITEM[index1].QUANTITY = jsPOITEM.QUANTITY;// 1;
                    POITEM[index1].QUANTITYSpecified = true;
                    POITEM[index1].PO_UNIT = jsPOITEM.PO_UNIT;// "JB";
                    POITEM[index1].NET_PRICE = jsPOITEM.NET_PRICE;// 47112.57M;
                    POITEM[index1].NET_PRICESpecified = true;
                    POITEM[index1].ACCTASSCAT = jsPOITEM.ACCTASSCAT;// "I";

                    POITEMX[index1] = new sap.posurveyor.create.BAPIMEPOITEMX();
                    POITEMX[index1].PO_ITEM = jsPOITEM.PO_ITEM; //(indexmode1).ToString("00000");//R
                    POITEMX[index1].DELETE_IND = null;
                    POITEMX[index1].SHORT_TEXT = "X";
                    POITEMX[index1].PLANT = "X";
                    POITEMX[index1].TRACKINGNO = "X";
                    POITEMX[index1].MATL_GROUP = "X";
                    POITEMX[index1].QUANTITY = "X";
                    POITEMX[index1].PO_UNIT = "X";
                    POITEMX[index1].NET_PRICE = "X";
                    POITEMX[index1].ACCTASSCAT = "X";

                    index1 += 1;
                   // indexmode1 += 10;
                }


                //sap.posurveyor.create.BAPIMEPOITEM[] POITEM = new sap.posurveyor.create.BAPIMEPOITEM[1];
                //POITEM[0] = new sap.posurveyor.create.BAPIMEPOITEM();
                //POITEM[0].PO_ITEM = "00010";
                //POITEM[0].DELETE_IND = null;
                //POITEM[0].SHORT_TEXT = "loading AL 100%";
                //POITEM[0].PLANT = "1100";
                //POITEM[0].TRACKINGNO = "CF11170501";
                //POITEM[0].MATL_GROUP = "IA5310";
                //POITEM[0].QUANTITY = 1;
                //POITEM[0].QUANTITYSpecified = true;
                //POITEM[0].PO_UNIT = "JB";
                //POITEM[0].NET_PRICE = 47112.57M;
                //POITEM[0].NET_PRICESpecified = true;
                //POITEM[0].ACCTASSCAT = "I";

                //sap.posurveyor.create.BAPIMEPOITEMX[] POITEMX = new sap.posurveyor.create.BAPIMEPOITEMX[1];
                //POITEMX[0] = new sap.posurveyor.create.BAPIMEPOITEMX();
                //POITEMX[0].PO_ITEM = "00010";//R
                //POITEMX[0].DELETE_IND = null;
                //POITEMX[0].SHORT_TEXT = "X";
                //POITEMX[0].PLANT = "X";
                //POITEMX[0].TRACKINGNO = "X";
                //POITEMX[0].MATL_GROUP = "X";
                //POITEMX[0].QUANTITY = "X";
                //POITEMX[0].PO_UNIT = "X";
                //POITEMX[0].NET_PRICE = "X";
                //POITEMX[0].ACCTASSCAT = "X";

              // indexmode1 = 10;
                index1 = 0;
                sap.posurveyor.create.BAPIMEPOSCHEDULE[] POSCHEDULE = new sap.posurveyor.create.BAPIMEPOSCHEDULE[jsonContent.mPOSCHEDULE.Count];
                sap.posurveyor.create.BAPIMEPOSCHEDULX[] POSCHEDULEX = new sap.posurveyor.create.BAPIMEPOSCHEDULX[jsonContent.mPOSCHEDULE.Count];
                foreach (com.pttict.sap.Interface.Model.POSuveyorModel.BAPIMEPOSCHEDULE jsPOSCHEDULE in jsonContent.mPOSCHEDULE)///Loop ให้วนแถววนคอรัม
                {

                    POSCHEDULE[index1] = new sap.posurveyor.create.BAPIMEPOSCHEDULE();
                    POSCHEDULE[index1].PO_ITEM = jsPOSCHEDULE.PO_ITEM; //(indexmode1).ToString("00000"); //"00010";
                    POSCHEDULE[index1].SCHED_LINE = jsPOSCHEDULE.SCHED_LINE;// "0001";
                    POSCHEDULE[index1].DEL_DATCAT_EXT = jsPOSCHEDULE.DEL_DATCAT_EXT;//"D";
                    POSCHEDULE[index1].DELIVERY_DATE = jsPOSCHEDULE.DELIVERY_DATE;//"2017-06-01";
                    POSCHEDULE[index1].QUANTITY = jsPOSCHEDULE.QUANTITY;//1;
                    POSCHEDULE[index1].QUANTITYSpecified = true;

                    POSCHEDULEX[index1] = new sap.posurveyor.create.BAPIMEPOSCHEDULX();
                    POSCHEDULEX[index1].PO_ITEM = jsPOSCHEDULE.PO_ITEM; //(indexmode1).ToString("00000"); //"00010";
                    POSCHEDULEX[index1].SCHED_LINE = jsPOSCHEDULE.SCHED_LINE;// "0001";
                    POSCHEDULEX[index1].PO_ITEMX = "X";
                    POSCHEDULEX[index1].SCHED_LINEX = "X";
                    POSCHEDULEX[index1].DEL_DATCAT_EXT = "X";
                    POSCHEDULEX[index1].DELIVERY_DATE = "X";
                    POSCHEDULEX[index1].QUANTITY = "X";

                    index1 += 1;
                   // indexmode1 += 10;
                }

                //sap.posurveyor.create.BAPIMEPOSCHEDULE[] POSCHEDULE = new sap.posurveyor.create.BAPIMEPOSCHEDULE[1];
                //POSCHEDULE[0] = new sap.posurveyor.create.BAPIMEPOSCHEDULE();
                //POSCHEDULE[0].PO_ITEM = "00010"; //R
                //POSCHEDULE[0].SCHED_LINE = "0001";
                //POSCHEDULE[0].DEL_DATCAT_EXT = "D";
                //POSCHEDULE[0].DELIVERY_DATE = "2017-06-01";
                //POSCHEDULE[0].QUANTITY = 1;
                //POSCHEDULE[0].QUANTITYSpecified = true;

                //sap.posurveyor.create.BAPIMEPOSCHEDULX[] POSCHEDULEX = new sap.posurveyor.create.BAPIMEPOSCHEDULX[1];
                //POSCHEDULEX[0] = new sap.posurveyor.create.BAPIMEPOSCHEDULX();
                //POSCHEDULEX[0].PO_ITEM = "00010";//R
                //POSCHEDULEX[0].SCHED_LINE = "0001";
                //POSCHEDULEX[0].PO_ITEMX = "X";
                //POSCHEDULEX[0].SCHED_LINEX = "X";
                //POSCHEDULEX[0].DEL_DATCAT_EXT = "X";
                //POSCHEDULEX[0].DELIVERY_DATE = "X";
                //POSCHEDULEX[0].QUANTITY = "X";


                sap.posurveyor.create.BAPIMEDCM_ALLVERSIONS[] ALLVERSIONS = new sap.posurveyor.create.BAPIMEDCM_ALLVERSIONS[0];
                sap.posurveyor.create.BAPIPAREX[] EXTENSIONIN = new sap.posurveyor.create.BAPIPAREX[0];
                sap.posurveyor.create.BAPIPAREX[] EXTENSIONOUT = new sap.posurveyor.create.BAPIPAREX[0];
                sap.posurveyor.create.BAPI_INVOICE_PLAN_HEADER[]     INVPLANHEADER  = new sap.posurveyor.create.BAPI_INVOICE_PLAN_HEADER[0];
                sap.posurveyor.create.BAPI_INVOICE_PLAN_HEADERX[]    INVPLANHEADERX = new sap.posurveyor.create.BAPI_INVOICE_PLAN_HEADERX[0];
                sap.posurveyor.create.BAPI_INVOICE_PLAN_ITEM[]       INVPLANITEM    = new sap.posurveyor.create.BAPI_INVOICE_PLAN_ITEM[0];
                sap.posurveyor.create.BAPI_INVOICE_PLAN_ITEMX[]      INVPLANITEMX   = new sap.posurveyor.create.BAPI_INVOICE_PLAN_ITEMX[0];
                sap.posurveyor.create.BAPIMEPOACCOUNTPROFITSEGMENT[] POACCOUNTPROFITSEGMENT = new sap.posurveyor.create.BAPIMEPOACCOUNTPROFITSEGMENT[0];
                sap.posurveyor.create.BAPIMEPOADDRDELIVERY[]POADDRDELIVERY = new sap.posurveyor.create.BAPIMEPOADDRDELIVERY[0];
                sap.posurveyor.create.BAPIMEPOCOMPONENT[]   POCOMPONENTS = new sap.posurveyor.create.BAPIMEPOCOMPONENT[0];
                sap.posurveyor.create.BAPIMEPOCOMPONENTX[]  POCOMPONENTSX = new sap.posurveyor.create.BAPIMEPOCOMPONENTX[0];
                sap.posurveyor.create.BAPIMEPOCONDHEADER[]  POCONDHEADER = new sap.posurveyor.create.BAPIMEPOCONDHEADER[0];
                sap.posurveyor.create.BAPIMEPOCONDHEADERX[] POCONDHEADERX = new sap.posurveyor.create.BAPIMEPOCONDHEADERX[0];
                sap.posurveyor.create.BAPIESUCC[] POCONTRACTLIMITS = new sap.posurveyor.create.BAPIESUCC[0];
                sap.posurveyor.create.BAPIEIPO[]  POEXPIMPITEM      = new sap.posurveyor.create.BAPIEIPO[0];
                sap.posurveyor.create.BAPIEIPOX[] POEXPIMPITEMX    = new sap.posurveyor.create.BAPIEIPOX[0];
                sap.posurveyor.create.BAPIESUHC[] POLIMITS         = new sap.posurveyor.create.BAPIESUHC[0];
                sap.posurveyor.create.BAPIEKKOP[] POPARTNER = new sap.posurveyor.create.BAPIEKKOP[0];                
                sap.posurveyor.create.BAPIESLLC[]           POSERVICES     = new sap.posurveyor.create.BAPIESLLC[0];
                sap.posurveyor.create.BAPIESLLTX[]          POSERVICESTEXT = new sap.posurveyor.create.BAPIESLLTX[0];
                sap.posurveyor.create.BAPIITEMSHIP[]        POSHIPPING     = new sap.posurveyor.create.BAPIITEMSHIP[0];
                sap.posurveyor.create.BAPIMEPOSHIPPEXP[]    POSHIPPINGEXP  = new sap.posurveyor.create.BAPIMEPOSHIPPEXP[0];
                sap.posurveyor.create.BAPIITEMSHIPX[]       POSHIPPINGX    = new sap.posurveyor.create.BAPIITEMSHIPX[0];
                sap.posurveyor.create.BAPIESKLC[] POSRVACCESSVALUES = new sap.posurveyor.create.BAPIESKLC[0];
                sap.posurveyor.create.BAPIMEPOTEXTHEADER[]  POTEXTHEADER   = new sap.posurveyor.create.BAPIMEPOTEXTHEADER[0];
                sap.posurveyor.create.BAPIMEPOTEXT[]        POTEXTITEM     = new sap.posurveyor.create.BAPIMEPOTEXT[0];
                sap.posurveyor.create.BAPIRET2[] returnValue = new sap.posurveyor.create.BAPIRET2[0];
                sap.posurveyor.create.BAPIMEPOSERIALNO[]    SERIALNUMBER   = new sap.posurveyor.create.BAPIMEPOSERIALNO[0];
                sap.posurveyor.create.BAPIMEPOSERIALNOX[]   SERIALNUMBERX = new sap.posurveyor.create.BAPIMEPOSERIALNOX[0];


                sap.posurveyor.create.BAPIMEPOADDRVENDOR POADDRVENDOR = new sap.posurveyor.create.BAPIMEPOADDRVENDOR();
                sap.posurveyor.create.BAPIEIKP POEXPIMPHEADER    = new sap.posurveyor.create.BAPIEIKP();
                sap.posurveyor.create.BAPIEIKPX POEXPIMPHEADERX  = new sap.posurveyor.create.BAPIEIKPX();
                sap.posurveyor.create.BAPIMEDCM VERSIONS         = new sap.posurveyor.create.BAPIMEDCM();

                sap.posurveyor.create.BAPIMEPOHEADER POHEADER_resCreate = new sap.posurveyor.create.BAPIMEPOHEADER();

                //SAPService.Url = "http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SurveyorCreateRequest_Sync_Out_SI&interfaceNamespace=http://thaioilgroup.com/_i_purchaseorder/purchasing/surveyor_create/";
                //SAPService.Credentials = AuthServiceConnect("ZPI_IF_CIP", "1@zpi_if_cip");
                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);

                string EXPPURCHASEORDER = "";
                sap.posurveyor.create.BAPIEIKP EXPPOEXPIMPHEADER = new sap.posurveyor.create.BAPIEIKP();
                POHEADER_resCreate = SAPService.SurveyorCreateRequest_Sync_Out_SI(
                //string result = SAPService.SurveyorCreateRequest_Sync_Out_SI(
                   null, /*I_NO_COMMIT,*/
                   null, /*MEMORY_COMPLETE,*/
                   null, /*MEMORY_UNCOMPLET*/
                   null, /*NO_AUTHORITY,*/
                   null, /*NO_MESSAGE_REQ,*/
                   null, /*NO_MESSAGING,*/
                   null, /*NO_PRICE_FROM_PO*/
                   null, /*PARK_COMPLETE,*/
                   null, /*PARK_UNCOMPLETE,*/
                   POADDRVENDOR,
                   POEXPIMPHEADER,
                   POEXPIMPHEADERX,
                   POHEADER,
                   POHEADERX,
                   null,// TESTRUN,
                   VERSIONS,
                   ref ALLVERSIONS,
                   ref EXTENSIONIN,
                   ref EXTENSIONOUT,
                   ref INVPLANHEADER,
                   ref INVPLANHEADERX,
                   ref INVPLANITEM,
                   ref INVPLANITEMX,
                   ref POACCOUNT,
                   ref POACCOUNTPROFITSEGMENT,
                   ref POACCOUNTX,
                   ref POADDRDELIVERY,
                   ref POCOMPONENTS,
                   ref POCOMPONENTSX,
                   ref POCOND,
                   ref POCONDHEADER,
                   ref POCONDHEADERX,
                   ref POCONDX,
                   ref POCONTRACTLIMITS,
                   ref POEXPIMPITEM,
                   ref POEXPIMPITEMX,
                   ref POITEM,
                   ref POITEMX,
                   ref POLIMITS,
                   ref POPARTNER,
                   ref POSCHEDULE,
                   ref POSCHEDULEX,
                   ref POSERVICES,
                   ref POSERVICESTEXT,
                   ref POSHIPPING,
                   ref POSHIPPINGEXP,
                   ref POSHIPPINGX,
                   ref POSRVACCESSVALUES,
                   ref POTEXTHEADER,
                   ref POTEXTITEM,
                   ref returnValue,
                   ref SERIALNUMBER,
                   ref SERIALNUMBERX,
                   out EXPPOEXPIMPHEADER,
                   out EXPPURCHASEORDER
                    );

                string logon = "";
                if (EXPPURCHASEORDER == null)
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon = "E";
                        }
                    }

                }
                else
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "S")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon = "S";
                        }
                    }
                }

                // return res;
                return "|" + logon + "M" + msg + "R" + EXPPURCHASEORDER;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region "Change"
        //public void Change(string jsonConfig, string content)
        public string Change(string JsonConfig, string content, ref string msg)
        {
            try
            {
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<POSuveyorModel>(content);
                string res = "";
                sap.posurveyor.change.SurveyorChangeRequest_Sync_Out_SIService SAPService = new sap.posurveyor.change.SurveyorChangeRequest_Sync_Out_SIService();

                string PURCHASEORDER = jsonContent.PURCHASEORDER;// "4900000053"; //po  
                
                //sap.posurveyor.change.BAPIMEPOITEM[] POITEM = new sap.posurveyor.change.BAPIMEPOITEM[1];
                //POITEM[0] = new sap.posurveyor.change.BAPIMEPOITEM();
                //POITEM[0].PO_ITEM = "00010";
                //POITEM[0].DELETE_IND = null;
                //POITEM[0].SHORT_TEXT = "loading AL 100%";
                //POITEM[0].PLANT = "1100";
                //POITEM[0].TRACKINGNO = "CF11170501";
                //POITEM[0].MATL_GROUP = "IA5310";
                //POITEM[0].QUANTITY = 1;
                //POITEM[0].QUANTITYSpecified = true;
                //POITEM[0].PO_UNIT = "JB";
                //POITEM[0].NET_PRICE = 1400.00M;
                //POITEM[0].NET_PRICESpecified = true;
                //POITEM[0].ACCTASSCAT = "I";

                //sap.posurveyor.change.BAPIMEPOITEMX[] POITEMX = new sap.posurveyor.change.BAPIMEPOITEMX[1];
                //POITEMX[0] = new sap.posurveyor.change.BAPIMEPOITEMX();
                //POITEMX[0].PO_ITEM = "00010";//R
                //POITEMX[0].DELETE_IND = null;
                //POITEMX[0].SHORT_TEXT = "X";
                //POITEMX[0].PLANT = "X";
                //POITEMX[0].TRACKINGNO = "X";
                //POITEMX[0].MATL_GROUP = "X";
                //POITEMX[0].QUANTITY = "X";
                //POITEMX[0].PO_UNIT = "X";
                //POITEMX[0].NET_PRICE = "X";
                //POITEMX[0].ACCTASSCAT = "X";

                //long indexmode1 = 10;
                int index1 = 0;              
                sap.posurveyor.change.BAPIMEPOITEM[] POITEM = new sap.posurveyor.change.BAPIMEPOITEM[jsonContent.mPOITEM.Count];
                sap.posurveyor.change.BAPIMEPOITEMX[] POITEMX = new sap.posurveyor.change.BAPIMEPOITEMX[jsonContent.mPOITEM.Count];               
                foreach (com.pttict.sap.Interface.Model.POSuveyorModel.BAPIMEPOITEM jsPOITEM in jsonContent.mPOITEM)///Loop ให้วนแถววนคอรัม
                {

                    POITEM[index1] = new sap.posurveyor.change.BAPIMEPOITEM();
                    POITEM[index1].PO_ITEM = jsPOITEM.PO_ITEM; //"00010";
                    POITEM[index1].DELETE_IND = null;
                    POITEM[index1].SHORT_TEXT = jsPOITEM.SHORT_TEXT;//"loading AL 100%";
                    POITEM[index1].PLANT = jsPOITEM.PLANT;//"1100";
                    POITEM[index1].TRACKINGNO = jsPOITEM.TRACKINGNO;// "CF11170501";
                    POITEM[index1].MATL_GROUP = jsPOITEM.MATL_GROUP;// "IA5310";
                    POITEM[index1].QUANTITY = jsPOITEM.QUANTITY;// 1;
                    POITEM[index1].QUANTITYSpecified = true;
                    POITEM[index1].PO_UNIT = jsPOITEM.PO_UNIT;// "JB";
                    POITEM[index1].NET_PRICE = jsPOITEM.NET_PRICE;// 47112.57M;
                    POITEM[index1].NET_PRICESpecified = true;
                    POITEM[index1].ACCTASSCAT = jsPOITEM.ACCTASSCAT;// "I";


                    POITEMX[index1] = new sap.posurveyor.change.BAPIMEPOITEMX();
                    POITEMX[index1].PO_ITEM = jsPOITEM.PO_ITEM;//(indexmode1).ToString("00000");//R
                    POITEMX[index1].DELETE_IND = null;
                    POITEMX[index1].SHORT_TEXT = "X";
                    POITEMX[index1].PLANT = "X";
                    POITEMX[index1].TRACKINGNO = "X";
                    POITEMX[index1].MATL_GROUP = "X";
                    POITEMX[index1].QUANTITY = "X";
                    POITEMX[index1].PO_UNIT = "X";
                    POITEMX[index1].NET_PRICE = "X";
                    POITEMX[index1].ACCTASSCAT = "X";

                    index1 += 1;
                   // indexmode1 += 10;
                }


                //sap.posurveyor.change.BAPIMEPOSCHEDULE[] POSCHEDULE = new sap.posurveyor.change.BAPIMEPOSCHEDULE[1];
                //POSCHEDULE[0] = new sap.posurveyor.change.BAPIMEPOSCHEDULE();
                //POSCHEDULE[0].PO_ITEM = "00010"; //R
                //POSCHEDULE[0].SCHED_LINE = "0001";
                //POSCHEDULE[0].DEL_DATCAT_EXT = "D";
                //POSCHEDULE[0].DELIVERY_DATE = "2017-06-01";
                //POSCHEDULE[0].QUANTITY = 1;
                //POSCHEDULE[0].QUANTITYSpecified = true;

                //sap.posurveyor.change.BAPIMEPOSCHEDULX[] POSCHEDULEX = new sap.posurveyor.change.BAPIMEPOSCHEDULX[1];
                //POSCHEDULEX[0] = new sap.posurveyor.change.BAPIMEPOSCHEDULX();
                //POSCHEDULEX[0].PO_ITEM = "00010";//R
                //POSCHEDULEX[0].SCHED_LINE = "0001";
                //POSCHEDULEX[0].PO_ITEMX = "X";
                //POSCHEDULEX[0].SCHED_LINEX = "X";
                //POSCHEDULEX[0].DEL_DATCAT_EXT = "X";
                //POSCHEDULEX[0].DELIVERY_DATE = "X";
                //POSCHEDULEX[0].QUANTITY = "X";

               // indexmode1 = 10;
                index1 = 0;
                sap.posurveyor.change.BAPIMEPOSCHEDULE[] POSCHEDULE = new sap.posurveyor.change.BAPIMEPOSCHEDULE[jsonContent.mPOSCHEDULE.Count];
                sap.posurveyor.change.BAPIMEPOSCHEDULX[] POSCHEDULEX = new sap.posurveyor.change.BAPIMEPOSCHEDULX[jsonContent.mPOSCHEDULE.Count];
                foreach (com.pttict.sap.Interface.Model.POSuveyorModel.BAPIMEPOSCHEDULE jsPOSCHEDULE in jsonContent.mPOSCHEDULE)///Loop ให้วนแถววนคอรัม
                {

                    POSCHEDULE[index1] = new sap.posurveyor.change.BAPIMEPOSCHEDULE();
                    POSCHEDULE[index1].PO_ITEM = jsPOSCHEDULE.PO_ITEM; //(indexmode1).ToString("00000"); //"00010";
                    POSCHEDULE[index1].SCHED_LINE = jsPOSCHEDULE.SCHED_LINE;// "0001";
                    POSCHEDULE[index1].DEL_DATCAT_EXT = jsPOSCHEDULE.DEL_DATCAT_EXT;//"D";
                    POSCHEDULE[index1].DELIVERY_DATE = jsPOSCHEDULE.DELIVERY_DATE;//"2017-06-01";
                    POSCHEDULE[index1].QUANTITY = jsPOSCHEDULE.QUANTITY;//1;
                    POSCHEDULE[index1].QUANTITYSpecified = true;

                    POSCHEDULEX[index1] = new sap.posurveyor.change.BAPIMEPOSCHEDULX();
                    POSCHEDULEX[index1].PO_ITEM = jsPOSCHEDULE.PO_ITEM; //(indexmode1).ToString("00000"); //"00010";
                    POSCHEDULEX[index1].SCHED_LINE = jsPOSCHEDULE.SCHED_LINE;// "0001";
                    POSCHEDULEX[index1].PO_ITEMX = "X";
                    POSCHEDULEX[index1].SCHED_LINEX = "X";
                    POSCHEDULEX[index1].DEL_DATCAT_EXT = "X";
                    POSCHEDULEX[index1].DELIVERY_DATE = "X";
                    POSCHEDULEX[index1].QUANTITY = "X";

                    index1 += 1;
                   // indexmode1 += 10;
                }

                sap.posurveyor.change.BAPIMEDCM_ALLVERSIONS[] ALLVERSIONS = new sap.posurveyor.change.BAPIMEDCM_ALLVERSIONS[0];
                sap.posurveyor.change.BAPIPAREX[] EXTENSIONIN = new sap.posurveyor.change.BAPIPAREX[0];
                sap.posurveyor.change.BAPIPAREX[] EXTENSIONOUT = new sap.posurveyor.change.BAPIPAREX[0];
                sap.posurveyor.change.BAPI_INVOICE_PLAN_HEADER[] INVPLANHEADER = new sap.posurveyor.change.BAPI_INVOICE_PLAN_HEADER[0];
                sap.posurveyor.change.BAPI_INVOICE_PLAN_HEADERX[] INVPLANHEADERX = new sap.posurveyor.change.BAPI_INVOICE_PLAN_HEADERX[0];
                sap.posurveyor.change.BAPI_INVOICE_PLAN_ITEM[] INVPLANITEM = new sap.posurveyor.change.BAPI_INVOICE_PLAN_ITEM[0];
                sap.posurveyor.change.BAPI_INVOICE_PLAN_ITEMX[] INVPLANITEMX = new sap.posurveyor.change.BAPI_INVOICE_PLAN_ITEMX[0];
                sap.posurveyor.change.BAPIMEPOACCOUNTPROFITSEGMENT[] POACCOUNTPROFITSEGMENT = new sap.posurveyor.change.BAPIMEPOACCOUNTPROFITSEGMENT[0];
                sap.posurveyor.change.BAPIMEPOADDRDELIVERY[] POADDRDELIVERY = new sap.posurveyor.change.BAPIMEPOADDRDELIVERY[0];
                sap.posurveyor.change.BAPIMEPOCOMPONENT[] POCOMPONENTS = new sap.posurveyor.change.BAPIMEPOCOMPONENT[0];
                sap.posurveyor.change.BAPIMEPOCOMPONENTX[] POCOMPONENTSX = new sap.posurveyor.change.BAPIMEPOCOMPONENTX[0];
                sap.posurveyor.change.BAPIMEPOCONDHEADER[] POCONDHEADER = new sap.posurveyor.change.BAPIMEPOCONDHEADER[0];
                sap.posurveyor.change.BAPIMEPOCONDHEADERX[] POCONDHEADERX = new sap.posurveyor.change.BAPIMEPOCONDHEADERX[0];
                sap.posurveyor.change.BAPIESUCC[] POCONTRACTLIMITS = new sap.posurveyor.change.BAPIESUCC[0];
                sap.posurveyor.change.BAPIEIPO[] POEXPIMPITEM = new sap.posurveyor.change.BAPIEIPO[0];
                sap.posurveyor.change.BAPIEIPOX[] POEXPIMPITEMX = new sap.posurveyor.change.BAPIEIPOX[0];
                sap.posurveyor.change.BAPIESUHC[] POLIMITS = new sap.posurveyor.change.BAPIESUHC[0];
                sap.posurveyor.change.BAPIEKKOP[] POPARTNER = new sap.posurveyor.change.BAPIEKKOP[0];
                sap.posurveyor.change.BAPIESLLC[] POSERVICES = new sap.posurveyor.change.BAPIESLLC[0];
                sap.posurveyor.change.BAPIESLLTX[] POSERVICESTEXT = new sap.posurveyor.change.BAPIESLLTX[0];
                sap.posurveyor.change.BAPIITEMSHIP[] POSHIPPING = new sap.posurveyor.change.BAPIITEMSHIP[0];
                sap.posurveyor.change.BAPIMEPOSHIPPEXP[] POSHIPPINGEXP = new sap.posurveyor.change.BAPIMEPOSHIPPEXP[0];
                sap.posurveyor.change.BAPIITEMSHIPX[] POSHIPPINGX = new sap.posurveyor.change.BAPIITEMSHIPX[0];
                sap.posurveyor.change.BAPIESKLC[] POSRVACCESSVALUES = new sap.posurveyor.change.BAPIESKLC[0];
                sap.posurveyor.change.BAPIMEPOTEXTHEADER[] POTEXTHEADER = new sap.posurveyor.change.BAPIMEPOTEXTHEADER[0];
                sap.posurveyor.change.BAPIMEPOTEXT[] POTEXTITEM = new sap.posurveyor.change.BAPIMEPOTEXT[0];
                sap.posurveyor.change.BAPIRET2[] returnValue = new sap.posurveyor.change.BAPIRET2[0];
                sap.posurveyor.change.BAPIMEPOSERIALNO[] SERIALNUMBER = new sap.posurveyor.change.BAPIMEPOSERIALNO[0];
                sap.posurveyor.change.BAPIMEPOSERIALNOX[] SERIALNUMBERX = new sap.posurveyor.change.BAPIMEPOSERIALNOX[0];
                sap.posurveyor.change.BAPIMEPOACCOUNT[] POACCOUNT = new sap.posurveyor.change.BAPIMEPOACCOUNT[0];
                sap.posurveyor.change.BAPIMEPOACCOUNTX[] POACCOUNTX = new sap.posurveyor.change.BAPIMEPOACCOUNTX[0];
                sap.posurveyor.change.BAPIMEPOCOND[] POCOND = new sap.posurveyor.change.BAPIMEPOCOND[0];
                sap.posurveyor.change.BAPIMEPOCONDX[] POCONDX = new sap.posurveyor.change.BAPIMEPOCONDX[0];
                sap.posurveyor.change.BAPIEKES[] POCONFIRMATION = new sap.posurveyor.change.BAPIEKES[0];
                sap.posurveyor.change.BAPIEKBE[] POHISTORY = new sap.posurveyor.change.BAPIEKBE[0];
                sap.posurveyor.change.BAPIEKBE_MA[] POHISTORY_MA = new sap.posurveyor.change.BAPIEKBE_MA[0];
                sap.posurveyor.change.BAPIEKBES[] POHISTORY_TOTALS = new sap.posurveyor.change.BAPIEKBES[0];

                sap.posurveyor.change.BAPIMEPOADDRVENDOR POADDRVENDOR = new sap.posurveyor.change.BAPIMEPOADDRVENDOR();
                sap.posurveyor.change.BAPIEIKP POEXPIMPHEADER = new sap.posurveyor.change.BAPIEIKP();
                sap.posurveyor.change.BAPIEIKPX POEXPIMPHEADERX = new sap.posurveyor.change.BAPIEIKPX();
                sap.posurveyor.change.BAPIMEDCM VERSIONS = new sap.posurveyor.change.BAPIMEDCM();
                sap.posurveyor.change.BAPIMEPOHEADER POHEADER = new sap.posurveyor.change.BAPIMEPOHEADER();
                sap.posurveyor.change.BAPIMEPOHEADERX POHEADERX = new sap.posurveyor.change.BAPIMEPOHEADERX();
                sap.posurveyor.change.BAPIEIKP EXPPOEXPIMPHEADER = new sap.posurveyor.change.BAPIEIKP();

                sap.posurveyor.change.BAPIMEPOHEADER POHEADER_res = new sap.posurveyor.change.BAPIMEPOHEADER();

                //SAPService.Url = "http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SurveyorChangeRequest_Sync_Out_SI&interfaceNamespace=http://thaioilgroup.com/_i_purchaseorder/purchasing/surveyor_change/";
                //SAPService.Credentials = AuthServiceConnect("ZPI_IF_CIP", "1@zpi_if_cip");
                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);


                string EXPHEADER = "";
                //SAPService.SurveyorChangeRequest_Sync_Out_SI(               
                POHEADER_res = SAPService.SurveyorChangeRequest_Sync_Out_SI(
                  null, /*I_NO_COMMIT,*/
                  null, /*MEMORY_COMPLETE,*/
                  null, /*MEMORY_UNCOMPLET*/
                  null, /*NO_AUTHORITY,*/
                  null, /*NO_MESSAGE_REQ,*/
                  null, /*NO_MESSAGING,*/
                  null, /*NO_PRICE_FROM_PO*/
                  null, /*PARK_COMPLETE,*/
                  null, /*PARK_UNCOMPLETE,*/
                  POADDRVENDOR,
                  POEXPIMPHEADER,
                  POEXPIMPHEADERX,
                  POHEADER,
                  POHEADERX,
                  PURCHASEORDER,
                  null,// TESTRUN,
                  VERSIONS,
                  ref ALLVERSIONS,
                  ref EXTENSIONIN,
                  ref EXTENSIONOUT,
                  ref INVPLANHEADER,
                  ref INVPLANHEADERX,
                  ref INVPLANITEM,
                  ref INVPLANITEMX,
                  ref POACCOUNT,
                  ref POACCOUNTPROFITSEGMENT,
                  ref POACCOUNTX,
                  ref POADDRDELIVERY,
                  ref POCOMPONENTS,
                  ref POCOMPONENTSX,
                  ref POCOND,
                  ref POCONDHEADER,
                  ref POCONDHEADERX,
                  ref POCONDX,
                  ref POCONFIRMATION,
                  ref POCONTRACTLIMITS,
                  ref POEXPIMPITEM,
                  ref POEXPIMPITEMX,
                  ref POHISTORY,
                  ref POHISTORY_MA,
                  ref POHISTORY_TOTALS,
                  ref POITEM,
                  ref POITEMX,
                  ref POLIMITS,
                  ref POPARTNER,
                  ref POSCHEDULE,
                  ref POSCHEDULEX,
                  ref POSERVICES,
                  ref POSERVICESTEXT,
                  ref POSHIPPING,
                  ref POSHIPPINGEXP,
                  ref POSHIPPINGX,
                  ref POSRVACCESSVALUES,
                  ref POTEXTHEADER,
                  ref POTEXTITEM,
                  ref returnValue,
                  ref SERIALNUMBER,
                  ref SERIALNUMBERX,
                  out EXPPOEXPIMPHEADER
                  );

                string pono = "";
                if (POHEADER_res != null) {
                    pono = string.IsNullOrEmpty(POHEADER_res.PO_NUMBER) ? "" : POHEADER_res.PO_NUMBER;
                }

                string logon = "S";

                if (returnValue != null) {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon = "E";
                        }
                    }
                }
                
                return "|" + logon + "M" + msg + "R" + pono;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion



        #region "Delete"
       // public void Delete(string jsonConfig, string content)
        public string Delete(string JsonConfig, string content, ref string msg)
        {
            try
            {
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<POSuveyorModel>(content);
                string res = "";
                sap.posurveyor.change.SurveyorChangeRequest_Sync_Out_SIService SAPService = new sap.posurveyor.change.SurveyorChangeRequest_Sync_Out_SIService();

                string PURCHASEORDER = jsonContent.PURCHASEORDER;// "4900000053"; //po  
                //string PURCHASEORDER = "4900000053"; //po
                //sap.posurveyor.change.BAPIMEPOITEM[] POITEM = new sap.posurveyor.change.BAPIMEPOITEM[1];
                //POITEM[0] = new sap.posurveyor.change.BAPIMEPOITEM();
                //POITEM[0].PO_ITEM = "00010";
                //POITEM[0].DELETE_IND = "X";


                //sap.posurveyor.change.BAPIMEPOITEMX[] POITEMX = new sap.posurveyor.change.BAPIMEPOITEMX[1];
                //POITEMX[0] = new sap.posurveyor.change.BAPIMEPOITEMX();
                //POITEMX[0].PO_ITEM = "00010";//R
                //POITEMX[0].DELETE_IND = "X";


              //  long indexmode1 = 10;
                int index1 = 0;
                sap.posurveyor.change.BAPIMEPOITEM[] POITEM = new sap.posurveyor.change.BAPIMEPOITEM[jsonContent.mPOITEM.Count];
                sap.posurveyor.change.BAPIMEPOITEMX[] POITEMX = new sap.posurveyor.change.BAPIMEPOITEMX[jsonContent.mPOITEM.Count];
                foreach (com.pttict.sap.Interface.Model.POSuveyorModel.BAPIMEPOITEM jsPOITEMDel in jsonContent.mPOITEM)///Loop ให้วนแถววนคอรัม
                {

                    POITEM[index1] = new sap.posurveyor.change.BAPIMEPOITEM();
                    // POITEM[index1].PO_ITEM = (indexmode1).ToString("00000"); //"00010";
                    POITEM[index1].PO_ITEM = jsPOITEMDel.PO_ITEM;//"00010";
                    POITEM[index1].DELETE_IND = "X";

                    POITEMX[index1] = new sap.posurveyor.change.BAPIMEPOITEMX();
                    POITEMX[index1].PO_ITEM = jsPOITEMDel.PO_ITEM;//"00010";
                    POITEMX[index1].DELETE_IND = "X";

                    index1 += 1;
                   // indexmode1 += 10;
                }



                sap.posurveyor.change.BAPIMEPOSCHEDULE[] POSCHEDULE = new sap.posurveyor.change.BAPIMEPOSCHEDULE[0];
                sap.posurveyor.change.BAPIMEPOSCHEDULX[] POSCHEDULEX = new sap.posurveyor.change.BAPIMEPOSCHEDULX[0];



                sap.posurveyor.change.BAPIMEDCM_ALLVERSIONS[] ALLVERSIONS = new sap.posurveyor.change.BAPIMEDCM_ALLVERSIONS[0];
                sap.posurveyor.change.BAPIPAREX[] EXTENSIONIN = new sap.posurveyor.change.BAPIPAREX[0];
                sap.posurveyor.change.BAPIPAREX[] EXTENSIONOUT = new sap.posurveyor.change.BAPIPAREX[0];
                sap.posurveyor.change.BAPI_INVOICE_PLAN_HEADER[] INVPLANHEADER = new sap.posurveyor.change.BAPI_INVOICE_PLAN_HEADER[0];
                sap.posurveyor.change.BAPI_INVOICE_PLAN_HEADERX[] INVPLANHEADERX = new sap.posurveyor.change.BAPI_INVOICE_PLAN_HEADERX[0];
                sap.posurveyor.change.BAPI_INVOICE_PLAN_ITEM[] INVPLANITEM = new sap.posurveyor.change.BAPI_INVOICE_PLAN_ITEM[0];
                sap.posurveyor.change.BAPI_INVOICE_PLAN_ITEMX[] INVPLANITEMX = new sap.posurveyor.change.BAPI_INVOICE_PLAN_ITEMX[0];
                sap.posurveyor.change.BAPIMEPOACCOUNTPROFITSEGMENT[] POACCOUNTPROFITSEGMENT = new sap.posurveyor.change.BAPIMEPOACCOUNTPROFITSEGMENT[0];
                sap.posurveyor.change.BAPIMEPOADDRDELIVERY[] POADDRDELIVERY = new sap.posurveyor.change.BAPIMEPOADDRDELIVERY[0];
                sap.posurveyor.change.BAPIMEPOCOMPONENT[] POCOMPONENTS = new sap.posurveyor.change.BAPIMEPOCOMPONENT[0];
                sap.posurveyor.change.BAPIMEPOCOMPONENTX[] POCOMPONENTSX = new sap.posurveyor.change.BAPIMEPOCOMPONENTX[0];
                sap.posurveyor.change.BAPIMEPOCONDHEADER[] POCONDHEADER = new sap.posurveyor.change.BAPIMEPOCONDHEADER[0];
                sap.posurveyor.change.BAPIMEPOCONDHEADERX[] POCONDHEADERX = new sap.posurveyor.change.BAPIMEPOCONDHEADERX[0];
                sap.posurveyor.change.BAPIESUCC[] POCONTRACTLIMITS = new sap.posurveyor.change.BAPIESUCC[0];
                sap.posurveyor.change.BAPIEIPO[] POEXPIMPITEM = new sap.posurveyor.change.BAPIEIPO[0];
                sap.posurveyor.change.BAPIEIPOX[] POEXPIMPITEMX = new sap.posurveyor.change.BAPIEIPOX[0];
                sap.posurveyor.change.BAPIESUHC[] POLIMITS = new sap.posurveyor.change.BAPIESUHC[0];
                sap.posurveyor.change.BAPIEKKOP[] POPARTNER = new sap.posurveyor.change.BAPIEKKOP[0];
                sap.posurveyor.change.BAPIESLLC[] POSERVICES = new sap.posurveyor.change.BAPIESLLC[0];
                sap.posurveyor.change.BAPIESLLTX[] POSERVICESTEXT = new sap.posurveyor.change.BAPIESLLTX[0];
                sap.posurveyor.change.BAPIITEMSHIP[] POSHIPPING = new sap.posurveyor.change.BAPIITEMSHIP[0];
                sap.posurveyor.change.BAPIMEPOSHIPPEXP[] POSHIPPINGEXP = new sap.posurveyor.change.BAPIMEPOSHIPPEXP[0];
                sap.posurveyor.change.BAPIITEMSHIPX[] POSHIPPINGX = new sap.posurveyor.change.BAPIITEMSHIPX[0];
                sap.posurveyor.change.BAPIESKLC[] POSRVACCESSVALUES = new sap.posurveyor.change.BAPIESKLC[0];
                sap.posurveyor.change.BAPIMEPOTEXTHEADER[] POTEXTHEADER = new sap.posurveyor.change.BAPIMEPOTEXTHEADER[0];
                sap.posurveyor.change.BAPIMEPOTEXT[] POTEXTITEM = new sap.posurveyor.change.BAPIMEPOTEXT[0];
                sap.posurveyor.change.BAPIRET2[] returnValue = new sap.posurveyor.change.BAPIRET2[0];
                sap.posurveyor.change.BAPIMEPOSERIALNO[] SERIALNUMBER = new sap.posurveyor.change.BAPIMEPOSERIALNO[0];
                sap.posurveyor.change.BAPIMEPOSERIALNOX[] SERIALNUMBERX = new sap.posurveyor.change.BAPIMEPOSERIALNOX[0];
                sap.posurveyor.change.BAPIMEPOACCOUNT[] POACCOUNT = new sap.posurveyor.change.BAPIMEPOACCOUNT[0];
                sap.posurveyor.change.BAPIMEPOACCOUNTX[] POACCOUNTX = new sap.posurveyor.change.BAPIMEPOACCOUNTX[0];
                sap.posurveyor.change.BAPIMEPOCOND[] POCOND = new sap.posurveyor.change.BAPIMEPOCOND[0];
                sap.posurveyor.change.BAPIMEPOCONDX[] POCONDX = new sap.posurveyor.change.BAPIMEPOCONDX[0];
                sap.posurveyor.change.BAPIEKES[] POCONFIRMATION = new sap.posurveyor.change.BAPIEKES[0];
                sap.posurveyor.change.BAPIEKBE[] POHISTORY = new sap.posurveyor.change.BAPIEKBE[0];
                sap.posurveyor.change.BAPIEKBE_MA[] POHISTORY_MA = new sap.posurveyor.change.BAPIEKBE_MA[0];
                sap.posurveyor.change.BAPIEKBES[] POHISTORY_TOTALS = new sap.posurveyor.change.BAPIEKBES[0];

                sap.posurveyor.change.BAPIMEPOADDRVENDOR POADDRVENDOR = new sap.posurveyor.change.BAPIMEPOADDRVENDOR();
                sap.posurveyor.change.BAPIEIKP POEXPIMPHEADER = new sap.posurveyor.change.BAPIEIKP();
                sap.posurveyor.change.BAPIEIKPX POEXPIMPHEADERX = new sap.posurveyor.change.BAPIEIKPX();
                sap.posurveyor.change.BAPIMEDCM VERSIONS = new sap.posurveyor.change.BAPIMEDCM();
                sap.posurveyor.change.BAPIMEPOHEADER POHEADER = new sap.posurveyor.change.BAPIMEPOHEADER();
                sap.posurveyor.change.BAPIMEPOHEADERX POHEADERX = new sap.posurveyor.change.BAPIMEPOHEADERX();
                sap.posurveyor.change.BAPIEIKP EXPPOEXPIMPHEADER = new sap.posurveyor.change.BAPIEIKP();

                sap.posurveyor.change.BAPIMEPOHEADER POHEADER_res = new sap.posurveyor.change.BAPIMEPOHEADER();

                //SAPService.Url = "http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SurveyorChangeRequest_Sync_Out_SI&interfaceNamespace=http://thaioilgroup.com/_i_purchaseorder/purchasing/surveyor_change/";
                //SAPService.Credentials = AuthServiceConnect("ZPI_IF_CIP", "1@zpi_if_cip");
                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);


                string EXPHEADER = "";
                //SAPService.SurveyorChangeRequest_Sync_Out_SI(
                POHEADER_res = SAPService.SurveyorChangeRequest_Sync_Out_SI(
                  null, /*I_NO_COMMIT,*/
                  null, /*MEMORY_COMPLETE,*/
                  null, /*MEMORY_UNCOMPLET*/
                  null, /*NO_AUTHORITY,*/
                  null, /*NO_MESSAGE_REQ,*/
                  null, /*NO_MESSAGING,*/
                  null, /*NO_PRICE_FROM_PO*/
                  null, /*PARK_COMPLETE,*/
                  null, /*PARK_UNCOMPLETE,*/
                  POADDRVENDOR,
                  POEXPIMPHEADER,
                  POEXPIMPHEADERX,
                  POHEADER,
                  POHEADERX,
                  PURCHASEORDER,
                  null,// TESTRUN,
                  VERSIONS,
                  ref ALLVERSIONS,
                  ref EXTENSIONIN,
                  ref EXTENSIONOUT,
                  ref INVPLANHEADER,
                  ref INVPLANHEADERX,
                  ref INVPLANITEM,
                  ref INVPLANITEMX,
                  ref POACCOUNT,
                  ref POACCOUNTPROFITSEGMENT,
                  ref POACCOUNTX,
                  ref POADDRDELIVERY,
                  ref POCOMPONENTS,
                  ref POCOMPONENTSX,
                  ref POCOND,
                  ref POCONDHEADER,
                  ref POCONDHEADERX,
                  ref POCONDX,
                  ref POCONFIRMATION,
                  ref POCONTRACTLIMITS,
                  ref POEXPIMPITEM,
                  ref POEXPIMPITEMX,
                  ref POHISTORY,
                  ref POHISTORY_MA,
                  ref POHISTORY_TOTALS,
                  ref POITEM,
                  ref POITEMX,
                  ref POLIMITS,
                  ref POPARTNER,
                  ref POSCHEDULE,
                  ref POSCHEDULEX,
                  ref POSERVICES,
                  ref POSERVICESTEXT,
                  ref POSHIPPING,
                  ref POSHIPPINGEXP,
                  ref POSHIPPINGX,
                  ref POSRVACCESSVALUES,
                  ref POTEXTHEADER,
                  ref POTEXTITEM,
                  ref returnValue,
                  ref SERIALNUMBER,
                  ref SERIALNUMBERX,
                  out EXPPOEXPIMPHEADER
                  );

                string pono = "";
                if (POHEADER_res != null)
                    pono = POHEADER_res.PO_NUMBER;

                string logon = "S";
                if (returnValue != null)
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon = "E";
                        }
                    }
                }

                //if (returnValue[0].MESSAGE_V2 == POHEADER_res.PO_NUMBER)
                //{


                //}
                //else
                //{
                //    int rcount = returnValue.Length;
                //    for (int i = 0; i < rcount; i++)
                //    {
                //        if (returnValue[i].TYPE == "S")
                //        {
                //            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                //            logon = "S";
                //        }
                //    }
                //}

                return "|" + logon + "M" + msg + "R" + pono;
                //E|xxxxxx|msg
                //return res;
               
            }
            catch (Exception ex)
                 {
                throw ex;
                 }
            }
        
        #endregion



        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;

            return NC;
        }
    }
}
