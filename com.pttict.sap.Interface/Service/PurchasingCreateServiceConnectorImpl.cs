﻿using com.pttict.downstream.common.utilities;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common;

namespace com.pttict.sap.Interface.Service
{
    public class PurchasingCreateServiceConnectorImpl : BasicBean , DownstreamConnector<string>
    {
        public DownstreamResponse<string> connect(string config, string content)
        {
            //Debug.WriteLine("#################### PurchasingService ####################");
            //log.Info("#################### Start PurchasingService ####################");
            DownstreamResponse<string> downResp = new DownstreamResponse<string>();
            downResp.setNameSpace(ServiceConstant.NAMESPACE);
            downResp.setResultCode(ServiceConstant.RESP_SYETEM_ERROR[0]);
            downResp.setResultDesc(ServiceConstant.RESP_SYETEM_ERROR[1]);

            PurchasingService service = new PurchasingService();

            string msg = "";
            string res = service.Create(config, content, ref msg);

            if (res != null && res != "")
            {
                downResp.setResultCode(ServiceConstant.RESP_SUCCESS[0]);
                downResp.setResultDesc(ServiceConstant.RESP_SUCCESS[1]);
                downResp.setResponseData(res);
            }
            else
            {
                downResp.setResultCode(ServiceConstant.RESP_SAP_ERROR[0]);
                downResp.setResultDesc(msg);                
                downResp.setResponseData(res);
            }

            //log.Info("#################### End MailService ####################");
            return downResp;
        }
    }
}
