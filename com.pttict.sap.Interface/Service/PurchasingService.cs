﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace com.pttict.sap.Interface.Service
{
    public class PurchasingService : BasicBean
    {
        public string Create(string JsonConfig, string JsonContent, ref string msg)
        {

            sap.po.create.PurchasingCreateRequest_Sync_Out_SIService SAPService = new sap.po.create.PurchasingCreateRequest_Sync_Out_SIService();

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonContent = javaScriptSerializer.Deserialize<PurchasingCreateModel>(JsonContent);

            sap.po.create.ZPOHEADER ZPOHEADER = jsonContent.ZPOHEADER;

            sap.po.create.ZPOHEADERX ZPOHEADERX = jsonContent.ZPOHEADERX;
            //sap.po.create.BAPIRET2[] RETURN = jsonContent.RETURN.ToArray();
            sap.po.create.BAPIRET2[] RETURN = new sap.po.create.BAPIRET2[1];
            sap.po.create.ZPOACCOUNT[] ZPOACCOUNT = jsonContent.ZPOACCOUNT.ToArray();
            sap.po.create.ZPOACCOUNTX[] ZPOACCOUNTX = jsonContent.ZPOACCOUNTX.ToArray();
            sap.po.create.ZPOITEM[] ZPOITEM = jsonContent.ZPOITEM.ToArray();
            sap.po.create.ZPOITEMX[] ZPOITEMX = jsonContent.ZPOITEMX.ToArray();

            //var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
            SAPService.Url = jsonConfig.sap_url;
            SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass); 

            string result = SAPService.PurchasingCreateRequest_Sync_Out_SI(ZPOHEADER, ZPOHEADERX, ref RETURN, ref ZPOACCOUNT, ref ZPOACCOUNTX, ref ZPOITEM, ref ZPOITEMX);
            if (string.IsNullOrEmpty( result ))
            {
                int rcount = RETURN.Length;
                for (int i = 0; i < rcount; i++)
                {
                    if (RETURN[i].TYPE == "E")
                        msg += RETURN[i].MESSAGE + " " + RETURN[i].MESSAGE_V1 + " " + RETURN[i].MESSAGE_V2 + " " + RETURN[i].MESSAGE_V3 + " " + RETURN[i].MESSAGE_V4;
                }
            }

            return result;
        }

        public String Change(string JsonConfig, string JsonContent, ref string msg)
        {
            try
            {

                string res = "S";

                sap.po.change.PurchasingChangeRequest_Sync_Out_SIService SAPService = new sap.po.change.PurchasingChangeRequest_Sync_Out_SIService();

                //PurchasingChangeWebService.PurchasingChangeRequest_Sync_Out_SIService ChangeService = new PurchasingChangeWebService.PurchasingChangeRequest_Sync_Out_SIService();

                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<PurchasingChangeModel>(JsonContent);

                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

                string s = SAPService.Url;

                SAPService.Url = jsonConfig.sap_url;
                // Conecting web service
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);


                sap.po.change.BAPIMEPOHEADER POHEADER = jsonContent.POHEADER;
                sap.po.change.BAPIMEPOHEADERX POHEADERX = jsonContent.POHEADERX;
                string PURCHASEORDER = jsonContent.PURCHASEORDER;
                sap.po.change.BAPIMEPOITEM[] POITEM = jsonContent.POITEM.ToArray();
                sap.po.change.BAPIMEPOITEMX[] POITEMX = jsonContent.POITEMX.ToArray();
                sap.po.change.BAPIRET2[] RETURN = jsonContent.RETURN.ToArray();
                string MEMORY_COMPLETE = jsonContent.MEMORY_COMPLETE;
                string MEMORY_UNCOMPLETE = jsonContent.MEMORY_UNCOMPLETE;
                string NO_AUTHORITY = jsonContent.NO_AUTHORITY;
                string NO_MESSAGE_REQ = jsonContent.NO_MESSAGE_REQ;
                string NO_MESSAGING = jsonContent.NO_MESSAGING;
                string NO_PRICE_FROM_PO = jsonContent.NO_PRICE_FROM_PO;
                string PARK_COMPLETE = jsonContent.PARK_COMPLETE;
                string PARK_UNCOMPLETE = jsonContent.PARK_UNCOMPLETE;
                sap.po.change.BAPIMEPOADDRVENDOR POADDRVENDOR = jsonContent.POADDRVENDOR;
                sap.po.change.BAPIEIKP POEXPIMPHEADER = jsonContent.POEXPIMPHEADER;
                sap.po.change.BAPIEIKPX POEXPIMPHEADERX = jsonContent.POEXPIMPHEADERX;
                string TESTRUN = jsonContent.TESTRUN;
                sap.po.change.BAPIMEDCM VERSIONS = jsonContent.VERSIONS;
                sap.po.change.BAPIMEDCM_ALLVERSIONS[] ALLVERSIONS = jsonContent.ALLVERSIONS.ToArray();
                sap.po.change.BAPIPAREX[] EXTENSIONIN = jsonContent.EXTENSIONIN.ToArray();
                sap.po.change.BAPIPAREX[] EXTENSIONOUT = jsonContent.EXTENSIONOUT.ToArray();
                sap.po.change.BAPI_INVOICE_PLAN_HEADER[] INVPLANHEADER = jsonContent.INVPLANHEADER.ToArray();
                sap.po.change.BAPI_INVOICE_PLAN_HEADERX[] INVPLANHEADERX = jsonContent.INVPLANHEADERX.ToArray();
                sap.po.change.BAPI_INVOICE_PLAN_ITEM[] INVPLANITEM = jsonContent.INVPLANITEM.ToArray();
                sap.po.change.BAPI_INVOICE_PLAN_ITEMX[] INVPLANITEMX = jsonContent.INVPLANITEMX.ToArray();
                sap.po.change.BAPIMEPOACCOUNT[] POACCOUNT = jsonContent.POACCOUNT.ToArray();
                sap.po.change.BAPIMEPOACCOUNTPROFITSEGMENT[] POACCOUNTPROFITSEGMENT = jsonContent.POACCOUNTPROFITSEGMENT.ToArray();
                sap.po.change.BAPIMEPOACCOUNTX[] POACCOUNTX = jsonContent.POACCOUNTX.ToArray();
                sap.po.change.BAPIMEPOADDRDELIVERY[] POADDRDELIVERY = jsonContent.POADDRDELIVERY.ToArray();
                sap.po.change.BAPIMEPOCOMPONENT[] POCOMPONENTS = jsonContent.POCOMPONENTS.ToArray();
                sap.po.change.BAPIMEPOCOMPONENTX[] POCOMPONENTSX = jsonContent.POCOMPONENTSX.ToArray();
                sap.po.change.BAPIMEPOCOND[] POCOND = jsonContent.POCOND.ToArray();
                sap.po.change.BAPIMEPOCONDHEADER[] POCONDHEADER = jsonContent.POCONDHEADER.ToArray();
                sap.po.change.BAPIMEPOCONDHEADERX[] POCONDHEADERX = jsonContent.POCONDHEADERX.ToArray();
                sap.po.change.BAPIMEPOCONDX[] POCONDX = jsonContent.POCONDX.ToArray();
                sap.po.change.BAPIEKES[] POCONFIRMATION = jsonContent.POCONFIRMATION.ToArray();
                sap.po.change.BAPIESUCC[] POCONTRACTLIMITS = jsonContent.POCONTRACTLIMITS.ToArray();
                sap.po.change.BAPIEIPO[] POEXPIMPITEM = jsonContent.POEXPIMPITEM.ToArray();
                sap.po.change.BAPIEIPOX[] POEXPIMPITEMX = jsonContent.POEXPIMPITEMX.ToArray();
                sap.po.change.BAPIEKBE[] POHISTORY = jsonContent.POHISTORY.ToArray();
                sap.po.change.BAPIEKBE_MA[] POHISTORY_MA = jsonContent.POHISTORY_MA.ToArray();
                sap.po.change.BAPIEKBES[] POHISTORY_TOTALS = jsonContent.POHISTORY_TOTALS.ToArray();
                sap.po.change.BAPIESUHC[] POLIMITS = jsonContent.POLIMITS.ToArray();
                sap.po.change.BAPIEKKOP[] POPARTNER = jsonContent.POPARTNER.ToArray();
                sap.po.change.BAPIMEPOSCHEDULE[] POSCHEDULE = jsonContent.POSCHEDULE.ToArray();
                sap.po.change.BAPIMEPOSCHEDULX[] POSCHEDULEX = jsonContent.POSCHEDULEX.ToArray();
                sap.po.change.BAPIESLLC[] POSERVICES = jsonContent.POSERVICES.ToArray();
                sap.po.change.BAPIESLLTX[] POSERVICESTEXT = jsonContent.POSERVICESTEXT.ToArray();
                sap.po.change.BAPIITEMSHIP[] POSHIPPING = jsonContent.POSHIPPING.ToArray();
                sap.po.change.BAPIMEPOSHIPPEXP[] POSHIPPINGEXP = jsonContent.POSHIPPINGEXP.ToArray();
                sap.po.change.BAPIITEMSHIPX[] POSHIPPINGX = jsonContent.POSHIPPINGX.ToArray();
                sap.po.change.BAPIESKLC[] POSRVACCESSVALUES = jsonContent.POSRVACCESSVALUES.ToArray();
                sap.po.change.BAPIMEPOTEXTHEADER[] POTEXTHEADER = jsonContent.POTEXTHEADER.ToArray();
                sap.po.change.BAPIMEPOTEXT[] POTEXTITEM = jsonContent.POTEXTITEM.ToArray();
                sap.po.change.BAPIMEPOSERIALNO[] SERIALNUMBER = jsonContent.SERIALNUMBER.ToArray();
                sap.po.change.BAPIMEPOSERIALNOX[] SERIALNUMBERX = jsonContent.SERIALNUMBERX.ToArray();
                sap.po.change.BAPIEIKP EXPPOEXPIMPHEADER = jsonContent.EXPPOEXPIMPHEADER;

                sap.po.change.BAPIMEPOHEADER POHEADER_Res = new sap.po.change.BAPIMEPOHEADER();


                POHEADER_Res = SAPService.PurchasingChangeRequest_Sync_Out_SI(
                    MEMORY_COMPLETE,
                    MEMORY_UNCOMPLETE,
                    NO_AUTHORITY,
                    NO_MESSAGE_REQ,
                    NO_MESSAGING,
                    NO_PRICE_FROM_PO,
                    PARK_COMPLETE,
                    PARK_UNCOMPLETE,
                    POADDRVENDOR,
                    POEXPIMPHEADER,
                    POEXPIMPHEADERX,
                    POHEADER,
                    POHEADERX,
                    PURCHASEORDER,
                    TESTRUN,
                    VERSIONS,
                    ref ALLVERSIONS,
                    ref EXTENSIONIN,
                    ref EXTENSIONOUT,
                    ref INVPLANHEADER,
                    ref INVPLANHEADERX,
                    ref INVPLANITEM,
                    ref INVPLANITEMX,
                    ref POACCOUNT,
                    ref POACCOUNTPROFITSEGMENT,
                    ref POACCOUNTX,
                    ref POADDRDELIVERY,
                    ref POCOMPONENTS,
                    ref POCOMPONENTSX,
                    ref POCOND,
                    ref POCONDHEADER,
                    ref POCONDHEADERX,
                    ref POCONDX,
                    ref POCONFIRMATION,
                    ref POCONTRACTLIMITS,
                    ref POEXPIMPITEM,
                    ref POEXPIMPITEMX,
                    ref POHISTORY,
                    ref POHISTORY_MA,
                    ref POHISTORY_TOTALS,
                    ref POITEM,
                    ref POITEMX,
                    ref POLIMITS,
                    ref POPARTNER,
                    ref POSCHEDULE,
                    ref POSCHEDULEX,
                    ref POSERVICES,
                    ref POSERVICESTEXT,
                    ref POSHIPPING,
                    ref POSHIPPINGEXP,
                    ref POSHIPPINGX,
                    ref POSRVACCESSVALUES,
                    ref POTEXTHEADER,
                    ref POTEXTITEM,
                    ref RETURN,
                    ref SERIALNUMBER,
                    ref SERIALNUMBERX,
                    out EXPPOEXPIMPHEADER);

                int rcount = RETURN.Length;
                for (int i = 0; i < rcount; i++)
                {
                    if (RETURN[i].TYPE == "E")
                    {
                        res = "E";
                        msg += RETURN[i].MESSAGE + " " + RETURN[i].MESSAGE_V1 + " " + RETURN[i].MESSAGE_V2 + " " + RETURN[i].MESSAGE_V3 + " " + RETURN[i].MESSAGE_V4;
                    }
                        
                }

                return res;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }

        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;

            return NC;
        }

    }
}
