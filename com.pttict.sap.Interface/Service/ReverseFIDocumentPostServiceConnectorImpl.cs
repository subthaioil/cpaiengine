﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.sap.reversefidocument.post;

namespace com.pttict.sap.Interface.Service
{
    public class ReverseFIDocumentPostServiceConnectorImpl : BasicBean
    {
        public void connect(string config, string year, string company, string document)
        {
            try
            {
                ReverseFIDocumentService service = new ReverseFIDocumentService();
                service.Connect(config, year, company, document);
            }
            catch(Exception ex)
            {

            }
        }
    }
}
