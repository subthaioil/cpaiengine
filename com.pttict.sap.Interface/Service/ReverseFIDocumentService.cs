﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;

namespace com.pttict.sap.Interface.Service
{
    public class ReverseFIDocumentService : BasicBean
    {
        public ReverseFIDocumentSendResult Connect(string config, string year, string company, string document)
        {
            ReverseFIDocumentSendResult returnResult = new ReverseFIDocumentSendResult();
            try
            {
                string result = "";
                sap.reversefidocument.post.FIDocumentReverseRequest_Sync_Out_SIService SAPService = new sap.reversefidocument.post.FIDocumentReverseRequest_Sync_Out_SIService();
                SAPService.Credentials = AuthServiceConnect(config);

                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jConfig = javaScriptSerializer.Deserialize<ConfigModel>(config);
                SAPService.Url = jConfig.sap_url;

                string returnDocumentNo = SAPService.FIDocumentReverseRequest_Sync_Out_SI(document, company, year, out  result);
                if(returnDocumentNo == document)
                {
                    returnResult.Pass = false;
                }
                else
                {
                    returnResult.Pass = true;
                }
                returnResult.ReturnDocumentNo = returnDocumentNo;
                returnResult.ResultText = result;

                return returnResult;
            }
            catch(Exception ex)
            {
                returnResult.Pass = false;
                returnResult.ReturnDocumentNo = "";
                returnResult.ResultText = "Error - " + ex.Message;
                return returnResult;
            }
        }

        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
    }

    public class ReverseFIDocumentSendResult
    {
        public bool Pass { get; set; }
        public string ReturnDocumentNo { get; set; }
        public string ResultText { get; set; }
    }
}
