﻿using com.pttict.downstream.common.utilities;
using com.pttict.downstream.common.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common;

namespace com.pttict.sap.Interface.Service
{
    public class SaleOrderCreateServiceConnectorImpl : BasicBean, DownstreamConnector<string>
    {
        public DownstreamResponse<string> connect(string Jsonconfig, string content)
        {

            DownstreamResponse<string> downResp = new DownstreamResponse<string>();
            downResp.setNameSpace(ServiceConstant.NAMESPACE);
            downResp.setResultCode(ServiceConstant.RESP_SYETEM_ERROR[0]);
            downResp.setResultDesc(ServiceConstant.RESP_SYETEM_ERROR[1]);

            SaleOrderService service = new SaleOrderService();
            string msg = "";
            string res = service.Create(Jsonconfig, content, ref msg);


            if (!(res ?? "").Contains("E"))
            {
                downResp.setResultCode(ServiceConstant.RESP_SUCCESS[0]);
                downResp.setResultDesc(ServiceConstant.RESP_SUCCESS[1]);
                downResp.setResponseData(res);
            }
            else
            {
                downResp.setResultCode(ServiceConstant.RESP_SAP_ERROR[0]);
                downResp.setResultDesc(msg);
                downResp.setResponseData(res);
            }

            return downResp;
        }

    }

}
