﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using static com.pttict.sap.Interface.Model.SaleOrderModel;

namespace com.pttict.sap.Interface.Service
{
    public class SaleOrderService : BasicBean
    {
        public string Create(string JsonConfig, string content, ref string msg)
        {
            try
            {
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<SaleOrderModel>(content);

                sap.saleorder.create.SalesOrderCreateRequest_Sync_Out_SIService SAPService = new sap.saleorder.create.SalesOrderCreateRequest_Sync_Out_SIService();
                sap.saleorder.create.BAPISDHD1 ORDER_HEADER_IN = new sap.saleorder.create.BAPISDHD1();
                ORDER_HEADER_IN.DOC_TYPE = jsonContent.mORDER_HEADER_IN[0].DOC_TYPE; //"Z1I6";
                ORDER_HEADER_IN.SALES_ORG = jsonContent.mORDER_HEADER_IN[0].SALES_ORG;// "1100";
                ORDER_HEADER_IN.DISTR_CHAN = jsonContent.mORDER_HEADER_IN[0].DISTR_CHAN;// "32";
                ORDER_HEADER_IN.DIVISION = jsonContent.mORDER_HEADER_IN[0].DIVISION;//"00";
                ORDER_HEADER_IN.REQ_DATE_H = jsonContent.mORDER_HEADER_IN[0].REQ_DATE_H;//"2017-06-15";
                ORDER_HEADER_IN.DATE_TYPE = jsonContent.mORDER_HEADER_IN[0].DATE_TYPE;//"1";
                ORDER_HEADER_IN.PURCH_NO_C = jsonContent.mORDER_HEADER_IN[0].PURCH_NO_C;// "S660/S55";
                ORDER_HEADER_IN.PURCH_NO_S = jsonContent.mORDER_HEADER_IN[0].PURCH_NO_S;//"S660/S55";
                ORDER_HEADER_IN.CURRENCY = jsonContent.mORDER_HEADER_IN[0].CURRENCY;//"THB";
                if (ORDER_HEADER_IN.DOC_TYPE == "Z1P3")
                {
                    ORDER_HEADER_IN.DLVSCHDUSE = "CL";
                }

                sap.saleorder.create.BAPISDHD1X ORDER_HEADER_INX = new sap.saleorder.create.BAPISDHD1X();
                ORDER_HEADER_INX.DOC_TYPE = "X";
                ORDER_HEADER_INX.SALES_ORG = "X";
                ORDER_HEADER_INX.DISTR_CHAN = "X";
                ORDER_HEADER_INX.DIVISION = "X";
                ORDER_HEADER_INX.REQ_DATE_H = "X";
                ORDER_HEADER_INX.DATE_TYPE = "X";
                ORDER_HEADER_INX.PURCH_DATE = "X";
                ORDER_HEADER_INX.PMNTTRMS = "X";
                ORDER_HEADER_INX.PRICE_DATE = "X";
                ORDER_HEADER_INX.QT_VALID_F = "X";
                ORDER_HEADER_INX.QT_VALID_T = "X";
                ORDER_HEADER_INX.CT_VALID_F = "X";
                ORDER_HEADER_INX.CT_VALID_T = "X";
                ORDER_HEADER_INX.PURCH_NO_C = "X";
                ORDER_HEADER_INX.PURCH_NO_S = "X";
                ORDER_HEADER_INX.REF_1_S = "X";
                ORDER_HEADER_INX.DOC_DATE = "X";
                ORDER_HEADER_INX.WAR_DATE = "X";
                ORDER_HEADER_INX.DUN_DATE = "X";
                ORDER_HEADER_INX.BILL_DATE = "X";
                ORDER_HEADER_INX.SERV_DATE = "X";
                ORDER_HEADER_INX.CURRENCY = "X";
                if (ORDER_HEADER_IN.DOC_TYPE == "Z1P3")
                {
                    ORDER_HEADER_INX.DLVSCHDUSE = "X";
                }

                int index1 = 0;
                long indexmode1 = 0;
                sap.saleorder.create.BAPICOND[] ORDER_CONDITIONS_IN;
                sap.saleorder.create.BAPICONDX[] ORDER_CONDITIONS_INX;
                //SWAP=Z1I6, Throughput=Z1V1, Clear Line=Z1P3, Borrow TOP=Z1I4, Borrow TLB=Z3I4
                if (ORDER_HEADER_IN.DOC_TYPE == "Z1I6" || ORDER_HEADER_IN.DOC_TYPE == "Z1V1")
                {
                    index1 = 0;
                    indexmode1 = 10;
                    ORDER_CONDITIONS_IN = new sap.saleorder.create.BAPICOND[jsonContent.mORDER_CONDITIONS_IN.Count];
                    ORDER_CONDITIONS_INX = new sap.saleorder.create.BAPICONDX[jsonContent.mORDER_CONDITIONS_IN.Count];
                    foreach (BAPICOND jsORDER_CONDITIONS_IN in jsonContent.mORDER_CONDITIONS_IN)
                    {

                        ORDER_CONDITIONS_IN[index1] = new sap.saleorder.create.BAPICOND();
                        ORDER_CONDITIONS_IN[index1].ITM_NUMBER = (indexmode1).ToString("000000"); //"000010";
                        ORDER_CONDITIONS_IN[index1].COND_TYPE = jsORDER_CONDITIONS_IN.COND_TYPE;// "ZROE";
                        ORDER_CONDITIONS_IN[index1].COND_VALUE = jsORDER_CONDITIONS_IN.COND_VALUE;//33.3M;
                        ORDER_CONDITIONS_IN[index1].COND_VALUESpecified = true;
                        ORDER_CONDITIONS_IN[index1].CURRENCY = jsORDER_CONDITIONS_IN.CURRENCY;//"TH5";
                        ORDER_CONDITIONS_IN[index1].COND_P_UNT = jsORDER_CONDITIONS_IN.COND_P_UNT;//0;
                        ORDER_CONDITIONS_IN[index1].COND_P_UNTSpecified = true;

                        ORDER_CONDITIONS_INX[index1] = new sap.saleorder.create.BAPICONDX();
                        ORDER_CONDITIONS_INX[index1].ITM_NUMBER = (indexmode1).ToString("000000"); //"000010";
                        ORDER_CONDITIONS_INX[index1].COND_TYPE = "X";
                        ORDER_CONDITIONS_INX[index1].UPDATEFLAG = "I";
                        ORDER_CONDITIONS_INX[index1].COND_VALUE = "X";
                        ORDER_CONDITIONS_INX[index1].CURRENCY = "X";
                        ORDER_CONDITIONS_INX[index1].COND_P_UNT = "X";

                        index1 += 1;
                        indexmode1 += 10;
                    }
                }
                else
                {
                    ORDER_CONDITIONS_IN = null;
                    ORDER_CONDITIONS_INX = null;
                }


                sap.saleorder.create.BAPISDCOND[] ORDER_CONDITIONS_OUT = new sap.saleorder.create.BAPISDCOND[0];
                sap.saleorder.create.BAPICONDHD[] ORDER_COND_HEAD = new sap.saleorder.create.BAPICONDHD[0];
                sap.saleorder.create.BAPICONDIT[] ORDER_COND_ITEM = new sap.saleorder.create.BAPICONDIT[0];
                sap.saleorder.create.BAPISDHD[] ORDER_HEADERS_OUT = new sap.saleorder.create.BAPISDHD[0];

                index1 = 0;
                indexmode1 = 10;
                sap.saleorder.create.BAPISDITM[] ORDER_ITEMS_IN = new sap.saleorder.create.BAPISDITM[jsonContent.mORDER_ITEMS_IN.Count];
                sap.saleorder.create.BAPISDITMX[] ORDER_ITEMS_INX = new sap.saleorder.create.BAPISDITMX[jsonContent.mORDER_ITEMS_IN.Count];
                foreach (BAPISDITM jsORDER_ITEMS_IN in jsonContent.mORDER_ITEMS_IN)
                {

                    ORDER_ITEMS_IN[index1] = new sap.saleorder.create.BAPISDITM();
                    ORDER_ITEMS_IN[index1].ITM_NUMBER = (indexmode1).ToString("000000"); //"000010";
                    ORDER_ITEMS_IN[index1].MATERIAL = jsORDER_ITEMS_IN.MATERIAL;// "YFLR";
                    ORDER_ITEMS_IN[index1].PLANT = jsORDER_ITEMS_IN.PLANT;//"1200";
                    ORDER_ITEMS_IN[index1].ITEM_CATEG = jsORDER_ITEMS_IN.ITEM_CATEG;//"ZTW1";
                    ORDER_ITEMS_IN[index1].SALES_UNIT = jsORDER_ITEMS_IN.SALES_UNIT;// "MT";


                    ORDER_ITEMS_INX[index1] = new sap.saleorder.create.BAPISDITMX();
                    ORDER_ITEMS_INX[index1].ITM_NUMBER = (indexmode1).ToString("000000"); //"000010";
                    ORDER_ITEMS_INX[index1].MATERIAL = "X";
                    ORDER_ITEMS_INX[index1].BILL_DATE = "X";
                    ORDER_ITEMS_INX[index1].PLANT = "X";
                    ORDER_ITEMS_INX[index1].ITEM_CATEG = "X";
                    ORDER_ITEMS_INX[index1].PURCH_DATE = "X";
                    ORDER_ITEMS_INX[index1].PO_DAT_S = "X";
                    ORDER_ITEMS_INX[index1].FIX_VAL_DY = "X";
                    ORDER_ITEMS_INX[index1].PRICE_DATE = "X";
                    ORDER_ITEMS_INX[index1].SERV_DATE = "X";
                    ORDER_ITEMS_INX[index1].SALES_UNIT = "X";

                    index1 += 1;
                    indexmode1 += 10;
                }


                sap.saleorder.create.BAPISDIT[] ORDER_ITEMS_OUT = new sap.saleorder.create.BAPISDIT[0];
                sap.saleorder.create.ZBAPISDITM_ISOIL[] ORDER_ITM_ISOIL_EXT_IN = new sap.saleorder.create.ZBAPISDITM_ISOIL[0];
                sap.saleorder.create.ZBAPISDITM_ISOIL[] ORDER_ITM_ISOIL_EXT_OUT = new sap.saleorder.create.ZBAPISDITM_ISOIL[0];


                index1 = 0;
                sap.saleorder.create.BAPIPARNR[] ORDER_PARTNERS = new sap.saleorder.create.BAPIPARNR[jsonContent.mORDER_PARTNERS.Count];
                foreach (BAPIPARNR jsORDER_PARTNERS in jsonContent.mORDER_PARTNERS)
                {
                    ORDER_PARTNERS[index1] = new sap.saleorder.create.BAPIPARNR();
                    ORDER_PARTNERS[index1].PARTN_ROLE = jsORDER_PARTNERS.PARTN_ROLE_SP;//Global Config
                    ORDER_PARTNERS[index1].PARTN_ROLE = jsORDER_PARTNERS.PARTN_ROLE_BP;//Global Config
                    ORDER_PARTNERS[index1].PARTN_ROLE = jsORDER_PARTNERS.PARTN_ROLE_PY;//Global Config
                    ORDER_PARTNERS[index1].PARTN_ROLE = jsORDER_PARTNERS.PARTN_ROLE_SH;//Global Config
                    ORDER_PARTNERS[index1].PARTN_NUMB = jsORDER_PARTNERS.PARTN_NUMB; //"0000400004";
                    ORDER_PARTNERS[index1].ITM_NUMBER = jsORDER_PARTNERS.ITM_NUMBER; //"000000";//Global Config
                    index1 += 1;
                }

                sap.saleorder.create.BAPISDPART[] ORDER_PARTNERS_OUT = new sap.saleorder.create.BAPISDPART[0];


                indexmode1 = 10;
                index1 = 0;
                sap.saleorder.create.BAPISCHDL[] ORDER_SCHEDULES_IN = new sap.saleorder.create.BAPISCHDL[jsonContent.mORDER_SCHEDULES_IN.Count];
                sap.saleorder.create.BAPISCHDLX[] ORDER_SCHEDULES_INX = new sap.saleorder.create.BAPISCHDLX[jsonContent.mORDER_SCHEDULES_IN.Count];
                foreach (BAPISCHDL jsORDER_SCHEDULES_IN in jsonContent.mORDER_SCHEDULES_IN)
                {

                    ORDER_SCHEDULES_IN[index1] = new sap.saleorder.create.BAPISCHDL();
                    ORDER_SCHEDULES_IN[index1].ITM_NUMBER = (indexmode1).ToString("000000"); //"000010";
                    ORDER_SCHEDULES_IN[index1].REQ_DATE = jsORDER_SCHEDULES_IN.REQ_DATE;//"2017-06-15";
                    ORDER_SCHEDULES_IN[index1].DATE_TYPE = jsORDER_SCHEDULES_IN.DATE_TYPE;// "1";
                    ORDER_SCHEDULES_IN[index1].REQ_QTY = jsORDER_SCHEDULES_IN.REQ_QTY; //36727.615M;
                    ORDER_SCHEDULES_IN[index1].REQ_QTYSpecified = true;

                    ORDER_SCHEDULES_INX[index1] = new sap.saleorder.create.BAPISCHDLX();
                    ORDER_SCHEDULES_INX[index1].ITM_NUMBER = (indexmode1).ToString("000000"); //"000010";
                    ORDER_SCHEDULES_INX[index1].REQ_DATE = "X";
                    ORDER_SCHEDULES_INX[index1].DATE_TYPE = "X";
                    ORDER_SCHEDULES_INX[index1].REQ_QTY = "X";
                    ORDER_SCHEDULES_INX[index1].TP_DATE = "X";
                    ORDER_SCHEDULES_INX[index1].MS_DATE = "X";
                    ORDER_SCHEDULES_INX[index1].LOAD_DATE = "X";
                    ORDER_SCHEDULES_INX[index1].GI_DATE = "X";
                    ORDER_SCHEDULES_INX[index1].DLV_DATE = "X";
                    indexmode1 += 10;
                    index1 += 1;
                }


                sap.saleorder.create.BAPISDHEDU[] ORDER_SCHEDULES_OUT = new sap.saleorder.create.BAPISDHEDU[0];
                sap.saleorder.create.BAPISDHDST[] ORDER_STATUSHEADERS_OUT = new sap.saleorder.create.BAPISDHDST[0];
                sap.saleorder.create.BAPISDITST[] ORDER_STATUSITEMS_OUT = new sap.saleorder.create.BAPISDITST[0];
                sap.saleorder.create.BAPISDTEXT[] ORDER_TEXT = new sap.saleorder.create.BAPISDTEXT[0];
                sap.saleorder.create.BAPISDTEHD[] ORDER_TEXTHEADERS_OUT = new sap.saleorder.create.BAPISDTEHD[0];
                sap.saleorder.create.BAPITEXTLI[] ORDER_TEXTLINES_OUT = new sap.saleorder.create.BAPITEXTLI[0];
                sap.saleorder.create.BAPIRET2[] returnValue = new sap.saleorder.create.BAPIRET2[0];

                //SAPService.Url = "http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SalesOrderCreateRequest_Sync_Out_SI&interfaceNamespace=http://thaioilgroup.com/_i_salesorder/sales/salesorder_create/";              
                //SAPService.Credentials = AuthServiceConnect("ZPI_IF_CIP", "1@zpi_if_cip");
                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);

                string I_TESTRUN = "";

                //SAPService.SalesOrderCreateRequest_Sync_Out_SI(
                string result = SAPService.SalesOrderCreateRequest_Sync_Out_SI(
                    I_TESTRUN,
                    ORDER_HEADER_IN,
                    ORDER_HEADER_INX,
                   ref ORDER_CONDITIONS_IN,
                   ref ORDER_CONDITIONS_INX,
                   ref ORDER_CONDITIONS_OUT,
                   ref ORDER_COND_HEAD,
                   ref ORDER_COND_ITEM,
                   ref ORDER_HEADERS_OUT,
                   ref ORDER_ITEMS_IN,
                   ref ORDER_ITEMS_INX,
                   ref ORDER_ITEMS_OUT,
                   ref ORDER_ITM_ISOIL_EXT_IN,
                   ref ORDER_ITM_ISOIL_EXT_OUT,
                   ref ORDER_PARTNERS,
                   ref ORDER_PARTNERS_OUT,
                   ref ORDER_SCHEDULES_IN,
                   ref ORDER_SCHEDULES_INX,
                   ref ORDER_SCHEDULES_OUT,
                   ref ORDER_STATUSHEADERS_OUT,
                   ref ORDER_STATUSITEMS_OUT,
                   ref ORDER_TEXT,
                   ref ORDER_TEXTHEADERS_OUT,
                   ref ORDER_TEXTLINES_OUT,
                   ref returnValue
                    );

                msg = "";
                string logon = "";
                if (string.IsNullOrEmpty(result))
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "E";
                        }
                        else if (returnValue[i].TYPE == "S")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "S";
                        }
                    }

                }
                else
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "E";
                        }
                        else if (returnValue[i].TYPE == "S")
                        {
                            if (!string.IsNullOrEmpty(returnValue[i].MESSAGE_V2) && !string.IsNullOrEmpty(returnValue[i].MESSAGE_V3)
                                && returnValue[i].MESSAGE.IndexOf("delivery") > 0)
                            {
                                result = returnValue[i].MESSAGE_V2 + "|" + returnValue[i].MESSAGE_V3;
                            }
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "S";
                        }
                    }
                }

                //return res;
                //return "|" + logon + "#" + msg + "$" + (result??"");
                return logon + "$" + (result ?? "");

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string Change(string JsonConfig, string content, ref string msg)
        {
            try
            {

                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<SaleOrderModel>(content);
                sap.saleorder.change.SalesOrderChangeRequest_Sync_Out_SIService SAPService = new sap.saleorder.change.SalesOrderChangeRequest_Sync_Out_SIService();

                string SALESDOCUMENT_IN = jsonContent.SALESDOCUMENT_IN;

                sap.saleorder.change.BAPISDH1 ORDER_HEADER_IN = new sap.saleorder.change.BAPISDH1();
                //SWAP=Z1I6, Throughput=Z1V1, Clear Line=Z1P3, Borrow TOP=Z1I4, Borrow TLB=Z3I4
                string DOC_TYPE = jsonContent.mORDER_HEADER_IN[0].DOC_TYPE;
                ORDER_HEADER_IN.REQ_DATE_H = jsonContent.mORDER_HEADER_IN[0].REQ_DATE_H;//"2017-06-15";

                sap.saleorder.change.BAPISDH1X ORDER_HEADER_INX = new sap.saleorder.change.BAPISDH1X();
                ORDER_HEADER_INX.UPDATEFLAG = "U";
                ORDER_HEADER_INX.REQ_DATE_H = "X";


                int index1 = 0;
                sap.saleorder.change.BAPICOND[] ORDER_CONDITIONS_IN;
                sap.saleorder.change.BAPICONDX[] ORDER_CONDITIONS_INX;
                //SWAP=Z1I6, Throughput=Z1V1, Clear Line=Z1P3, Borrow TOP=Z1I4, Borrow TLB=Z3I4
                if (DOC_TYPE == "Z1I6" || DOC_TYPE == "Z1V1")
                {
                    index1 = 0;
                    ORDER_CONDITIONS_IN = new sap.saleorder.change.BAPICOND[jsonContent.mORDER_CONDITIONS_IN.Count];
                    ORDER_CONDITIONS_INX = new sap.saleorder.change.BAPICONDX[jsonContent.mORDER_CONDITIONS_IN.Count];
                    foreach (BAPICOND jsORDER_CONDITIONS_IN in jsonContent.mORDER_CONDITIONS_IN)
                    {

                        ORDER_CONDITIONS_IN[index1] = new sap.saleorder.change.BAPICOND();
                        ORDER_CONDITIONS_IN[index1].ITM_NUMBER = jsORDER_CONDITIONS_IN.ITM_NUMBER; //"000010";
                        ORDER_CONDITIONS_IN[index1].COND_TYPE = jsORDER_CONDITIONS_IN.COND_TYPE;// "ZROE";
                        ORDER_CONDITIONS_IN[index1].COND_VALUE = jsORDER_CONDITIONS_IN.COND_VALUE;//33.3M;
                        ORDER_CONDITIONS_IN[index1].COND_VALUESpecified = true;
                        ORDER_CONDITIONS_IN[index1].CURRENCY = jsORDER_CONDITIONS_IN.CURRENCY;//"TH5";
                        ORDER_CONDITIONS_IN[index1].COND_P_UNT = jsORDER_CONDITIONS_IN.COND_P_UNT;//0;
                        ORDER_CONDITIONS_IN[index1].COND_P_UNTSpecified = true;

                        ORDER_CONDITIONS_INX[index1] = new sap.saleorder.change.BAPICONDX();
                        ORDER_CONDITIONS_INX[index1].ITM_NUMBER = jsORDER_CONDITIONS_IN.ITM_NUMBER; //"000010";
                        ORDER_CONDITIONS_INX[index1].COND_TYPE = "X";
                        ORDER_CONDITIONS_INX[index1].UPDATEFLAG = "I";
                        ORDER_CONDITIONS_INX[index1].COND_VALUE = "X";
                        ORDER_CONDITIONS_INX[index1].CURRENCY = "X";
                        ORDER_CONDITIONS_INX[index1].COND_P_UNT = "X";

                        index1 += 1;
                    }
                }
                else
                {
                    ORDER_CONDITIONS_IN = null;
                    ORDER_CONDITIONS_INX = null;
                }


                sap.saleorder.change.BAPISDCOND[] ORDER_CONDITIONS_OUT = new sap.saleorder.change.BAPISDCOND[0];
                sap.saleorder.change.BAPICONDHD[] ORDER_COND_HEAD = new sap.saleorder.change.BAPICONDHD[0];
                sap.saleorder.change.BAPICONDIT[] ORDER_COND_ITEM = new sap.saleorder.change.BAPICONDIT[0];
                sap.saleorder.change.BAPISDHD[] ORDER_HEADERS_OUT = new sap.saleorder.change.BAPISDHD[0];

                index1 = 0;
                sap.saleorder.change.BAPISDITM[] ORDER_ITEMS_IN = new sap.saleorder.change.BAPISDITM[jsonContent.mORDER_ITEMS_IN.Count];
                sap.saleorder.change.BAPISDITMX[] ORDER_ITEMS_INX = new sap.saleorder.change.BAPISDITMX[jsonContent.mORDER_ITEMS_IN.Count];
                foreach (BAPISDITM jsORDER_ITEMS_IN in jsonContent.mORDER_ITEMS_IN)
                {

                    ORDER_ITEMS_IN[index1] = new sap.saleorder.change.BAPISDITM();
                    ORDER_ITEMS_IN[index1].ITM_NUMBER = jsORDER_ITEMS_IN.ITM_NUMBER; //"000010";
                    ORDER_ITEMS_IN[index1].MATERIAL = jsORDER_ITEMS_IN.MATERIAL;// "YFLR";
                    ORDER_ITEMS_IN[index1].PLANT = jsORDER_ITEMS_IN.PLANT;//"1200";
                    ORDER_ITEMS_IN[index1].ITEM_CATEG = jsORDER_ITEMS_IN.ITEM_CATEG;//"ZTW1";
                    ORDER_ITEMS_IN[index1].PURCH_DATE = jsORDER_ITEMS_IN.PURCH_DATE;//2017-06-01
                    ORDER_ITEMS_IN[index1].SALES_UNIT = jsORDER_ITEMS_IN.SALES_UNIT;// "MT";


                    ORDER_ITEMS_INX[index1] = new sap.saleorder.change.BAPISDITMX();
                    ORDER_ITEMS_INX[index1].ITM_NUMBER = jsORDER_ITEMS_IN.ITM_NUMBER; //"000010";
                    ORDER_ITEMS_INX[index1].UPDATEFLAG = "U";
                    ORDER_ITEMS_INX[index1].MATERIAL = "X";
                    ORDER_ITEMS_INX[index1].REASON_REJ = "X";
                    ORDER_ITEMS_INX[index1].PLANT = "X";
                    ORDER_ITEMS_INX[index1].ITEM_CATEG = "X";
                    ORDER_ITEMS_INX[index1].SALES_UNIT = "X";

                    index1 += 1;

                }


                sap.saleorder.change.BAPISDIT[] ORDER_ITEMS_OUT = new sap.saleorder.change.BAPISDIT[0];
                sap.saleorder.change.ZBAPISDITM_ISOIL[] ORDER_ITM_ISOIL_EXT_IN = new sap.saleorder.change.ZBAPISDITM_ISOIL[0];
                sap.saleorder.change.ZBAPISDITM_ISOIL[] ORDER_ITM_ISOIL_EXT_OUT = new sap.saleorder.change.ZBAPISDITM_ISOIL[0];
                sap.saleorder.change.BAPISDPART[] ORDER_PARTNERS_OUT = new sap.saleorder.change.BAPISDPART[0];

                index1 = 0;
                sap.saleorder.change.BAPISCHDL[] ORDER_SCHEDULES_IN = new sap.saleorder.change.BAPISCHDL[jsonContent.mORDER_SCHEDULES_IN.Count];
                sap.saleorder.change.BAPISCHDLX[] ORDER_SCHEDULES_INX = new sap.saleorder.change.BAPISCHDLX[jsonContent.mORDER_SCHEDULES_IN.Count];
                foreach (BAPISCHDL jsORDER_SCHEDULES_IN in jsonContent.mORDER_SCHEDULES_IN)
                {

                    ORDER_SCHEDULES_IN[index1] = new sap.saleorder.change.BAPISCHDL();
                    ORDER_SCHEDULES_IN[index1].ITM_NUMBER = jsORDER_SCHEDULES_IN.ITM_NUMBER; //"000010";
                    ORDER_SCHEDULES_IN[index1].SCHED_LINE = jsORDER_SCHEDULES_IN.SCHED_LINE;//"0001";
                    ORDER_SCHEDULES_IN[index1].REQ_DATE = jsORDER_SCHEDULES_IN.REQ_DATE;// "2017-06-01";
                    ORDER_SCHEDULES_IN[index1].REQ_QTY = jsORDER_SCHEDULES_IN.REQ_QTY; //36727.615M;
                    ORDER_SCHEDULES_IN[index1].REQ_QTYSpecified = true;

                    ORDER_SCHEDULES_INX[index1] = new sap.saleorder.change.BAPISCHDLX();
                    ORDER_SCHEDULES_INX[index1].ITM_NUMBER = jsORDER_SCHEDULES_IN.ITM_NUMBER;//"000010";
                    ORDER_SCHEDULES_INX[index1].SCHED_LINE = jsORDER_SCHEDULES_IN.SCHED_LINE;
                    ORDER_SCHEDULES_INX[index1].UPDATEFLAG = "I";
                    ORDER_SCHEDULES_INX[index1].REQ_DATE = "X";
                    ORDER_SCHEDULES_INX[index1].REQ_QTY = "X";
                    index1 += 1;
                }


                sap.saleorder.change.BAPISDHEDU[] ORDER_SCHEDULES_OUT = new sap.saleorder.change.BAPISDHEDU[0];
                sap.saleorder.change.BAPISDHDST[] ORDER_STATUSHEADERS_OUT = new sap.saleorder.change.BAPISDHDST[0];
                sap.saleorder.change.BAPISDITST[] ORDER_STATUSITEMS_OUT = new sap.saleorder.change.BAPISDITST[0];
                sap.saleorder.change.BAPISDTEXT[] ORDER_TEXT = new sap.saleorder.change.BAPISDTEXT[0];
                sap.saleorder.change.BAPISDTEHD[] ORDER_TEXTHEADERS_OUT = new sap.saleorder.change.BAPISDTEHD[0];
                sap.saleorder.change.BAPITEXTLI[] ORDER_TEXTLINES_OUT = new sap.saleorder.change.BAPITEXTLI[0];
                sap.saleorder.change.BAPIPARNRC[] PARTNERCHANGES = new sap.saleorder.change.BAPIPARNRC[0];
                sap.saleorder.change.BAPIRET2[] returnValue = new sap.saleorder.change.BAPIRET2[0];

                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);

                string res = SAPService.SalesOrderChangeRequest_Sync_Out_SI(
                    ORDER_HEADER_IN,
                    ORDER_HEADER_INX,
                    SALESDOCUMENT_IN,
                   ref ORDER_CONDITIONS_IN,
                   ref ORDER_CONDITIONS_INX,
                   ref ORDER_CONDITIONS_OUT,
                   ref ORDER_COND_HEAD,
                   ref ORDER_COND_ITEM,
                   ref ORDER_HEADERS_OUT,
                   ref ORDER_ITEMS_IN,
                   ref ORDER_ITEMS_INX,
                   ref ORDER_ITEMS_OUT,
                   ref ORDER_ITM_ISOIL_EXT_IN,
                   ref ORDER_ITM_ISOIL_EXT_OUT,
                   ref ORDER_PARTNERS_OUT,
                   ref ORDER_SCHEDULES_IN,
                   ref ORDER_SCHEDULES_INX,
                   ref ORDER_SCHEDULES_OUT,
                   ref ORDER_STATUSHEADERS_OUT,
                   ref ORDER_STATUSITEMS_OUT,
                   ref ORDER_TEXT,
                   ref ORDER_TEXTHEADERS_OUT,
                   ref ORDER_TEXTLINES_OUT,
                   ref PARTNERCHANGES,
                   ref returnValue
                    );

                string logon = "";
                if (string.IsNullOrEmpty(res))
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "E";
                        }
                        else if (returnValue[i].TYPE == "S")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "S";
                        }
                    }

                }
                else
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "E";
                        }
                        else if (returnValue[i].TYPE == "S")
                        {
                            if (!string.IsNullOrEmpty(returnValue[i].MESSAGE_V2) && !string.IsNullOrEmpty(returnValue[i].MESSAGE_V3)
                                && returnValue[i].MESSAGE.IndexOf("delivery") > 0)
                            {
                                res = returnValue[i].MESSAGE_V2 + "|" + returnValue[i].MESSAGE_V3;
                            }
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "S";
                        }
                    }
                }


                return logon + "$" + (res ?? "");


            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public string Delete(string JsonConfig, string content, ref string msg)
        {

            try
            {

                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<SaleOrderModel>(content);
                sap.saleorder.change.SalesOrderChangeRequest_Sync_Out_SIService SAPService = new sap.saleorder.change.SalesOrderChangeRequest_Sync_Out_SIService();

                string SALESDOCUMENT_IN = jsonContent.SALESDOCUMENT_IN;//"1140000314";

                sap.saleorder.change.BAPISDH1 ORDER_HEADER_IN = new sap.saleorder.change.BAPISDH1();
                //SWAP=Z1I6, Throughput=Z1V1, Clear Line=Z1P3, Borrow TOP=Z1I4, Borrow TLB=Z3I4
                string DOC_TYPE = jsonContent.mORDER_HEADER_IN[0].DOC_TYPE;
                ORDER_HEADER_IN.REQ_DATE_H = jsonContent.mORDER_HEADER_IN[0].REQ_DATE_H;//"2017-06-30";//"2017-06-15";2017-06-01

                sap.saleorder.change.BAPISDH1X ORDER_HEADER_INX = new sap.saleorder.change.BAPISDH1X();
                ORDER_HEADER_INX.UPDATEFLAG = "U";
                ORDER_HEADER_INX.REQ_DATE_H = "X";

                sap.saleorder.change.BAPICOND[] ORDER_CONDITIONS_IN = new sap.saleorder.change.BAPICOND[0];
                sap.saleorder.change.BAPICONDX[] ORDER_CONDITIONS_INX = new sap.saleorder.change.BAPICONDX[0];
                sap.saleorder.change.BAPISDCOND[] ORDER_CONDITIONS_OUT = new sap.saleorder.change.BAPISDCOND[0];
                sap.saleorder.change.BAPICONDHD[] ORDER_COND_HEAD = new sap.saleorder.change.BAPICONDHD[0];
                sap.saleorder.change.BAPICONDIT[] ORDER_COND_ITEM = new sap.saleorder.change.BAPICONDIT[0];
                sap.saleorder.change.BAPISDHD[] ORDER_HEADERS_OUT = new sap.saleorder.change.BAPISDHD[0];

                int index1 = 0;
                sap.saleorder.change.BAPISDITM[] ORDER_ITEMS_IN = new sap.saleorder.change.BAPISDITM[jsonContent.mORDER_ITEMS_IN.Count];
                sap.saleorder.change.BAPISDITMX[] ORDER_ITEMS_INX = new sap.saleorder.change.BAPISDITMX[jsonContent.mORDER_ITEMS_IN.Count];
                foreach (BAPISDITM jsORDER_ITEMS_IN in jsonContent.mORDER_ITEMS_IN)
                {

                    ORDER_ITEMS_IN[index1] = new sap.saleorder.change.BAPISDITM();
                    ORDER_ITEMS_IN[index1].ITM_NUMBER = jsORDER_ITEMS_IN.ITM_NUMBER; //"000010";
                    ORDER_ITEMS_IN[index1].MATERIAL = jsORDER_ITEMS_IN.MATERIAL;// "YFLR";
                    ORDER_ITEMS_IN[index1].REASON_REJ = jsORDER_ITEMS_IN.REASON_REJ;// "Z0";
                    ORDER_ITEMS_IN[index1].PLANT = jsORDER_ITEMS_IN.PLANT;//"1200";
                    ORDER_ITEMS_IN[index1].ITEM_CATEG = jsORDER_ITEMS_IN.ITEM_CATEG;//"ZTW1";
                    ORDER_ITEMS_IN[index1].SALES_UNIT = jsORDER_ITEMS_IN.SALES_UNIT;// "MT";


                    ORDER_ITEMS_INX[index1] = new sap.saleorder.change.BAPISDITMX();
                    ORDER_ITEMS_INX[index1].ITM_NUMBER = jsORDER_ITEMS_IN.ITM_NUMBER; //"000010";
                    ORDER_ITEMS_INX[index1].UPDATEFLAG = "U";
                    ORDER_ITEMS_INX[index1].MATERIAL = "X";
                    ORDER_ITEMS_INX[index1].REASON_REJ = "X";
                    ORDER_ITEMS_INX[index1].PLANT = "X";
                    ORDER_ITEMS_INX[index1].ITEM_CATEG = "X";
                    ORDER_ITEMS_INX[index1].SALES_UNIT = "X";

                    index1 += 1;

                }

                sap.saleorder.change.BAPISDIT[] ORDER_ITEMS_OUT = new sap.saleorder.change.BAPISDIT[0];
                sap.saleorder.change.ZBAPISDITM_ISOIL[] ORDER_ITM_ISOIL_EXT_IN = new sap.saleorder.change.ZBAPISDITM_ISOIL[0];
                sap.saleorder.change.ZBAPISDITM_ISOIL[] ORDER_ITM_ISOIL_EXT_OUT = new sap.saleorder.change.ZBAPISDITM_ISOIL[0];
                sap.saleorder.change.BAPISDPART[] ORDER_PARTNERS_OUT = new sap.saleorder.change.BAPISDPART[0];


                index1 = 0;
                sap.saleorder.change.BAPISCHDL[] ORDER_SCHEDULES_IN = new sap.saleorder.change.BAPISCHDL[jsonContent.mORDER_SCHEDULES_IN.Count];
                sap.saleorder.change.BAPISCHDLX[] ORDER_SCHEDULES_INX = new sap.saleorder.change.BAPISCHDLX[jsonContent.mORDER_SCHEDULES_IN.Count];
                foreach (BAPISCHDL jsORDER_SCHEDULES_IN in jsonContent.mORDER_SCHEDULES_IN)
                {

                    ORDER_SCHEDULES_IN[index1] = new sap.saleorder.change.BAPISCHDL();
                    ORDER_SCHEDULES_IN[index1].ITM_NUMBER = jsORDER_SCHEDULES_IN.ITM_NUMBER; //"000010";
                    ORDER_SCHEDULES_IN[index1].SCHED_LINE = jsORDER_SCHEDULES_IN.SCHED_LINE;//"0001";
                    ORDER_SCHEDULES_IN[index1].REQ_DATE = jsORDER_SCHEDULES_IN.REQ_DATE;// "2017-06-01";
                    ORDER_SCHEDULES_IN[index1].REQ_QTY = jsORDER_SCHEDULES_IN.REQ_QTY; //36727.615M;
                    ORDER_SCHEDULES_IN[index1].REQ_QTYSpecified = true;

                    ORDER_SCHEDULES_INX[index1] = new sap.saleorder.change.BAPISCHDLX();
                    ORDER_SCHEDULES_INX[index1].ITM_NUMBER = jsORDER_SCHEDULES_IN.ITM_NUMBER;//"000010";
                    ORDER_SCHEDULES_INX[index1].SCHED_LINE = jsORDER_SCHEDULES_IN.SCHED_LINE;
                    ORDER_SCHEDULES_INX[index1].UPDATEFLAG = "I";
                    ORDER_SCHEDULES_INX[index1].REQ_DATE = "X";
                    ORDER_SCHEDULES_INX[index1].REQ_QTY = "X";
                    index1 += 1;
                }

                sap.saleorder.change.BAPISDHEDU[] ORDER_SCHEDULES_OUT = new sap.saleorder.change.BAPISDHEDU[0];
                sap.saleorder.change.BAPISDHDST[] ORDER_STATUSHEADERS_OUT = new sap.saleorder.change.BAPISDHDST[0];
                sap.saleorder.change.BAPISDITST[] ORDER_STATUSITEMS_OUT = new sap.saleorder.change.BAPISDITST[0];
                sap.saleorder.change.BAPISDTEXT[] ORDER_TEXT = new sap.saleorder.change.BAPISDTEXT[0];
                sap.saleorder.change.BAPISDTEHD[] ORDER_TEXTHEADERS_OUT = new sap.saleorder.change.BAPISDTEHD[0];
                sap.saleorder.change.BAPITEXTLI[] ORDER_TEXTLINES_OUT = new sap.saleorder.change.BAPITEXTLI[0];
                sap.saleorder.change.BAPIPARNRC[] PARTNERCHANGES = new sap.saleorder.change.BAPIPARNRC[0];
                sap.saleorder.change.BAPIRET2[] returnValue = new sap.saleorder.change.BAPIRET2[0];

                //SAPService.Url = "http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=SalesOrderChangeRequest_Sync_Out_SI&interfaceNamespace=http://thaioilgroup.com/_i_salesorder/sales/salesorder_change/";
                //SAPService.Credentials = AuthServiceConnect("ZPI_IF_CIP", "1@zpi_if_cip");

                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);

                string res = SAPService.SalesOrderChangeRequest_Sync_Out_SI(
                    ORDER_HEADER_IN,
                    ORDER_HEADER_INX,
                    SALESDOCUMENT_IN,
                   ref ORDER_CONDITIONS_IN,
                   ref ORDER_CONDITIONS_INX,
                   ref ORDER_CONDITIONS_OUT,
                   ref ORDER_COND_HEAD,
                   ref ORDER_COND_ITEM,
                   ref ORDER_HEADERS_OUT,
                   ref ORDER_ITEMS_IN,
                   ref ORDER_ITEMS_INX,
                   ref ORDER_ITEMS_OUT,
                   ref ORDER_ITM_ISOIL_EXT_IN,
                   ref ORDER_ITM_ISOIL_EXT_OUT,
                   ref ORDER_PARTNERS_OUT,
                   ref ORDER_SCHEDULES_IN,
                   ref ORDER_SCHEDULES_INX,
                   ref ORDER_SCHEDULES_OUT,
                   ref ORDER_STATUSHEADERS_OUT,
                   ref ORDER_STATUSITEMS_OUT,
                   ref ORDER_TEXT,
                   ref ORDER_TEXTHEADERS_OUT,
                   ref ORDER_TEXTLINES_OUT,
                   ref PARTNERCHANGES,
                   ref returnValue
                    );

                string logon = "";
                if (res == "")
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "E";
                        }
                        else if (returnValue[i].TYPE == "S")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "S";
                        }
                    }

                }
                else
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "E";
                        }
                        else if (returnValue[i].TYPE == "S")
                        {
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon += "S";
                        }
                    }
                }
                return logon + "$" + (res ?? "");
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;

            return NC;
        }
    }
}
