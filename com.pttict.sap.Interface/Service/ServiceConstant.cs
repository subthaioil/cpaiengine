﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Service
{
    public class ServiceConstant
    {
        public static string NAMESPACE = "SAP";
        //public static string SERVICE_EWS = "EWS";
        //public static string SERVICE_SMTP = "SMTP";

        //For
        public static string[] RESP_SUCCESS = { "1", "SUCCESS" };
        public static string[] RESP_SYETEM_ERROR = { "900001", "System Error" };
        public static string[] RESP_ADDRESS_ERROR = { "900002", "Adderss exception" };
        public static string[] RESP_MESSAGE_ERROR = { "900003", "Message exception" };
        public static string[] RESP_SAP_ERROR = { "900004", "SAP exception" };

    }
}
