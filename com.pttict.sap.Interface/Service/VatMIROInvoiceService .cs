﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using static com.pttict.sap.Interface.Model.VatMIROInvoiceServiceModel;


namespace com.pttict.sap.Interface.Service
{
    public class VatMIROInvoiceService: BasicBean
    {

        //public void Create(string JsonConfig, string content, ref string msg)
        //{
            public string Create(string JsonConfig, string content, ref string msg)
        {
            try
            {
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<VatMIROInvoiceServiceModel>(content);
                sap.vatpostingMIRO.cerate.VATPostingCreateRequest1_Sync_Out_SIService SAPService = new sap.vatpostingMIRO.cerate.VATPostingCreateRequest1_Sync_Out_SIService();

                sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_ADDRESSDATA AddressData = new sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_ADDRESSDATA();

                sap.vatpostingMIRO.cerate.ZHEADERDATA headerdata = new sap.vatpostingMIRO.cerate.ZHEADERDATA();  
                headerdata.INVOICE_IND = jsonContent.headerdata[0].INVOICE_IND; //"X";
                headerdata.DOC_TYPE = jsonContent.headerdata[0].DOC_TYPE; //"RE";
                headerdata.DOC_DATE = jsonContent.headerdata[0].DOC_DATE; //"2017-05-08";
                headerdata.PSTNG_DATE = jsonContent.headerdata[0].PSTNG_DATE; //"2017-05-08";
                headerdata.REF_DOC_NO = jsonContent.headerdata[0].REF_DOC_NO; //"CF11170501";
                headerdata.COMP_CODE = jsonContent.headerdata[0].COMP_CODE; //"1100";
                headerdata.DIFF_INV = jsonContent.headerdata[0].DIFF_INV; //"CUSTOMS";
                headerdata.CURRENCY = jsonContent.headerdata[0].CURRENCY; //"THB";
                headerdata.EXCH_RATE = jsonContent.headerdata[0].EXCH_RATE; //32.909M;
                headerdata.EXCH_RATESpecified = true;
                headerdata.GROSS_AMOUNT = jsonContent.headerdata[1].GROSS_AMOUNT; //1273495.00M;
                headerdata.GROSS_AMOUNTSpecified = true;
                headerdata.HEADER_TXT = jsonContent.headerdata[0].HEADER_TXT; //"VAT Crude/CF11170501/ETHA";
                headerdata.BUSINESS_PLACE = jsonContent.headerdata[0].BUSINESS_PLACE; //"0000";
                headerdata.ALLOC_NMBR = jsonContent.headerdata[0].ALLOC_NMBR; //"CF11170501";
                headerdata.PMNTTRMS = jsonContent.headerdata[0].PMNTTRMS; //"Z001";

                //headerdata.INVOICE_IND = "X";
                //headerdata.DOC_TYPE = "RE";
                //headerdata.DOC_DATE = "2017-05-08";
                //headerdata.PSTNG_DATE = "2017-05-08";
                //headerdata.REF_DOC_NO = "CF11170501";
                //headerdata.COMP_CODE = "1100";
                //headerdata.DIFF_INV = "CUSTOMS";
                //headerdata.CURRENCY = "THB";
                //headerdata.EXCH_RATE = 32.909M;
                //headerdata.EXCH_RATESpecified = true;
                //headerdata.GROSS_AMOUNT = 1273495.00M;
                //headerdata.GROSS_AMOUNTSpecified = true;
                //headerdata.HEADER_TXT = "VAT Crude/CF11170501/ETHA";
                //headerdata.BUSINESS_PLACE = "0000";
                //headerdata.ALLOC_NMBR = "CF11170501";
                //headerdata.PMNTTRMS = "Z001";


                sap.vatpostingMIRO.cerate.ZACCOUNTINGDATA[] accountingdata = new sap.vatpostingMIRO.cerate.ZACCOUNTINGDATA[0];
                sap.vatpostingMIRO.cerate.BAPIPAREX[] extension = new sap.vatpostingMIRO.cerate.BAPIPAREX[0];
                sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_GL_ACCOUNT[] GLaccount = new sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_GL_ACCOUNT[0];


                int index1 = 0;
                sap.vatpostingMIRO.cerate.ZITEMDATE[] itemData = new sap.vatpostingMIRO.cerate.ZITEMDATE[jsonContent.itemData.Count];
                index1 = 0;
                foreach (ZITEMDATE itemData1 in jsonContent.itemData)
                {
                    itemData[index1] = new sap.vatpostingMIRO.cerate.ZITEMDATE();
                    itemData[index1].INVOICE_DOC_ITEM = (index1 + 1).ToString("0000"); //itemData1.INVOICE_DOC_ITEM;
                    itemData[index1].PO_NUMBER = itemData1.PO_NUMBER;
                    itemData[index1].PO_ITEM = itemData1.PO_ITEM;
                    itemData[index1].TAX_CODE = itemData1.TAX_CODE;
                    itemData[index1].ITEM_AMOUNT = itemData1.ITEM_AMOUNT;
                    itemData[index1].ITEM_AMOUNTSpecified = true;
                    itemData[index1].QUANTITY = itemData1.QUANTITY;
                    itemData[index1].QUANTITYSpecified = true;
                    itemData[index1].PO_UNIT = itemData1.PO_UNIT;
                    itemData[index1].ITEM_TEXT = itemData1.ITEM_TEXT;
                    index1 += 1;
                }


                //sap.vatpostingMIRO.cerate.ZITEMDATE[] itemData = new sap.vatpostingMIRO.cerate.ZITEMDATE[1];
                //// itemData[0] = new sap.vatpostingMIRO.cerate.ZITEMDATE();
                //itemData[0] = new sap.vatpostingMIRO.cerate.ZITEMDATE();
                //itemData[0].INVOICE_DOC_ITEM = "0001";
                //itemData[0].PO_NUMBER = "4111000841";
                //itemData[0].PO_ITEM = "0040";
                //itemData[0].TAX_CODE = "D7";
                //itemData[0].ITEM_AMOUNT = 1273495.00M;
                //itemData[0].ITEM_AMOUNTSpecified = true;
                //itemData[0].QUANTITY = 1;
                //itemData[0].QUANTITYSpecified = true;
                //itemData[0].PO_UNIT = "LE";


                //sap.vatpostingMIRO.cerate.ZRETURN returnValue = new sap.vatpostingMIRO.cerate.ZRETURN[1];
                //returnValue[0] = new sap.vatpostingMIRO.cerate.ZRETURN();
                sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_MATERIAL[] Material = new sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_MATERIAL[0];
                sap.vatpostingMIRO.cerate.ZRETURN[] returnValue = new sap.vatpostingMIRO.cerate.ZRETURN[0];
                //returnValue[0] = new sap.vatpostingMIRO.cerate.ZRETURN();

                sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_TAX[] Taxdata = new sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_TAX[0];
                sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_TM_ITEM[] TMitem = new sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_TM_ITEM[0];
                sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_VENDORSPLIT[] VendorSdplit = new sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_VENDORSPLIT[0];
                sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_WITHTAX[] WithtaxData = new sap.vatpostingMIRO.cerate.BAPI_INCINV_CREATE_WITHTAX[0];



                //SAPService.Url = "http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=VATPostingCreateRequest1_Sync_Out_SI&interfaceNamespace=http%3A%2F%2Fthaioilgroup.com%2F_i_accounting%2Fvat%2Fvat_posting1%2F";
                //SAPService.Credentials = AuthServiceConnect("ZPI_IF_CIP", "1@zpi_if_cip");
                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);

                
                string FiscalYear = "";
                string InvoiceDoc = "";
                string result = SAPService.VATPostingCreateRequest1_Sync_Out_SI(
                    AddressData,
                    headerdata,
                    ref accountingdata,
                    ref extension, 
                    ref GLaccount, 
                    ref itemData,
                    ref Material,
                    ref returnValue,
                    ref Taxdata,
                    ref TMitem,
                    ref VendorSdplit,
                    ref WithtaxData,
                    out FiscalYear,
                    out InvoiceDoc
                    );

                string logon = "";

                if (result == "")
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon = "Error";
                    }
                }
                logon = "ServiceSuccess";
                return "|"+logon+ "M" + msg + "R" +result+"|"+InvoiceDoc;
                //return "F" + result + "|" + InvoiceDoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;

            return NC;
        }
    }
}
