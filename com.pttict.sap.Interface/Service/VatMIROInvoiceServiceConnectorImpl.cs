﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common.utilities;
using com.pttict.downstream.common.model;
using com.pttict.downstream.common;

namespace com.pttict.sap.Interface.Service
{
    public class VatMIROInvoiceServiceConnectorImpl : BasicBean, DownstreamConnector<string>
    {
        public DownstreamResponse<string> connect(string config, string content)
        {
            try
            {
                DownstreamResponse<string> downResp = new DownstreamResponse<string>();
                downResp.setNameSpace(ServiceConstant.NAMESPACE);
                downResp.setResultCode(ServiceConstant.RESP_SYETEM_ERROR[0]);
                downResp.setResultDesc(ServiceConstant.RESP_SYETEM_ERROR[1]);

                VatMIROInvoiceService service = new VatMIROInvoiceService();
                string msg = "";                
                string res = service.Create(config, content, ref msg);
                
                if (res != null && res != "")
                {
                    downResp.setResultCode(ServiceConstant.RESP_SUCCESS[0]);
                    downResp.setResultDesc(ServiceConstant.RESP_SUCCESS[1]);
                    downResp.setResponseData(res);
                }
                else
                {
                    downResp.setResultCode(ServiceConstant.RESP_SAP_ERROR[0]);
                    downResp.setResultDesc(msg);
                    downResp.setResponseData(res);
                }

                return downResp;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}



    //public class VatMIROInvoiceServiceConnectorImpl : BasicBean
    //{
    //    public void connect(string config, string content)
    //    {
    //        try
    //        {
    //            VatMIROInvoiceService service = new VatMIROInvoiceService();
    //            service.Create(config, content);
    //            string msg = "";

//        }
//        catch (Exception ex)
//        {
//            throw ex;
//        }

//    }


