﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using static com.pttict.sap.Interface.Model.VatpostingCreateModel;
//using com.pttict.sap.Interface.sap.vatposting.create;

namespace com.pttict.sap.Interface.Service
{
    public class VatService : BasicBean
    {

        //public void Create(string jsonConfig, string content, ref string msg)
        //{


        public string Create(string JsonConfig, string content, ref string msg)
        {
            try
            {
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<VatpostingCreateModel>(content);

                sap.vatposting.create.VATPostingCreateRequest2_Sync_Out_SIService SAPService = new sap.vatposting.create.VATPostingCreateRequest2_Sync_Out_SIService();
                sap.vatposting.create.BAPIACCAHD contractHeader = new sap.vatposting.create.BAPIACCAHD();
                sap.vatposting.create.BAPIACPA09 customerCpd = new sap.vatposting.create.BAPIACPA09();

                sap.vatposting.create.BAPIACHE09 documentHeader = new sap.vatposting.create.BAPIACHE09();
                documentHeader.OBJ_TYPE = jsonContent.documentHeader[0].OBJ_TYPE; //"FOTP";
                documentHeader.OBJ_KEY = jsonContent.documentHeader[0].OBJ_KEY; //"XLC028001";
                documentHeader.BUS_ACT = jsonContent.documentHeader[0].BUS_ACT; //"RFBU";
                documentHeader.USERNAME = jsonContent.documentHeader[0].USERNAME; //"DOTNET";
                documentHeader.HEADER_TXT = jsonContent.documentHeader[0].HEADER_TXT; //"VAT Crude/CF11170401/GBAS";
                documentHeader.COMP_CODE = jsonContent.documentHeader[0].COMP_CODE; //"1100";
                documentHeader.DOC_DATE = jsonContent.documentHeader[0].DOC_DATE; //"2017-04-27";
                documentHeader.PSTNG_DATE = jsonContent.documentHeader[0].PSTNG_DATE; //"2017-04-27";
                documentHeader.FISC_YEAR = jsonContent.documentHeader[0].FISC_YEAR; //"2017";
                documentHeader.FIS_PERIOD = jsonContent.documentHeader[0].FIS_PERIOD; //"04";
                documentHeader.DOC_TYPE = jsonContent.documentHeader[0].DOC_TYPE; //"KR";
                documentHeader.REF_DOC_NO = jsonContent.documentHeader[0].REF_DOC_NO; //"XLC028001";


                long indexmode1 = 0;
                int index1 = 0;
                sap.vatposting.create.BAPIACAP09[] accountPayable = new sap.vatposting.create.BAPIACAP09[jsonContent.accountPay.Count];
                foreach (BAPIACAP09 accountPay in jsonContent.accountPay)///Loop ให้วนแถววนคอรัม
                {

                    accountPayable[index1] = new sap.vatposting.create.BAPIACAP09();
                    accountPayable[index1].ITEMNO_ACC = (indexmode1 + 1).ToString("0000000000"); //"0000000001";
                    accountPayable[index1].VENDOR_NO = accountPay.VENDOR_NO; //"CUSTOMS"-->GLOBAL_CONFIG ;
                    accountPayable[index1].REF_KEY_1 = accountPay.REF_KEY_1; //"XLC028001" -->GLOBAL_CONFIG;
                    accountPayable[index1].PMNTTRMS = accountPay.PMNTTRMS;//"0001" -->GLOBAL_CONFIG;
                    accountPayable[index1].ITEM_TEXT = accountPay.ITEM_TEXT; //"CF11170401/GBASE TYPE 2/PRIME SAILOR/VAT";
                    accountPayable[index1].BUSINESSPLACE = accountPay.BUSINESSPLACE; //"0000" -->GLOBAL_CONFIG;
                    accountPayable[index1].TAX_CODE = accountPay.TAX_CODE; //"D7"-->GLOBAL_CONFIG;
                    index1 += 1;
                    indexmode1 += 1;
                }

                index1 = 0;
                sap.vatposting.create.BAPIACTX09[] accountTax = new sap.vatposting.create.BAPIACTX09[jsonContent.accountTax.Count];                
                foreach (BAPIACTX09 accountTax1 in jsonContent.accountTax)
                {
                    accountTax[index1] = new sap.vatposting.create.BAPIACTX09();
                    accountTax[index1].ITEMNO_ACC = (indexmode1 + 1).ToString("0000000000");
                    accountTax[index1].GL_ACCOUNT = accountTax1.GL_ACCOUNT;
                    accountTax[index1].TAX_CODE = accountTax1.TAX_CODE;
                    index1 += 1;
                    indexmode1 += 1;
                }

                index1 = 0;
                sap.vatposting.create.BAPIACGL09[] accountGL = new sap.vatposting.create.BAPIACGL09[jsonContent.accountGL.Count];
                foreach (BAPIACGL09 accountGL1 in jsonContent.accountGL)
                {
                    accountGL[index1] = new sap.vatposting.create.BAPIACGL09();
                    accountGL[index1].ITEMNO_ACC = (indexmode1 + 1).ToString("0000000000");
                    accountGL[index1].GL_ACCOUNT = accountGL1.GL_ACCOUNT;
                    accountGL[index1].ALLOC_NMBR = accountGL1.ALLOC_NMBR;
                    index1 += 1;
                    indexmode1 += 1;
                }


                sap.vatposting.create.BAPIACCR09[] currencyAmount = new sap.vatposting.create.BAPIACCR09[jsonContent.currencyAm.Count];               
                decimal total_DOCCUR = 0;
                decimal total_BASE = 0;
                index1 = 1;
                indexmode1 = 1;

                int rowcount = jsonContent.currencyAm.Count;
                total_BASE = jsonContent.currencyAm[rowcount - 1].DISC_BASE;

                foreach (BAPIACCR09 currencyAm1 in jsonContent.currencyAm)
                {
                    if (index1 < rowcount)
                    {
                        //No1
                        currencyAmount[index1] = new sap.vatposting.create.BAPIACCR09();
                        currencyAmount[index1].ITEMNO_ACC = (indexmode1 + 1).ToString("0000000000"); //"0000000002";
                        //currencyAmount[index1].CURR_TYPE = "00";
                        currencyAmount[index1].CURRENCY = currencyAm1.CURRENCY; //"THB";
                        currencyAmount[index1].AMT_DOCCUR = currencyAm1.AMT_DOCCUR;//68781149;
                        currencyAmount[index1].AMT_DOCCURSpecified = true;//true;
                        currencyAmount[index1].AMT_BASE = currencyAm1.AMT_BASE; //982587843;
                        currencyAmount[index1].AMT_BASESpecified = true; //true;
                        total_DOCCUR += currencyAm1.AMT_DOCCUR;//68781149;
                        index1 += 1;
                        indexmode1 += 1;
                    }

                }


                ///*
                ////No2
                //currencyAmount[1] = new sap.vatposting.create.BAPIACCR09();
                //currencyAmount[1].ITEMNO_ACC = "0000000003";
                //currencyAmount[1].CURR_TYPE = "00";
                //currencyAmount[1].CURRENCY = "THB";
                //currencyAmount[1].AMT_DOCCUR = 54897678;
                //currencyAmount[1].AMT_DOCCURSpecified = true;
                //currencyAmount[1].AMT_BASE = 784252548;
                //currencyAmount[1].AMT_BASESpecified = true;
                //total_DOCCUR += 54897678;
                //total_BASE += 784252548;

                ////No3
                //currencyAmount[2] = new sap.vatposting.create.BAPIACCR09();
                //currencyAmount[2].ITEMNO_ACC = "0000000004";
                //currencyAmount[2].CURR_TYPE = "00";
                //currencyAmount[2].CURRENCY = "THB";
                //currencyAmount[2].AMT_DOCCUR = 10048316;
                //currencyAmount[2].AMT_DOCCURSpecified = true;
                //currencyAmount[2].AMT_BASE = 143547368;
                //currencyAmount[2].AMT_BASESpecified = true;
                //total_DOCCUR += 10048316;
                //total_BASE += 143547368;
                //*/

              //  total
                total_BASE = jsonContent.currencyAm[rowcount - 1].DISC_BASE;
                currencyAmount[0] = new sap.vatposting.create.BAPIACCR09();
                currencyAmount[0].ITEMNO_ACC = "0000000001";
              //  currencyAmount[0].CURR_TYPE = "00";
                currencyAmount[0].CURRENCY = "THB";
                total_DOCCUR = 0 - total_DOCCUR;
                currencyAmount[0].AMT_DOCCUR = total_DOCCUR;
                currencyAmount[0].AMT_DOCCURSpecified = true;
                total_BASE = 0 - total_BASE;
                currencyAmount[0].DISC_BASE = total_BASE;                
                currencyAmount[0].DISC_BASESpecified = true;


                sap.vatposting.create.BAPIACEXTC[] extension1 = new sap.vatposting.create.BAPIACEXTC[0];

                sap.vatposting.create.BAPIPAREX[] extension2 = new sap.vatposting.create.BAPIPAREX[jsonContent.extension2.Count];
                index1 = 0;
                indexmode1 = 1;
                foreach (BAPIPAREX extension in jsonContent.extension2)
                {
                    extension2[index1] = new sap.vatposting.create.BAPIPAREX();
                    extension2[index1].STRUCTURE = (indexmode1 + 1).ToString("0000000000");//"0000000002";
                    extension2[index1].VALUEPART1 = extension.VALUEPART1; //"C_ACCIT-SGTXT";
                    extension2[index1].VALUEPART2 = extension.VALUEPART2;//"CF11170401/GBASE TYPE 2/PRIME SAILOR/VAT";
                    index1 += 1;
                    indexmode1 += 1;
                }


                ///*
                //extension2[1] = new sap.vatposting.create.BAPIPAREX();
                //extension2[1].STRUCTURE = "0000000003";
                //extension2[1].VALUEPART1 = "C_ACCIT-SGTXT";
                //extension2[1].VALUEPART2 = "CF11170401/GBASE TYPE 2/PRIME SAILOR/VAT";

                //extension2[2] = new sap.vatposting.create.BAPIPAREX();
                //extension2[2].STRUCTURE = "0000000004";
                //extension2[2].VALUEPART1 = "C_ACCIT-SGTXT";
                //extension2[2].VALUEPART2 = "CF11170401/GBASE TYPE 2/PRIME SAILOR/Import Duty";
                //*/


                sap.vatposting.create.BAPIACAR09[] accountdReceivable = new sap.vatposting.create.BAPIACAR09[0];
                sap.vatposting.create.BAPIACCAIT[] contracItiem = new sap.vatposting.create.BAPIACCAIT[0];
                sap.vatposting.create.BAPIACKEC9[] criteria = new sap.vatposting.create.BAPIACKEC9[0];
                sap.vatposting.create.BAPIACPC09[] paymentCard = new sap.vatposting.create.BAPIACPC09[0];
                sap.vatposting.create.BAPIACRE09[] realEstate = new sap.vatposting.create.BAPIACRE09[0];
                sap.vatposting.create.BAPIRET2[] returnValue = new sap.vatposting.create.BAPIRET2[0];           
                sap.vatposting.create.BAPIACKEV9[] valueField = new sap.vatposting.create.BAPIACKEV9[0];



                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);


                string objSys = "";
                string objType = "";
                string result = SAPService.VATPostingCreateRequest2_Sync_Out_SI(
                    contractHeader,
                    customerCpd,
                    documentHeader,
                    ref accountGL,
                    ref accountPayable,
                    ref accountdReceivable,
                    ref accountTax,
                    ref contracItiem,
                    ref criteria,
                    ref currencyAmount,
                    ref extension1,
                    ref extension2,
                    ref paymentCard,
                    ref realEstate,
                    ref returnValue,
                    ref valueField,                    
                    out objSys,
                    out objType                    
                    );

                string logon = "";

                if (result == "")
                {
                    int rcount = returnValue.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (returnValue[i].TYPE == "E")
                            msg += returnValue[i].MESSAGE + " " + returnValue[i].MESSAGE_V1 + " " + returnValue[i].MESSAGE_V2 + " " + returnValue[i].MESSAGE_V3 + " " + returnValue[i].MESSAGE_V4;
                            logon = "Error";
                    }
                }
                logon = "ServiceSuccess";
                return "|" + logon + "M" + msg + "R" + result ;
             //   return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;

            return NC;
        }
    }
}
