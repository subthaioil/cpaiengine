﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace com.pttict.sap.Interface.sap.memorecord.create {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1586.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="MemorecordCreateRequest_Sync_Out_SIBinding", Namespace="http://thaioilgroup.com/_i_accounting/memo/memorecord_create/")]
    public partial class MemorecordCreateRequest_Sync_Out_SIService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback MemorecordCreateRequest_Sync_Out_SIOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public MemorecordCreateRequest_Sync_Out_SIService() {
            this.Url = global::com.pttict.sap.Interface.Properties.Settings.Default.com_pttict_sap_Interface_sap_memorecord_create_MemorecordCreateRequest_Sync_Out_SIService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event MemorecordCreateRequest_Sync_Out_SICompletedEventHandler MemorecordCreateRequest_Sync_Out_SICompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://sap.com/xi/WebService/soap1.1", RequestElementName="ZFICMB001", RequestNamespace="urn:sap-com:document:sap:rfc:functions", ResponseElementName="ZFICMB001.Response", ResponseNamespace="urn:sap-com:document:sap:rfc:functions", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("E_IDENR", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MemorecordCreateRequest_Sync_Out_SI(
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string AVDAT, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string BUKRS, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string DATUM, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string DISPW, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string DSART, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string GRUPP, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string GVALT, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string KURST, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string MERKM, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string REFER, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string SGTXT, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string TESTRUN, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string VOART, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] decimal WRSHB, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string XINVR, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string ZUONR, 
                    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] out BAPIRETURN1 RETURN) {
            object[] results = this.Invoke("MemorecordCreateRequest_Sync_Out_SI", new object[] {
                        AVDAT,
                        BUKRS,
                        DATUM,
                        DISPW,
                        DSART,
                        GRUPP,
                        GVALT,
                        KURST,
                        MERKM,
                        REFER,
                        SGTXT,
                        TESTRUN,
                        VOART,
                        WRSHB,
                        XINVR,
                        ZUONR});
            RETURN = ((BAPIRETURN1)(results[1]));
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void MemorecordCreateRequest_Sync_Out_SIAsync(
                    string AVDAT, 
                    string BUKRS, 
                    string DATUM, 
                    string DISPW, 
                    string DSART, 
                    string GRUPP, 
                    string GVALT, 
                    string KURST, 
                    string MERKM, 
                    string REFER, 
                    string SGTXT, 
                    string TESTRUN, 
                    string VOART, 
                    decimal WRSHB, 
                    string XINVR, 
                    string ZUONR) {
            this.MemorecordCreateRequest_Sync_Out_SIAsync(AVDAT, BUKRS, DATUM, DISPW, DSART, GRUPP, GVALT, KURST, MERKM, REFER, SGTXT, TESTRUN, VOART, WRSHB, XINVR, ZUONR, null);
        }
        
        /// <remarks/>
        public void MemorecordCreateRequest_Sync_Out_SIAsync(
                    string AVDAT, 
                    string BUKRS, 
                    string DATUM, 
                    string DISPW, 
                    string DSART, 
                    string GRUPP, 
                    string GVALT, 
                    string KURST, 
                    string MERKM, 
                    string REFER, 
                    string SGTXT, 
                    string TESTRUN, 
                    string VOART, 
                    decimal WRSHB, 
                    string XINVR, 
                    string ZUONR, 
                    object userState) {
            if ((this.MemorecordCreateRequest_Sync_Out_SIOperationCompleted == null)) {
                this.MemorecordCreateRequest_Sync_Out_SIOperationCompleted = new System.Threading.SendOrPostCallback(this.OnMemorecordCreateRequest_Sync_Out_SIOperationCompleted);
            }
            this.InvokeAsync("MemorecordCreateRequest_Sync_Out_SI", new object[] {
                        AVDAT,
                        BUKRS,
                        DATUM,
                        DISPW,
                        DSART,
                        GRUPP,
                        GVALT,
                        KURST,
                        MERKM,
                        REFER,
                        SGTXT,
                        TESTRUN,
                        VOART,
                        WRSHB,
                        XINVR,
                        ZUONR}, this.MemorecordCreateRequest_Sync_Out_SIOperationCompleted, userState);
        }
        
        private void OnMemorecordCreateRequest_Sync_Out_SIOperationCompleted(object arg) {
            if ((this.MemorecordCreateRequest_Sync_Out_SICompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.MemorecordCreateRequest_Sync_Out_SICompleted(this, new MemorecordCreateRequest_Sync_Out_SICompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class BAPIRETURN1 {
        
        private string tYPEField;
        
        private string idField;
        
        private string nUMBERField;
        
        private string mESSAGEField;
        
        private string lOG_NOField;
        
        private string lOG_MSG_NOField;
        
        private string mESSAGE_V1Field;
        
        private string mESSAGE_V2Field;
        
        private string mESSAGE_V3Field;
        
        private string mESSAGE_V4Field;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TYPE {
            get {
                return this.tYPEField;
            }
            set {
                this.tYPEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ID {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string NUMBER {
            get {
                return this.nUMBERField;
            }
            set {
                this.nUMBERField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MESSAGE {
            get {
                return this.mESSAGEField;
            }
            set {
                this.mESSAGEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string LOG_NO {
            get {
                return this.lOG_NOField;
            }
            set {
                this.lOG_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string LOG_MSG_NO {
            get {
                return this.lOG_MSG_NOField;
            }
            set {
                this.lOG_MSG_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MESSAGE_V1 {
            get {
                return this.mESSAGE_V1Field;
            }
            set {
                this.mESSAGE_V1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MESSAGE_V2 {
            get {
                return this.mESSAGE_V2Field;
            }
            set {
                this.mESSAGE_V2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MESSAGE_V3 {
            get {
                return this.mESSAGE_V3Field;
            }
            set {
                this.mESSAGE_V3Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MESSAGE_V4 {
            get {
                return this.mESSAGE_V4Field;
            }
            set {
                this.mESSAGE_V4Field = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1586.0")]
    public delegate void MemorecordCreateRequest_Sync_Out_SICompletedEventHandler(object sender, MemorecordCreateRequest_Sync_Out_SICompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1586.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class MemorecordCreateRequest_Sync_Out_SICompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal MemorecordCreateRequest_Sync_Out_SICompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public BAPIRETURN1 RETURN {
            get {
                this.RaiseExceptionIfNecessary();
                return ((BAPIRETURN1)(this.results[1]));
            }
        }
    }
}

#pragma warning restore 1591